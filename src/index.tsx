import React, { useState } from "react";
import ReactDOM from "react-dom";
import {
  ApolloClient,
  ApolloProvider,
  InMemoryCache,
  createHttpLink,
} from "@apollo/client";
import generatedIntrospection from "./backendApi/introspection";
import { ConfigProvider, Modal } from "antd";
import deDE from "antd/lib/locale/de_DE";
import { UserContext } from "./contexts/UserContext";
import { App } from "./App";
import { ErrorDisplay } from "./components/ErrorDisplay";
import reportWebVitals from "./reportWebVitals";
//import { createNetworkStatusNotifier } from "react-apollo-network-status";

import "./index.css";

require("moment/locale/de"); // -> localize momentJS

//const { link, useApolloNetworkStatus } = createNetworkStatusNotifier();

const user =
  new URLSearchParams(window.location.search).get("u")?.trim() || null; // FIXME: verify either via nginx or the backend.

const apolloClient = new ApolloClient({
  uri: "/graphql",
  cache: new InMemoryCache({
    possibleTypes: generatedIntrospection.possibleTypes,
  }),
  //link: link.concat(createHttpLink()),
});

const Root: React.FC = () => {
  //const networkStatus = useApolloNetworkStatus();

  const [showNetworkErrors, setShowNetworkErrors] = useState(false);

  let networkErrors: string[] = [];

  // if (networkStatus.queryError) {
  //   if (networkStatus.queryError.networkError) {
  //     networkErrors.push(
  //       "[Netzwerk]: " + networkStatus.queryError.networkError.message
  //     );
  //   }

  //   if (networkStatus.queryError.graphQLErrors) {
  //     networkStatus.queryError.graphQLErrors.forEach(({ message }) =>
  //       networkErrors.push("[GraphQL]: " + message)
  //     );
  //   }
  // }

  // if (networkStatus.mutationError) {
  //   if (networkStatus.mutationError.networkError) {
  //     networkErrors.push(
  //       "[Netzwerk]: " + networkStatus.mutationError.networkError.message
  //     );
  //   }

  //   if (networkStatus.mutationError.graphQLErrors) {
  //     networkStatus.mutationError.graphQLErrors.forEach(({ message }) =>
  //       networkErrors.push("[GraphQL]: " + message)
  //     );
  //   }
  // }

  // if (networkStatus.queryError) {
  //   console.debug("Query error:", networkStatus.queryError);
  // }

  // if (networkStatus.mutationError) {
  //   console.debug("Mutation error:", networkStatus.mutationError);
  // }

  return (
    <ApolloProvider client={apolloClient}>
      <ConfigProvider locale={deDE}>
        <UserContext.Provider value={user ? { name: user } : undefined}>
          <App
            networkBusy={
              false
              //!!networkStatus.numPendingMutations ||
              //!!networkStatus.numPendingQueries
            }
            networkHadErrors={!!networkErrors.length}
            onShowNetworkErrors={() => setShowNetworkErrors(true)}
          />
          <Modal
            visible={networkErrors.length && showNetworkErrors ? true : false}
            bodyStyle={{ padding: 0 }}
            closable={true}
            onCancel={() => setShowNetworkErrors(false)}
            footer={null}
          >
            <ErrorDisplay
              title={{
                main: "Verbindungsprobleme!",
                sub: "Ist das Internet kaputt?",
              }}
              errors={networkErrors}
            />
          </Modal>
        </UserContext.Provider>
      </ConfigProvider>
    </ApolloProvider>
  );
};

ReactDOM.render(
  <React.StrictMode>
    <Root />
  </React.StrictMode>,
  document.getElementById("root")
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
