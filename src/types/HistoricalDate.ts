import moment from "moment";

const dateRegex = /^~?\??\d\d\d\d(-\d\d(-\d\d(\/\d\d\d\d(-\d\d(-\d\d)?)?)?)?)?$/;

const dateFormats = ["YYYY-MM-DD", "YYYY-MM", "YYYY"];

type HistoricalDateValue = string | [string, string]; // the latter variant means [between, and]

export class HistoricalDate {
  readonly value: HistoricalDateValue | Error;

  private momentValue?: moment.Moment | [moment.Moment, moment.Moment] | Error;

  readonly isApproximation: boolean = false;

  readonly isPresumption: boolean = false;

  serialize(): string {
    let serialization: string = "";

    if (this.value instanceof Error) {
      return "";
    }

    if (this.isApproximation) {
      serialization += "~";
    }

    if (this.isPresumption) {
      serialization += "?";
    }

    if (typeof this.value === "string") {
      serialization += this.value;
    } else {
      serialization += this.value[0] + "/" + this.value[1];
    }

    return serialization;
  }

  asMoment() {
    if (!this.momentValue) {
      if (this.value instanceof Error) {
        this.momentValue = this.value;
      } else if (this.value instanceof Array) {
        this.momentValue = [
          moment(this.value[0], dateFormats, true),
          moment(this.value[1], dateFormats, true),
        ];
      } else {
        this.momentValue = moment(this.value, dateFormats, true);
      }
    }

    return this.momentValue;
  }

  asOneMoment() {
    const moments = this.asMoment();

    return moments instanceof Error
      ? moments
      : moments instanceof Array
      ? moments[0]
      : moments;
  }

  isValid() {
    const momentValue = this.asMoment();

    if (momentValue instanceof Error) {
      return false;
    } else if (!(momentValue instanceof Array)) {
      if (!momentValue.isValid()) {
        return false;
      }
    } else {
      if (!momentValue[0].isValid()) {
        return false;
      }

      if (!momentValue[1].isValid()) {
        return false;
      }
    }

    return true;
  }

  displayYear() {
    if (this.value instanceof Error) {
      return undefined;
    } else if (typeof this.value === "string") {
      return this.value.substring(0, 4);
    } else {
      return (
        this.value[0].substring(0, 4) + "~" + this.value[1].substring(0, 4)
      ); // FIXME: use a different sign for "in-between"?
    }
  }

  constructor(literalValue: string) {
    if (!dateRegex.test(literalValue)) {
      this.value = new Error(
        "Illegal formatting of historical date: " + literalValue
      );
    } else {
      if (literalValue.startsWith("~")) {
        this.isApproximation = true;

        literalValue = literalValue.substr(1);
      }

      if (literalValue.startsWith("?")) {
        this.isPresumption = true;

        literalValue = literalValue.substr(1);
      }

      if (literalValue.includes("/")) {
        this.value = literalValue.split("/", 2) as [string, string];
      } else {
        this.value = literalValue;
      }
    }
  }
}
