export class GlobalState<T> {
  private value: T;

  private subscribers: ((value: T) => void)[] = [];

  constructor(initialValue: T) {
    this.value = initialValue;
  }

  getValue = () => this.value;

  setValue = (newState: T | ((previousValue: T) => T)) => {
    if (this.getValue() !== newState) {
      this.value =
        newState instanceof Function ? newState(this.value) : newState;

      this.subscribers.forEach((v) => {
        v(this.value);
      });
    }
  };

  subscribe = (callback: (value: T) => void) => {
    if (this.subscribers.indexOf(callback) === -1) {
      this.subscribers.push(callback);
    }
  };

  unsubscribe = (callback: (value: T) => void) => {
    this.subscribers = this.subscribers.filter((v) => v !== callback);
  };
}
