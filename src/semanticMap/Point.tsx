import React from "react";

interface PointProps {
  id: string;
  longitude: number;
  latitude: number;
  circle?: {
    color?: string;
    weight?: number;
    opacity?: number;
    fillOpacity?: number;
  };
  icon?: string;
  clickable?: boolean;
  draggable?: boolean;
}

export const Point: React.FC<PointProps> = (props) => {
  const {
    id,
    longitude,
    latitude,
    circle,
    icon,
    clickable = true,
    draggable = false,
  } = props;

  return (
    <div
      className="semantic-map__feature semantic-map__geometry"
      data-semantic-map-geometry-type="Point"
      data-semantic-map-geometry-coordinates={`[${longitude}, ${latitude}]`}
    >
      <div
        className="semantic-map__property"
        data-semantic-map-property-key="id"
        data-semantic-map-property-value={id || undefined}
      />
      {circle && (
        <div
          className="semantic-map__property"
          data-semantic-map-property-key="circle"
          data-semantic-map-property-parse="json"
          data-semantic-map-property-value={JSON.stringify(circle)}
        />
      )}
      {icon && (
        <div
          className="semantic-map__property"
          data-semantic-map-property-key="icon"
          data-semantic-map-property-value={icon}
        />
      )}
      {!clickable && (
        <div
          className="semantic-map__property"
          data-semantic-map-property-key="clickable"
          data-semantic-map-property-value={false}
        />
      )}
      {draggable && (
        <div
          className="semantic-map__property"
          data-semantic-map-property-key="draggable"
          data-semantic-map-property-value={true}
        />
      )}
    </div>
  );
};
