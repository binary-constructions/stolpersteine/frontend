import React from "react";

export const Attribution: React.FC = (props) => {
  const { children } = props;

  return <div className="semantic-map__attribution">{children}</div>;
};
