import React, { useState, useEffect } from "react";

import { useSemanticMap } from "../hooks/useSemanticMap";

import "./SemanticMap.css";

import "leaflet/dist/leaflet.css";

interface SemanticMapProps {
  tileUrlTemplate: string;

  maxZoom: number;

  maxNativeZoom: number;

  view: {
    longitude: number; // TODO: currently only used as initial value

    latitude: number; // TODO: currently only used as initial value

    zoom: number;

    // TODO: offset
  };

  onViewChange: (view: {
    longitude: number; // FIXME: currently not updated

    latitude: number; // FIXME: currently not updated

    zoom: number;

    // TODO: offset
  }) => void;

  onMapClick?: (coordinates: { longitude: number; latitude: number }) => void;

  onFeatureClick?: (id: string) => void;

  onFeatureDrag?: (
    id: string,
    coordinates: { longitude: number; latitude: number }
  ) => void;
}

let nextMapId: number = 0;

export const SemanticMap: React.FC<SemanticMapProps> = (props) => {
  const {
    tileUrlTemplate,
    maxZoom,
    maxNativeZoom,
    view,
    onViewChange,
    onMapClick,
    onFeatureClick,
    onFeatureDrag,
    children,
  } = props;

  const [savedMapId, setSavedMapId] = useState<number | undefined>(undefined);

  const mapId = savedMapId !== undefined ? savedMapId : nextMapId++;

  useEffect(() => setSavedMapId(mapId), [mapId]); // happens just once

  const [initialView] = useState(view);

  const map = useSemanticMap(mapId);

  useEffect(() => {
    if (map) {
      map.renderer.setZoom(view.zoom);
    }
  }, [map, mapId, view.zoom]);

  useEffect(() => {
    if (map) {
      map.renderer.onZoom = (zoom: number) => {
        onViewChange({ ...initialView, zoom });
      };
    }
  }, [map, mapId, onViewChange, initialView]);

  useEffect(() => {
    if (map) {
      map.renderer.onMapClick = !onMapClick
        ? undefined
        : (details: { longitude: number; latitude: number }) =>
            onMapClick(details);
    }
  }, [map, mapId, onMapClick]);

  useEffect(() => {
    if (map) {
      map.renderer.onFeatureClick = !onFeatureClick
        ? undefined
        : (feature: any) => onFeatureClick(feature.properties.id);
    }
  }, [map, mapId, onFeatureClick]);

  useEffect(() => {
    if (map) {
      map.renderer.onFeatureDrag = !onFeatureDrag
        ? undefined
        : (feature: any, longitude: number, latitude: number) =>
            onFeatureDrag(feature.properties.id, {
              longitude,
              latitude,
            });
    }
  }, [map, mapId, onFeatureDrag]);

  return (
    <div className="SemanticMap">
      <div
        className="semantic-map"
        data-semantic-map-id={mapId}
        data-semantic-map-tile-url-template={tileUrlTemplate}
        data-semantic-map-max-zoom={maxZoom}
        data-semantic-map-max-native-zoom={maxNativeZoom}
        data-semantic-map-initial-zoom={initialView.zoom}
        data-semantic-map-initial-center={`[${initialView.longitude}, ${initialView.latitude}]`}
        data-semantic-map-disable-doubleclick-default="true"
        data-semantic-map-hide-zoom-control="true"
      >
        {children}
      </div>
    </div>
  );
};
