import React from "react";

interface IconProps {
  name?: string;
  size?: number;
}

export const Icon: React.FC<IconProps> = (props) => {
  const { name, size, children } = props;

  return (
    <div
      className="semantic-map__icon"
      data-semantic-map-icon-name={name || null}
      data-semantic-map-icon-size={size || null}
    >
      {children}
    </div>
  );
};
