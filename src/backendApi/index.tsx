import { gql } from '@apollo/client';
import * as Apollo from '@apollo/client';
export type Maybe<T> = T | null;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  /** The javascript `Date` as string. Type represents date and time as the ISO Date string. */
  DateTime: string;
  /**
   * A date that may be exact to the day ("YYYY-MM-DD"), less specific ("YYYY-MM" or
   * "YYYY", meaning "sometime in") or a time-frame ("<earliest>/<latest>", meaning
   * "in-between").  There may also be a prefix of "~", "?" or "~?" to mark the whole
   * expression as an approximation, a presumption or an approximate presumption
   * respectively.  (Note: in the future ISO-style times and time-zones might also be
   * allowed, so that should be taken into account when parsing.)
   */
  HistoricalDate: any;
};

/** A family name given to a person at birth (as opposed to by marriage). */
export type BirthName = PersonName & TypedLiteral & {
  __typename?: 'BirthName';
  value: Scalars['String'];
};

/** An object representing a primary unit of user-created and -changeable content. */
export type Content = {
  id: Scalars['ID'];
  created?: Maybe<Scalars['DateTime']>;
  lastModified?: Maybe<Scalars['DateTime']>;
  deleted?: Maybe<Scalars['DateTime']>;
  reviewStatus: ReviewStatus;
};


/**
 * A name indicating the family affiliation of a person (without further
 * information on whether it was assumed at birth or at marriage).
 */
export type FamilyName = PersonName & TypedLiteral & {
  __typename?: 'FamilyName';
  value: Scalars['String'];
};

export enum Fate {
  Unknown = 'UNKNOWN',
  Murdered = 'MURDERED',
  Survived = 'SURVIVED',
  Escaped = 'ESCAPED'
}

/**
 * Gender identities as either actually or at least presumably attributed to a
 * person by themselves and/or others (falling back to classical ideas of gender
 * dichotomy in cases where nothing to the contrary is specifically known).
 */
export enum Gender {
  Unspecified = 'UNSPECIFIED',
  NonBinary = 'NON_BINARY',
  Female = 'FEMALE',
  Male = 'MALE'
}

/** A geographic point on the map. */
export type GeoPoint = {
  __typename?: 'GeoPoint';
  longitude: Scalars['Float'];
  latitude: Scalars['Float'];
};

/** A name given to a person usually at or soon after birth. */
export type GivenName = PersonName & TypedLiteral & {
  __typename?: 'GivenName';
  value: Scalars['String'];
};


/** An object representing an Image. */
export type Image = Node & {
  __typename?: 'Image';
  id: Scalars['ID'];
  /** Information on the authorship of the image and its copyright. */
  authorshipRemark: Scalars['String'];
  /** The caption text for the image. */
  caption?: Maybe<Scalars['String']>;
  /** The text to use as alternative content for the image (e.g. when rendering for a braille display). */
  alternativeText?: Maybe<Scalars['String']>;
  /**
   * Supposed to return 'true'.
   * @deprecated Meant to be used for debugging only!
   */
  debugResolvesAsEntity: Scalars['Boolean'];
  /**
   * The internal representation of the object as a JSON string.
   * @deprecated Meant to be used for debugging only!
   */
  debugInternalJsonRepresentation: Scalars['String'];
  /**
   * Supposed to return 'true'.
   * @deprecated Meant to be used for debugging only!
   */
  debugResolvesAsNode: Scalars['Boolean'];
  fileName: Scalars['String'];
  path: Scalars['String'];
};

export type ImageInput = {
  /**
   * The base64 encoded image data (needs to be set for new images and only should
   * be set for existing ones if the image data changes).
   */
  data?: Maybe<Scalars['String']>;
  /** The file name of the image (needs to be set for new images but is optional for existing ones). */
  fileName?: Maybe<Scalars['String']>;
  /** Information on the authorship of the image and its copyright. */
  authorshipRemark: Scalars['String'];
  /** The caption text for the image. */
  caption?: Maybe<Scalars['String']>;
  /** The text to use as alternative content for the image (e.g. when rendering for a braille display). */
  alternativeText?: Maybe<Scalars['String']>;
};

/** A localization target */
export type Locale = {
  __typename?: 'Locale';
  code: Scalars['String'];
  label: Scalars['String'];
  /**
   * Supposed to return 'true'.
   * @deprecated Meant to be used for debugging only!
   */
  debugResolvesAsEntity: Scalars['Boolean'];
  /**
   * The internal representation of the object as a JSON string.
   * @deprecated Meant to be used for debugging only!
   */
  debugInternalJsonRepresentation: Scalars['String'];
};

export type LocaleInput = {
  /** The code to use for the Locale (of the for "xx" or "xx-XX", e.g. "de" or "de-DE"). */
  code: Scalars['String'];
  /** The label to use for the Locale. */
  label: Scalars['String'];
};

/** A family name given to a person at marriage. */
export type MarriedName = PersonName & TypedLiteral & {
  __typename?: 'MarriedName';
  value: Scalars['String'];
};

export type Mutation = {
  __typename?: 'Mutation';
  /** Mark the Content entity with the specified id as deleted. */
  delete: Content;
  /** Mark the Content entity with the specified id as undeleted. */
  undelete: Content;
  /** Change the review status of the Content entity with the specified id. */
  setReviewStatus: Content;
  /** Create a new Image object. */
  createImage: Image;
  /** Update an existing Image object. */
  updateImage: Image;
  /** Add a new Locale. */
  addLocale?: Maybe<Locale>;
  /** Update a Locale. */
  updateLocale?: Maybe<Locale>;
  /** Remove a Locale. */
  removeLocale?: Maybe<Locale>;
  /** Create a new Occupation object. */
  createOccupation: Occupation;
  /** Update an existing Occupation object. */
  updateOccupation: Occupation;
  /** Create a new PersecutionPretext object. */
  createPersecutionPretext: PersecutionPretext;
  /** Update an existing PersecutionPretext object. */
  updatePersecutionPretext: PersecutionPretext;
  /** Create a new Person object. */
  createPerson: Person;
  /** Update an existing Image object. */
  updatePerson: Person;
  /** Change the ReviewStatus of the Person with the specified id. */
  setPersonReviewStatus: Person;
  /**
   * Mark the specified Person entity as deleted.
   * @deprecated This actually does the same as a simple delete.
   */
  deepDeletePerson: Person;
  /** Create a new Place object. */
  createPlace: Place;
  /** Restore a previously existing Place object. */
  restorePlace: Place;
  /** Update an existing Place object. */
  updatePlace: Place;
  /** Create or update a Relationship between two StolpersteinSubject objects. */
  setRelationship: Relationship;
  /** Remove a Relationship between two StolpersteinSubject objects (inverted or not). */
  unsetRelationship?: Maybe<Relationship>;
  /** Create a new RelationshipType object. */
  createRelationshipType: RelationshipType;
  /** Update an existing RelationshipType object. */
  updateRelationshipType: RelationshipType;
  /** Create a new Spot object. */
  createSpot: Spot;
  /** Update an existing Spot object. */
  updateSpot: Spot;
  /** Mark the Spot and all associated Stolperstein and StolpersteinSubject entities as deleted. */
  deepDeleteSpot: Spot;
  /** Create a new Stolperstein object. */
  createStolperstein: Stolperstein;
  /** Update an existing Image object. */
  updateStolperstein: Stolperstein;
  /** Change the ReviewStatus of the Stolperstein with the specified id. */
  setStolpersteinReviewStatus: Stolperstein;
  /** Mark the Stolperstein and all associated Stolperstein and StolpersteinSubject entities as deleted. */
  deepDeleteStolperstein: Stolperstein;
};


export type MutationDeleteArgs = {
  id: Scalars['String'];
};


export type MutationUndeleteArgs = {
  id: Scalars['String'];
};


export type MutationSetReviewStatusArgs = {
  status: ReviewStatus;
  id: Scalars['String'];
};


export type MutationCreateImageArgs = {
  input: ImageInput;
};


export type MutationUpdateImageArgs = {
  input: ImageInput;
  id: Scalars['String'];
};


export type MutationAddLocaleArgs = {
  label: Scalars['String'];
  code: Scalars['String'];
};


export type MutationUpdateLocaleArgs = {
  label: Scalars['String'];
  code: Scalars['String'];
};


export type MutationRemoveLocaleArgs = {
  code: Scalars['String'];
};


export type MutationCreateOccupationArgs = {
  input: OccupationInput;
};


export type MutationUpdateOccupationArgs = {
  input: OccupationInput;
  id: Scalars['String'];
};


export type MutationCreatePersecutionPretextArgs = {
  input: PersecutionPretextInput;
};


export type MutationUpdatePersecutionPretextArgs = {
  input: PersecutionPretextInput;
  id: Scalars['String'];
};


export type MutationCreatePersonArgs = {
  input: PersonInput;
};


export type MutationUpdatePersonArgs = {
  input: PersonInput;
  id: Scalars['String'];
};


export type MutationSetPersonReviewStatusArgs = {
  status: ReviewStatus;
  id: Scalars['String'];
};


export type MutationDeepDeletePersonArgs = {
  id: Scalars['String'];
};


export type MutationCreatePlaceArgs = {
  input: PlaceInput;
};


export type MutationRestorePlaceArgs = {
  input: PlaceInput;
  id: Scalars['String'];
};


export type MutationUpdatePlaceArgs = {
  input: PlaceInput;
  id: Scalars['String'];
};


export type MutationSetRelationshipArgs = {
  typeId: Scalars['String'];
  objectId: Scalars['String'];
  subjectId: Scalars['String'];
};


export type MutationUnsetRelationshipArgs = {
  otherId: Scalars['String'];
  oneId: Scalars['String'];
};


export type MutationCreateRelationshipTypeArgs = {
  input: RelationshipTypeInput;
};


export type MutationUpdateRelationshipTypeArgs = {
  input: RelationshipTypeInput;
  id: Scalars['String'];
};


export type MutationCreateSpotArgs = {
  input: SpotInput;
};


export type MutationUpdateSpotArgs = {
  input: SpotInput;
  id: Scalars['String'];
};


export type MutationDeepDeleteSpotArgs = {
  id: Scalars['String'];
};


export type MutationCreateStolpersteinArgs = {
  input: StolpersteinInput;
};


export type MutationUpdateStolpersteinArgs = {
  input: StolpersteinInput;
  id: Scalars['String'];
};


export type MutationSetStolpersteinReviewStatusArgs = {
  status: ReviewStatus;
  id: Scalars['String'];
};


export type MutationDeepDeleteStolpersteinArgs = {
  id: Scalars['String'];
};

/** The type of a name of a person. */
export enum NameType {
  Given = 'GIVEN',
  Nickname = 'NICKNAME',
  Pseudonym = 'PSEUDONYM',
  Family = 'FAMILY',
  Birth = 'BIRTH',
  Married = 'MARRIED'
}

/** A name given to a person by friends or others. */
export type Nickname = PersonName & TypedLiteral & {
  __typename?: 'Nickname';
  value: Scalars['String'];
};

/** An object uniquely identified by an ID. */
export type Node = {
  id: Scalars['ID'];
};

/** An object representing a persons occupation. */
export type Occupation = Node & {
  __typename?: 'Occupation';
  id: Scalars['ID'];
  label: Scalars['String'];
  label_f?: Maybe<Scalars['String']>;
  label_m?: Maybe<Scalars['String']>;
  localizations: Array<OccupationLocalization>;
  /**
   * Supposed to return 'true'.
   * @deprecated Meant to be used for debugging only!
   */
  debugResolvesAsEntity: Scalars['Boolean'];
  /**
   * The internal representation of the object as a JSON string.
   * @deprecated Meant to be used for debugging only!
   */
  debugInternalJsonRepresentation: Scalars['String'];
  /**
   * Supposed to return 'true'.
   * @deprecated Meant to be used for debugging only!
   */
  debugResolvesAsNode: Scalars['Boolean'];
  locale: Locale;
};

export type OccupationInput = {
  locale: Scalars['String'];
  label: Scalars['String'];
  label_f?: Maybe<Scalars['String']>;
  label_m?: Maybe<Scalars['String']>;
  localizations?: Maybe<Array<OccupationLocalizationInput>>;
};

/** A localization for an Occupation object. */
export type OccupationLocalization = {
  __typename?: 'OccupationLocalization';
  localeCode: Scalars['String'];
  label: Scalars['String'];
  label_f?: Maybe<Scalars['String']>;
  label_m?: Maybe<Scalars['String']>;
  locale: Locale;
};

export type OccupationLocalizationInput = {
  locale: Scalars['String'];
  label: Scalars['String'];
  label_f?: Maybe<Scalars['String']>;
  label_m?: Maybe<Scalars['String']>;
};

/** An object representing a persecution pretext. */
export type PersecutionPretext = Node & {
  __typename?: 'PersecutionPretext';
  id: Scalars['ID'];
  localeCode: Scalars['String'];
  label: Scalars['String'];
  label_f?: Maybe<Scalars['String']>;
  label_m?: Maybe<Scalars['String']>;
  localizations: Array<PersecutionPretextLocalization>;
  /**
   * Supposed to return 'true'.
   * @deprecated Meant to be used for debugging only!
   */
  debugResolvesAsEntity: Scalars['Boolean'];
  /**
   * The internal representation of the object as a JSON string.
   * @deprecated Meant to be used for debugging only!
   */
  debugInternalJsonRepresentation: Scalars['String'];
  /**
   * Supposed to return 'true'.
   * @deprecated Meant to be used for debugging only!
   */
  debugResolvesAsNode: Scalars['Boolean'];
  locale: Locale;
};

export type PersecutionPretextInput = {
  locale: Scalars['String'];
  label: Scalars['String'];
  label_f?: Maybe<Scalars['String']>;
  label_m?: Maybe<Scalars['String']>;
  localizations?: Maybe<Array<PersecutionPretextLocalizationInput>>;
};

/** A localization for a PersecutionPretext object. */
export type PersecutionPretextLocalization = {
  __typename?: 'PersecutionPretextLocalization';
  localeCode: Scalars['String'];
  label: Scalars['String'];
  label_f?: Maybe<Scalars['String']>;
  label_m?: Maybe<Scalars['String']>;
  locale: Locale;
};

export type PersecutionPretextLocalizationInput = {
  locale: Scalars['String'];
  label: Scalars['String'];
  label_f?: Maybe<Scalars['String']>;
  label_m?: Maybe<Scalars['String']>;
};

/** An object representing a person. */
export type Person = Node & Content & StolpersteinSubject & {
  __typename?: 'Person';
  id: Scalars['ID'];
  /** The date this content object was created.  May be null if the creation time is unknown. */
  created?: Maybe<Scalars['DateTime']>;
  lastModified?: Maybe<Scalars['DateTime']>;
  deleted?: Maybe<Scalars['DateTime']>;
  reviewStatus: ReviewStatus;
  subjectOf: Array<Stolperstein>;
  subjectOfIds: Array<Scalars['String']>;
  names: Array<PersonName>;
  occupationIds: Array<Scalars['String']>;
  title?: Maybe<PersonTitle>;
  gender: Gender;
  bornDate?: Maybe<Scalars['HistoricalDate']>;
  bornPlaceId?: Maybe<Scalars['String']>;
  diedDate?: Maybe<Scalars['HistoricalDate']>;
  diedPlaceId?: Maybe<Scalars['String']>;
  persecutionPretextIds: Array<Scalars['String']>;
  fate: Fate;
  escapedToPlaceId?: Maybe<Scalars['String']>;
  imageIds: Array<Scalars['String']>;
  /** The HTML-formatted biography text for the person. */
  biographyHTML?: Maybe<Scalars['String']>;
  /** HTML-formatted internal notes. */
  editingNotesHTML?: Maybe<Scalars['String']>;
  /**
   * Supposed to return 'true'.
   * @deprecated Meant to be used for debugging only!
   */
  debugResolvesAsEntity: Scalars['Boolean'];
  /**
   * The internal representation of the object as a JSON string.
   * @deprecated Meant to be used for debugging only!
   */
  debugInternalJsonRepresentation: Scalars['String'];
  /**
   * Supposed to return 'true'.
   * @deprecated Meant to be used for debugging only!
   */
  debugResolvesAsNode: Scalars['Boolean'];
  /**
   * Supposed to return 'true'.
   * @deprecated Meant to be used for debugging only!
   */
  debugResolvesAsContent: Scalars['Boolean'];
  /**
   * Supposed to return 'true'.
   * @deprecated Meant to be used for debugging only!
   */
  debugResolvesAsStolpersteinSubject: Scalars['Boolean'];
  occupations: Array<Occupation>;
  bornPlace?: Maybe<Place>;
  diedPlace?: Maybe<Place>;
  persecutionPretexts: Array<PersecutionPretext>;
  images: Array<Image>;
};

export type PersonInput = {
  /** A list of the names of the person in display order.  Must contain at least one name. */
  names: Array<PersonNameInput>;
  /** A list of the IDs of the occupations of the person. */
  occupationIds?: Maybe<Array<Scalars['String']>>;
  /** A list of the IDs of the pretexts used in persecution of the person. */
  persecutionPretextIds?: Maybe<Array<Scalars['String']>>;
  title?: Maybe<Scalars['String']>;
  gender?: Maybe<Gender>;
  /** The date the person was born. */
  bornDate?: Maybe<Scalars['HistoricalDate']>;
  /** The ID of the place that the person was born at. */
  bornPlaceId?: Maybe<Scalars['String']>;
  /** The date the person died. */
  diedDate?: Maybe<Scalars['HistoricalDate']>;
  /** The ID of the place that the person died at. */
  diedPlaceId?: Maybe<Scalars['String']>;
  imageIds?: Maybe<Array<Scalars['String']>>;
  fate?: Maybe<Fate>;
  escapedToPlaceId?: Maybe<Scalars['String']>;
  /** The HTML-formatted biography text for the person. */
  biographyHTML?: Maybe<Scalars['String']>;
  /** HTML-formatted internal notes. */
  editingNotesHTML?: Maybe<Scalars['String']>;
};

/** The name of a person. */
export type PersonName = {
  value: Scalars['String'];
};

export type PersonNameInput = {
  name: Scalars['String'];
  type: NameType;
};

/** Condensed information on a Person. */
export type PersonSummary = {
  __typename?: 'PersonSummary';
  id: Scalars['String'];
  created?: Maybe<Scalars['DateTime']>;
  lastModified?: Maybe<Scalars['DateTime']>;
  deleted?: Maybe<Scalars['DateTime']>;
  reviewStatus?: Maybe<ReviewStatus>;
  stolpersteinIds: Array<Scalars['String']>;
  names: Array<PersonName>;
  title?: Maybe<Scalars['String']>;
  gender: Gender;
  bornDate?: Maybe<Scalars['String']>;
  diedDate?: Maybe<Scalars['String']>;
  persecutionPretextIds: Array<Scalars['String']>;
  fate: Fate;
  imageCount: Scalars['Int'];
};

/** The title of a person. */
export type PersonTitle = TypedLiteral & {
  __typename?: 'PersonTitle';
  value: Scalars['String'];
};

/** An object representing a labeled geographic area. */
export type Place = Node & {
  __typename?: 'Place';
  id: Scalars['ID'];
  label: Scalars['String'];
  parentId?: Maybe<Scalars['String']>;
  placeType: PlaceType;
  /** The list of alternative names for this Place. */
  alternativeNames: Array<Scalars['String']>;
  /**
   * Supposed to return 'true'.
   * @deprecated Meant to be used for debugging only!
   */
  debugResolvesAsEntity: Scalars['Boolean'];
  /**
   * The internal representation of the object as a JSON string.
   * @deprecated Meant to be used for debugging only!
   */
  debugInternalJsonRepresentation: Scalars['String'];
  /**
   * Supposed to return 'true'.
   * @deprecated Meant to be used for debugging only!
   */
  debugResolvesAsNode: Scalars['Boolean'];
  /** Get the parent of this Place, if any. */
  parent?: Maybe<Place>;
  /** Get an ordered list of all the parents of this Place (with the immediate parent coming first). */
  parents?: Maybe<Array<Place>>;
  /** Get a list of all the children of this Place. */
  children?: Maybe<Array<Place>>;
};

export type PlaceInput = {
  label: Scalars['String'];
  /** The ID of a place containing this one. */
  parentId?: Maybe<Scalars['String']>;
  /** The list of alternative names this place is known by. */
  alternativeNames?: Maybe<Array<Scalars['String']>>;
};

/** Condensed information on a place as well as aassociated persons and Stolpersteine. */
export type PlaceSummary = {
  __typename?: 'PlaceSummary';
  id: Scalars['String'];
  label: Scalars['String'];
  parentId?: Maybe<Scalars['String']>;
  alternativeNames: Array<Scalars['String']>;
};

/** The type of a geographic place. */
export enum PlaceType {
  Unspecified = 'UNSPECIFIED',
  Country = 'COUNTRY',
  Province = 'PROVINCE',
  RuralDistrict = 'RURAL_DISTRICT',
  Locality = 'LOCALITY',
  District = 'DISTRICT'
}

/** A name assumed by a person publicly for a particular purpose. */
export type Pseudonym = PersonName & TypedLiteral & {
  __typename?: 'Pseudonym';
  value: Scalars['String'];
};

export type Query = {
  __typename?: 'Query';
  /** Get the 'Stolperstein' entity with the specified id if it exists. */
  stolperstein?: Maybe<Stolperstein>;
  /** Get a list of either the specified or all 'Stolperstein' entities. */
  stolpersteine: Array<Stolperstein>;
  /** Get the 'Spot' entity with the specified id if it exists. */
  spot?: Maybe<Spot>;
  /** Get a list of either the specified or all 'Spot' entities. */
  spots: Array<Spot>;
  /** Get the 'RelationshipType' entity with the specified id if it exists. */
  relationshipType?: Maybe<RelationshipType>;
  /** Get a list of either the specified or all 'RelationshipType' entities. */
  relationshipTypes: Array<RelationshipType>;
  /** Get the 'Relationship' entity with the specified id if it exists. */
  relationshipById?: Maybe<Relationship>;
  /** Get a list of either the specified or all 'Relationship' entities. */
  relationships: Array<Relationship>;
  /** Get the 'Place' entity with the specified id if it exists. */
  place?: Maybe<Place>;
  /** Get a list of either the specified or all 'Place' entities. */
  places: Array<Place>;
  /** Get the 'Person' entity with the specified id if it exists. */
  person?: Maybe<Person>;
  /** Get a list of either the specified or all 'Person' entities. */
  persons: Array<Person>;
  /** Get the 'PersecutionPretext' entity with the specified id if it exists. */
  persecutionPretext?: Maybe<PersecutionPretext>;
  /** Get a list of either the specified or all 'PersecutionPretext' entities. */
  persecutionPretexts: Array<PersecutionPretext>;
  /** Get the 'Occupation' entity with the specified id if it exists. */
  occupation?: Maybe<Occupation>;
  /** Get a list of either the specified or all 'Occupation' entities. */
  occupations: Array<Occupation>;
  /** Get the 'Locale' entity with the specified id if it exists. */
  locale?: Maybe<Locale>;
  /** Get a list of either the specified or all 'Locale' entities. */
  locales: Array<Locale>;
  /** Get the 'Image' entity with the specified id if it exists. */
  image?: Maybe<Image>;
  /** Get a list of either the specified or all 'Image' entities. */
  images: Array<Image>;
  nodes: Array<Node>;
  /** Get the Node with the specified id, if it exists. */
  node?: Maybe<Node>;
  /** Get all Content objects. */
  contents: Array<Content>;
  /** Get the Content object with the specified id, if it exists. */
  content?: Maybe<Content>;
  /** Get all StolpersteinSubject objects. */
  stolpersteinSubjects: Array<StolpersteinSubject>;
  /** Get the StolpersteinSubject object with the specified id, if it exists. */
  stolpersteinSubject?: Maybe<StolpersteinSubject>;
  /** Get a list of all Place objects without a parent. */
  placesByParent: Array<Place>;
  /**
   * Get the (non-inverted) Relationship between the two specified
   * StolpersteinSubject objects.  (Inverse relationships need to be queried separately!)
   */
  relationship?: Maybe<Relationship>;
  /** Get a list of all Relationships with the specified ID as the subject. */
  relationshipsWithSubject: Array<Relationship>;
  /** Get a list of all Relationships with the specified ID as the object. */
  relationshipsWithObject: Array<Relationship>;
  /** @deprecated Summary queries were only introduced as a hack and will probably be removed again in the future. */
  spotSummaries: Array<SpotSummary>;
  /** @deprecated Summary queries were only introduced as a hack and will probably be removed again in the future. */
  placeSummaries: Array<PlaceSummary>;
};


export type QueryStolpersteinArgs = {
  id: Scalars['String'];
};


export type QueryStolpersteineArgs = {
  ids?: Maybe<Array<Scalars['String']>>;
};


export type QuerySpotArgs = {
  id: Scalars['String'];
};


export type QuerySpotsArgs = {
  ids?: Maybe<Array<Scalars['String']>>;
};


export type QueryRelationshipTypeArgs = {
  id: Scalars['String'];
};


export type QueryRelationshipTypesArgs = {
  ids?: Maybe<Array<Scalars['String']>>;
};


export type QueryRelationshipByIdArgs = {
  id: Scalars['String'];
};


export type QueryRelationshipsArgs = {
  ids?: Maybe<Array<Scalars['String']>>;
};


export type QueryPlaceArgs = {
  id: Scalars['String'];
};


export type QueryPlacesArgs = {
  ids?: Maybe<Array<Scalars['String']>>;
};


export type QueryPersonArgs = {
  id: Scalars['String'];
};


export type QueryPersonsArgs = {
  ids?: Maybe<Array<Scalars['String']>>;
};


export type QueryPersecutionPretextArgs = {
  id: Scalars['String'];
};


export type QueryPersecutionPretextsArgs = {
  ids?: Maybe<Array<Scalars['String']>>;
};


export type QueryOccupationArgs = {
  id: Scalars['String'];
};


export type QueryOccupationsArgs = {
  ids?: Maybe<Array<Scalars['String']>>;
};


export type QueryLocaleArgs = {
  code: Scalars['String'];
};


export type QueryLocalesArgs = {
  codes?: Maybe<Array<Scalars['String']>>;
};


export type QueryImageArgs = {
  id: Scalars['String'];
};


export type QueryImagesArgs = {
  ids?: Maybe<Array<Scalars['String']>>;
};


export type QueryNodeArgs = {
  id: Scalars['String'];
};


export type QueryContentArgs = {
  id: Scalars['String'];
};


export type QueryStolpersteinSubjectArgs = {
  id: Scalars['String'];
};


export type QueryPlacesByParentArgs = {
  id?: Maybe<Scalars['String']>;
};


export type QueryRelationshipArgs = {
  objectId: Scalars['String'];
  subjectId: Scalars['String'];
};


export type QueryRelationshipsWithSubjectArgs = {
  subjectId: Scalars['String'];
};


export type QueryRelationshipsWithObjectArgs = {
  objectId: Scalars['String'];
};

/** A relationship between two StolpersteinSubject objects. */
export type Relationship = Node & {
  __typename?: 'Relationship';
  id: Scalars['ID'];
  subjectId: Scalars['ID'];
  objectId: Scalars['ID'];
  typeId: Scalars['ID'];
  /**
   * Supposed to return 'true'.
   * @deprecated Meant to be used for debugging only!
   */
  debugResolvesAsEntity: Scalars['Boolean'];
  /**
   * The internal representation of the object as a JSON string.
   * @deprecated Meant to be used for debugging only!
   */
  debugInternalJsonRepresentation: Scalars['String'];
  /**
   * Supposed to return 'true'.
   * @deprecated Meant to be used for debugging only!
   */
  debugResolvesAsNode: Scalars['Boolean'];
  subject: StolpersteinSubject;
  object: StolpersteinSubject;
  type: RelationshipType;
};

/** An object representing a type of relationship. */
export type RelationshipType = Node & {
  __typename?: 'RelationshipType';
  id: Scalars['ID'];
  localeCode: Scalars['String'];
  subjectLabel: Scalars['String'];
  subjectLabel_f?: Maybe<Scalars['String']>;
  subjectLabel_m?: Maybe<Scalars['String']>;
  objectLabel: Scalars['String'];
  objectLabel_f?: Maybe<Scalars['String']>;
  objectLabel_m?: Maybe<Scalars['String']>;
  localizations: Array<RelationshipTypeLocalization>;
  /**
   * Supposed to return 'true'.
   * @deprecated Meant to be used for debugging only!
   */
  debugResolvesAsEntity: Scalars['Boolean'];
  /**
   * The internal representation of the object as a JSON string.
   * @deprecated Meant to be used for debugging only!
   */
  debugInternalJsonRepresentation: Scalars['String'];
  /**
   * Supposed to return 'true'.
   * @deprecated Meant to be used for debugging only!
   */
  debugResolvesAsNode: Scalars['Boolean'];
  locale: Locale;
};

export type RelationshipTypeInput = {
  locale: Scalars['String'];
  subjectLabel: Scalars['String'];
  subjectLabel_f?: Maybe<Scalars['String']>;
  subjectLabel_m?: Maybe<Scalars['String']>;
  objectLabel: Scalars['String'];
  objectLabel_f?: Maybe<Scalars['String']>;
  objectLabel_m?: Maybe<Scalars['String']>;
  localizations?: Maybe<Array<RelationshipTypeLocalizationInput>>;
};

/** A localization object for a RelationshipType object. */
export type RelationshipTypeLocalization = {
  __typename?: 'RelationshipTypeLocalization';
  localeCode: Scalars['String'];
  subjectLabel: Scalars['String'];
  subjectLabel_f?: Maybe<Scalars['String']>;
  subjectLabel_m?: Maybe<Scalars['String']>;
  objectLabel: Scalars['String'];
  objectLabel_f?: Maybe<Scalars['String']>;
  objectLabel_m?: Maybe<Scalars['String']>;
  locale: Locale;
};

export type RelationshipTypeLocalizationInput = {
  locale: Scalars['String'];
  subjectLabel: Scalars['String'];
  subjectLabel_f?: Maybe<Scalars['String']>;
  subjectLabel_m?: Maybe<Scalars['String']>;
  objectLabel: Scalars['String'];
  objectLabel_f?: Maybe<Scalars['String']>;
  objectLabel_m?: Maybe<Scalars['String']>;
};

/** The review status of a content item. */
export enum ReviewStatus {
  Undefined = 'UNDEFINED',
  Stub = 'STUB',
  WorkInProgress = 'WORK_IN_PROGRESS',
  NeedsReview = 'NEEDS_REVIEW',
  Released = 'RELEASED'
}

/** An object representing a geographic spot. */
export type Spot = Node & Content & {
  __typename?: 'Spot';
  id: Scalars['ID'];
  /** The date this content object was created.  May be null if the creation time is unknown. */
  created?: Maybe<Scalars['DateTime']>;
  lastModified?: Maybe<Scalars['DateTime']>;
  deleted?: Maybe<Scalars['DateTime']>;
  reviewStatus: ReviewStatus;
  point: GeoPoint;
  street?: Maybe<Scalars['String']>;
  streetDetail?: Maybe<StreetDetail>;
  postalCode?: Maybe<Scalars['String']>;
  placeId?: Maybe<Scalars['String']>;
  note?: Maybe<Scalars['String']>;
  imageIds: Array<Scalars['String']>;
  /**
   * Supposed to return 'true'.
   * @deprecated Meant to be used for debugging only!
   */
  debugResolvesAsEntity: Scalars['Boolean'];
  /**
   * The internal representation of the object as a JSON string.
   * @deprecated Meant to be used for debugging only!
   */
  debugInternalJsonRepresentation: Scalars['String'];
  /**
   * Supposed to return 'true'.
   * @deprecated Meant to be used for debugging only!
   */
  debugResolvesAsNode: Scalars['Boolean'];
  /**
   * Supposed to return 'true'.
   * @deprecated Meant to be used for debugging only!
   */
  debugResolvesAsContent: Scalars['Boolean'];
  place?: Maybe<Place>;
  images: Array<Image>;
  /** Get all Stolperstein objects located at this spot. */
  stolpersteine: Array<Stolperstein>;
  /** Get the ids of all non-deleted stolpersteine present at this spot. */
  stolpersteinIds: Array<Scalars['String']>;
};

export type SpotInput = {
  longitude: Scalars['Float'];
  latitude: Scalars['Float'];
  street?: Maybe<Scalars['String']>;
  corner?: Maybe<Scalars['String']>;
  number?: Maybe<Scalars['String']>;
  postalCode?: Maybe<Scalars['String']>;
  placeId?: Maybe<Scalars['String']>;
  note?: Maybe<Scalars['String']>;
  imageIds?: Maybe<Array<Scalars['String']>>;
};

/** Condensed information on a spot as well as aassociated persons and Stolpersteine. */
export type SpotSummary = {
  __typename?: 'SpotSummary';
  id: Scalars['String'];
  created?: Maybe<Scalars['DateTime']>;
  lastModified?: Maybe<Scalars['DateTime']>;
  deleted?: Maybe<Scalars['DateTime']>;
  reviewStatus?: Maybe<ReviewStatus>;
  point: GeoPoint;
  street?: Maybe<Scalars['String']>;
  streetNumber?: Maybe<Scalars['String']>;
  streetCorner?: Maybe<Scalars['String']>;
  postalCode?: Maybe<Scalars['String']>;
  placeId?: Maybe<Scalars['String']>;
  imageCount: Scalars['Int'];
  stolpersteinSummaries: Array<StolpersteinSummary>;
};

/** An object representing a Stolperstein. */
export type Stolperstein = Node & Content & {
  __typename?: 'Stolperstein';
  id: Scalars['ID'];
  /** The date this content object was created.  May be null if the creation time is unknown. */
  created?: Maybe<Scalars['DateTime']>;
  lastModified?: Maybe<Scalars['DateTime']>;
  deleted?: Maybe<Scalars['DateTime']>;
  reviewStatus: ReviewStatus;
  spotId?: Maybe<Scalars['String']>;
  laid?: Maybe<Scalars['HistoricalDate']>;
  /** The text inscribed on the stone (containing newlines). */
  inscription?: Maybe<Scalars['String']>;
  imageId?: Maybe<Scalars['String']>;
  subjectIds: Array<Scalars['String']>;
  historicalStreet?: Maybe<Scalars['String']>;
  historicalStreetDetail?: Maybe<StreetDetail>;
  historicalPlaceId?: Maybe<Scalars['String']>;
  /** Information about people/groups that initiated the laying of this Stolperstein. */
  initiativeHTML?: Maybe<Scalars['String']>;
  /** Information about people/groups that helped finance the laying of this Stolperstein. */
  patronageHTML?: Maybe<Scalars['String']>;
  /** Information about people/groups that helped gather information about the subject of the Stolperstein. */
  dataGatheringHTML?: Maybe<Scalars['String']>;
  /** Information about people/groups that funded this Stolperstein. */
  fundingHTML?: Maybe<Scalars['String']>;
  /**
   * Supposed to return 'true'.
   * @deprecated Meant to be used for debugging only!
   */
  debugResolvesAsEntity: Scalars['Boolean'];
  /**
   * The internal representation of the object as a JSON string.
   * @deprecated Meant to be used for debugging only!
   */
  debugInternalJsonRepresentation: Scalars['String'];
  /**
   * Supposed to return 'true'.
   * @deprecated Meant to be used for debugging only!
   */
  debugResolvesAsNode: Scalars['Boolean'];
  /**
   * Supposed to return 'true'.
   * @deprecated Meant to be used for debugging only!
   */
  debugResolvesAsContent: Scalars['Boolean'];
  spot?: Maybe<Spot>;
  image?: Maybe<Image>;
  subjects: Array<StolpersteinSubject>;
  historicalPlace?: Maybe<Place>;
};

export type StolpersteinInput = {
  /** The ID of the Spot of the Stolperstein. */
  spotId?: Maybe<Scalars['String']>;
  /** The date when the Stolperstein was laid. */
  laid?: Maybe<Scalars['HistoricalDate']>;
  /** The text inscribed on the Stolperstein (usually multi-line). */
  inscription?: Maybe<Scalars['String']>;
  /** The id of an image object. */
  imageId?: Maybe<Scalars['String']>;
  /** A list of the IDs of the subjects of this Stolperstein. */
  subjectIds?: Maybe<Array<Scalars['String']>>;
  historicalStreet?: Maybe<Scalars['String']>;
  historicalCorner?: Maybe<Scalars['String']>;
  historicalNumber?: Maybe<Scalars['String']>;
  historicalPlaceId?: Maybe<Scalars['String']>;
  /** Information about people/groups that initiated the laying of this Stolperstein. */
  initiativeHTML?: Maybe<Scalars['String']>;
  /** Information about people/groups that helped finance the laying of this Stolperstein. */
  patronageHTML?: Maybe<Scalars['String']>;
  /** Information about people/groups that helped gather information about the subject of the Stolperstein. */
  dataGatheringHTML?: Maybe<Scalars['String']>;
  /** Information about people/groups that funded this Stolperstein. */
  fundingHTML?: Maybe<Scalars['String']>;
};

/** An object that can be the subject of a Stolperstein. */
export type StolpersteinSubject = {
  id: Scalars['ID'];
  created?: Maybe<Scalars['DateTime']>;
  lastModified?: Maybe<Scalars['DateTime']>;
  deleted?: Maybe<Scalars['DateTime']>;
  reviewStatus: ReviewStatus;
  subjectOf: Array<Stolperstein>;
  subjectOfIds: Array<Scalars['String']>;
};

/** Condensed information on a Stolperstein as well as aassociated persons. */
export type StolpersteinSummary = {
  __typename?: 'StolpersteinSummary';
  id: Scalars['String'];
  created?: Maybe<Scalars['DateTime']>;
  lastModified?: Maybe<Scalars['DateTime']>;
  deleted?: Maybe<Scalars['DateTime']>;
  reviewStatus?: Maybe<ReviewStatus>;
  spotId?: Maybe<Scalars['String']>;
  laid?: Maybe<Scalars['String']>;
  hasImage: Scalars['Boolean'];
  personSummaries: Array<PersonSummary>;
};

export type StreetDetail = StreetDetailCorner | StreetDetailNumber;

/** A street detail given as a cornering street. */
export type StreetDetailCorner = TypedLiteral & {
  __typename?: 'StreetDetailCorner';
  value: Scalars['String'];
};

/** A street detail given as a street number. */
export type StreetDetailNumber = TypedLiteral & {
  __typename?: 'StreetDetailNumber';
  value: Scalars['String'];
};

/** A literal value differentiated by type. */
export type TypedLiteral = {
  value: Scalars['String'];
};

export type GeoPointFragment = (
  { __typename?: 'GeoPoint' }
  & Pick<GeoPoint, 'longitude' | 'latitude'>
);

type PersonName_BirthName_Fragment = (
  { __typename: 'BirthName' }
  & Pick<BirthName, 'value'>
);

type PersonName_FamilyName_Fragment = (
  { __typename: 'FamilyName' }
  & Pick<FamilyName, 'value'>
);

type PersonName_GivenName_Fragment = (
  { __typename: 'GivenName' }
  & Pick<GivenName, 'value'>
);

type PersonName_MarriedName_Fragment = (
  { __typename: 'MarriedName' }
  & Pick<MarriedName, 'value'>
);

type PersonName_Nickname_Fragment = (
  { __typename: 'Nickname' }
  & Pick<Nickname, 'value'>
);

type PersonName_Pseudonym_Fragment = (
  { __typename: 'Pseudonym' }
  & Pick<Pseudonym, 'value'>
);

export type PersonNameFragment = PersonName_BirthName_Fragment | PersonName_FamilyName_Fragment | PersonName_GivenName_Fragment | PersonName_MarriedName_Fragment | PersonName_Nickname_Fragment | PersonName_Pseudonym_Fragment;

export type SpotSummaryFragment = (
  { __typename?: 'SpotSummary' }
  & Pick<SpotSummary, 'id' | 'created' | 'lastModified' | 'deleted' | 'reviewStatus' | 'street' | 'streetNumber' | 'streetCorner' | 'postalCode' | 'placeId' | 'imageCount'>
  & { point: (
    { __typename?: 'GeoPoint' }
    & GeoPointFragment
  ), stolpersteinSummaries: Array<(
    { __typename?: 'StolpersteinSummary' }
    & Pick<StolpersteinSummary, 'id' | 'created' | 'lastModified' | 'deleted' | 'reviewStatus' | 'spotId' | 'hasImage' | 'laid'>
    & { personSummaries: Array<(
      { __typename?: 'PersonSummary' }
      & Pick<PersonSummary, 'id' | 'created' | 'lastModified' | 'deleted' | 'reviewStatus' | 'stolpersteinIds' | 'bornDate' | 'diedDate' | 'title' | 'gender' | 'persecutionPretextIds' | 'fate' | 'imageCount'>
      & { names: Array<(
        { __typename?: 'BirthName' }
        & PersonName_BirthName_Fragment
      ) | (
        { __typename?: 'FamilyName' }
        & PersonName_FamilyName_Fragment
      ) | (
        { __typename?: 'GivenName' }
        & PersonName_GivenName_Fragment
      ) | (
        { __typename?: 'MarriedName' }
        & PersonName_MarriedName_Fragment
      ) | (
        { __typename?: 'Nickname' }
        & PersonName_Nickname_Fragment
      ) | (
        { __typename?: 'Pseudonym' }
        & PersonName_Pseudonym_Fragment
      )> }
    )> }
  )> }
);

export type PlaceSummaryFragment = (
  { __typename?: 'PlaceSummary' }
  & Pick<PlaceSummary, 'id' | 'label' | 'parentId' | 'alternativeNames'>
);

export type ImageFragment = (
  { __typename?: 'Image' }
  & Pick<Image, 'id' | 'path' | 'fileName' | 'authorshipRemark' | 'caption' | 'alternativeText'>
);

export type PersonFragment = (
  { __typename?: 'Person' }
  & Pick<Person, 'id' | 'gender' | 'bornDate' | 'diedDate' | 'imageIds' | 'fate' | 'escapedToPlaceId' | 'biographyHTML' | 'editingNotesHTML'>
  & { subjectOf: Array<(
    { __typename?: 'Stolperstein' }
    & Pick<Stolperstein, 'id'>
  )>, names: Array<(
    { __typename?: 'BirthName' }
    & PersonName_BirthName_Fragment
  ) | (
    { __typename?: 'FamilyName' }
    & PersonName_FamilyName_Fragment
  ) | (
    { __typename?: 'GivenName' }
    & PersonName_GivenName_Fragment
  ) | (
    { __typename?: 'MarriedName' }
    & PersonName_MarriedName_Fragment
  ) | (
    { __typename?: 'Nickname' }
    & PersonName_Nickname_Fragment
  ) | (
    { __typename?: 'Pseudonym' }
    & PersonName_Pseudonym_Fragment
  )>, occupations: Array<(
    { __typename?: 'Occupation' }
    & Pick<Occupation, 'id'>
  )>, persecutionPretexts: Array<(
    { __typename?: 'PersecutionPretext' }
    & Pick<PersecutionPretext, 'id'>
  )>, title?: Maybe<(
    { __typename?: 'PersonTitle' }
    & Pick<PersonTitle, 'value'>
  )>, bornPlace?: Maybe<(
    { __typename?: 'Place' }
    & Pick<Place, 'id'>
  )>, diedPlace?: Maybe<(
    { __typename?: 'Place' }
    & Pick<Place, 'id'>
  )> }
  & IContent_Person_Fragment
);

export type StolpersteinFragment = (
  { __typename?: 'Stolperstein' }
  & Pick<Stolperstein, 'id' | 'laid' | 'inscription' | 'historicalStreet' | 'initiativeHTML' | 'patronageHTML' | 'dataGatheringHTML' | 'fundingHTML'>
  & { spot?: Maybe<(
    { __typename?: 'Spot' }
    & Pick<Spot, 'id'>
  )>, image?: Maybe<(
    { __typename?: 'Image' }
    & Pick<Image, 'id'>
  )>, historicalStreetDetail?: Maybe<(
    { __typename: 'StreetDetailCorner' }
    & Pick<StreetDetailCorner, 'value'>
  ) | (
    { __typename: 'StreetDetailNumber' }
    & Pick<StreetDetailNumber, 'value'>
  )>, historicalPlace?: Maybe<(
    { __typename?: 'Place' }
    & Pick<Place, 'id'>
  )>, subjects: Array<(
    { __typename?: 'Person' }
    & Pick<Person, 'id'>
  )> }
  & IContent_Stolperstein_Fragment
);

export type PlaceFragment = (
  { __typename?: 'Place' }
  & Pick<Place, 'id' | 'label' | 'placeType' | 'alternativeNames'>
  & { parent?: Maybe<(
    { __typename?: 'Place' }
    & Pick<Place, 'id'>
  )> }
);

export type SpotFragment = (
  { __typename?: 'Spot' }
  & Pick<Spot, 'id' | 'street' | 'postalCode' | 'placeId' | 'imageIds' | 'stolpersteinIds' | 'note'>
  & { point: (
    { __typename?: 'GeoPoint' }
    & GeoPointFragment
  ), streetDetail?: Maybe<(
    { __typename: 'StreetDetailCorner' }
    & Pick<StreetDetailCorner, 'value'>
  ) | (
    { __typename: 'StreetDetailNumber' }
    & Pick<StreetDetailNumber, 'value'>
  )> }
  & IContent_Spot_Fragment
);

export type PersecutionPretextFragment = (
  { __typename?: 'PersecutionPretext' }
  & Pick<PersecutionPretext, 'id' | 'label' | 'label_f' | 'label_m'>
  & { locale: (
    { __typename?: 'Locale' }
    & Pick<Locale, 'code'>
  ), localizations: Array<(
    { __typename?: 'PersecutionPretextLocalization' }
    & Pick<PersecutionPretextLocalization, 'label' | 'label_f' | 'label_m'>
    & { locale: (
      { __typename?: 'Locale' }
      & Pick<Locale, 'code'>
    ) }
  )> }
);

export type OccupationFragment = (
  { __typename?: 'Occupation' }
  & Pick<Occupation, 'id' | 'label' | 'label_f' | 'label_m'>
  & { locale: (
    { __typename?: 'Locale' }
    & Pick<Locale, 'code'>
  ), localizations: Array<(
    { __typename?: 'OccupationLocalization' }
    & Pick<OccupationLocalization, 'label' | 'label_f' | 'label_m'>
    & { locale: (
      { __typename?: 'Locale' }
      & Pick<Locale, 'code'>
    ) }
  )> }
);

export type RelationshipTypeFragment = (
  { __typename?: 'RelationshipType' }
  & Pick<RelationshipType, 'id' | 'subjectLabel' | 'subjectLabel_f' | 'subjectLabel_m' | 'objectLabel' | 'objectLabel_f' | 'objectLabel_m'>
  & { locale: (
    { __typename?: 'Locale' }
    & Pick<Locale, 'code'>
  ), localizations: Array<(
    { __typename?: 'RelationshipTypeLocalization' }
    & Pick<RelationshipTypeLocalization, 'subjectLabel' | 'subjectLabel_f' | 'subjectLabel_m' | 'objectLabel' | 'objectLabel_f' | 'objectLabel_m'>
    & { locale: (
      { __typename?: 'Locale' }
      & Pick<Locale, 'code'>
    ) }
  )> }
);

export type RelationshipFragment = (
  { __typename?: 'Relationship' }
  & { type: (
    { __typename?: 'RelationshipType' }
    & Pick<RelationshipType, 'id'>
  ), subject: (
    { __typename?: 'Person' }
    & Pick<Person, 'id'>
  ), object: (
    { __typename?: 'Person' }
    & Pick<Person, 'id'>
  ) }
);

export type LocaleFragment = (
  { __typename?: 'Locale' }
  & Pick<Locale, 'code' | 'label'>
);

type INode_Image_Fragment = (
  { __typename?: 'Image' }
  & Pick<Image, 'id'>
);

type INode_Occupation_Fragment = (
  { __typename?: 'Occupation' }
  & Pick<Occupation, 'id'>
);

type INode_PersecutionPretext_Fragment = (
  { __typename?: 'PersecutionPretext' }
  & Pick<PersecutionPretext, 'id'>
);

type INode_Person_Fragment = (
  { __typename?: 'Person' }
  & Pick<Person, 'id'>
);

type INode_Place_Fragment = (
  { __typename?: 'Place' }
  & Pick<Place, 'id'>
);

type INode_Relationship_Fragment = (
  { __typename?: 'Relationship' }
  & Pick<Relationship, 'id'>
);

type INode_RelationshipType_Fragment = (
  { __typename?: 'RelationshipType' }
  & Pick<RelationshipType, 'id'>
);

type INode_Spot_Fragment = (
  { __typename?: 'Spot' }
  & Pick<Spot, 'id'>
);

type INode_Stolperstein_Fragment = (
  { __typename?: 'Stolperstein' }
  & Pick<Stolperstein, 'id'>
);

export type INodeFragment = INode_Image_Fragment | INode_Occupation_Fragment | INode_PersecutionPretext_Fragment | INode_Person_Fragment | INode_Place_Fragment | INode_Relationship_Fragment | INode_RelationshipType_Fragment | INode_Spot_Fragment | INode_Stolperstein_Fragment;

type Node_Image_Fragment = (
  { __typename: 'Image' }
  & INode_Image_Fragment
);

type Node_Occupation_Fragment = (
  { __typename: 'Occupation' }
  & INode_Occupation_Fragment
);

type Node_PersecutionPretext_Fragment = (
  { __typename: 'PersecutionPretext' }
  & INode_PersecutionPretext_Fragment
);

type Node_Person_Fragment = (
  { __typename: 'Person' }
  & INode_Person_Fragment
);

type Node_Place_Fragment = (
  { __typename: 'Place' }
  & INode_Place_Fragment
);

type Node_Relationship_Fragment = (
  { __typename: 'Relationship' }
  & INode_Relationship_Fragment
);

type Node_RelationshipType_Fragment = (
  { __typename: 'RelationshipType' }
  & INode_RelationshipType_Fragment
);

type Node_Spot_Fragment = (
  { __typename: 'Spot' }
  & INode_Spot_Fragment
);

type Node_Stolperstein_Fragment = (
  { __typename: 'Stolperstein' }
  & INode_Stolperstein_Fragment
);

export type NodeFragment = Node_Image_Fragment | Node_Occupation_Fragment | Node_PersecutionPretext_Fragment | Node_Person_Fragment | Node_Place_Fragment | Node_Relationship_Fragment | Node_RelationshipType_Fragment | Node_Spot_Fragment | Node_Stolperstein_Fragment;

type IContent_Person_Fragment = (
  { __typename?: 'Person' }
  & Pick<Person, 'created' | 'lastModified' | 'deleted' | 'reviewStatus'>
);

type IContent_Spot_Fragment = (
  { __typename?: 'Spot' }
  & Pick<Spot, 'created' | 'lastModified' | 'deleted' | 'reviewStatus'>
);

type IContent_Stolperstein_Fragment = (
  { __typename?: 'Stolperstein' }
  & Pick<Stolperstein, 'created' | 'lastModified' | 'deleted' | 'reviewStatus'>
);

export type IContentFragment = IContent_Person_Fragment | IContent_Spot_Fragment | IContent_Stolperstein_Fragment;

type Content_Person_Fragment = (
  { __typename: 'Person' }
  & IContent_Person_Fragment
);

type Content_Spot_Fragment = (
  { __typename: 'Spot' }
  & IContent_Spot_Fragment
);

type Content_Stolperstein_Fragment = (
  { __typename: 'Stolperstein' }
  & IContent_Stolperstein_Fragment
);

export type ContentFragment = Content_Person_Fragment | Content_Spot_Fragment | Content_Stolperstein_Fragment;

export type StolpersteinSubjectFragment = (
  { __typename: 'Person' }
  & { subjectOf: Array<(
    { __typename?: 'Stolperstein' }
    & Pick<Stolperstein, 'id'>
  )> }
);

export type SetStolpersteinReviewStatusMutationVariables = Exact<{
  id: Scalars['String'];
  status: ReviewStatus;
}>;


export type SetStolpersteinReviewStatusMutation = (
  { __typename?: 'Mutation' }
  & { setStolpersteinReviewStatus: (
    { __typename?: 'Stolperstein' }
    & StolpersteinFragment
  ) }
);

export type DeepDeleteSpotMutationVariables = Exact<{
  id: Scalars['String'];
}>;


export type DeepDeleteSpotMutation = (
  { __typename?: 'Mutation' }
  & { deepDeleteSpot: (
    { __typename?: 'Spot' }
    & SpotFragment
  ) }
);

export type DeepDeleteStolpersteinMutationVariables = Exact<{
  id: Scalars['String'];
}>;


export type DeepDeleteStolpersteinMutation = (
  { __typename?: 'Mutation' }
  & { deepDeleteStolperstein: (
    { __typename?: 'Stolperstein' }
    & StolpersteinFragment
  ) }
);

export type SetRelationshipMutationVariables = Exact<{
  typeId: Scalars['String'];
  subjectId: Scalars['String'];
  objectId: Scalars['String'];
}>;


export type SetRelationshipMutation = (
  { __typename?: 'Mutation' }
  & { setRelationship: (
    { __typename?: 'Relationship' }
    & RelationshipFragment
  ) }
);

export type UnsetRelationshipMutationVariables = Exact<{
  oneId: Scalars['String'];
  otherId: Scalars['String'];
}>;


export type UnsetRelationshipMutation = (
  { __typename?: 'Mutation' }
  & { unsetRelationship?: Maybe<(
    { __typename?: 'Relationship' }
    & RelationshipFragment
  )> }
);

export type CreateSpotMutationVariables = Exact<{
  input: SpotInput;
}>;


export type CreateSpotMutation = (
  { __typename?: 'Mutation' }
  & { createSpot: (
    { __typename?: 'Spot' }
    & SpotFragment
  ) }
);

export type UpdateSpotMutationVariables = Exact<{
  id: Scalars['String'];
  input: SpotInput;
}>;


export type UpdateSpotMutation = (
  { __typename?: 'Mutation' }
  & { updateSpot: (
    { __typename?: 'Spot' }
    & SpotFragment
  ) }
);

export type CreateImageMutationVariables = Exact<{
  input: ImageInput;
}>;


export type CreateImageMutation = (
  { __typename?: 'Mutation' }
  & { createImage: (
    { __typename?: 'Image' }
    & ImageFragment
  ) }
);

export type UpdateImageMutationVariables = Exact<{
  id: Scalars['String'];
  input: ImageInput;
}>;


export type UpdateImageMutation = (
  { __typename?: 'Mutation' }
  & { updateImage: (
    { __typename?: 'Image' }
    & ImageFragment
  ) }
);

export type CreateStolpersteinMutationVariables = Exact<{
  input: StolpersteinInput;
}>;


export type CreateStolpersteinMutation = (
  { __typename?: 'Mutation' }
  & { createStolperstein: (
    { __typename?: 'Stolperstein' }
    & StolpersteinFragment
  ) }
);

export type UpdateStolpersteinMutationVariables = Exact<{
  id: Scalars['String'];
  input: StolpersteinInput;
}>;


export type UpdateStolpersteinMutation = (
  { __typename?: 'Mutation' }
  & { updateStolperstein: (
    { __typename?: 'Stolperstein' }
    & StolpersteinFragment
  ) }
);

export type CreatePersonMutationVariables = Exact<{
  input: PersonInput;
}>;


export type CreatePersonMutation = (
  { __typename?: 'Mutation' }
  & { createPerson: (
    { __typename?: 'Person' }
    & PersonFragment
  ) }
);

export type UpdatePersonMutationVariables = Exact<{
  id: Scalars['String'];
  input: PersonInput;
}>;


export type UpdatePersonMutation = (
  { __typename?: 'Mutation' }
  & { updatePerson: (
    { __typename?: 'Person' }
    & PersonFragment
  ) }
);

export type CreateOccupationMutationVariables = Exact<{
  input: OccupationInput;
}>;


export type CreateOccupationMutation = (
  { __typename?: 'Mutation' }
  & { createOccupation: (
    { __typename?: 'Occupation' }
    & OccupationFragment
  ) }
);

export type UpdateOccupationMutationVariables = Exact<{
  id: Scalars['String'];
  input: OccupationInput;
}>;


export type UpdateOccupationMutation = (
  { __typename?: 'Mutation' }
  & { updateOccupation: (
    { __typename?: 'Occupation' }
    & OccupationFragment
  ) }
);

export type CreatePersecutionPretextMutationVariables = Exact<{
  input: PersecutionPretextInput;
}>;


export type CreatePersecutionPretextMutation = (
  { __typename?: 'Mutation' }
  & { createPersecutionPretext: (
    { __typename?: 'PersecutionPretext' }
    & PersecutionPretextFragment
  ) }
);

export type UpdatePersecutionPretextMutationVariables = Exact<{
  id: Scalars['String'];
  input: PersecutionPretextInput;
}>;


export type UpdatePersecutionPretextMutation = (
  { __typename?: 'Mutation' }
  & { updatePersecutionPretext: (
    { __typename?: 'PersecutionPretext' }
    & PersecutionPretextFragment
  ) }
);

export type CreatePlaceMutationVariables = Exact<{
  input: PlaceInput;
}>;


export type CreatePlaceMutation = (
  { __typename?: 'Mutation' }
  & { createPlace: (
    { __typename?: 'Place' }
    & PlaceFragment
  ) }
);

export type UpdatePlaceMutationVariables = Exact<{
  id: Scalars['String'];
  input: PlaceInput;
}>;


export type UpdatePlaceMutation = (
  { __typename?: 'Mutation' }
  & { updatePlace: (
    { __typename?: 'Place' }
    & PlaceFragment
  ) }
);

export type CreateRelationshipTypeMutationVariables = Exact<{
  input: RelationshipTypeInput;
}>;


export type CreateRelationshipTypeMutation = (
  { __typename?: 'Mutation' }
  & { createRelationshipType: (
    { __typename?: 'RelationshipType' }
    & RelationshipTypeFragment
  ) }
);

export type UpdateRelationshipTypeMutationVariables = Exact<{
  id: Scalars['String'];
  input: RelationshipTypeInput;
}>;


export type UpdateRelationshipTypeMutation = (
  { __typename?: 'Mutation' }
  & { updateRelationshipType: (
    { __typename?: 'RelationshipType' }
    & RelationshipTypeFragment
  ) }
);

export type AddLocaleMutationVariables = Exact<{
  code: Scalars['String'];
  label: Scalars['String'];
}>;


export type AddLocaleMutation = (
  { __typename?: 'Mutation' }
  & { addLocale?: Maybe<(
    { __typename?: 'Locale' }
    & LocaleFragment
  )> }
);

export type SummariesQueryVariables = Exact<{ [key: string]: never; }>;


export type SummariesQuery = (
  { __typename?: 'Query' }
  & { spotSummaries: Array<(
    { __typename?: 'SpotSummary' }
    & SpotSummaryFragment
  )>, placeSummaries: Array<(
    { __typename?: 'PlaceSummary' }
    & PlaceSummaryFragment
  )> }
);

export type SpotsQueryVariables = Exact<{
  ids?: Maybe<Array<Scalars['String']>>;
}>;


export type SpotsQuery = (
  { __typename?: 'Query' }
  & { spots: Array<(
    { __typename?: 'Spot' }
    & { place?: Maybe<(
      { __typename?: 'Place' }
      & Pick<Place, 'label'>
    )> }
    & SpotFragment
  )> }
);

export type ImagesQueryVariables = Exact<{
  ids?: Maybe<Array<Scalars['String']>>;
}>;


export type ImagesQuery = (
  { __typename?: 'Query' }
  & { images: Array<(
    { __typename?: 'Image' }
    & ImageFragment
  )> }
);

export type StolpersteineQueryVariables = Exact<{
  ids?: Maybe<Array<Scalars['String']>>;
}>;


export type StolpersteineQuery = (
  { __typename?: 'Query' }
  & { stolpersteine: Array<(
    { __typename?: 'Stolperstein' }
    & StolpersteinFragment
  )> }
);

export type StolpersteinQueryVariables = Exact<{
  id: Scalars['String'];
}>;


export type StolpersteinQuery = (
  { __typename?: 'Query' }
  & { stolperstein?: Maybe<(
    { __typename?: 'Stolperstein' }
    & StolpersteinFragment
  )> }
);

export type PersonsQueryVariables = Exact<{
  ids?: Maybe<Array<Scalars['String']>>;
}>;


export type PersonsQuery = (
  { __typename?: 'Query' }
  & { persons: Array<(
    { __typename?: 'Person' }
    & PersonFragment
  )> }
);

export type OccupationsQueryVariables = Exact<{
  ids?: Maybe<Array<Scalars['String']>>;
}>;


export type OccupationsQuery = (
  { __typename?: 'Query' }
  & { occupations: Array<(
    { __typename?: 'Occupation' }
    & OccupationFragment
  )> }
);

export type PersecutionPretextsQueryVariables = Exact<{
  ids?: Maybe<Array<Scalars['String']>>;
}>;


export type PersecutionPretextsQuery = (
  { __typename?: 'Query' }
  & { persecutionPretexts: Array<(
    { __typename?: 'PersecutionPretext' }
    & PersecutionPretextFragment
  )> }
);

export type PlacesQueryVariables = Exact<{
  ids?: Maybe<Array<Scalars['String']>>;
}>;


export type PlacesQuery = (
  { __typename?: 'Query' }
  & { places: Array<(
    { __typename?: 'Place' }
    & PlaceFragment
  )> }
);

export type RelationshipTypesQueryVariables = Exact<{
  ids?: Maybe<Array<Scalars['String']>>;
}>;


export type RelationshipTypesQuery = (
  { __typename?: 'Query' }
  & { relationshipTypes: Array<(
    { __typename?: 'RelationshipType' }
    & RelationshipTypeFragment
  )> }
);

export type RelationshipsOfQueryVariables = Exact<{
  id: Scalars['String'];
}>;


export type RelationshipsOfQuery = (
  { __typename?: 'Query' }
  & { relationshipsWithSubject: Array<(
    { __typename?: 'Relationship' }
    & RelationshipFragment
  )>, relationshipsWithObject: Array<(
    { __typename?: 'Relationship' }
    & RelationshipFragment
  )> }
);

export type DumpQueryVariables = Exact<{ [key: string]: never; }>;


export type DumpQuery = (
  { __typename?: 'Query' }
  & { locales: Array<(
    { __typename?: 'Locale' }
    & LocaleFragment
  )>, persecutionPretexts: Array<(
    { __typename?: 'PersecutionPretext' }
    & PersecutionPretextFragment
  )>, occupations: Array<(
    { __typename?: 'Occupation' }
    & OccupationFragment
  )>, relationshipTypes: Array<(
    { __typename?: 'RelationshipType' }
    & RelationshipTypeFragment
  )>, places: Array<(
    { __typename?: 'Place' }
    & PlaceFragment
  )>, spots: Array<(
    { __typename?: 'Spot' }
    & SpotFragment
  )>, relationships: Array<(
    { __typename?: 'Relationship' }
    & RelationshipFragment
  )>, persons: Array<(
    { __typename?: 'Person' }
    & PersonFragment
  )>, stolpersteine: Array<(
    { __typename?: 'Stolperstein' }
    & StolpersteinFragment
  )>, nodes: Array<(
    { __typename?: 'Image' }
    & Node_Image_Fragment
  ) | (
    { __typename?: 'Occupation' }
    & Node_Occupation_Fragment
  ) | (
    { __typename?: 'PersecutionPretext' }
    & Node_PersecutionPretext_Fragment
  ) | (
    { __typename?: 'Person' }
    & Node_Person_Fragment
  ) | (
    { __typename?: 'Place' }
    & Node_Place_Fragment
  ) | (
    { __typename?: 'Relationship' }
    & Node_Relationship_Fragment
  ) | (
    { __typename?: 'RelationshipType' }
    & Node_RelationshipType_Fragment
  ) | (
    { __typename?: 'Spot' }
    & Node_Spot_Fragment
  ) | (
    { __typename?: 'Stolperstein' }
    & Node_Stolperstein_Fragment
  )>, contents: Array<(
    { __typename?: 'Person' }
    & Content_Person_Fragment
  ) | (
    { __typename?: 'Spot' }
    & Content_Spot_Fragment
  ) | (
    { __typename?: 'Stolperstein' }
    & Content_Stolperstein_Fragment
  )>, stolpersteinSubjects: Array<(
    { __typename?: 'Person' }
    & StolpersteinSubjectFragment
  )> }
);

export const GeoPointFragmentDoc = gql`
    fragment GeoPoint on GeoPoint {
  longitude
  latitude
}
    `;
export const PersonNameFragmentDoc = gql`
    fragment PersonName on PersonName {
  __typename
  value
}
    `;
export const SpotSummaryFragmentDoc = gql`
    fragment SpotSummary on SpotSummary {
  id
  created
  lastModified
  deleted
  reviewStatus
  street
  streetNumber
  streetCorner
  postalCode
  placeId
  point {
    ...GeoPoint
  }
  imageCount
  stolpersteinSummaries {
    id
    created
    lastModified
    deleted
    reviewStatus
    spotId
    hasImage
    laid
    personSummaries {
      id
      created
      lastModified
      deleted
      reviewStatus
      stolpersteinIds
      bornDate
      diedDate
      title
      gender
      persecutionPretextIds
      fate
      names {
        ...PersonName
      }
      imageCount
    }
  }
}
    ${GeoPointFragmentDoc}
${PersonNameFragmentDoc}`;
export const PlaceSummaryFragmentDoc = gql`
    fragment PlaceSummary on PlaceSummary {
  id
  label
  parentId
  alternativeNames
}
    `;
export const ImageFragmentDoc = gql`
    fragment Image on Image {
  id
  path
  fileName
  authorshipRemark
  caption
  alternativeText
}
    `;
export const IContentFragmentDoc = gql`
    fragment IContent on Content {
  created
  lastModified
  deleted
  reviewStatus
}
    `;
export const PersonFragmentDoc = gql`
    fragment Person on Person {
  id
  subjectOf {
    id
  }
  names {
    ...PersonName
  }
  occupations {
    id
  }
  persecutionPretexts {
    id
  }
  title {
    value
  }
  gender
  bornDate
  bornPlace {
    id
  }
  diedDate
  diedPlace {
    id
  }
  imageIds
  fate
  escapedToPlaceId
  biographyHTML
  editingNotesHTML
  ...IContent
}
    ${PersonNameFragmentDoc}
${IContentFragmentDoc}`;
export const StolpersteinFragmentDoc = gql`
    fragment Stolperstein on Stolperstein {
  id
  spot {
    id
  }
  laid
  inscription
  image {
    id
  }
  historicalStreet
  historicalStreetDetail {
    __typename
    ... on StreetDetailCorner {
      value
    }
    ... on StreetDetailNumber {
      value
    }
  }
  historicalPlace {
    id
  }
  subjects {
    ... on Node {
      id
    }
  }
  initiativeHTML
  patronageHTML
  dataGatheringHTML
  fundingHTML
  ...IContent
}
    ${IContentFragmentDoc}`;
export const PlaceFragmentDoc = gql`
    fragment Place on Place {
  id
  label
  parent {
    id
  }
  placeType
  alternativeNames
}
    `;
export const SpotFragmentDoc = gql`
    fragment Spot on Spot {
  id
  point {
    ...GeoPoint
  }
  street
  streetDetail {
    __typename
    ... on StreetDetailCorner {
      value
    }
    ... on StreetDetailNumber {
      value
    }
  }
  postalCode
  placeId
  imageIds
  stolpersteinIds
  note
  ...IContent
}
    ${GeoPointFragmentDoc}
${IContentFragmentDoc}`;
export const PersecutionPretextFragmentDoc = gql`
    fragment PersecutionPretext on PersecutionPretext {
  id
  locale {
    code
  }
  label
  label_f
  label_m
  localizations {
    locale {
      code
    }
    label
    label_f
    label_m
  }
}
    `;
export const OccupationFragmentDoc = gql`
    fragment Occupation on Occupation {
  id
  locale {
    code
  }
  label
  label_f
  label_m
  localizations {
    locale {
      code
    }
    label
    label_f
    label_m
  }
}
    `;
export const RelationshipTypeFragmentDoc = gql`
    fragment RelationshipType on RelationshipType {
  id
  locale {
    code
  }
  subjectLabel
  subjectLabel_f
  subjectLabel_m
  objectLabel
  objectLabel_f
  objectLabel_m
  localizations {
    locale {
      code
    }
    subjectLabel
    subjectLabel_f
    subjectLabel_m
    objectLabel
    objectLabel_f
    objectLabel_m
  }
}
    `;
export const RelationshipFragmentDoc = gql`
    fragment Relationship on Relationship {
  type {
    id
  }
  subject {
    ... on Node {
      id
    }
  }
  object {
    ... on Node {
      id
    }
  }
}
    `;
export const LocaleFragmentDoc = gql`
    fragment Locale on Locale {
  code
  label
}
    `;
export const INodeFragmentDoc = gql`
    fragment INode on Node {
  id
}
    `;
export const NodeFragmentDoc = gql`
    fragment Node on Node {
  __typename
  ...INode
}
    ${INodeFragmentDoc}`;
export const ContentFragmentDoc = gql`
    fragment Content on Content {
  __typename
  ...IContent
}
    ${IContentFragmentDoc}`;
export const StolpersteinSubjectFragmentDoc = gql`
    fragment StolpersteinSubject on StolpersteinSubject {
  __typename
  subjectOf {
    id
  }
}
    `;
export const SetStolpersteinReviewStatusDocument = gql`
    mutation setStolpersteinReviewStatus($id: String!, $status: ReviewStatus!) {
  setStolpersteinReviewStatus(id: $id, status: $status) {
    ...Stolperstein
  }
}
    ${StolpersteinFragmentDoc}`;
export type SetStolpersteinReviewStatusMutationFn = Apollo.MutationFunction<SetStolpersteinReviewStatusMutation, SetStolpersteinReviewStatusMutationVariables>;

/**
 * __useSetStolpersteinReviewStatusMutation__
 *
 * To run a mutation, you first call `useSetStolpersteinReviewStatusMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useSetStolpersteinReviewStatusMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [setStolpersteinReviewStatusMutation, { data, loading, error }] = useSetStolpersteinReviewStatusMutation({
 *   variables: {
 *      id: // value for 'id'
 *      status: // value for 'status'
 *   },
 * });
 */
export function useSetStolpersteinReviewStatusMutation(baseOptions?: Apollo.MutationHookOptions<SetStolpersteinReviewStatusMutation, SetStolpersteinReviewStatusMutationVariables>) {
        return Apollo.useMutation<SetStolpersteinReviewStatusMutation, SetStolpersteinReviewStatusMutationVariables>(SetStolpersteinReviewStatusDocument, baseOptions);
      }
export type SetStolpersteinReviewStatusMutationHookResult = ReturnType<typeof useSetStolpersteinReviewStatusMutation>;
export type SetStolpersteinReviewStatusMutationResult = Apollo.MutationResult<SetStolpersteinReviewStatusMutation>;
export type SetStolpersteinReviewStatusMutationOptions = Apollo.BaseMutationOptions<SetStolpersteinReviewStatusMutation, SetStolpersteinReviewStatusMutationVariables>;
export const DeepDeleteSpotDocument = gql`
    mutation deepDeleteSpot($id: String!) {
  deepDeleteSpot(id: $id) {
    ...Spot
  }
}
    ${SpotFragmentDoc}`;
export type DeepDeleteSpotMutationFn = Apollo.MutationFunction<DeepDeleteSpotMutation, DeepDeleteSpotMutationVariables>;

/**
 * __useDeepDeleteSpotMutation__
 *
 * To run a mutation, you first call `useDeepDeleteSpotMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useDeepDeleteSpotMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [deepDeleteSpotMutation, { data, loading, error }] = useDeepDeleteSpotMutation({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useDeepDeleteSpotMutation(baseOptions?: Apollo.MutationHookOptions<DeepDeleteSpotMutation, DeepDeleteSpotMutationVariables>) {
        return Apollo.useMutation<DeepDeleteSpotMutation, DeepDeleteSpotMutationVariables>(DeepDeleteSpotDocument, baseOptions);
      }
export type DeepDeleteSpotMutationHookResult = ReturnType<typeof useDeepDeleteSpotMutation>;
export type DeepDeleteSpotMutationResult = Apollo.MutationResult<DeepDeleteSpotMutation>;
export type DeepDeleteSpotMutationOptions = Apollo.BaseMutationOptions<DeepDeleteSpotMutation, DeepDeleteSpotMutationVariables>;
export const DeepDeleteStolpersteinDocument = gql`
    mutation deepDeleteStolperstein($id: String!) {
  deepDeleteStolperstein(id: $id) {
    ...Stolperstein
  }
}
    ${StolpersteinFragmentDoc}`;
export type DeepDeleteStolpersteinMutationFn = Apollo.MutationFunction<DeepDeleteStolpersteinMutation, DeepDeleteStolpersteinMutationVariables>;

/**
 * __useDeepDeleteStolpersteinMutation__
 *
 * To run a mutation, you first call `useDeepDeleteStolpersteinMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useDeepDeleteStolpersteinMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [deepDeleteStolpersteinMutation, { data, loading, error }] = useDeepDeleteStolpersteinMutation({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useDeepDeleteStolpersteinMutation(baseOptions?: Apollo.MutationHookOptions<DeepDeleteStolpersteinMutation, DeepDeleteStolpersteinMutationVariables>) {
        return Apollo.useMutation<DeepDeleteStolpersteinMutation, DeepDeleteStolpersteinMutationVariables>(DeepDeleteStolpersteinDocument, baseOptions);
      }
export type DeepDeleteStolpersteinMutationHookResult = ReturnType<typeof useDeepDeleteStolpersteinMutation>;
export type DeepDeleteStolpersteinMutationResult = Apollo.MutationResult<DeepDeleteStolpersteinMutation>;
export type DeepDeleteStolpersteinMutationOptions = Apollo.BaseMutationOptions<DeepDeleteStolpersteinMutation, DeepDeleteStolpersteinMutationVariables>;
export const SetRelationshipDocument = gql`
    mutation setRelationship($typeId: String!, $subjectId: String!, $objectId: String!) {
  setRelationship(typeId: $typeId, subjectId: $subjectId, objectId: $objectId) {
    ...Relationship
  }
}
    ${RelationshipFragmentDoc}`;
export type SetRelationshipMutationFn = Apollo.MutationFunction<SetRelationshipMutation, SetRelationshipMutationVariables>;

/**
 * __useSetRelationshipMutation__
 *
 * To run a mutation, you first call `useSetRelationshipMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useSetRelationshipMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [setRelationshipMutation, { data, loading, error }] = useSetRelationshipMutation({
 *   variables: {
 *      typeId: // value for 'typeId'
 *      subjectId: // value for 'subjectId'
 *      objectId: // value for 'objectId'
 *   },
 * });
 */
export function useSetRelationshipMutation(baseOptions?: Apollo.MutationHookOptions<SetRelationshipMutation, SetRelationshipMutationVariables>) {
        return Apollo.useMutation<SetRelationshipMutation, SetRelationshipMutationVariables>(SetRelationshipDocument, baseOptions);
      }
export type SetRelationshipMutationHookResult = ReturnType<typeof useSetRelationshipMutation>;
export type SetRelationshipMutationResult = Apollo.MutationResult<SetRelationshipMutation>;
export type SetRelationshipMutationOptions = Apollo.BaseMutationOptions<SetRelationshipMutation, SetRelationshipMutationVariables>;
export const UnsetRelationshipDocument = gql`
    mutation unsetRelationship($oneId: String!, $otherId: String!) {
  unsetRelationship(oneId: $oneId, otherId: $otherId) {
    ...Relationship
  }
}
    ${RelationshipFragmentDoc}`;
export type UnsetRelationshipMutationFn = Apollo.MutationFunction<UnsetRelationshipMutation, UnsetRelationshipMutationVariables>;

/**
 * __useUnsetRelationshipMutation__
 *
 * To run a mutation, you first call `useUnsetRelationshipMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUnsetRelationshipMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [unsetRelationshipMutation, { data, loading, error }] = useUnsetRelationshipMutation({
 *   variables: {
 *      oneId: // value for 'oneId'
 *      otherId: // value for 'otherId'
 *   },
 * });
 */
export function useUnsetRelationshipMutation(baseOptions?: Apollo.MutationHookOptions<UnsetRelationshipMutation, UnsetRelationshipMutationVariables>) {
        return Apollo.useMutation<UnsetRelationshipMutation, UnsetRelationshipMutationVariables>(UnsetRelationshipDocument, baseOptions);
      }
export type UnsetRelationshipMutationHookResult = ReturnType<typeof useUnsetRelationshipMutation>;
export type UnsetRelationshipMutationResult = Apollo.MutationResult<UnsetRelationshipMutation>;
export type UnsetRelationshipMutationOptions = Apollo.BaseMutationOptions<UnsetRelationshipMutation, UnsetRelationshipMutationVariables>;
export const CreateSpotDocument = gql`
    mutation createSpot($input: SpotInput!) {
  createSpot(input: $input) {
    ...Spot
  }
}
    ${SpotFragmentDoc}`;
export type CreateSpotMutationFn = Apollo.MutationFunction<CreateSpotMutation, CreateSpotMutationVariables>;

/**
 * __useCreateSpotMutation__
 *
 * To run a mutation, you first call `useCreateSpotMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateSpotMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createSpotMutation, { data, loading, error }] = useCreateSpotMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useCreateSpotMutation(baseOptions?: Apollo.MutationHookOptions<CreateSpotMutation, CreateSpotMutationVariables>) {
        return Apollo.useMutation<CreateSpotMutation, CreateSpotMutationVariables>(CreateSpotDocument, baseOptions);
      }
export type CreateSpotMutationHookResult = ReturnType<typeof useCreateSpotMutation>;
export type CreateSpotMutationResult = Apollo.MutationResult<CreateSpotMutation>;
export type CreateSpotMutationOptions = Apollo.BaseMutationOptions<CreateSpotMutation, CreateSpotMutationVariables>;
export const UpdateSpotDocument = gql`
    mutation updateSpot($id: String!, $input: SpotInput!) {
  updateSpot(id: $id, input: $input) {
    ...Spot
  }
}
    ${SpotFragmentDoc}`;
export type UpdateSpotMutationFn = Apollo.MutationFunction<UpdateSpotMutation, UpdateSpotMutationVariables>;

/**
 * __useUpdateSpotMutation__
 *
 * To run a mutation, you first call `useUpdateSpotMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdateSpotMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updateSpotMutation, { data, loading, error }] = useUpdateSpotMutation({
 *   variables: {
 *      id: // value for 'id'
 *      input: // value for 'input'
 *   },
 * });
 */
export function useUpdateSpotMutation(baseOptions?: Apollo.MutationHookOptions<UpdateSpotMutation, UpdateSpotMutationVariables>) {
        return Apollo.useMutation<UpdateSpotMutation, UpdateSpotMutationVariables>(UpdateSpotDocument, baseOptions);
      }
export type UpdateSpotMutationHookResult = ReturnType<typeof useUpdateSpotMutation>;
export type UpdateSpotMutationResult = Apollo.MutationResult<UpdateSpotMutation>;
export type UpdateSpotMutationOptions = Apollo.BaseMutationOptions<UpdateSpotMutation, UpdateSpotMutationVariables>;
export const CreateImageDocument = gql`
    mutation createImage($input: ImageInput!) {
  createImage(input: $input) {
    ...Image
  }
}
    ${ImageFragmentDoc}`;
export type CreateImageMutationFn = Apollo.MutationFunction<CreateImageMutation, CreateImageMutationVariables>;

/**
 * __useCreateImageMutation__
 *
 * To run a mutation, you first call `useCreateImageMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateImageMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createImageMutation, { data, loading, error }] = useCreateImageMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useCreateImageMutation(baseOptions?: Apollo.MutationHookOptions<CreateImageMutation, CreateImageMutationVariables>) {
        return Apollo.useMutation<CreateImageMutation, CreateImageMutationVariables>(CreateImageDocument, baseOptions);
      }
export type CreateImageMutationHookResult = ReturnType<typeof useCreateImageMutation>;
export type CreateImageMutationResult = Apollo.MutationResult<CreateImageMutation>;
export type CreateImageMutationOptions = Apollo.BaseMutationOptions<CreateImageMutation, CreateImageMutationVariables>;
export const UpdateImageDocument = gql`
    mutation updateImage($id: String!, $input: ImageInput!) {
  updateImage(id: $id, input: $input) {
    ...Image
  }
}
    ${ImageFragmentDoc}`;
export type UpdateImageMutationFn = Apollo.MutationFunction<UpdateImageMutation, UpdateImageMutationVariables>;

/**
 * __useUpdateImageMutation__
 *
 * To run a mutation, you first call `useUpdateImageMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdateImageMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updateImageMutation, { data, loading, error }] = useUpdateImageMutation({
 *   variables: {
 *      id: // value for 'id'
 *      input: // value for 'input'
 *   },
 * });
 */
export function useUpdateImageMutation(baseOptions?: Apollo.MutationHookOptions<UpdateImageMutation, UpdateImageMutationVariables>) {
        return Apollo.useMutation<UpdateImageMutation, UpdateImageMutationVariables>(UpdateImageDocument, baseOptions);
      }
export type UpdateImageMutationHookResult = ReturnType<typeof useUpdateImageMutation>;
export type UpdateImageMutationResult = Apollo.MutationResult<UpdateImageMutation>;
export type UpdateImageMutationOptions = Apollo.BaseMutationOptions<UpdateImageMutation, UpdateImageMutationVariables>;
export const CreateStolpersteinDocument = gql`
    mutation createStolperstein($input: StolpersteinInput!) {
  createStolperstein(input: $input) {
    ...Stolperstein
  }
}
    ${StolpersteinFragmentDoc}`;
export type CreateStolpersteinMutationFn = Apollo.MutationFunction<CreateStolpersteinMutation, CreateStolpersteinMutationVariables>;

/**
 * __useCreateStolpersteinMutation__
 *
 * To run a mutation, you first call `useCreateStolpersteinMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateStolpersteinMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createStolpersteinMutation, { data, loading, error }] = useCreateStolpersteinMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useCreateStolpersteinMutation(baseOptions?: Apollo.MutationHookOptions<CreateStolpersteinMutation, CreateStolpersteinMutationVariables>) {
        return Apollo.useMutation<CreateStolpersteinMutation, CreateStolpersteinMutationVariables>(CreateStolpersteinDocument, baseOptions);
      }
export type CreateStolpersteinMutationHookResult = ReturnType<typeof useCreateStolpersteinMutation>;
export type CreateStolpersteinMutationResult = Apollo.MutationResult<CreateStolpersteinMutation>;
export type CreateStolpersteinMutationOptions = Apollo.BaseMutationOptions<CreateStolpersteinMutation, CreateStolpersteinMutationVariables>;
export const UpdateStolpersteinDocument = gql`
    mutation updateStolperstein($id: String!, $input: StolpersteinInput!) {
  updateStolperstein(id: $id, input: $input) {
    ...Stolperstein
  }
}
    ${StolpersteinFragmentDoc}`;
export type UpdateStolpersteinMutationFn = Apollo.MutationFunction<UpdateStolpersteinMutation, UpdateStolpersteinMutationVariables>;

/**
 * __useUpdateStolpersteinMutation__
 *
 * To run a mutation, you first call `useUpdateStolpersteinMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdateStolpersteinMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updateStolpersteinMutation, { data, loading, error }] = useUpdateStolpersteinMutation({
 *   variables: {
 *      id: // value for 'id'
 *      input: // value for 'input'
 *   },
 * });
 */
export function useUpdateStolpersteinMutation(baseOptions?: Apollo.MutationHookOptions<UpdateStolpersteinMutation, UpdateStolpersteinMutationVariables>) {
        return Apollo.useMutation<UpdateStolpersteinMutation, UpdateStolpersteinMutationVariables>(UpdateStolpersteinDocument, baseOptions);
      }
export type UpdateStolpersteinMutationHookResult = ReturnType<typeof useUpdateStolpersteinMutation>;
export type UpdateStolpersteinMutationResult = Apollo.MutationResult<UpdateStolpersteinMutation>;
export type UpdateStolpersteinMutationOptions = Apollo.BaseMutationOptions<UpdateStolpersteinMutation, UpdateStolpersteinMutationVariables>;
export const CreatePersonDocument = gql`
    mutation createPerson($input: PersonInput!) {
  createPerson(input: $input) {
    ...Person
  }
}
    ${PersonFragmentDoc}`;
export type CreatePersonMutationFn = Apollo.MutationFunction<CreatePersonMutation, CreatePersonMutationVariables>;

/**
 * __useCreatePersonMutation__
 *
 * To run a mutation, you first call `useCreatePersonMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreatePersonMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createPersonMutation, { data, loading, error }] = useCreatePersonMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useCreatePersonMutation(baseOptions?: Apollo.MutationHookOptions<CreatePersonMutation, CreatePersonMutationVariables>) {
        return Apollo.useMutation<CreatePersonMutation, CreatePersonMutationVariables>(CreatePersonDocument, baseOptions);
      }
export type CreatePersonMutationHookResult = ReturnType<typeof useCreatePersonMutation>;
export type CreatePersonMutationResult = Apollo.MutationResult<CreatePersonMutation>;
export type CreatePersonMutationOptions = Apollo.BaseMutationOptions<CreatePersonMutation, CreatePersonMutationVariables>;
export const UpdatePersonDocument = gql`
    mutation updatePerson($id: String!, $input: PersonInput!) {
  updatePerson(id: $id, input: $input) {
    ...Person
  }
}
    ${PersonFragmentDoc}`;
export type UpdatePersonMutationFn = Apollo.MutationFunction<UpdatePersonMutation, UpdatePersonMutationVariables>;

/**
 * __useUpdatePersonMutation__
 *
 * To run a mutation, you first call `useUpdatePersonMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdatePersonMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updatePersonMutation, { data, loading, error }] = useUpdatePersonMutation({
 *   variables: {
 *      id: // value for 'id'
 *      input: // value for 'input'
 *   },
 * });
 */
export function useUpdatePersonMutation(baseOptions?: Apollo.MutationHookOptions<UpdatePersonMutation, UpdatePersonMutationVariables>) {
        return Apollo.useMutation<UpdatePersonMutation, UpdatePersonMutationVariables>(UpdatePersonDocument, baseOptions);
      }
export type UpdatePersonMutationHookResult = ReturnType<typeof useUpdatePersonMutation>;
export type UpdatePersonMutationResult = Apollo.MutationResult<UpdatePersonMutation>;
export type UpdatePersonMutationOptions = Apollo.BaseMutationOptions<UpdatePersonMutation, UpdatePersonMutationVariables>;
export const CreateOccupationDocument = gql`
    mutation createOccupation($input: OccupationInput!) {
  createOccupation(input: $input) {
    ...Occupation
  }
}
    ${OccupationFragmentDoc}`;
export type CreateOccupationMutationFn = Apollo.MutationFunction<CreateOccupationMutation, CreateOccupationMutationVariables>;

/**
 * __useCreateOccupationMutation__
 *
 * To run a mutation, you first call `useCreateOccupationMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateOccupationMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createOccupationMutation, { data, loading, error }] = useCreateOccupationMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useCreateOccupationMutation(baseOptions?: Apollo.MutationHookOptions<CreateOccupationMutation, CreateOccupationMutationVariables>) {
        return Apollo.useMutation<CreateOccupationMutation, CreateOccupationMutationVariables>(CreateOccupationDocument, baseOptions);
      }
export type CreateOccupationMutationHookResult = ReturnType<typeof useCreateOccupationMutation>;
export type CreateOccupationMutationResult = Apollo.MutationResult<CreateOccupationMutation>;
export type CreateOccupationMutationOptions = Apollo.BaseMutationOptions<CreateOccupationMutation, CreateOccupationMutationVariables>;
export const UpdateOccupationDocument = gql`
    mutation updateOccupation($id: String!, $input: OccupationInput!) {
  updateOccupation(id: $id, input: $input) {
    ...Occupation
  }
}
    ${OccupationFragmentDoc}`;
export type UpdateOccupationMutationFn = Apollo.MutationFunction<UpdateOccupationMutation, UpdateOccupationMutationVariables>;

/**
 * __useUpdateOccupationMutation__
 *
 * To run a mutation, you first call `useUpdateOccupationMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdateOccupationMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updateOccupationMutation, { data, loading, error }] = useUpdateOccupationMutation({
 *   variables: {
 *      id: // value for 'id'
 *      input: // value for 'input'
 *   },
 * });
 */
export function useUpdateOccupationMutation(baseOptions?: Apollo.MutationHookOptions<UpdateOccupationMutation, UpdateOccupationMutationVariables>) {
        return Apollo.useMutation<UpdateOccupationMutation, UpdateOccupationMutationVariables>(UpdateOccupationDocument, baseOptions);
      }
export type UpdateOccupationMutationHookResult = ReturnType<typeof useUpdateOccupationMutation>;
export type UpdateOccupationMutationResult = Apollo.MutationResult<UpdateOccupationMutation>;
export type UpdateOccupationMutationOptions = Apollo.BaseMutationOptions<UpdateOccupationMutation, UpdateOccupationMutationVariables>;
export const CreatePersecutionPretextDocument = gql`
    mutation createPersecutionPretext($input: PersecutionPretextInput!) {
  createPersecutionPretext(input: $input) {
    ...PersecutionPretext
  }
}
    ${PersecutionPretextFragmentDoc}`;
export type CreatePersecutionPretextMutationFn = Apollo.MutationFunction<CreatePersecutionPretextMutation, CreatePersecutionPretextMutationVariables>;

/**
 * __useCreatePersecutionPretextMutation__
 *
 * To run a mutation, you first call `useCreatePersecutionPretextMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreatePersecutionPretextMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createPersecutionPretextMutation, { data, loading, error }] = useCreatePersecutionPretextMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useCreatePersecutionPretextMutation(baseOptions?: Apollo.MutationHookOptions<CreatePersecutionPretextMutation, CreatePersecutionPretextMutationVariables>) {
        return Apollo.useMutation<CreatePersecutionPretextMutation, CreatePersecutionPretextMutationVariables>(CreatePersecutionPretextDocument, baseOptions);
      }
export type CreatePersecutionPretextMutationHookResult = ReturnType<typeof useCreatePersecutionPretextMutation>;
export type CreatePersecutionPretextMutationResult = Apollo.MutationResult<CreatePersecutionPretextMutation>;
export type CreatePersecutionPretextMutationOptions = Apollo.BaseMutationOptions<CreatePersecutionPretextMutation, CreatePersecutionPretextMutationVariables>;
export const UpdatePersecutionPretextDocument = gql`
    mutation updatePersecutionPretext($id: String!, $input: PersecutionPretextInput!) {
  updatePersecutionPretext(id: $id, input: $input) {
    ...PersecutionPretext
  }
}
    ${PersecutionPretextFragmentDoc}`;
export type UpdatePersecutionPretextMutationFn = Apollo.MutationFunction<UpdatePersecutionPretextMutation, UpdatePersecutionPretextMutationVariables>;

/**
 * __useUpdatePersecutionPretextMutation__
 *
 * To run a mutation, you first call `useUpdatePersecutionPretextMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdatePersecutionPretextMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updatePersecutionPretextMutation, { data, loading, error }] = useUpdatePersecutionPretextMutation({
 *   variables: {
 *      id: // value for 'id'
 *      input: // value for 'input'
 *   },
 * });
 */
export function useUpdatePersecutionPretextMutation(baseOptions?: Apollo.MutationHookOptions<UpdatePersecutionPretextMutation, UpdatePersecutionPretextMutationVariables>) {
        return Apollo.useMutation<UpdatePersecutionPretextMutation, UpdatePersecutionPretextMutationVariables>(UpdatePersecutionPretextDocument, baseOptions);
      }
export type UpdatePersecutionPretextMutationHookResult = ReturnType<typeof useUpdatePersecutionPretextMutation>;
export type UpdatePersecutionPretextMutationResult = Apollo.MutationResult<UpdatePersecutionPretextMutation>;
export type UpdatePersecutionPretextMutationOptions = Apollo.BaseMutationOptions<UpdatePersecutionPretextMutation, UpdatePersecutionPretextMutationVariables>;
export const CreatePlaceDocument = gql`
    mutation createPlace($input: PlaceInput!) {
  createPlace(input: $input) {
    ...Place
  }
}
    ${PlaceFragmentDoc}`;
export type CreatePlaceMutationFn = Apollo.MutationFunction<CreatePlaceMutation, CreatePlaceMutationVariables>;

/**
 * __useCreatePlaceMutation__
 *
 * To run a mutation, you first call `useCreatePlaceMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreatePlaceMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createPlaceMutation, { data, loading, error }] = useCreatePlaceMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useCreatePlaceMutation(baseOptions?: Apollo.MutationHookOptions<CreatePlaceMutation, CreatePlaceMutationVariables>) {
        return Apollo.useMutation<CreatePlaceMutation, CreatePlaceMutationVariables>(CreatePlaceDocument, baseOptions);
      }
export type CreatePlaceMutationHookResult = ReturnType<typeof useCreatePlaceMutation>;
export type CreatePlaceMutationResult = Apollo.MutationResult<CreatePlaceMutation>;
export type CreatePlaceMutationOptions = Apollo.BaseMutationOptions<CreatePlaceMutation, CreatePlaceMutationVariables>;
export const UpdatePlaceDocument = gql`
    mutation updatePlace($id: String!, $input: PlaceInput!) {
  updatePlace(id: $id, input: $input) {
    ...Place
  }
}
    ${PlaceFragmentDoc}`;
export type UpdatePlaceMutationFn = Apollo.MutationFunction<UpdatePlaceMutation, UpdatePlaceMutationVariables>;

/**
 * __useUpdatePlaceMutation__
 *
 * To run a mutation, you first call `useUpdatePlaceMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdatePlaceMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updatePlaceMutation, { data, loading, error }] = useUpdatePlaceMutation({
 *   variables: {
 *      id: // value for 'id'
 *      input: // value for 'input'
 *   },
 * });
 */
export function useUpdatePlaceMutation(baseOptions?: Apollo.MutationHookOptions<UpdatePlaceMutation, UpdatePlaceMutationVariables>) {
        return Apollo.useMutation<UpdatePlaceMutation, UpdatePlaceMutationVariables>(UpdatePlaceDocument, baseOptions);
      }
export type UpdatePlaceMutationHookResult = ReturnType<typeof useUpdatePlaceMutation>;
export type UpdatePlaceMutationResult = Apollo.MutationResult<UpdatePlaceMutation>;
export type UpdatePlaceMutationOptions = Apollo.BaseMutationOptions<UpdatePlaceMutation, UpdatePlaceMutationVariables>;
export const CreateRelationshipTypeDocument = gql`
    mutation createRelationshipType($input: RelationshipTypeInput!) {
  createRelationshipType(input: $input) {
    ...RelationshipType
  }
}
    ${RelationshipTypeFragmentDoc}`;
export type CreateRelationshipTypeMutationFn = Apollo.MutationFunction<CreateRelationshipTypeMutation, CreateRelationshipTypeMutationVariables>;

/**
 * __useCreateRelationshipTypeMutation__
 *
 * To run a mutation, you first call `useCreateRelationshipTypeMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateRelationshipTypeMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createRelationshipTypeMutation, { data, loading, error }] = useCreateRelationshipTypeMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useCreateRelationshipTypeMutation(baseOptions?: Apollo.MutationHookOptions<CreateRelationshipTypeMutation, CreateRelationshipTypeMutationVariables>) {
        return Apollo.useMutation<CreateRelationshipTypeMutation, CreateRelationshipTypeMutationVariables>(CreateRelationshipTypeDocument, baseOptions);
      }
export type CreateRelationshipTypeMutationHookResult = ReturnType<typeof useCreateRelationshipTypeMutation>;
export type CreateRelationshipTypeMutationResult = Apollo.MutationResult<CreateRelationshipTypeMutation>;
export type CreateRelationshipTypeMutationOptions = Apollo.BaseMutationOptions<CreateRelationshipTypeMutation, CreateRelationshipTypeMutationVariables>;
export const UpdateRelationshipTypeDocument = gql`
    mutation updateRelationshipType($id: String!, $input: RelationshipTypeInput!) {
  updateRelationshipType(id: $id, input: $input) {
    ...RelationshipType
  }
}
    ${RelationshipTypeFragmentDoc}`;
export type UpdateRelationshipTypeMutationFn = Apollo.MutationFunction<UpdateRelationshipTypeMutation, UpdateRelationshipTypeMutationVariables>;

/**
 * __useUpdateRelationshipTypeMutation__
 *
 * To run a mutation, you first call `useUpdateRelationshipTypeMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdateRelationshipTypeMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updateRelationshipTypeMutation, { data, loading, error }] = useUpdateRelationshipTypeMutation({
 *   variables: {
 *      id: // value for 'id'
 *      input: // value for 'input'
 *   },
 * });
 */
export function useUpdateRelationshipTypeMutation(baseOptions?: Apollo.MutationHookOptions<UpdateRelationshipTypeMutation, UpdateRelationshipTypeMutationVariables>) {
        return Apollo.useMutation<UpdateRelationshipTypeMutation, UpdateRelationshipTypeMutationVariables>(UpdateRelationshipTypeDocument, baseOptions);
      }
export type UpdateRelationshipTypeMutationHookResult = ReturnType<typeof useUpdateRelationshipTypeMutation>;
export type UpdateRelationshipTypeMutationResult = Apollo.MutationResult<UpdateRelationshipTypeMutation>;
export type UpdateRelationshipTypeMutationOptions = Apollo.BaseMutationOptions<UpdateRelationshipTypeMutation, UpdateRelationshipTypeMutationVariables>;
export const AddLocaleDocument = gql`
    mutation addLocale($code: String!, $label: String!) {
  addLocale(code: $code, label: $label) {
    ...Locale
  }
}
    ${LocaleFragmentDoc}`;
export type AddLocaleMutationFn = Apollo.MutationFunction<AddLocaleMutation, AddLocaleMutationVariables>;

/**
 * __useAddLocaleMutation__
 *
 * To run a mutation, you first call `useAddLocaleMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useAddLocaleMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [addLocaleMutation, { data, loading, error }] = useAddLocaleMutation({
 *   variables: {
 *      code: // value for 'code'
 *      label: // value for 'label'
 *   },
 * });
 */
export function useAddLocaleMutation(baseOptions?: Apollo.MutationHookOptions<AddLocaleMutation, AddLocaleMutationVariables>) {
        return Apollo.useMutation<AddLocaleMutation, AddLocaleMutationVariables>(AddLocaleDocument, baseOptions);
      }
export type AddLocaleMutationHookResult = ReturnType<typeof useAddLocaleMutation>;
export type AddLocaleMutationResult = Apollo.MutationResult<AddLocaleMutation>;
export type AddLocaleMutationOptions = Apollo.BaseMutationOptions<AddLocaleMutation, AddLocaleMutationVariables>;
export const SummariesDocument = gql`
    query Summaries {
  spotSummaries {
    ...SpotSummary
  }
  placeSummaries {
    ...PlaceSummary
  }
}
    ${SpotSummaryFragmentDoc}
${PlaceSummaryFragmentDoc}`;

/**
 * __useSummariesQuery__
 *
 * To run a query within a React component, call `useSummariesQuery` and pass it any options that fit your needs.
 * When your component renders, `useSummariesQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useSummariesQuery({
 *   variables: {
 *   },
 * });
 */
export function useSummariesQuery(baseOptions?: Apollo.QueryHookOptions<SummariesQuery, SummariesQueryVariables>) {
        return Apollo.useQuery<SummariesQuery, SummariesQueryVariables>(SummariesDocument, baseOptions);
      }
export function useSummariesLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<SummariesQuery, SummariesQueryVariables>) {
          return Apollo.useLazyQuery<SummariesQuery, SummariesQueryVariables>(SummariesDocument, baseOptions);
        }
export type SummariesQueryHookResult = ReturnType<typeof useSummariesQuery>;
export type SummariesLazyQueryHookResult = ReturnType<typeof useSummariesLazyQuery>;
export type SummariesQueryResult = Apollo.QueryResult<SummariesQuery, SummariesQueryVariables>;
export const SpotsDocument = gql`
    query Spots($ids: [String!]) {
  spots(ids: $ids) {
    ...Spot
    place {
      label
    }
  }
}
    ${SpotFragmentDoc}`;

/**
 * __useSpotsQuery__
 *
 * To run a query within a React component, call `useSpotsQuery` and pass it any options that fit your needs.
 * When your component renders, `useSpotsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useSpotsQuery({
 *   variables: {
 *      ids: // value for 'ids'
 *   },
 * });
 */
export function useSpotsQuery(baseOptions?: Apollo.QueryHookOptions<SpotsQuery, SpotsQueryVariables>) {
        return Apollo.useQuery<SpotsQuery, SpotsQueryVariables>(SpotsDocument, baseOptions);
      }
export function useSpotsLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<SpotsQuery, SpotsQueryVariables>) {
          return Apollo.useLazyQuery<SpotsQuery, SpotsQueryVariables>(SpotsDocument, baseOptions);
        }
export type SpotsQueryHookResult = ReturnType<typeof useSpotsQuery>;
export type SpotsLazyQueryHookResult = ReturnType<typeof useSpotsLazyQuery>;
export type SpotsQueryResult = Apollo.QueryResult<SpotsQuery, SpotsQueryVariables>;
export const ImagesDocument = gql`
    query Images($ids: [String!]) {
  images(ids: $ids) {
    ...Image
  }
}
    ${ImageFragmentDoc}`;

/**
 * __useImagesQuery__
 *
 * To run a query within a React component, call `useImagesQuery` and pass it any options that fit your needs.
 * When your component renders, `useImagesQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useImagesQuery({
 *   variables: {
 *      ids: // value for 'ids'
 *   },
 * });
 */
export function useImagesQuery(baseOptions?: Apollo.QueryHookOptions<ImagesQuery, ImagesQueryVariables>) {
        return Apollo.useQuery<ImagesQuery, ImagesQueryVariables>(ImagesDocument, baseOptions);
      }
export function useImagesLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<ImagesQuery, ImagesQueryVariables>) {
          return Apollo.useLazyQuery<ImagesQuery, ImagesQueryVariables>(ImagesDocument, baseOptions);
        }
export type ImagesQueryHookResult = ReturnType<typeof useImagesQuery>;
export type ImagesLazyQueryHookResult = ReturnType<typeof useImagesLazyQuery>;
export type ImagesQueryResult = Apollo.QueryResult<ImagesQuery, ImagesQueryVariables>;
export const StolpersteineDocument = gql`
    query Stolpersteine($ids: [String!]) {
  stolpersteine(ids: $ids) {
    ...Stolperstein
  }
}
    ${StolpersteinFragmentDoc}`;

/**
 * __useStolpersteineQuery__
 *
 * To run a query within a React component, call `useStolpersteineQuery` and pass it any options that fit your needs.
 * When your component renders, `useStolpersteineQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useStolpersteineQuery({
 *   variables: {
 *      ids: // value for 'ids'
 *   },
 * });
 */
export function useStolpersteineQuery(baseOptions?: Apollo.QueryHookOptions<StolpersteineQuery, StolpersteineQueryVariables>) {
        return Apollo.useQuery<StolpersteineQuery, StolpersteineQueryVariables>(StolpersteineDocument, baseOptions);
      }
export function useStolpersteineLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<StolpersteineQuery, StolpersteineQueryVariables>) {
          return Apollo.useLazyQuery<StolpersteineQuery, StolpersteineQueryVariables>(StolpersteineDocument, baseOptions);
        }
export type StolpersteineQueryHookResult = ReturnType<typeof useStolpersteineQuery>;
export type StolpersteineLazyQueryHookResult = ReturnType<typeof useStolpersteineLazyQuery>;
export type StolpersteineQueryResult = Apollo.QueryResult<StolpersteineQuery, StolpersteineQueryVariables>;
export const StolpersteinDocument = gql`
    query Stolperstein($id: String!) {
  stolperstein(id: $id) {
    ...Stolperstein
  }
}
    ${StolpersteinFragmentDoc}`;

/**
 * __useStolpersteinQuery__
 *
 * To run a query within a React component, call `useStolpersteinQuery` and pass it any options that fit your needs.
 * When your component renders, `useStolpersteinQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useStolpersteinQuery({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useStolpersteinQuery(baseOptions: Apollo.QueryHookOptions<StolpersteinQuery, StolpersteinQueryVariables>) {
        return Apollo.useQuery<StolpersteinQuery, StolpersteinQueryVariables>(StolpersteinDocument, baseOptions);
      }
export function useStolpersteinLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<StolpersteinQuery, StolpersteinQueryVariables>) {
          return Apollo.useLazyQuery<StolpersteinQuery, StolpersteinQueryVariables>(StolpersteinDocument, baseOptions);
        }
export type StolpersteinQueryHookResult = ReturnType<typeof useStolpersteinQuery>;
export type StolpersteinLazyQueryHookResult = ReturnType<typeof useStolpersteinLazyQuery>;
export type StolpersteinQueryResult = Apollo.QueryResult<StolpersteinQuery, StolpersteinQueryVariables>;
export const PersonsDocument = gql`
    query Persons($ids: [String!]) {
  persons(ids: $ids) {
    ...Person
  }
}
    ${PersonFragmentDoc}`;

/**
 * __usePersonsQuery__
 *
 * To run a query within a React component, call `usePersonsQuery` and pass it any options that fit your needs.
 * When your component renders, `usePersonsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = usePersonsQuery({
 *   variables: {
 *      ids: // value for 'ids'
 *   },
 * });
 */
export function usePersonsQuery(baseOptions?: Apollo.QueryHookOptions<PersonsQuery, PersonsQueryVariables>) {
        return Apollo.useQuery<PersonsQuery, PersonsQueryVariables>(PersonsDocument, baseOptions);
      }
export function usePersonsLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<PersonsQuery, PersonsQueryVariables>) {
          return Apollo.useLazyQuery<PersonsQuery, PersonsQueryVariables>(PersonsDocument, baseOptions);
        }
export type PersonsQueryHookResult = ReturnType<typeof usePersonsQuery>;
export type PersonsLazyQueryHookResult = ReturnType<typeof usePersonsLazyQuery>;
export type PersonsQueryResult = Apollo.QueryResult<PersonsQuery, PersonsQueryVariables>;
export const OccupationsDocument = gql`
    query Occupations($ids: [String!]) {
  occupations(ids: $ids) {
    ...Occupation
  }
}
    ${OccupationFragmentDoc}`;

/**
 * __useOccupationsQuery__
 *
 * To run a query within a React component, call `useOccupationsQuery` and pass it any options that fit your needs.
 * When your component renders, `useOccupationsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useOccupationsQuery({
 *   variables: {
 *      ids: // value for 'ids'
 *   },
 * });
 */
export function useOccupationsQuery(baseOptions?: Apollo.QueryHookOptions<OccupationsQuery, OccupationsQueryVariables>) {
        return Apollo.useQuery<OccupationsQuery, OccupationsQueryVariables>(OccupationsDocument, baseOptions);
      }
export function useOccupationsLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<OccupationsQuery, OccupationsQueryVariables>) {
          return Apollo.useLazyQuery<OccupationsQuery, OccupationsQueryVariables>(OccupationsDocument, baseOptions);
        }
export type OccupationsQueryHookResult = ReturnType<typeof useOccupationsQuery>;
export type OccupationsLazyQueryHookResult = ReturnType<typeof useOccupationsLazyQuery>;
export type OccupationsQueryResult = Apollo.QueryResult<OccupationsQuery, OccupationsQueryVariables>;
export const PersecutionPretextsDocument = gql`
    query PersecutionPretexts($ids: [String!]) {
  persecutionPretexts(ids: $ids) {
    ...PersecutionPretext
  }
}
    ${PersecutionPretextFragmentDoc}`;

/**
 * __usePersecutionPretextsQuery__
 *
 * To run a query within a React component, call `usePersecutionPretextsQuery` and pass it any options that fit your needs.
 * When your component renders, `usePersecutionPretextsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = usePersecutionPretextsQuery({
 *   variables: {
 *      ids: // value for 'ids'
 *   },
 * });
 */
export function usePersecutionPretextsQuery(baseOptions?: Apollo.QueryHookOptions<PersecutionPretextsQuery, PersecutionPretextsQueryVariables>) {
        return Apollo.useQuery<PersecutionPretextsQuery, PersecutionPretextsQueryVariables>(PersecutionPretextsDocument, baseOptions);
      }
export function usePersecutionPretextsLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<PersecutionPretextsQuery, PersecutionPretextsQueryVariables>) {
          return Apollo.useLazyQuery<PersecutionPretextsQuery, PersecutionPretextsQueryVariables>(PersecutionPretextsDocument, baseOptions);
        }
export type PersecutionPretextsQueryHookResult = ReturnType<typeof usePersecutionPretextsQuery>;
export type PersecutionPretextsLazyQueryHookResult = ReturnType<typeof usePersecutionPretextsLazyQuery>;
export type PersecutionPretextsQueryResult = Apollo.QueryResult<PersecutionPretextsQuery, PersecutionPretextsQueryVariables>;
export const PlacesDocument = gql`
    query Places($ids: [String!]) {
  places(ids: $ids) {
    ...Place
  }
}
    ${PlaceFragmentDoc}`;

/**
 * __usePlacesQuery__
 *
 * To run a query within a React component, call `usePlacesQuery` and pass it any options that fit your needs.
 * When your component renders, `usePlacesQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = usePlacesQuery({
 *   variables: {
 *      ids: // value for 'ids'
 *   },
 * });
 */
export function usePlacesQuery(baseOptions?: Apollo.QueryHookOptions<PlacesQuery, PlacesQueryVariables>) {
        return Apollo.useQuery<PlacesQuery, PlacesQueryVariables>(PlacesDocument, baseOptions);
      }
export function usePlacesLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<PlacesQuery, PlacesQueryVariables>) {
          return Apollo.useLazyQuery<PlacesQuery, PlacesQueryVariables>(PlacesDocument, baseOptions);
        }
export type PlacesQueryHookResult = ReturnType<typeof usePlacesQuery>;
export type PlacesLazyQueryHookResult = ReturnType<typeof usePlacesLazyQuery>;
export type PlacesQueryResult = Apollo.QueryResult<PlacesQuery, PlacesQueryVariables>;
export const RelationshipTypesDocument = gql`
    query RelationshipTypes($ids: [String!]) {
  relationshipTypes(ids: $ids) {
    ...RelationshipType
  }
}
    ${RelationshipTypeFragmentDoc}`;

/**
 * __useRelationshipTypesQuery__
 *
 * To run a query within a React component, call `useRelationshipTypesQuery` and pass it any options that fit your needs.
 * When your component renders, `useRelationshipTypesQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useRelationshipTypesQuery({
 *   variables: {
 *      ids: // value for 'ids'
 *   },
 * });
 */
export function useRelationshipTypesQuery(baseOptions?: Apollo.QueryHookOptions<RelationshipTypesQuery, RelationshipTypesQueryVariables>) {
        return Apollo.useQuery<RelationshipTypesQuery, RelationshipTypesQueryVariables>(RelationshipTypesDocument, baseOptions);
      }
export function useRelationshipTypesLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<RelationshipTypesQuery, RelationshipTypesQueryVariables>) {
          return Apollo.useLazyQuery<RelationshipTypesQuery, RelationshipTypesQueryVariables>(RelationshipTypesDocument, baseOptions);
        }
export type RelationshipTypesQueryHookResult = ReturnType<typeof useRelationshipTypesQuery>;
export type RelationshipTypesLazyQueryHookResult = ReturnType<typeof useRelationshipTypesLazyQuery>;
export type RelationshipTypesQueryResult = Apollo.QueryResult<RelationshipTypesQuery, RelationshipTypesQueryVariables>;
export const RelationshipsOfDocument = gql`
    query RelationshipsOf($id: String!) {
  relationshipsWithSubject(subjectId: $id) {
    ...Relationship
  }
  relationshipsWithObject(objectId: $id) {
    ...Relationship
  }
}
    ${RelationshipFragmentDoc}`;

/**
 * __useRelationshipsOfQuery__
 *
 * To run a query within a React component, call `useRelationshipsOfQuery` and pass it any options that fit your needs.
 * When your component renders, `useRelationshipsOfQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useRelationshipsOfQuery({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useRelationshipsOfQuery(baseOptions: Apollo.QueryHookOptions<RelationshipsOfQuery, RelationshipsOfQueryVariables>) {
        return Apollo.useQuery<RelationshipsOfQuery, RelationshipsOfQueryVariables>(RelationshipsOfDocument, baseOptions);
      }
export function useRelationshipsOfLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<RelationshipsOfQuery, RelationshipsOfQueryVariables>) {
          return Apollo.useLazyQuery<RelationshipsOfQuery, RelationshipsOfQueryVariables>(RelationshipsOfDocument, baseOptions);
        }
export type RelationshipsOfQueryHookResult = ReturnType<typeof useRelationshipsOfQuery>;
export type RelationshipsOfLazyQueryHookResult = ReturnType<typeof useRelationshipsOfLazyQuery>;
export type RelationshipsOfQueryResult = Apollo.QueryResult<RelationshipsOfQuery, RelationshipsOfQueryVariables>;
export const DumpDocument = gql`
    query Dump {
  locales {
    ...Locale
  }
  persecutionPretexts {
    ...PersecutionPretext
  }
  occupations {
    ...Occupation
  }
  relationshipTypes {
    ...RelationshipType
  }
  places {
    ...Place
  }
  spots {
    ...Spot
  }
  relationships {
    ...Relationship
  }
  persons {
    ...Person
  }
  stolpersteine {
    ...Stolperstein
  }
  nodes {
    ...Node
  }
  contents {
    ...Content
  }
  stolpersteinSubjects {
    ...StolpersteinSubject
  }
}
    ${LocaleFragmentDoc}
${PersecutionPretextFragmentDoc}
${OccupationFragmentDoc}
${RelationshipTypeFragmentDoc}
${PlaceFragmentDoc}
${SpotFragmentDoc}
${RelationshipFragmentDoc}
${PersonFragmentDoc}
${StolpersteinFragmentDoc}
${NodeFragmentDoc}
${ContentFragmentDoc}
${StolpersteinSubjectFragmentDoc}`;

/**
 * __useDumpQuery__
 *
 * To run a query within a React component, call `useDumpQuery` and pass it any options that fit your needs.
 * When your component renders, `useDumpQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useDumpQuery({
 *   variables: {
 *   },
 * });
 */
export function useDumpQuery(baseOptions?: Apollo.QueryHookOptions<DumpQuery, DumpQueryVariables>) {
        return Apollo.useQuery<DumpQuery, DumpQueryVariables>(DumpDocument, baseOptions);
      }
export function useDumpLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<DumpQuery, DumpQueryVariables>) {
          return Apollo.useLazyQuery<DumpQuery, DumpQueryVariables>(DumpDocument, baseOptions);
        }
export type DumpQueryHookResult = ReturnType<typeof useDumpQuery>;
export type DumpLazyQueryHookResult = ReturnType<typeof useDumpLazyQuery>;
export type DumpQueryResult = Apollo.QueryResult<DumpQuery, DumpQueryVariables>;