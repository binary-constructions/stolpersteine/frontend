
      export interface PossibleTypesResultData {
        possibleTypes: {
          [key: string]: string[]
        }
      }
      const result: PossibleTypesResultData = {
  "possibleTypes": {
    "Content": [
      "Person",
      "Spot",
      "Stolperstein"
    ],
    "Node": [
      "Image",
      "Occupation",
      "PersecutionPretext",
      "Person",
      "Place",
      "Relationship",
      "RelationshipType",
      "Spot",
      "Stolperstein"
    ],
    "PersonName": [
      "BirthName",
      "FamilyName",
      "GivenName",
      "MarriedName",
      "Nickname",
      "Pseudonym"
    ],
    "StolpersteinSubject": [
      "Person"
    ],
    "StreetDetail": [
      "StreetDetailCorner",
      "StreetDetailNumber"
    ],
    "TypedLiteral": [
      "BirthName",
      "FamilyName",
      "GivenName",
      "MarriedName",
      "Nickname",
      "PersonTitle",
      "Pseudonym",
      "StreetDetailCorner",
      "StreetDetailNumber"
    ]
  }
};
      export default result;
    