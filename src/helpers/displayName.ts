import { PersonNameFragment } from "../backendApi";

export interface NameWithSorts {
  displayName: string;

  firstNameSort: string;

  lastNameSort: string;
}

export const displayNameWithSorts = (
  names: PersonNameFragment[]
): NameWithSorts | undefined => {
  if (!names.length) {
    return undefined;
  }

  let firstNames: string = "";

  let firstNameSorts: string[] = [];

  let primaryLastName: string = "";

  const otherLastNames: string[] = [];

  let lastNameSorts: string[] = [];

  const pseudonyms: string[] = [];

  for (const name of names) {
    switch (name.__typename) {
      case "GivenName":
        firstNames += name.value + " ";

        firstNameSorts.push(name.value);

        break;
      case "Nickname":
        firstNames += '"' + name.value + '" ';

        firstNameSorts.push(name.value);

        break;
      case "FamilyName":
        if (!primaryLastName) {
          primaryLastName = name.value;
        } else {
          otherLastNames.push("auch: " + name.value);
        }

        lastNameSorts.push(name.value);

        break;
      case "BirthName":
        if (!primaryLastName) {
          primaryLastName = name.value;
        } else {
          otherLastNames.push("geboren: " + name.value);
        }

        lastNameSorts.push(name.value);

        break;
      case "MarriedName":
        if (!primaryLastName) {
          primaryLastName = name.value;
        } else {
          otherLastNames.push("verheiratet: " + name.value);
        }

        lastNameSorts.push(name.value);

        break;
      case "Pseudonym":
        pseudonyms.push(name.value);

        break;
      default:
        /* assert() */ throw new Error("Unknown name type!");
    }
  }

  if (primaryLastName) {
    primaryLastName += " ";
  }

  let fullName: string;

  if (!pseudonyms.length) {
    fullName =
      firstNames +
      primaryLastName +
      (otherLastNames.length ? "(" + otherLastNames.join(", ") + ")" : "");
  } else if (!firstNames && !primaryLastName) {
    if (pseudonyms.length === 1) {
      fullName = pseudonyms[0] + " (Pseudonym)";
    } else {
      fullName = pseudonyms.join(", ") + " (Pseudonyme)";
    }
  } else {
    fullName =
      firstNames +
      primaryLastName +
      (otherLastNames.length ? "(" + otherLastNames.join(", ") + ") " : "") +
      "alias " +
      pseudonyms.join(", ");
  }

  return {
    displayName: fullName.trim(),
    firstNameSort: firstNameSorts.join(" "),
    lastNameSort: lastNameSorts.join(" "),
  };
};

export const displayName = (
  names: PersonNameFragment[]
): string | undefined => {
  return displayNameWithSorts(names)?.displayName;
};
