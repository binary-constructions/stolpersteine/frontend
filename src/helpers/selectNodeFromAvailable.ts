import { INode } from "../interfaces/INode";

export const selectNodeFromAvailable = <T extends INode>(
  id: string,
  available: undefined | Error | T[]
): undefined | Error | T => {
  if (!available || available instanceof Error) {
    return available;
  } else {
    return (
      available.find((node) => node.id === id) ||
      new Error("Das gewählte Objekt wurde nicht gefunden.")
    );
  }
};
