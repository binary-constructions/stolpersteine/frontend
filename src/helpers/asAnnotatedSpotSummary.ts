import { PersonNameFragment, SpotSummary } from "../backendApi";
import { IAnnotatedSpotSummary } from "../interfaces/IAnnotatedSpotSummary";
import { displayStreet } from "../helpers/displayStreet";
import { displayNameWithSorts } from "../helpers/displayName";
import { HistoricalDate } from "../types/HistoricalDate";

export const asAnnotatedSpotSummary = (
  spot: SpotSummary,
  placeAnnotationsMap: {
    [placeId: string]:
      | undefined
      | { _placeLabel: string; _placeAltLabels: string[] };
  }
): IAnnotatedSpotSummary => ({
  ...spot,
  ...(spot.placeId ? placeAnnotationsMap[spot.placeId] || {} : {}),
  _streetLabel:
    displayStreet(
      spot.street || "",
      spot.streetNumber || spot.streetCorner || "",
      spot.streetNumber ? "StreetDetailNumber" : "StreetDetailCorner"
    ) || "",
  _lastTouched: [
    spot.created || "",
    spot.lastModified || "",
    spot.deleted || "",
  ].reduce((a, c) => (c && c > a ? c : a), ""),
  stolpersteinSummaries: spot.stolpersteinSummaries.map((v) => ({
    ...v,
    _lastTouched: [
      v.created || "",
      v.lastModified || "",
      v.deleted || "",
    ].reduce((a, c) => (c && c > a ? c : a), ""),
    personSummaries: v.personSummaries.map((w) => ({
      ...w,
      _name: displayNameWithSorts(w.names as PersonNameFragment[]),
      _bornYear: w.bornDate
        ? new HistoricalDate(w.bornDate).displayYear()
        : undefined,
      _diedYear: w.diedDate
        ? new HistoricalDate(w.diedDate).displayYear()
        : undefined,
      _lastTouched: [
        w.created || "",
        w.lastModified || "",
        w.deleted || "",
      ].reduce((a, c) => (c && c > a ? c : a), ""),
    })),
  })),
  __typename: "SpotSummary",
});
