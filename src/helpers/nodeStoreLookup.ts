import { INode } from "../interfaces/INode";

type NodeStore<T extends INode> = {
  nodesById: { [id: string]: undefined | Error | T };
};

/**
 * @returns An array of exactly the same length as `ids` mapping those
 * ids to the state of the corresponding node in the cache.
 */
export const nodeStoreLookup = <T extends INode>(
  store: NodeStore<T>,
  ids: string[]
): (undefined | Error | T)[] => ids.map((v) => store.nodesById[v]);
