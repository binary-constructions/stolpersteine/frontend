import moment from "moment";

export const displayDate = (value: string): string | undefined => {
  const date = value
    ? moment(value, ["YYYY", "YYYY-MM", "YYYY-MM-DD"])
    : undefined;

  return (
    date &&
    (!date.isValid()
      ? "ungültiges Datum"
      : value.length === 4
      ? value
      : value.length === 7
      ? moment(value, "YYYY-MM").format("MMM YYYY")
      : moment(value, "YYYY-MM-DD").format("D. MMM YYYY"))
  );
};
