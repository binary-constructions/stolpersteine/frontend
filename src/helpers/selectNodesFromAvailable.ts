import { INode } from "../interfaces/INode";

const NODE_NOT_FOUND_ERROR = new Error(
  "Das gewählte Objekt wurde nicht gefunden."
);

export const selectNodesFromAvailable = <T extends INode>(
  ids: string[],
  available: undefined | Error | T[]
): (undefined | Error | T)[] =>
  ids.map((id) =>
    !available || available instanceof Error
      ? available
      : available.find((node) => node.id === id) || NODE_NOT_FOUND_ERROR
  );

export const selectNodesFromAvailableOrFail = <T extends INode>(
  ids: string[],
  available: undefined | Error | T[]
): undefined | Error | T[] => {
  if (!available || available instanceof Error) {
    return available;
  } else {
    const selected: T[] = [];

    for (const id of ids) {
      const node = available.find((node) => node.id === id);

      if (!node) {
        throw new Error(
          "Mindestens eines der gewählten Objekte wurde nicht gefunden."
        );
      }

      selected.push(node);
    }

    return selected;
  }
};
