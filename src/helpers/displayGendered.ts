import { Gender } from "../backendApi";

export const displayGendered = (
  gender: Gender,
  label: string,
  labelF?: string | null,
  labelM?: string | null
): string => {
  switch (gender) {
    case Gender.Female:
      return labelF || label;
    case Gender.Male:
      return labelM || label;
    default:
      return label;
  }
};
