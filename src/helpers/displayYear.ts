import moment from "moment";

export const displayYear = (value: string): string | undefined => {
  const date = value
    ? moment(value, ["YYYY", "YYYY-MM", "YYYY-MM-DD"])
    : undefined;

  return date && (!date.isValid() ? "ungültiges Datum" : date.format("YYYY"));
};
