import { INode } from "../interfaces/INode";

type NodeStore<T extends INode> = {
  nodesById: { [id: string]: undefined | Error | T };
};

/**
 * @returns An array of exactly the same length as `ids` where those
 * ids were mapped to their corresponding nodes; if that's not
 * possible `undefined` is returned if not all of the specified ids
 * have been loaded yet or otherwise an Error object if one was
 * returned for _any_ of the specified ids.
 */
export const nodeStoreFetch = <T extends INode>(
  store: NodeStore<T>,
  ids: string[]
): undefined | Error | T[] =>
  ids
    .map((v) => store.nodesById[v])
    .reduce<undefined | Error | T[]>(
      (a, c) =>
        !a || !c
          ? undefined
          : a instanceof Error || c instanceof Error
          ? new Error(
              "Mindestens eins der gewählten Objekte konnte nicht geladen werden."
            )
          : [...a, c],
      []
    );
