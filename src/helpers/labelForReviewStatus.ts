import { ReviewStatus } from "../backendApi";

export const labelForReviewStatus = (status: ReviewStatus): string => {
  switch (status) {
    case ReviewStatus.Undefined:
      return "undefiniert";
    case ReviewStatus.Stub:
      return "Platzhalter";
    case ReviewStatus.WorkInProgress:
      return "in Bearbeitung";
    case ReviewStatus.NeedsReview:
      return "benötigt Review";
    case ReviewStatus.Released:
      return "freigegeben";
  }
};
