import { Fate } from "../backendApi";

export const displayFate = (fate: Fate): string | undefined => {
  switch (fate) {
    case Fate.Murdered:
      return "Ermordet";
    case Fate.Escaped:
      return "Flucht";
    case Fate.Survived:
      return "Überlebt";
    case Fate.Unknown:
      return undefined;
    default:
      /* assert() */ throw new Error("Unbekannte Verbleibs-Kategorie.");
  }
};
