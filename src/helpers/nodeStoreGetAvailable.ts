import { INode } from "../interfaces/INode";

type NodeStore<T extends INode> = {
  nodesById: { [id: string]: undefined | Error | T };
};

/**
 * @returns An array of all the successfuly loaded nodes in the cache.
 */
export const nodeStoreGetAvailable = <T extends INode>(
  store: NodeStore<T>
): T[] =>
  Object.values(store.nodesById).filter(
    (v): v is T => !!v && !(v instanceof Error)
  );
