import { Gender } from "../backendApi";

export const displayGender = (gender: Gender): string | undefined => {
  switch (gender) {
    case Gender.NonBinary:
      return "nicht-binär";
    case Gender.Female:
      return "weiblich";
    case Gender.Male:
      return "männlich";
    case Gender.Unspecified:
      return undefined;
    default:
      /* assert() */ throw new Error("Unbekannte Geschlechts-Kategorie.");
  }
};
