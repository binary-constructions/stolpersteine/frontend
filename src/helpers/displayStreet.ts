export const displayStreet = (
  street: string,
  detail: string,
  detailType: "StreetDetailNumber" | "StreetDetailCorner"
): string | undefined =>
  street.trim()
    ? detailType === "StreetDetailCorner" && detail.trim()
      ? `${street} / Ecke ${detail}`
      : detailType === "StreetDetailNumber" && detail.trim()
      ? `${street} ${detail}`
      : street
    : detailType === "StreetDetailCorner" && detail.trim()
    ? `Ecke ${detail}`
    : detailType === "StreetDetailNumber" && detail.trim()
    ? `Hausnummer ${detail}`
    : undefined;
