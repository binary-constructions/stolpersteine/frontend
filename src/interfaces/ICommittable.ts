import { IPotentialNode } from "./INode";
import { IEditable, isValidEdit } from "./IEditable";
import { hasErrors } from "./IValidated";

export interface ICommittable<B, E>
  extends IEditable<B, E>,
    IPotentialNode<any> {
  _commit?: boolean | Error; // falsy means no commit is going on, true a commit is in progress and Error... there was an error!
}

export const isCommittable = (committable: ICommittable<any, any>): boolean =>
  !isCommitting(committable) &&
  (committable.id !== undefined
    ? isValidEdit(committable)
    : !hasErrors(committable)); // FIXME

export const isCommitting = (committable: ICommittable<any, any>): boolean =>
  committable._commit === true;

export const hasCommitError = (committable: ICommittable<any, any>): boolean =>
  committable._commit instanceof Error ? true : false;

export const getCommitError = (
  committable: ICommittable<any, any>
): Error | undefined =>
  committable._commit instanceof Error ? committable._commit : undefined;

export const withCommitStatus = <T extends ICommittable<any, any>>(
  committable: T,
  status: boolean | Error
): T => ({
  ...committable,
  _commit: status,
});
