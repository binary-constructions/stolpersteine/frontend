import { ImageFragment, ImageInput } from "../backendApi";

import { IEditable, withoutUndefinedProperties, changed } from "./IEditable";
import { ICommittable } from "./ICommittable";
import { IValidationMessages } from "./IValidated";
import { INode, IPotentialNode, isNode } from "./INode";
import { ITyped } from "./ITyped";

export interface IImageEditData {
  dataUrl: string | null;

  fileName: string | null;

  authorshipRemark: string;

  caption: string;

  alternativeText: string;
}

export type IImageP = IPotentialNode<ImageFragment> & {
  dataUrl?: string; // the image data URL if changed locally
};

export type IImage = IImageP & ICommittable<IImageP, IImageEditData> & ITyped;

export type IExistingImage = IImage & INode;

export const toImageInput = (image: IImageP): ImageInput => ({
  data: image.dataUrl && image.dataUrl.replace(/^data:.+;base64,/, ""),
  fileName: image.fileName || undefined,
  authorshipRemark: image.authorshipRemark,
  caption: image.caption || undefined,
  alternativeText: image.alternativeText || undefined,
});

const imagesDiffer = (
  one: IImageP,
  other: IImageP
): boolean => // compares image data by size only for speed reasons
  JSON.stringify(toImageInput({ ...one, dataUrl: undefined })) !==
    JSON.stringify(toImageInput({ ...other, dataUrl: undefined })) ||
  (one.dataUrl ? one.dataUrl.length : 0) !==
    (other.dataUrl ? other.dataUrl.length : 0);

const wrapOrigin = <T extends IImageP>(origin: T): T & IImage => {
  const initialEditData: IImageEditData = {
    dataUrl: origin.dataUrl || null,
    fileName: origin.fileName || null,
    authorshipRemark: origin.authorshipRemark,
    caption: origin.caption || "",
    alternativeText: origin.alternativeText || "",
  };

  const withEdit = <T extends IEditable<IImageP, IImageEditData>>(
    editable: T,
    data: Partial<IImageEditData>
  ): T => {
    // has `origin` and `initialEditData` as closure

    const errors: IValidationMessages = {};

    const warnings: IValidationMessages = {};

    const fullEditData = {
      ...editable._currentEditData,
      ...withoutUndefinedProperties(data),
    };

    const edited = {
      ...editable,
      ...origin, // reset editable values
    };

    // dataUrl
    edited.dataUrl = fullEditData.dataUrl || undefined;

    if (!isNode(editable) && !edited.dataUrl) {
      errors.data = ""; // required!
    }

    // fileName
    edited.fileName = fullEditData.fileName ? fullEditData.fileName.trim() : "";

    if (!isNode(editable) && !edited.fileName) {
      errors.fileName = ""; // required!
    } else if (edited.fileName && !edited.fileName.includes(".")) {
      warnings.fileName = "Bitte die Datei-Endung (.jpg o.ä.) mit angeben!";
    }

    // authorshipRemark
    edited.authorshipRemark = fullEditData.authorshipRemark.trim();

    if (!edited.authorshipRemark) {
      errors.authorshipRemark = ""; // required!
    }

    // caption
    edited.caption = fullEditData.caption.trim() || undefined;

    // alternativeText
    edited.alternativeText = fullEditData.alternativeText.trim() || undefined;

    return {
      ...edited,
      _errors: errors,
      _warnings: warnings,
      _origin:
        editable._currentEditData !== initialEditData &&
        imagesDiffer(origin, edited)
          ? origin
          : undefined,
      _currentEditData: fullEditData,
    };
  };

  return withEdit(
    {
      // -> validate origin data
      __typename: "Image",
      ...origin,
      _errors: {},
      _warnings: {},
      _revert: (revertible) =>
        revertible._origin
          ? { ...revertible, ...fromImageP(revertible._origin) }
          : revertible,
      _currentEditData: initialEditData,
      _applyNewEditData: withEdit,
    },
    {}
  );
};

export const fromImageP = <T extends IImageP>(imageP: T): T & IImage =>
  wrapOrigin(imageP);

export const fromImageFragment = (fragment: ImageFragment): IExistingImage =>
  fromImageP(fragment);

export const newImage = (): IImage =>
  wrapOrigin({
    fileName: "",
    authorshipRemark: "",
    path: "",
  });

export const changedImage = <T extends IImage>(
  image: T,
  data: Partial<IImageEditData>
): T => changed<IImageP, IImageEditData, T>(image, data);
