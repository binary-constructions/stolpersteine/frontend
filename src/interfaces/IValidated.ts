export type IValidationMessages = {
  [field: string]: string;
};

export interface IValidated {
  _errors: IValidationMessages;

  _warnings: IValidationMessages;
}

export type IMaybeValidated<T extends IValidated> = Omit<
  T,
  "_errors" | "_warnings"
> &
  Partial<Pick<T, "_errors" | "_warnings">>;

export const hasErrors = (validated: IValidated): boolean =>
  Object.getOwnPropertyNames(validated._errors).length !== 0;

export const getError = (
  validated: IValidated,
  field: string
): string | undefined =>
  field in validated._errors ? validated._errors[field] : undefined;

export const hasWarnings = (validated: IValidated): boolean =>
  Object.getOwnPropertyNames(validated._warnings).length !== 0;

export const getWarning = (
  validated: IValidated,
  field: string
): string | undefined =>
  field in validated._warnings ? validated._warnings[field] : undefined;

export const getValidationStatus = (
  validated: IValidated,
  field: string
): { error?: string; warning?: string } => ({
  error: getError(validated, field),
  warning: getWarning(validated, field),
});
