import moment from "moment";

import {
  PersonFragment,
  PersonNameFragment,
  Gender,
  PersonInput,
  NameType,
  Fate,
} from "../backendApi";

import { IEditable, withoutUndefinedProperties, changed } from "./IEditable";
import { ICommittable } from "./ICommittable";
import { IValidationMessages } from "./IValidated";
import { IContent, IPotentialContent } from "./IContent";
import { ITyped } from "./ITyped";

export interface IPersonEditData {
  gender: Gender;

  title: string;

  names: PersonNameFragment[];

  occupationIds: string[];

  persecutionPretextIds: string[];

  bornDate: string;

  bornPlaceId: string | null;

  diedDate: string;

  diedPlaceId: string | null;

  imageIds: string[];

  fate: Fate;

  escapedToPlaceId: string | null;

  biographyHTML: string;

  editingNotesHTML: string;
}

export type IPersonP = IPotentialContent<PersonFragment>;

export type IPerson = IPersonP &
  ICommittable<IPersonP, IPersonEditData> &
  ITyped;

export type IExistingPerson = IPerson & IContent;

const nameTypeToInput = (nameType: string): NameType => {
  switch (nameType) {
    case "GivenName":
      return NameType.Given;
    case "Nickname":
      return NameType.Nickname;
    case "Pseudonym":
      return NameType.Pseudonym;
    case "FamilyName":
      return NameType.Family;
    case "BirthName":
      return NameType.Birth;
    case "MarriedName":
      return NameType.Married;
    default:
      throw new Error(
        "The provided typename does not identify a person name type."
      );
  }
};

export const toPersonInput = (person: IPersonP): PersonInput => ({
  gender: person.gender,
  title: person.title?.value || undefined,
  names: person.names.map((name) => ({
    name: name.value,
    type: nameTypeToInput(name.__typename),
  })),
  occupationIds: person.occupations.length
    ? person.occupations.map((occupation) => occupation.id)
    : undefined,
  persecutionPretextIds: person.persecutionPretexts.length
    ? person.persecutionPretexts.map((pretext) => pretext.id)
    : undefined,
  bornDate: person.bornDate || undefined,
  bornPlaceId: person.bornPlace ? person.bornPlace.id : undefined,
  diedDate: person.diedDate || undefined,
  diedPlaceId: person.diedPlace ? person.diedPlace.id : undefined,
  imageIds: person.imageIds,
  fate: person.fate,
  escapedToPlaceId: person.escapedToPlaceId || undefined,
  biographyHTML: person.biographyHTML || undefined,
  editingNotesHTML: person.editingNotesHTML || undefined,
});

const personsDiffer = (one: IPersonP, other: IPersonP): boolean =>
  JSON.stringify(toPersonInput(one)) !== JSON.stringify(toPersonInput(other));

const wrapOrigin = <T extends IPersonP>(origin: T): T & IPerson => {
  const initialEditData: IPersonEditData = {
    gender: origin.gender,
    title: origin.title?.value || "",
    names: origin.names,
    occupationIds: origin.occupations.map((occupation) => occupation.id),
    persecutionPretextIds: origin.persecutionPretexts.map(
      (pretext) => pretext.id
    ),
    bornDate: origin.bornDate || "",
    bornPlaceId: origin.bornPlace?.id || null,
    diedDate: origin.diedDate || "",
    diedPlaceId: origin.diedPlace?.id || null,
    imageIds: origin.imageIds,
    fate: origin.fate,
    escapedToPlaceId: origin.escapedToPlaceId || null,
    biographyHTML: origin.biographyHTML || "",
    editingNotesHTML: origin.editingNotesHTML || "",
  };

  const withEdit = <T extends IEditable<IPersonP, IPersonEditData>>(
    editable: T,
    data: Partial<IPersonEditData>
  ): T => {
    // has `origin` and `initialEditData` as closure

    const errors: IValidationMessages = {};

    const warnings: IValidationMessages = {};

    const fullEditData = {
      ...editable._currentEditData,
      ...withoutUndefinedProperties(data),
    };

    const edited = {
      ...editable,
      ...origin, // reset editable values
    };

    // gender
    edited.gender = fullEditData.gender;

    // title
    const titleValue = fullEditData.title.trim();

    edited.title = titleValue ? { value: titleValue } : null;

    // names
    edited.names = fullEditData.names.map((name) => ({
      ...name,
      value: name.value.trim(),
    }));

    edited.names = edited.names.filter((name) => !!name.value);

    if (!edited.names.length) {
      errors.names = ""; // required!
    }

    // occupationIds
    edited.occupations = fullEditData.occupationIds.map((id) => ({ id }));

    // persecutionPretextIds
    edited.persecutionPretexts = fullEditData.persecutionPretextIds.map(
      (id) => ({ id })
    );

    // bornDate
    const bornDateValue = fullEditData.bornDate.trim() || undefined;

    if (
      bornDateValue &&
      !moment(bornDateValue, ["YYYY", "YYYY-MM", "YYYY-MM-DD"]).isValid()
    ) {
      errors.bornDate = "Das Datum ist ungültig!";
    } else {
      edited.bornDate = bornDateValue;
    }

    // bornPlaceId
    edited.bornPlace =
      fullEditData.bornPlaceId !== null
        ? { id: fullEditData.bornPlaceId }
        : null;

    // diedDate
    const diedDateValue = fullEditData.diedDate.trim() || undefined;

    if (
      diedDateValue &&
      !moment(diedDateValue, ["YYYY", "YYYY-MM", "YYYY-MM-DD"]).isValid()
    ) {
      errors.diedDate = "Das Datum ist ungültig!";
    } else {
      edited.diedDate = diedDateValue;
    }

    // diedPlaceId
    edited.diedPlace =
      fullEditData.diedPlaceId !== null
        ? { id: fullEditData.diedPlaceId }
        : null;

    // biographyHTML
    edited.biographyHTML =
      fullEditData.biographyHTML === "<p><br></p>"
        ? undefined
        : fullEditData.biographyHTML || undefined;

    // editingNotesHTML
    edited.editingNotesHTML =
      fullEditData.editingNotesHTML === "<p><br></p>"
        ? undefined
        : fullEditData.editingNotesHTML || undefined;

    // imageIds

    edited.imageIds = fullEditData.imageIds;

    // fate

    edited.fate = fullEditData.fate;

    // escapedToPlaceId

    edited.escapedToPlaceId = fullEditData.escapedToPlaceId;

    return {
      ...edited,
      _errors: errors,
      _warnings: warnings,
      _origin:
        editable._currentEditData !== initialEditData &&
        (Object.keys(errors).length || personsDiffer(origin, edited))
          ? origin
          : undefined,
      _currentEditData: fullEditData,
    };
  };

  return withEdit(
    {
      // -> validate origin data
      __typename: "Person",
      ...origin,
      _errors: {},
      _warnings: {},
      _revert: (revertible) =>
        revertible._origin
          ? { ...revertible, ...fromPersonP(revertible._origin) }
          : revertible,
      _currentEditData: initialEditData,
      _applyNewEditData: withEdit,
    },
    {}
  );
};

export const fromPersonP = <T extends IPersonP>(personP: T): T & IPerson =>
  wrapOrigin(personP);

export const fromPersonFragment = (fragment: PersonFragment): IExistingPerson =>
  fromPersonP(fragment);

export const newPerson = (): IPerson =>
  wrapOrigin({
    gender: Gender.Unspecified,
    names: [],
    occupations: [],
    persecutionPretexts: [],
    subjectOf: [],
    title: null,
    imageIds: [],
    fate: Fate.Unknown,
    escapedToPlaceId: null,
    bornPlace: null,
    diedPlace: null,
  });

export const changedPerson = <T extends IPerson>(
  person: T,
  data: Partial<IPersonEditData>
): T => changed<IPersonP, IPersonEditData, T>(person, data);
