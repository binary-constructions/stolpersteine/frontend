import { ReviewStatus } from "../backendApi";
import { INode, isNode } from "./INode";

export interface IContent extends INode {
  created?: string | null;

  lastModified?: string | null;

  deleted?: string | null;

  reviewStatus: ReviewStatus;
}

export type IPotentialContent<T extends IContent> = Omit<
  T,
  "id" | "reviewStatus"
> &
  Partial<IContent>;

export const isContent = <T extends IPotentialContent<any>>(
  potentialContent: T
): potentialContent is IContent & T =>
  isNode(potentialContent) && potentialContent.reviewStatus !== undefined;
