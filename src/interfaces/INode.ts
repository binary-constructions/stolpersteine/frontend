export interface INode {
  id: string;
}

export type IPotentialNode<T extends INode> = Omit<T, "id"> & Partial<INode>;

export const isNode = <T extends IPotentialNode<any>>(
  potentialNode: T
): potentialNode is INode & T => potentialNode.id !== undefined;
