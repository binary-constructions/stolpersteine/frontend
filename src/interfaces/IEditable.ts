import { IValidated, hasErrors } from "./IValidated";
import { IRevertible, isRevertible } from "./IRevertible";

export interface IEditable<B, E> extends IValidated, IRevertible<B> {
  _currentEditData: E;

  _applyNewEditData: <T extends IEditable<B, E>>(
    editable: T,
    data: Partial<E>
  ) => T;
}

//export const editable = <B, E, T extends IEditable<B, E>>(editable: T): E => editable._currentEditData;
export const editable = <B, E>(editable: IEditable<B, E>): E =>
  editable._currentEditData;

export const isValidEdit = (editable: IEditable<any, any>): boolean =>
  !hasErrors(editable) && isRevertible(editable);

export const isUnchanged = (editable: IEditable<any, any>): boolean =>
  !hasErrors(editable) && !isRevertible(editable);

export const changed = <B, E, T extends IEditable<B, E>>(
  editable: T,
  data: Partial<E>
): T => editable._applyNewEditData(editable, data);

// helper function to use in implementing apply()
export const withoutUndefinedProperties = <T extends { [key: string]: any }>(
  data: T
): Partial<T> => {
  const sanitized = { ...data };

  Object.keys(sanitized).forEach(
    (key) => sanitized[key] === undefined && delete sanitized[key]
  );

  return sanitized;
};
