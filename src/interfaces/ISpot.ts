import { SpotFragment, SpotInput, StreetDetail } from "../backendApi";

import { IEditable, withoutUndefinedProperties, changed } from "./IEditable";
import { ICommittable } from "./ICommittable";
import { IValidationMessages, hasErrors } from "./IValidated";
import { IContent, IPotentialContent } from "./IContent";
import { ITyped } from "./ITyped";
import { IGeoPoint, fromGeoPointFragment } from "./IGeoPoint";

export interface ISpotEditData {
  street: string;

  streetDetail: StreetDetail & ITyped;

  postalCode: string;

  placeId: string | null;

  imageIds: string[];

  note: string;

  point: IGeoPoint;
}

export type ISpotP = IPotentialContent<SpotFragment>;

export type ISpot = ISpotP & ICommittable<ISpotP, ISpotEditData> & ITyped;

export type IExistingSpot = ISpot & IContent;

export const toSpotInput = (spot: ISpotP): SpotInput => ({
  longitude: spot.point.longitude,
  latitude: spot.point.latitude,
  street: spot.street || undefined,
  corner:
    spot.streetDetail && spot.streetDetail.__typename === "StreetDetailCorner"
      ? spot.streetDetail.value
      : undefined,
  number:
    spot.streetDetail && spot.streetDetail.__typename === "StreetDetailNumber"
      ? spot.streetDetail.value
      : undefined,
  postalCode: spot.postalCode || undefined,
  placeId: spot.placeId,
  imageIds: spot.imageIds,
  note: spot.note || undefined,
});

const spotsDiffer = (one: ISpotP, other: ISpotP): boolean =>
  JSON.stringify(toSpotInput(one)) !== JSON.stringify(toSpotInput(other));

const wrapOrigin = <T extends ISpotP>(origin: T): T & ISpot => {
  const initialEditData: ISpotEditData = {
    street: origin.street || "",
    streetDetail: origin.streetDetail || {
      __typename: "StreetDetailNumber",
      value: "",
    },
    postalCode: origin.postalCode || "",
    placeId: origin.placeId || null,
    imageIds: origin.imageIds || [],
    note: origin.note || "",
    point: fromGeoPointFragment(origin.point),
  };

  const withEdit = <T extends IEditable<ISpotP, ISpotEditData>>(
    editable: T,
    data: Partial<ISpotEditData>
  ): T => {
    // has `origin` and `initialEditData` as closure

    const errors: IValidationMessages = {};

    const warnings: IValidationMessages = {};

    const fullEditData = {
      ...editable._currentEditData,
      ...withoutUndefinedProperties(data),
    };

    const edited = {
      ...editable,
      ...origin, // reset editable values
    };

    // street
    edited.street = fullEditData.street.trim() || undefined;

    if (edited.street && /[Ss]tr\./.test(edited.street)) {
      warnings.street = '"Straße" bitte nicht abkürzen!';
    }

    // streetDetail
    const streetDetailValue = fullEditData.streetDetail.value.trim();

    edited.streetDetail = streetDetailValue
      ? { ...fullEditData.streetDetail, value: streetDetailValue }
      : null;

    if (
      edited.streetDetail &&
      edited.streetDetail.__typename === "StreetDetailCorner" &&
      /[Ss]tr\./.test(edited.streetDetail.value)
    ) {
      warnings.streetDetail = '"Straße" bitte nicht abkürzen.';
    }

    // postalCode
    edited.postalCode = fullEditData.postalCode.trim() || undefined;

    if (edited.postalCode && edited.postalCode.length !== 5) {
      warnings.postalCode = "Soll vermutlich fünfstellig sein?";
    }

    // placeId
    edited.placeId = fullEditData.placeId;

    // imageIds
    edited.imageIds = fullEditData.imageIds;

    // note
    edited.note = fullEditData.note.trim() || undefined;

    // point
    if (hasErrors(fullEditData.point)) {
      errors.point = "Die geographischen Koordinaten sind ungültig.";
    }

    edited.point = fullEditData.point; // update even in case of errors (because point is a validated editable itself)

    return {
      ...edited,
      _errors: errors,
      _warnings: warnings,
      _origin:
        editable._currentEditData !== initialEditData &&
        (Object.keys(errors).length || spotsDiffer(origin, edited))
          ? origin
          : undefined,
      _currentEditData: fullEditData,
    };
  };

  return withEdit(
    {
      // -> validate origin data
      __typename: "Spot",
      ...origin,
      _errors: {},
      _warnings: {},
      _revert: (revertible) =>
        revertible._origin
          ? { ...revertible, ...fromSpotP(revertible._origin) }
          : revertible,
      _currentEditData: initialEditData,
      _applyNewEditData: withEdit,
    },
    {}
  );
};

export const fromSpotP = <T extends ISpotP>(spotP: T): T & ISpot =>
  wrapOrigin(spotP);

export const fromSpotFragment = (fragment: SpotFragment): IExistingSpot =>
  fromSpotP(fragment);

export const newSpot = (point: {
  longitude: number;
  latitude: number;
}): ISpot =>
  wrapOrigin({
    point: point,
    streetDetail: null,
    placeId: null,
    imageIds: [],
    stolpersteinIds: [],
  });

export const changedSpot = <T extends ISpot>(
  spot: T,
  data: Partial<ISpotEditData>
): T => changed<ISpotP, ISpotEditData, T>(spot, data);

export const withLocalStolpersteineUpdate = <T extends ISpot>(
  spot: T,
  stolpersteinIds: string[]
): T => ({
  ...spot,
  stolpersteinIds,
});
