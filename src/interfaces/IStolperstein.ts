import {
  StolpersteinFragment,
  StolpersteinInput,
  StolpersteinQuery,
  StreetDetail,
  ReviewStatus,
} from "../backendApi";

import moment from "moment";

import { IEditable, withoutUndefinedProperties, changed } from "./IEditable";
import { ICommittable } from "./ICommittable";
import { IValidationMessages } from "./IValidated";
import { IContent, IPotentialContent } from "./IContent";
import { ITyped } from "./ITyped";
import { IExistingSpot } from "./ISpot";

export interface IStolpersteinEditData {
  spotId: string | null;

  laid: string;

  inscription: string;

  imageId: string | null;

  subjectIds: string[];

  historicalStreet: string;

  historicalStreetDetail: StreetDetail & ITyped;

  historicalPlaceId: string | null;

  initiativeHTML: string;

  patronageHTML: string;

  dataGatheringHTML: string;

  fundingHTML: string;
}

export type IStolpersteinP = IPotentialContent<StolpersteinFragment>;

export type IStolperstein = IStolpersteinP &
  ICommittable<IStolpersteinP, IStolpersteinEditData> &
  ITyped;

export type IExistingStolperstein = IStolperstein & IContent;

export const toStolpersteinInput = (
  stolperstein: IStolpersteinP,
  skipMarkup?: boolean
): StolpersteinInput => ({
  spotId: stolperstein.spot?.id,
  laid: stolperstein.laid || undefined,
  inscription: stolperstein.inscription || undefined,
  imageId: stolperstein.image ? stolperstein.image.id : undefined,
  subjectIds: stolperstein.subjects.map((subject) => subject.id),
  historicalStreet: stolperstein.historicalStreet || undefined,
  historicalCorner:
    stolperstein.historicalStreetDetail &&
    stolperstein.historicalStreetDetail.__typename === "StreetDetailCorner"
      ? stolperstein.historicalStreetDetail.value
      : undefined,
  historicalNumber:
    stolperstein.historicalStreetDetail &&
    stolperstein.historicalStreetDetail.__typename === "StreetDetailNumber"
      ? stolperstein.historicalStreetDetail.value
      : undefined,
  historicalPlaceId: stolperstein.historicalPlace?.id,
  initiativeHTML: !skipMarkup
    ? stolperstein.initiativeHTML || undefined
    : undefined,
  patronageHTML: !skipMarkup
    ? stolperstein.patronageHTML || undefined
    : undefined,
  dataGatheringHTML: !skipMarkup
    ? stolperstein.dataGatheringHTML || undefined
    : undefined,
  fundingHTML: !skipMarkup ? stolperstein.fundingHTML || undefined : undefined,
});

const stolpersteineDiffer = (
  one: IStolpersteinP,
  other: IStolpersteinP
): boolean =>
  JSON.stringify(toStolpersteinInput(one)) !==
  JSON.stringify(toStolpersteinInput(other));

const wrapOrigin = <T extends IStolpersteinP>(origin: T): T & IStolperstein => {
  const initialEditData: IStolpersteinEditData = {
    spotId: origin.spot?.id || null,
    laid: origin.laid || "",
    inscription: origin.inscription || "",
    imageId: origin.image ? origin.image.id : null,
    subjectIds: origin.subjects.map((subject) => subject.id),
    historicalStreet: origin.historicalStreet || "",
    historicalStreetDetail: origin.historicalStreetDetail || {
      __typename: "StreetDetailNumber",
      value: "",
    },
    historicalPlaceId: origin.historicalPlace?.id || null,
    initiativeHTML: origin.initiativeHTML || "",
    patronageHTML: origin.patronageHTML || "",
    dataGatheringHTML: origin.dataGatheringHTML || "",
    fundingHTML: origin.fundingHTML || "",
  };

  const withEdit = <T extends IEditable<IStolpersteinP, IStolpersteinEditData>>(
    editable: T,
    data: Partial<IStolpersteinEditData>
  ): T => {
    // has `origin` and `initialEditData` as closure

    const errors: IValidationMessages = {};

    const warnings: IValidationMessages = {};

    const fullEditData = {
      ...editable._currentEditData,
      ...withoutUndefinedProperties(data),
    };

    const edited = {
      ...editable,
      ...origin, // reset editable values
    };

    // spotId
    edited.spot = fullEditData.spotId ? { id: fullEditData.spotId } : null;

    // laid
    const laidValue = fullEditData.laid.trim() || undefined;

    if (
      laidValue &&
      !moment(laidValue, ["YYYY", "YYYY-MM", "YYYY-MM-DD"]).isValid()
    ) {
      errors.laid = "Das Datum ist ungültig!";
    } else {
      edited.laid = laidValue;
    }

    // inscription
    edited.inscription = fullEditData.inscription.trim() || undefined;

    // imageId
    edited.image =
      fullEditData.imageId !== null ? { id: fullEditData.imageId } : null;

    // subjectIds
    edited.subjects = fullEditData.subjectIds.map((id) => ({ id }));

    // historicalStreet
    edited.historicalStreet = fullEditData.historicalStreet.trim() || undefined;

    if (edited.historicalStreet && /[Ss]tr\./.test(edited.historicalStreet)) {
      warnings.historicalStreet = '"Straße" bitte nicht abkürzen!';
    }

    // historicalStreetDetail
    const historicalStreetDetailValue = fullEditData.historicalStreetDetail.value.trim();

    edited.historicalStreetDetail = historicalStreetDetailValue
      ? {
          ...fullEditData.historicalStreetDetail,
          value: historicalStreetDetailValue,
        }
      : null;

    if (
      edited.historicalStreetDetail &&
      edited.historicalStreetDetail.__typename === "StreetDetailCorner" &&
      /[Ss]tr\./.test(edited.historicalStreetDetail.value)
    ) {
      warnings.historicalStreetDetail = '"Straße" bitte nicht abkürzen.';
    }

    // historicalPlaceId
    edited.historicalPlace =
      fullEditData.historicalPlaceId !== null
        ? { id: fullEditData.historicalPlaceId }
        : null;

    // initiativeHTML
    edited.initiativeHTML =
      fullEditData.initiativeHTML === "<p><br></p>"
        ? undefined
        : fullEditData.initiativeHTML || undefined;

    // patronageHTML
    edited.patronageHTML =
      fullEditData.patronageHTML === "<p><br></p>"
        ? undefined
        : fullEditData.patronageHTML || undefined;

    // dataGatheringHTML
    edited.dataGatheringHTML =
      fullEditData.dataGatheringHTML === "<p><br></p>"
        ? undefined
        : fullEditData.dataGatheringHTML || undefined;

    // fundingHTML
    edited.fundingHTML =
      fullEditData.fundingHTML === "<p><br></p>"
        ? undefined
        : fullEditData.fundingHTML || undefined;

    return {
      ...edited,
      _errors: errors,
      _warnings: warnings,
      _origin:
        editable._currentEditData !== initialEditData &&
        (Object.keys(errors).length || stolpersteineDiffer(origin, edited))
          ? origin
          : undefined,
      _currentEditData: fullEditData,
    };
  };

  return withEdit(
    {
      // -> validate origin data
      __typename: "Stolperstein",
      ...origin,
      _errors: {},
      _warnings: {},
      _revert: (revertible) =>
        revertible._origin
          ? { ...revertible, ...fromStolpersteinP(revertible._origin) }
          : revertible,
      _currentEditData: initialEditData,
      _applyNewEditData: withEdit,
    },
    {}
  );
};

export const fromStolpersteinP = <T extends IStolpersteinP>(
  stolpersteinP: T
): T & IStolperstein => wrapOrigin(stolpersteinP);

export const fromStolpersteinFragment = (
  fragment: StolpersteinFragment
): IExistingStolperstein => fromStolpersteinP(fragment);

export const fromStolpersteinQuery = (
  query: StolpersteinQuery
): IExistingStolperstein | null =>
  query.stolperstein ? fromStolpersteinFragment(query.stolperstein) : null;

export const newStolperstein = (spot: IExistingSpot): IStolperstein =>
  wrapOrigin({
    spot: { id: spot.id },
    image: null,
    subjects: [],
    historicalStreetDetail: null,
    historicalPlace: null,
    reviewStatus: ReviewStatus.Undefined,
  });

export const changedStolperstein = <T extends IStolperstein>(
  stolperstein: T,
  data: Partial<IStolpersteinEditData>
): T => changed<IStolpersteinP, IStolpersteinEditData, T>(stolperstein, data);

export const withLocalReviewStatusChange = <T extends IStolperstein>(
  stolperstein: T,
  reviewStatus: ReviewStatus
): T => ({
  ...stolperstein,
  reviewStatus,
});
