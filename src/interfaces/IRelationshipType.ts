import { RelationshipTypeFragment, RelationshipTypeInput } from "../backendApi";

import { IEditable, withoutUndefinedProperties, changed } from "./IEditable";
import { ICommittable } from "./ICommittable";
import { IValidationMessages } from "./IValidated";
import { INode, IPotentialNode } from "./INode";
import { ITyped } from "./ITyped";

interface IRelationshipTypeL10nEditData {
  locale: string;

  subjectLabel: string;

  subjectLabel_f: string;

  subjectLabel_m: string;

  objectLabel: string;

  objectLabel_f: string;

  objectLabel_m: string;
}

export interface IRelationshipTypeEditData {
  locale: "de"; // FIXME -> IExistingLocale

  subjectLabel: string;

  subjectLabel_f: string;

  subjectLabel_m: string;

  objectLabel: string;

  objectLabel_f: string;

  objectLabel_m: string;

  // TODO: localizations
}

export type IRelationshipTypeP = IPotentialNode<RelationshipTypeFragment>;

export type IRelationshipType = IRelationshipTypeP &
  ICommittable<IRelationshipTypeP, IRelationshipTypeEditData> &
  ITyped;

export type IExistingRelationshipType = IRelationshipType & INode;

export const toRelationshipTypeInput = (
  relationshipType: IRelationshipTypeP
): RelationshipTypeInput => ({
  locale: relationshipType.locale.code,
  subjectLabel: relationshipType.subjectLabel,
  subjectLabel_f: relationshipType.subjectLabel_f || undefined,
  subjectLabel_m: relationshipType.subjectLabel_m || undefined,
  objectLabel: relationshipType.objectLabel,
  objectLabel_f: relationshipType.objectLabel_f || undefined,
  objectLabel_m: relationshipType.objectLabel_m || undefined,
  // TODO: localizations
});

const relationshipTypesDiffer = (
  one: IRelationshipTypeP,
  other: IRelationshipTypeP
): boolean =>
  JSON.stringify(toRelationshipTypeInput(one)) !==
  JSON.stringify(toRelationshipTypeInput(other));

const wrapOrigin = <T extends IRelationshipTypeP>(
  origin: T
): T & IRelationshipType => {
  const initialEditData: IRelationshipTypeEditData = {
    locale: "de", // FIXME
    subjectLabel: origin.subjectLabel,
    subjectLabel_f: origin.subjectLabel_f || "",
    subjectLabel_m: origin.subjectLabel_m || "",
    objectLabel: origin.objectLabel,
    objectLabel_f: origin.objectLabel_f || "",
    objectLabel_m: origin.objectLabel_m || "",
  };

  const withEdit = <
    T extends IEditable<IRelationshipTypeP, IRelationshipTypeEditData>
  >(
    editable: T,
    data: Partial<IRelationshipTypeEditData>
  ): T => {
    // has `origin` and `initialEditData` as closure

    const errors: IValidationMessages = {};

    const warnings: IValidationMessages = {};

    const fullEditData = {
      ...editable._currentEditData,
      ...withoutUndefinedProperties(data),
    };

    const edited = {
      ...editable,
      ...origin, // reset editable values
    };

    // locale
    //edited.locale = fullEditData.locale;

    // subjectLabel
    edited.subjectLabel = fullEditData.subjectLabel.trim();

    if (!edited.subjectLabel) {
      errors.subjectLabel = ""; // required!
    }

    // subjectLabel_f
    edited.subjectLabel_f = fullEditData.subjectLabel_f.trim() || undefined;

    // subjectLabel_m
    edited.subjectLabel_m = fullEditData.subjectLabel_m.trim() || undefined;

    // objectLabel
    edited.objectLabel = fullEditData.objectLabel.trim();

    if (!edited.objectLabel) {
      errors.objectLabel = ""; // required!
    }

    // objectLabel_f
    edited.objectLabel_f = fullEditData.objectLabel_f.trim() || undefined;

    // objectLabel_m
    edited.objectLabel_m = fullEditData.objectLabel_m.trim() || undefined;

    return {
      ...edited,
      _errors: errors,
      _warnings: warnings,
      _origin:
        editable._currentEditData !== initialEditData &&
        (Object.keys(errors).length || relationshipTypesDiffer(origin, edited))
          ? origin
          : undefined,
      _currentEditData: fullEditData,
    };
  };

  return withEdit(
    {
      // -> validate origin data
      __typename: "RelationshipType",
      ...origin,
      _errors: {},
      _warnings: {},
      _revert: (revertible) =>
        revertible._origin
          ? { ...revertible, ...fromRelationshipTypeP(revertible._origin) }
          : revertible,
      _currentEditData: initialEditData,
      _applyNewEditData: withEdit,
    },
    {}
  );
};

export const fromRelationshipTypeP = <T extends IRelationshipTypeP>(
  relationshipTypeP: T
): T & IRelationshipType => wrapOrigin(relationshipTypeP);

export const fromRelationshipTypeFragment = (
  fragment: RelationshipTypeFragment
): IExistingRelationshipType => fromRelationshipTypeP(fragment);

export const newRelationshipType = (): IRelationshipType =>
  wrapOrigin({
    locale: { code: "de" },
    subjectLabel: "",
    objectLabel: "",
    localizations: [],
  });

export const changedRelationshipType = <T extends IRelationshipType>(
  relationshipType: T,
  data: Partial<IRelationshipTypeEditData>
): T =>
  changed<IRelationshipTypeP, IRelationshipTypeEditData, T>(
    relationshipType,
    data
  );
