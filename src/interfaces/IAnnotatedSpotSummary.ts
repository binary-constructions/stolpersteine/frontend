import { SpotSummary, StolpersteinSummary, PersonSummary } from "../backendApi";
import { ITyped } from "../interfaces/ITyped";
import { NameWithSorts } from "../helpers/displayName";

interface IAnnotatedPersonSummary extends PersonSummary {
  _name?: NameWithSorts;

  _bornYear?: string;

  _diedYear?: string;

  _lastTouched: string;
}

interface IAnnotatedStolpersteinSummary extends StolpersteinSummary {
  personSummaries: IAnnotatedPersonSummary[];

  _lastTouched: string;
}

export interface IAnnotatedSpotSummary extends SpotSummary, ITyped {
  __typename: "SpotSummary";

  _placeLabel?: undefined | string;

  _placeAltLabels?: undefined | string[];

  _streetLabel?: undefined | string;

  _lastTouched: string;

  stolpersteinSummaries: IAnnotatedStolpersteinSummary[];
}
