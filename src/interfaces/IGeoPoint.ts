import { GeoPointFragment } from "../backendApi";

import { IEditable, withoutUndefinedProperties, changed } from "./IEditable";
import { IValidationMessages } from "./IValidated";
import { ITyped } from "./ITyped";

export interface IGeoPointEditData {
  longitude: string;

  latitude: string;
}

export type IGeoPoint = GeoPointFragment &
  IEditable<GeoPointFragment, IGeoPointEditData> &
  ITyped;

const fragmentsDiffer = (
  one: GeoPointFragment,
  other: GeoPointFragment
): boolean =>
  one.longitude !== other.longitude || one.latitude !== other.latitude;

const wrapOrigin = (origin: GeoPointFragment): IGeoPoint => {
  const initialEditData: IGeoPointEditData = {
    longitude: origin.longitude.toFixed(6),
    latitude: origin.latitude.toFixed(6),
  };

  const withEdit = <T extends IEditable<GeoPointFragment, IGeoPointEditData>>(
    editable: T,
    data: Partial<IGeoPointEditData>
  ): T => {
    // has `origin` and `initialEditData` as closure

    const errors: IValidationMessages = {};

    const fullEditData = {
      ...editable._currentEditData,
      ...withoutUndefinedProperties(data),
    };

    const edited = {
      ...editable,
      ...origin, // reset editable values
    };

    // longitude
    const parsedLongitude = parseFloat(fullEditData.longitude || "0");

    if (Number.isNaN(parsedLongitude)) {
      errors.longitude = "Bitte eine Zahl eingeben!";
    } else if (parsedLongitude > 180) {
      errors.longitude = "Der Längengrad muss kleiner oder gleich 180° sein.";
    } else if (parsedLongitude < -180) {
      errors.longitude = "Der Längengrad muss größer oder gleich -180° sein.";
    } else {
      edited.longitude = parsedLongitude;
    }

    // latitude
    const parsedLatitude = parseFloat(fullEditData.latitude || "0");

    if (Number.isNaN(parsedLatitude)) {
      errors.latitude = "Bitte eine Zahl eingeben!";
    } else if (parsedLatitude > 90) {
      errors.latitude = "Der Breitengrad muss kleiner oder gleich 90° sein.";
    } else if (parsedLatitude < -90) {
      errors.latitude = "Der Breitengrad muss größer oder gleich -90° sein.";
    } else {
      edited.latitude = parsedLatitude;
    }

    return {
      ...edited,
      _errors: errors,
      _warnings: {},
      _origin:
        editable._currentEditData !== initialEditData &&
        (Object.keys(errors).length || fragmentsDiffer(origin, edited))
          ? origin
          : undefined,
      _currentEditData: fullEditData,
    };
  };

  return withEdit(
    {
      // -> validate origin data
      __typename: "GeoPoint",
      ...origin,
      _errors: {},
      _warnings: {},
      _revert: (revertible) =>
        revertible._origin
          ? { ...revertible, ...fromGeoPointFragment(revertible._origin) }
          : revertible,
      _currentEditData: initialEditData,
      _applyNewEditData: withEdit,
    },
    {}
  );
};

export const fromGeoPointFragment = (fragment: GeoPointFragment): IGeoPoint =>
  wrapOrigin(fragment);

export const newGeoPoint = (origin: {
  longitude: number;
  latitude: number;
}): IGeoPoint => wrapOrigin(origin);

export const changedGeoPoint = <T extends IGeoPoint>(
  point: T,
  data: Partial<IGeoPointEditData>
): T => changed<GeoPointFragment, IGeoPointEditData, T>(point, data);
