import {
  PersecutionPretextFragment,
  PersecutionPretextInput,
} from "../backendApi";

import { IEditable, withoutUndefinedProperties, changed } from "./IEditable";
import { ICommittable } from "./ICommittable";
import { IValidationMessages } from "./IValidated";
import { INode, IPotentialNode } from "./INode";
import { ITyped } from "./ITyped";

interface IPersecutionPretextL10nEditData {
  locale: string;

  label: string;

  label_f: string;

  label_m: string;
}

export interface IPersecutionPretextEditData {
  locale: "de"; // FIXME -> IExistingLocale

  label: string;

  label_f: string;

  label_m: string;

  // TODO: localizations
}

export type IPersecutionPretextP = IPotentialNode<PersecutionPretextFragment>;

export type IPersecutionPretext = IPersecutionPretextP &
  ICommittable<IPersecutionPretextP, IPersecutionPretextEditData> &
  ITyped;

export type IExistingPersecutionPretext = IPersecutionPretext & INode;

export const toPersecutionPretextInput = (
  persecutionPretext: IPersecutionPretextP
): PersecutionPretextInput => ({
  locale: persecutionPretext.locale.code,
  label: persecutionPretext.label,
  label_f: persecutionPretext.label_f || undefined,
  label_m: persecutionPretext.label_m || undefined,
  // TODO: localizations
});

const persecutionPretextsDiffer = (
  one: IPersecutionPretextP,
  other: IPersecutionPretextP
): boolean =>
  JSON.stringify(toPersecutionPretextInput(one)) !==
  JSON.stringify(toPersecutionPretextInput(other));

const wrapOrigin = <T extends IPersecutionPretextP>(
  origin: T
): T & IPersecutionPretext => {
  const initialEditData: IPersecutionPretextEditData = {
    locale: "de", // FIXME
    label: origin.label,
    label_f: origin.label_f || "",
    label_m: origin.label_m || "",
  };

  const withEdit = <
    T extends IEditable<IPersecutionPretextP, IPersecutionPretextEditData>
  >(
    editable: T,
    data: Partial<IPersecutionPretextEditData>
  ): T => {
    // has `origin` and `initialEditData` as closure

    const errors: IValidationMessages = {};

    const warnings: IValidationMessages = {};

    const fullEditData = {
      ...editable._currentEditData,
      ...withoutUndefinedProperties(data),
    };

    const edited = {
      ...editable,
      ...origin, // reset editable values
    };

    // locale
    //edited.locale = fullEditData.locale;

    // label
    const labelValue = fullEditData.label.trim() || undefined;

    if (labelValue) {
      edited.label = labelValue;
    } else {
      errors.label = "Die primäre Bezeichnung darf nicht leer sein.";
    }

    // label_f
    edited.label_f = fullEditData.label_f.trim() || undefined;

    // label_m
    edited.label_m = fullEditData.label_m.trim() || undefined;

    return {
      ...edited,
      _errors: errors,
      _warnings: warnings,
      _origin:
        editable._currentEditData !== initialEditData &&
        (Object.keys(errors).length ||
          persecutionPretextsDiffer(origin, edited))
          ? origin
          : undefined,
      _currentEditData: fullEditData,
    };
  };

  return withEdit(
    {
      // -> validate origin data
      __typename: "PersecutionPretext",
      ...origin,
      _errors: {},
      _warnings: {},
      _revert: (revertible) =>
        revertible._origin
          ? { ...revertible, ...fromPersecutionPretextP(revertible._origin) }
          : revertible,
      _currentEditData: initialEditData,
      _applyNewEditData: withEdit,
    },
    {}
  );
};

export const fromPersecutionPretextP = <T extends IPersecutionPretextP>(
  persecutionPretextP: T
): T & IPersecutionPretext => wrapOrigin(persecutionPretextP);

export const fromPersecutionPretextFragment = (
  fragment: PersecutionPretextFragment
): IExistingPersecutionPretext => fromPersecutionPretextP(fragment);

export const newPersecutionPretext = (): IPersecutionPretext =>
  wrapOrigin({
    locale: { code: "de" },
    label: "",
    localizations: [],
  });

export const changedPersecutionPretext = <T extends IPersecutionPretext>(
  persecutionPretext: T,
  data: Partial<IPersecutionPretextEditData>
): T =>
  changed<IPersecutionPretextP, IPersecutionPretextEditData, T>(
    persecutionPretext,
    data
  );
