import { OccupationFragment, OccupationInput } from "../backendApi";

import { IEditable, withoutUndefinedProperties, changed } from "./IEditable";
import { ICommittable } from "./ICommittable";
import { IValidationMessages } from "./IValidated";
import { INode, IPotentialNode } from "./INode";
import { ITyped } from "./ITyped";

interface IOccupationL10nEditData {
  locale: string;

  label: string;

  label_f: string;

  label_m: string;
}

export interface IOccupationEditData {
  locale: "de"; // FIXME -> IExistingLocale

  label: string;

  label_f: string;

  label_m: string;

  // TODO: localizations
}

export type IOccupationP = IPotentialNode<OccupationFragment>;

export type IOccupation = IOccupationP &
  ICommittable<IOccupationP, IOccupationEditData> &
  ITyped;

export type IExistingOccupation = IOccupation & INode;

export const toOccupationInput = (
  occupation: IOccupationP
): OccupationInput => ({
  locale: occupation.locale.code,
  label: occupation.label,
  label_f: occupation.label_f || undefined,
  label_m: occupation.label_m || undefined,
  // TODO: localizations
});

const occupationsDiffer = (one: IOccupationP, other: IOccupationP): boolean =>
  JSON.stringify(toOccupationInput(one)) !==
  JSON.stringify(toOccupationInput(other));

const wrapOrigin = <T extends IOccupationP>(origin: T): T & IOccupation => {
  const initialEditData: IOccupationEditData = {
    locale: "de", // FIXME
    label: origin.label,
    label_f: origin.label_f || "",
    label_m: origin.label_m || "",
  };

  const withEdit = <T extends IEditable<IOccupationP, IOccupationEditData>>(
    editable: T,
    data: Partial<IOccupationEditData>
  ): T => {
    // has `origin` and `initialEditData` as closure

    const errors: IValidationMessages = {};

    const warnings: IValidationMessages = {};

    const fullEditData = {
      ...editable._currentEditData,
      ...withoutUndefinedProperties(data),
    };

    const edited = {
      ...editable,
      ...origin, // reset editable values
    };

    // locale
    //edited.locale = fullEditData.locale;

    // label
    const labelValue = fullEditData.label.trim() || undefined;

    if (labelValue) {
      edited.label = labelValue;
    } else {
      errors.label = "Die primäre Bezeichnung darf nicht leer sein.";
    }

    // label_f
    edited.label_f = fullEditData.label_f.trim() || undefined;

    // label_m
    edited.label_m = fullEditData.label_m.trim() || undefined;

    return {
      ...edited,
      _errors: errors,
      _warnings: warnings,
      _origin:
        editable._currentEditData !== initialEditData &&
        (Object.keys(errors).length || occupationsDiffer(origin, edited))
          ? origin
          : undefined,
      _currentEditData: fullEditData,
    };
  };

  return withEdit(
    {
      // -> validate origin data
      __typename: "Occupation",
      ...origin,
      _errors: {},
      _warnings: {},
      _revert: (revertible) =>
        revertible._origin
          ? { ...revertible, ...fromOccupationP(revertible._origin) }
          : revertible,
      _currentEditData: initialEditData,
      _applyNewEditData: withEdit,
    },
    {}
  );
};

export const fromOccupationP = <T extends IOccupationP>(
  occupationP: T
): T & IOccupation => wrapOrigin(occupationP);

export const fromOccupationFragment = (
  fragment: OccupationFragment
): IExistingOccupation => fromOccupationP(fragment);

export const newOccupation = (): IOccupation =>
  wrapOrigin({
    locale: { code: "de" },
    label: "",
    localizations: [],
  });

export const changedOccupation = <T extends IOccupation>(
  occupation: T,
  data: Partial<IOccupationEditData>
): T => changed<IOccupationP, IOccupationEditData, T>(occupation, data);
