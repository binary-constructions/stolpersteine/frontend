// TODO: stricter types for better type safety?

export interface IRevertible<T> {
  // only supposed to be set when there is an origin to go back to _and_ the origin
  // differs from the current state of the revertible object in any meaningful way
  _origin?: T;

  _revert: <U extends IRevertible<T>>(revertible: U) => U;
}

export const isRevertible = (editable: IRevertible<any>): boolean =>
  editable._origin !== undefined;

export const revert = <T extends IRevertible<any>>(revertible: T): T =>
  revertible._revert(revertible);
