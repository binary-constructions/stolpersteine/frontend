import { PlaceFragment, PlaceType, PlaceInput } from "../backendApi";

import { IEditable, withoutUndefinedProperties, changed } from "./IEditable";
import { ICommittable } from "./ICommittable";
import { IValidationMessages } from "./IValidated";
import { INode, IPotentialNode } from "./INode";
import { ITyped } from "./ITyped";

export interface IPlaceEditData {
  label: string;

  parentId: string | null;

  placeType: PlaceType;

  alternativeNames: string[];
}

export type IPlaceP = IPotentialNode<PlaceFragment>;

export type IPlace = IPlaceP & ICommittable<IPlaceP, IPlaceEditData> & ITyped;

export type IExistingPlace = IPlace & INode;

export const toPlaceInput = (place: IPlaceP): PlaceInput => ({
  label: place.label,
  parentId: place.parent?.id,
  //placeType: place.placeType,
  alternativeNames: place.alternativeNames,
});

const placesDiffer = (one: IPlaceP, other: IPlaceP): boolean =>
  JSON.stringify(toPlaceInput(one)) !== JSON.stringify(toPlaceInput(other));

const wrapOrigin = <T extends IPlaceP>(origin: T): T & IPlace => {
  const initialEditData: IPlaceEditData = {
    label: origin.label,
    parentId: origin.parent?.id || null,
    placeType: origin.placeType,
    alternativeNames: origin.alternativeNames,
  };

  const withEdit = <T extends IEditable<IPlaceP, IPlaceEditData>>(
    editable: T,
    data: Partial<IPlaceEditData>
  ): T => {
    // has `origin` and `initialEditData` as closure

    const errors: IValidationMessages = {};

    const warnings: IValidationMessages = {};

    const fullEditData = {
      ...editable._currentEditData,
      ...withoutUndefinedProperties(data),
    };

    const edited = {
      ...editable,
      ...origin, // reset editable values
    };

    // label
    edited.label = fullEditData.label.trim();

    if (!edited.label) {
      errors.label = ""; // required!
    }

    // parentId
    edited.parent =
      fullEditData.parentId !== null ? { id: fullEditData.parentId } : null;

    // placeType
    warnings.placeType = "Wird derzeit nicht gespeichert (TODO)!";

    // alternativeNames
    edited.alternativeNames = fullEditData.alternativeNames
      .map((name) => name.trim())
      .filter((name) => !!name);

    return {
      ...edited,
      _errors: errors,
      _warnings: warnings,
      _origin:
        editable._currentEditData !== initialEditData &&
        (Object.keys(errors).length || placesDiffer(origin, edited))
          ? origin
          : undefined,
      _currentEditData: fullEditData,
    };
  };

  return withEdit(
    {
      // -> validate origin data
      __typename: "Place",
      ...origin,
      _errors: {},
      _warnings: {},
      _revert: (revertible) =>
        revertible._origin
          ? { ...revertible, ...fromPlaceP(revertible._origin) }
          : revertible,
      _currentEditData: initialEditData,
      _applyNewEditData: withEdit,
    },
    {}
  );
};

export const fromPlaceP = <T extends IPlaceP>(placeP: T): T & IPlace =>
  wrapOrigin(placeP);

export const fromPlaceFragment = (fragment: PlaceFragment): IExistingPlace =>
  fromPlaceP(fragment);

export const newPlace = (parentId: string | null = null): IPlace =>
  wrapOrigin({
    label: "",
    parent: parentId ? { id: parentId } : null,
    placeType: PlaceType.Unspecified,
    alternativeNames: [],
  });

export const changedPlace = <T extends IPlace>(
  place: T,
  data: Partial<IPlaceEditData>
): T => changed<IPlaceP, IPlaceEditData, T>(place, data);
