import React, { useState, useContext } from "react";
import { Layout, PageHeader, Input, Button, Select, Card } from "antd";
import {
  InfoOutlined,
  SortAscendingOutlined,
  SortDescendingOutlined,
} from "@ant-design/icons";

import { Config } from "./globals/Config";
import { Sorts } from "./globals/Sorts";
import {
  usePreprocessedSummaries,
  IFilterConfig,
} from "./hooks/usePreprocessedSummaries";
import { ImprintModal } from "./components/ImprintModal";
import { NetworkStatus } from "./components/NetworkStatus";
import { SpotList } from "./components/SpotList";
import { Map } from "./components/Map";
import { SearchAndFilters } from "./components/SearchAndFilters";
import { PoiDrawerController } from "./components/PoiDrawerController";
import { IGeoPoint, newGeoPoint } from "./interfaces/IGeoPoint";
import { ISpot, changedSpot } from "./interfaces/ISpot";
import { isNode } from "./interfaces/INode";
import { isCommitting, isCommittable } from "./interfaces/ICommittable";
import { IAnnotatedSpotSummary } from "./interfaces/IAnnotatedSpotSummary";
import { UserContext } from "./contexts/UserContext";

import "./App.css";

type Sort = keyof typeof Sorts;

interface AppProps {
  networkBusy: boolean;

  networkHadErrors: boolean;

  onShowNetworkErrors: () => void;
}

export const App: React.FC<AppProps> = ({
  networkBusy,
  networkHadErrors,
  onShowNetworkErrors,
}) => {
  const user = useContext(UserContext);

  const [showImprintModal, setShowImprintModal] = useState(false);

  const [filterConfig, setFilterConfig] = useState<IFilterConfig>({
    includeDeleted: false,

    searchTerms: [],

    reviewStatus: undefined,

    placeIds: [],

    withImages: [],

    withImagesInvert: false,
  });

  const summaries = usePreprocessedSummaries(filterConfig);

  const [sort, setSort] = useState<Sort>("place");

  const [reverseSort, setReverseSort] = useState(false);

  const [selectedPoi, setSelectedPoi] = useState<
    undefined | IGeoPoint | IAnnotatedSpotSummary | ISpot
  >();

  const newPoiSelectable =
    !selectedPoi ||
    selectedPoi.__typename !== "Spot" ||
    (isNode(selectedPoi) &&
      !isCommitting(selectedPoi) &&
      !isCommittable(selectedPoi));

  const [edittingSpot, setEdittingSpot] = useState(false);

  return (
    <div className="App">
      <ImprintModal
        visible={showImprintModal}
        onClose={() => setShowImprintModal(false)}
      />
      <Layout style={{ height: "100%", position: "relative" }}>
        <Layout.Sider
          theme="light"
          width={Config.spotListWidth}
          style={{
            height: "100%",
          }}
        >
          <div
            style={{
              height: "100%",
              display: "flex",
              flexDirection: "column",
            }}
          >
            <PageHeader
              style={{ padding: 12 }}
              title="Stolpersteine"
              subTitle="in Brandenburg"
              extra={
                <Button
                  icon={<InfoOutlined />}
                  type="dashed"
                  shape="circle"
                  size="small"
                  onClick={() => setShowImprintModal(true)}
                />
              }
            />
            <SearchAndFilters
              summaries={summaries}
              config={filterConfig}
              onChange={(config) => setFilterConfig(config)}
            />
            <Card
              size="small"
              bordered={true}
              bodyStyle={{
                display: "flex",
                flexDirection: "row",
              }}
            >
              <div
                style={{
                  width: "100%",
                  display: "flex",
                  alignItems: "center",
                }}
              >
                <Input.Group compact style={{ flex: "1 1", display: "flex" }}>
                  <Button
                    disabled={summaries.updating}
                    icon={
                      reverseSort ? (
                        <SortDescendingOutlined />
                      ) : (
                        <SortAscendingOutlined />
                      )
                    }
                    onClick={() => setReverseSort(!reverseSort)}
                  />
                  <Select
                    disabled={summaries.updating}
                    value={sort}
                    onChange={(v: Sort) => setSort(v)}
                    style={{ flex: "1 1" }}
                  >
                    {Object.entries(Sorts).map(([key, value]) => (
                      <Select.Option key={key} value={key}>
                        {value.label}
                      </Select.Option>
                    ))}
                  </Select>
                </Input.Group>
              </div>
            </Card>
            <div style={{ flex: "1 1" }}>
              <SpotList
                spots={summaries.updating ? undefined : summaries.spots}
                sort={sort}
                reverseSort={reverseSort}
                onSelectSpot={
                  newPoiSelectable
                    ? (v) => {
                        setSelectedPoi(v);

                        setEdittingSpot(false);
                      }
                    : undefined
                }
                selectedSpotId={
                  selectedPoi && "id" in selectedPoi
                    ? selectedPoi.id
                    : undefined
                }
              />
            </div>
          </div>
        </Layout.Sider>
        <Layout style={{ position: "relative" }}>
          <NetworkStatus
            busy={networkBusy}
            hasErrors={networkHadErrors}
            onReload={summaries.refetch}
            onShowErrors={onShowNetworkErrors}
          />
          <Map
            spots={summaries.spots}
            selected={selectedPoi}
            onSelect={
              newPoiSelectable
                ? (v) => {
                    setSelectedPoi(v);

                    setEdittingSpot(false);
                  }
                : undefined
            }
            onMove={
              !user || !selectedPoi
                ? undefined
                : selectedPoi.__typename === "GeoPoint"
                ? (v) => setSelectedPoi(newGeoPoint(v))
                : selectedPoi.__typename === "Spot" &&
                  (!isNode(selectedPoi) || edittingSpot)
                ? (v) =>
                    setSelectedPoi(
                      changedSpot(selectedPoi, { point: newGeoPoint(v) })
                    )
                : undefined
            }
          />
        </Layout>
      </Layout>
      <PoiDrawerController
        selected={selectedPoi}
        onUpdate={(v) => setSelectedPoi(v)}
        editToggleState={edittingSpot}
        onEditToggleStateChange={(v) => setEdittingSpot(v)}
        onClose={() => setSelectedPoi(undefined)}
      />
    </div>
  );
};
