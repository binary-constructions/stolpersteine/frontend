import React, { useState } from "react";
import { Card, Input, Switch, Space, Cascader, Select } from "antd";
import {
  FilterOutlined,
  CheckOutlined,
  CloseOutlined,
} from "@ant-design/icons";

import { ReviewStatus } from "../backendApi";
import {
  usePreprocessedSummaries,
  IPlaceOption,
} from "../hooks/usePreprocessedSummaries";
import { IFilterConfig } from "../hooks/usePreprocessedSummaries";
import { labelForReviewStatus } from "../helpers/labelForReviewStatus";

interface SearchAndFiltersConfig {
  summaries: ReturnType<typeof usePreprocessedSummaries>;

  config: IFilterConfig;

  onChange: (config: IFilterConfig) => void;
}

function getChildrenPlaceIds(children: IPlaceOption[]): string[] {
  return [
    ...children.map((v) => v.value),
    ...children.reduce<string[]>(
      (a, c) => [...a, ...getChildrenPlaceIds(c.children)],
      []
    ),
  ];
}

export const SearchAndFilters: React.FC<SearchAndFiltersConfig> = ({
  summaries,
  config,
  onChange,
}) => {
  const [showFilters, setShowFilters] = useState(false);

  return (
    <>
      <Card
        size="small"
        bordered={true}
        bodyStyle={{
          display: "flex",
          flexDirection: "row",
          alignItems: "center",
        }}
      >
        <Input.Search
          placeholder="Suchtext"
          allowClear
          style={{ flex: "1 1" }}
          onSearch={(value, _) => {
            const newSearchTerms = value
              .split(" ")
              .map((v) => v.toLowerCase().trim())
              .filter((v) => !!v);

            if (newSearchTerms.length) {
              onChange({
                ...config,
                searchTerms: newSearchTerms,
              });
            } else if (config.searchTerms.length) {
              onChange({ ...config, searchTerms: [] });
            }
          }}
        />
        <Switch
          style={{ marginLeft: 12 }}
          disabled={summaries.updating}
          checked={showFilters}
          checkedChildren={<FilterOutlined />}
          unCheckedChildren={<FilterOutlined />}
          onChange={(checked, _) => setShowFilters(checked)}
        />
      </Card>
      {showFilters && (
        <Card
          size="small"
          bordered={true}
          bodyStyle={{
            display: "flex",
            flexDirection: "column",
            backgroundColor: "#f5f5f5",
          }}
        >
          <Space direction="vertical" style={{ width: "100%" }}>
            <Cascader
              style={{ width: "100%" }}
              placeholder="Verlegeort"
              options={summaries.placeOptions}
              onChange={(value) => {
                if (!value.length) {
                  onChange({ ...config, placeIds: [] });
                } else {
                  let children = summaries.placeOptions;

                  for (const placeId of value) {
                    console.assert(typeof placeId === "string");

                    const current = children.find((v) => v.value === placeId);

                    if (!current) {
                      throw new Error(
                        "Somehow a non-existant place was selected :("
                      );
                    }

                    children = current.children;
                  }

                  onChange({
                    ...config,
                    placeIds: [
                      value[0] as string,
                      ...getChildrenPlaceIds(children),
                    ],
                  });
                }
              }}
              expandTrigger="hover"
              changeOnSelect
            />
            <Select
              placeholder="Bearbeitungsstatus"
              value={config.reviewStatus}
              style={{ width: "100%" }}
              onChange={(v: ReviewStatus | "") => {
                if (v !== config.reviewStatus) {
                  onChange({
                    ...config,
                    reviewStatus: v || undefined,
                  });
                }
              }}
              disabled={summaries.updating}
            >
              <Select.Option value={""}>- alle -</Select.Option>
              {Object.entries(ReviewStatus).map(([key, value]) => (
                <Select.Option key={key} value={value}>
                  {labelForReviewStatus(value)}
                </Select.Option>
              ))}
            </Select>
            <div style={{ display: "flex", alignItems: "center" }}>
              <Switch
                checked={!config.withImagesInvert}
                onChange={(v) => onChange({ ...config, withImagesInvert: !v })}
                style={{ marginRight: 12 }}
                checkedChildren={<CheckOutlined />}
                unCheckedChildren={<CloseOutlined />}
              />
              <Select
                placeholder="Bilder"
                value={config.withImages}
                style={{ flex: "1 1" }}
                mode="multiple"
                allowClear
                disabled={summaries.updating}
                onChange={(value) => onChange({ ...config, withImages: value })}
              >
                <Select.Option value={"stolperstein"}>
                  Foto vom Stolperstein
                </Select.Option>
                <Select.Option value={"spot"}>Bilder zum Ort</Select.Option>
                <Select.Option value={"person"}>
                  Bilder zur Person
                </Select.Option>
              </Select>
            </div>
          </Space>
        </Card>
      )}
    </>
  );
};
