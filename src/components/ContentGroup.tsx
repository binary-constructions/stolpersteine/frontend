import React from "react";
import { Card, Collapse } from "antd";

import { useDisabled } from "../hooks/useDisabled";

import { ReplaceTransition } from "./ReplaceTransition";

import "./ContentGroup.css";

interface ContentGroupProps {
  visible?: boolean;

  expanded: boolean;

  focused?: boolean; // makes the item non-collapsible and hides the arrow

  disabled?: boolean;

  header: string;

  actions?: {
    icon: React.ReactElement;

    label: string; // TODO: use in a tooltip and for accessibility

    callback: () => void;
  }[];

  onExpandedChange?: () => void; // TODO: show non-clickable cursor if undefined
}

export const ContentGroup: React.FC<ContentGroupProps> = (props) => {
  const {
    visible = true,
    expanded,
    focused = false,
    disabled: propsDisabled = false,
    header,
    actions = [],
    onExpandedChange,
    children,
  } = props;

  const disabled = useDisabled(propsDisabled);

  const actionContent =
    actions.length > 0
      ? actions.map((action, index) => (
          <div
            key={index}
            style={{ marginLeft: "8px" }}
            onClick={(event) => {
              action.callback();

              event.stopPropagation();
            }}
          >
            {action.icon}
          </div>
        ))
      : undefined;

  return (
    <>
      {visible /* TODO: use some kind of appear/disappear transition effect */ && (
        <Collapse
          className="description-group"
          activeKey={focused || expanded ? ["content"] : []}
          onChange={
            onExpandedChange ? (active) => onExpandedChange() : undefined
          }
          bordered={false}
        >
          <Collapse.Panel
            forceRender
            key="content"
            header={header}
            disabled={disabled}
            extra={
              <ReplaceTransition
                showOther={!focused && !expanded}
                one={actionContent}
                other={actionContent}
                keep
              />
            }
            showArrow={!focused}
          >
            <Card
              bordered={false}
              bodyStyle={{ paddingTop: 0, paddingBottom: 0 }}
            >
              {children}
            </Card>
          </Collapse.Panel>
        </Collapse>
      )}
    </>
  );
};
