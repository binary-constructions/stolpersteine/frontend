import React from "react";
import { Alert, Button, Input, Select } from "antd";
import {
  PlusOutlined,
  EllipsisOutlined,
  ArrowRightOutlined,
} from "@ant-design/icons";

import { IExistingPlace } from "../interfaces/IPlace";
import { useDisabled } from "../hooks/useDisabled";

import "./PlaceSelector.css";

const maxParents = 23;

const selectorFor = (
  key: string,
  selection: IExistingPlace[],
  selected: string | null,
  parent: string | null,
  onSelect?: (id: string | null) => void,
  onAdd?: () => void,
  onView?: () => void,
  disabled?: boolean,
  disableId?: string
) => (
  <Input.Group key={key} compact style={{ display: "flex" }}>
    <Select
      style={{ flexGrow: 1, minWidth: 0 }}
      value={selected || ""}
      onChange={
        onSelect ? (value: string) => onSelect(value || parent) : undefined
      }
      dropdownRender={
        onAdd // FIXME: "+"-button press is often not registered (bug in AntD?)
          ? (menu) => (
              <>
                {menu}
                <Button type="dashed" block onClick={onAdd}>
                  <PlusOutlined />
                </Button>
              </>
            )
          : undefined
      }
      disabled={!!disabled}
    >
      <Select.Option value="">
        <EllipsisOutlined />
      </Select.Option>
      {selection.map((place, index) => (
        <Select.Option
          key={place.id}
          disabled={disableId !== undefined && place.id === disableId}
          value={place.id}
        >
          {place.label}
        </Select.Option>
      ))}
    </Select>
    {selected && onView && (
      <Button onClick={onView} disabled={disabled}>
        <ArrowRightOutlined />
      </Button>
    )}
  </Input.Group>
);

interface PlaceSelectorProps {
  id: string | null;

  disableId?: string; // disallows selection of the specified place or sub-places if defined

  available: IExistingPlace[];

  select?: {
    onSelect: (id: string | null) => void;

    onAdd?: (parentId: string | null) => void; // if defined creating new places will be allowed

    onView?: (place: IExistingPlace) => void; // if defined existing places can be opened for inspection
  };
}

export const PlaceSelector: React.FC<PlaceSelectorProps> = (props) => {
  const { id, disableId, available, select } = props;

  const disabled = useDisabled();

  const elements: React.ReactNode[] = [];

  const nextKey = (() => {
    // TODO: -> useKeys()
    const now = Date.now();

    let index = 0;

    return () => now + "-" + index++;
  })();

  if (id !== null) {
    let current = available.find((place) => place.id === id);

    if (!current) {
      throw new Error("Der gewählte Ort wurde nicht gefunden.");
    }

    const children = available.filter(
      (place) => place.parent && place.parent.id === id
    );

    if (children.length) {
      elements.push(
        selectorFor(
          nextKey(),
          children,
          null,
          id,
          select && select.onSelect,
          select && select.onAdd && ((onAdd) => () => onAdd(id))(select.onAdd),
          undefined,
          disabled,
          disableId
        )
      );
    } else if (select && select.onAdd) {
      elements.push(
        <Button
          key="add-child-place-button"
          type="dashed"
          block
          onClick={((onAdd) => () => onAdd(id))(select.onAdd)}
          disabled={disabled}
        >
          <PlusOutlined />
        </Button>
      );
    }

    let parentCount = 0;

    while (current) {
      if (++parentCount > maxParents) {
        throw new Error(
          "Zu viele Eltern-Orte! (Rekursives Eltern-Kind-Verhältnis?)"
        );
      }

      const parentId: string | null = current.parent ? current.parent.id : null;

      const siblings = available.filter((place) =>
        place.parent ? place.parent.id === parentId : parentId === null
      );

      elements.push(
        selectorFor(
          nextKey(),
          siblings,
          current.id,
          parentId,
          select && select.onSelect,
          select &&
            select.onAdd &&
            ((id, onAdd) => () => onAdd(id))(parentId, select.onAdd),
          select &&
            select.onView &&
            ((id, onView) => () => onView(id))(current, select.onView),
          disabled,
          disableId
        )
      );

      if (parentId) {
        const parent: IExistingPlace | undefined = available.find(
          (place: IExistingPlace): boolean => place.id === parentId
        );

        if (parent === undefined) {
          throw new Error(
            "Der gewählte Ort liegt in einem unbekannten Eltern-Ort."
          );
        }

        current = parent;
      } else {
        current = undefined;
      }
    }

    elements.reverse();
  } else {
    const rootPlaces = available.filter((place) => place.parent === null);

    if (rootPlaces.length) {
      elements.push(
        selectorFor(
          nextKey(),
          rootPlaces,
          null,
          null,
          select && select.onSelect,
          select &&
            select.onAdd &&
            ((onAdd) => () => onAdd(null))(select.onAdd),
          undefined,
          disabled,
          disableId
        )
      );
    } else if (select && select.onAdd) {
      elements.push(
        <Button
          key="add-root-place-button"
          type="dashed"
          block
          onClick={((onAdd) => () => onAdd(null))(select.onAdd)}
          disabled={disabled}
        >
          <PlusOutlined /> Neuer Ort
        </Button>
      );
    } else {
      elements.push(
        <Alert
          key="no-places-alert"
          type="warning"
          message="Scheinbar existieren noch keine Orte zum auswählen."
        />
      );
    }
  }

  return <div className="place-select">{elements}</div>;
};
