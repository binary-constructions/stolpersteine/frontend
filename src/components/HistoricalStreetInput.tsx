import React, { useMemo } from "react";
import { AutoComplete, Select, Input, Typography } from "antd";

import {
  IStolperstein,
  changedStolperstein,
} from "../interfaces/IStolperstein";
import { editable } from "../interfaces/IEditable";
import { useDisabled } from "../hooks/useDisabled";

// FIXME: having this whole thing separate from StreetInput is stupid...

import "./StreetInput.css";

export interface HistoricalStreetInputCompletions {
  [id: string]: {
    street: string;

    postalCode?: string;

    place?: {
      id: string;

      label: string;
    };
  };
}

interface HistoricalStreetInputProps<T extends IStolperstein> {
  stolperstein: T;

  onChange?: (stolperstein: T) => void;

  streetCompletions?: HistoricalStreetInputCompletions;

  cornerCompletions?: HistoricalStreetInputCompletions;
}

export const HistoricalStreetInput = <T extends IStolperstein>(
  props: HistoricalStreetInputProps<T>
) => {
  const {
    stolperstein,
    onChange,
    streetCompletions,
    cornerCompletions,
  } = props;

  const disabled = useDisabled();

  const { historicalStreet, historicalStreetDetail } = editable(stolperstein);

  const streetOptions = useMemo(
    () =>
      streetCompletions
        ? Object.entries(streetCompletions).map(([id, match]) => (
            <AutoComplete.Option key={id} value={id}>
              <Typography.Text>{match.street}</Typography.Text> <br />
              {match.postalCode || match.place ? (
                <Typography.Text type="secondary" style={{ fontSize: "small" }}>
                  {match.postalCode && match.place
                    ? match.postalCode + " " + match.place.label
                    : match.postalCode || match.place?.label}
                </Typography.Text>
              ) : null}
            </AutoComplete.Option>
          ))
        : [],
    [streetCompletions]
  );

  return (
    <div className="street-input">
      <AutoComplete
        className="street-input__street"
        disabled={disabled}
        placeholder="Straßename"
        filterOption={false}
        defaultActiveFirstOption={false}
        value={historicalStreet}
        onChange={
          onChange
            ? (value) =>
                onChange(
                  changedStolperstein(stolperstein, {
                    historicalStreet: value.toString(),
                  })
                )
            : undefined
        }
        onSelect={
          onChange
            ? (value) => {
                /* assert() */ if (!streetCompletions)
                  throw new Error("Selected a match from nothing...");

                const selected = streetCompletions[value.toString()];

                onChange(
                  changedStolperstein(stolperstein, {
                    historicalStreet: selected.street,
                    historicalPlaceId: selected.place
                      ? selected.place.id
                      : editable(stolperstein).historicalPlaceId,
                  })
                );
              }
            : undefined
        }
      >
        {streetOptions}
      </AutoComplete>
      <Input.Group compact className="street-input__detail">
        <Select
          disabled={disabled}
          style={{ width: "25%" }}
          defaultValue={
            historicalStreetDetail.__typename === "StreetDetailNumber"
              ? "#"
              : "/"
          }
          onChange={
            onChange
              ? (value: any) => {
                  /* assert() */ if (value !== "/" && value !== "#")
                    throw new Error(
                      "Unrecognized selection for street detail type :("
                    );

                  if (value === "/") {
                    onChange(
                      changedStolperstein(stolperstein, {
                        historicalStreetDetail: {
                          __typename: "StreetDetailCorner",
                          value: "",
                        },
                      })
                    );
                  } else {
                    onChange(
                      changedStolperstein(stolperstein, {
                        historicalStreetDetail: {
                          __typename: "StreetDetailNumber",
                          value: "",
                        },
                      })
                    );
                  }
                }
              : undefined
          }
        >
          <Select.Option value="#">#</Select.Option>
          <Select.Option value="/">/</Select.Option>
        </Select>
        {historicalStreetDetail.__typename === "StreetDetailNumber" ? (
          <Input
            disabled={disabled}
            style={{ width: "75%" }}
            value={historicalStreetDetail.value}
            placeholder="Hausnummer"
            onChange={
              onChange
                ? ({ target: { value } }) =>
                    onChange(
                      changedStolperstein(stolperstein, {
                        historicalStreetDetail: {
                          __typename: "StreetDetailNumber",
                          value,
                        },
                      })
                    )
                : undefined
            }
          />
        ) : (
          <AutoComplete
            disabled={disabled}
            style={{ width: "75%" }}
            value={historicalStreetDetail.value}
            placeholder="Straßename (Ecke)"
            onChange={
              onChange
                ? (value) =>
                    onChange(
                      changedStolperstein(stolperstein, {
                        historicalStreetDetail: {
                          __typename: "StreetDetailCorner",
                          value: value.toString(),
                        },
                      })
                    )
                : undefined
            }
          >
            {
              [
                /* TODO: completions */
              ]
            }
          </AutoComplete>
        )}
      </Input.Group>
    </div>
  );
};
