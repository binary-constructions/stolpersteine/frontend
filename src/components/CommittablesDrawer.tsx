import React, { useContext } from "react";
import { Button, Card, Select, Modal } from "antd";

import { ITrigger } from "../hooks/useTrigger";
import {
  ICommittable,
  isCommitting,
  isCommittable,
  hasCommitError,
  getCommitError,
} from "../interfaces/ICommittable";
import { isRevertible } from "../interfaces/IRevertible";
import { INode, isNode } from "../interfaces/INode";
import { UserContext } from "../contexts/UserContext";

import { Drawer, DrawerSize, DrawerBailStyle } from "./Drawer";

interface CommittablesDrawerProps<
  T extends ICommittable<any, any>,
  U extends T & INode
> {
  visible: boolean;

  title: string;

  primary?: boolean; // close action type is "back" if false (default), "close" otherwise

  modal?: boolean; // defaults to true

  busy?: boolean; // if busy for reasons other than being in commit (e.g. because necessary data is still loading; defaults to false)

  awaitingEdit?: boolean;

  disabled?: boolean;

  size?: DrawerSize; // defaults to small

  committable?: {
    // if undefined the drawer will just display its children (i.e. they will be considered to be in a pre-committable state)
    selected: T;

    select?: {
      selection: U[]; // if the array contains more than one item a selector will be displayed

      toLabel: (option: U) => string;

      onSelect?: (selected: U) => void; // if undefined the selector will be disabled (as it will be anyway when editing)
    };

    edit?: {
      // if defined the drawer will be in edit mode
      onCommit: () => void; // will only be used if committable

      onRevert: () => void; // will only be used if revertible and not committing
    };

    onEditToggle?: () => void; // if undefined the toggle will be disabled; if `selected` is not a node it will not be visible at all

    // a delete button will be displayed if the following is true and the node is not being edited
    delete?: {
      areYouSureText: string;

      callback: () => void;
    };
  };

  onClose: () => void; // will only be used if not revertible or committing

  resetTrigger: ITrigger;

  children?: React.ReactNode;
}

export const CommittablesDrawer = <
  T extends ICommittable<any, any>,
  U extends T & INode
>(
  props: CommittablesDrawerProps<T, U>
) => {
  const {
    visible,
    title,
    primary = false,
    modal = true,
    busy: inputBusy = false,
    awaitingEdit = false,
    disabled: inputDisabled = false,
    size = DrawerSize.Small,
    committable,
    onClose,
    resetTrigger,
    children,
  } = props;

  const user = useContext(UserContext);

  const selectOptions: React.ReactNode[] = [];

  if (committable && committable.select) {
    let index = 0;

    for (const option of committable.select.selection) {
      selectOptions.push(
        <Select.Option key={index} value={option.id}>
          {committable.select.toLabel(option)}
        </Select.Option>
      );

      index++;
    }
  }

  const busy = inputBusy || (committable && isCommitting(committable.selected));

  const disabled = inputDisabled || busy;

  return (
    <Drawer
      title={title}
      visible={visible}
      modal={modal}
      busy={busy}
      awaitingEdit={awaitingEdit}
      disabled={disabled}
      size={size}
      bailStyles={{
        one: primary ? DrawerBailStyle.Close : DrawerBailStyle.Back,
        other: DrawerBailStyle.Revert,
      }}
      bailStyleIsOther={committable && isRevertible(committable.selected)}
      onBail={
        !committable
          ? onClose
          : busy || isCommitting(committable.selected)
          ? undefined
          : committable.edit && isRevertible(committable.selected)
          ? committable.edit.onRevert
          : onClose
      }
      edit={
        user && committable && isNode(committable.selected)
          ? {
              active: !!committable.edit,
              onToggle:
                !busy &&
                !isCommitting(committable.selected) &&
                !isRevertible(committable.selected)
                  ? committable.onEditToggle
                  : undefined,
            }
          : undefined
      }
      errorMessage={
        committable && committable.edit && hasCommitError(committable.selected)
          ? getCommitError(committable.selected)?.message ||
            "Speichern fehlgeschlagen! Besteht eine Internet-Verbindung?"
          : undefined
      }
      actionAreaContent={
        !user ? undefined : committable && committable.edit ? (
          <Button
            type="primary"
            style={{ width: "100%" }}
            disabled={
              awaitingEdit || disabled || !isCommittable(committable.selected)
            }
            loading={isCommitting(committable.selected)}
            onClick={
              !busy && isCommittable(committable.selected)
                ? committable.edit.onCommit
                : undefined
            }
          >
            {committable.selected.id === undefined ? "Erstellen" : "Speichern"}
          </Button>
        ) : committable && committable.delete ? (
          <Button
            danger
            style={{ width: "100%" }}
            loading={isCommitting(committable.selected)}
            disabled={!!busy}
            onClick={() =>
              Modal.confirm({
                title:
                  committable.delete?.areYouSureText ||
                  "Das Objekt wirklich löschen?",
                okText: "Löschen",
                okType: "danger",
                cancelText: "Abbrechen",
                onOk: () =>
                  committable.delete
                    ? committable.delete.callback()
                    : undefined,
              })
            }
          >
            Löschen
          </Button>
        ) : undefined
      }
      resetTrigger={resetTrigger}
    >
      {committable &&
      isNode(committable.selected) &&
      committable.select &&
      selectOptions.length > 1 ? (
        <Card bordered={false}>
          <Select
            value={committable.selected.id}
            onChange={
              committable.select.onSelect &&
              ((key: string) => {
                /* assert() */ if (!committable.select)
                  throw new Error("Where did the selection go?");

                const selected = committable.select.selection.find(
                  (option) => option.id === key
                );

                /* assert() */ if (!selected)
                  throw new Error("Where did the selected item go?");

                /* assert() */ if (!committable.select.onSelect)
                  throw new Error("The selector should be disabled currently.");

                committable.select.onSelect(selected);
              })
            }
            disabled={
              awaitingEdit ||
              disabled ||
              !committable.select.onSelect ||
              isCommitting(committable.selected) ||
              !!committable.edit
            }
            style={{ width: "100%" }}
          >
            {selectOptions}
          </Select>
        </Card>
      ) : undefined}
      {children}
    </Drawer>
  );
};
