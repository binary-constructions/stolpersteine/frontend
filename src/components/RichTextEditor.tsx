import React, { useState, useEffect } from "react";
import ReactQuill, { Quill } from "react-quill";

import { useComponentId } from "../hooks/useComponentId";

import "react-quill/dist/quill.snow.css";

import "./RichTextEditor.css";

const icons = Quill.import("ui/icons");

icons["bold"] =
  '<svg viewBox="64 64 896 896" focusable="false" class="" data-icon="bold" width="1em" height="1em" fill="currentColor" aria-hidden="true"><path d="M697.8 481.4c33.6-35 54.2-82.3 54.2-134.3v-10.2C752 229.3 663.9 142 555.3 142H259.4c-15.1 0-27.4 12.3-27.4 27.4v679.1c0 16.3 13.2 29.5 29.5 29.5h318.7c117 0 211.8-94.2 211.8-210.5v-11c0-73-37.4-137.3-94.2-175.1zM328 238h224.7c57.1 0 103.3 44.4 103.3 99.3v9.5c0 54.8-46.3 99.3-103.3 99.3H328V238zm366.6 429.4c0 62.9-51.7 113.9-115.5 113.9H328V542.7h251.1c63.8 0 115.5 51 115.5 113.9v10.8z"></path></svg>';
icons["italic"] =
  '<svg viewBox="64 64 896 896" focusable="false" class="" data-icon="italic" width="1em" height="1em" fill="currentColor" aria-hidden="true"><path d="M798 160H366c-4.4 0-8 3.6-8 8v64c0 4.4 3.6 8 8 8h181.2l-156 544H229c-4.4 0-8 3.6-8 8v64c0 4.4 3.6 8 8 8h432c4.4 0 8-3.6 8-8v-64c0-4.4-3.6-8-8-8H474.4l156-544H798c4.4 0 8-3.6 8-8v-64c0-4.4-3.6-8-8-8z"></path></svg>';
icons["link"] =
  '<svg viewBox="64 64 896 896" focusable="false" class="" data-icon="link" width="1em" height="1em" fill="currentColor" aria-hidden="true"><path d="M574 665.4a8.03 8.03 0 00-11.3 0L446.5 781.6c-53.8 53.8-144.6 59.5-204 0-59.5-59.5-53.8-150.2 0-204l116.2-116.2c3.1-3.1 3.1-8.2 0-11.3l-39.8-39.8a8.03 8.03 0 00-11.3 0L191.4 526.5c-84.6 84.6-84.6 221.5 0 306s221.5 84.6 306 0l116.2-116.2c3.1-3.1 3.1-8.2 0-11.3L574 665.4zm258.6-474c-84.6-84.6-221.5-84.6-306 0L410.3 307.6a8.03 8.03 0 000 11.3l39.7 39.7c3.1 3.1 8.2 3.1 11.3 0l116.2-116.2c53.8-53.8 144.6-59.5 204 0 59.5 59.5 53.8 150.2 0 204L665.3 562.6a8.03 8.03 0 000 11.3l39.8 39.8c3.1 3.1 8.2 3.1 11.3 0l116.2-116.2c84.5-84.6 84.5-221.5 0-306.1zM610.1 372.3a8.03 8.03 0 00-11.3 0L372.3 598.7a8.03 8.03 0 000 11.3l39.6 39.6c3.1 3.1 8.2 3.1 11.3 0l226.4-226.4c3.1-3.1 3.1-8.2 0-11.3l-39.5-39.6z"></path></svg>';
icons["list"]["bullet"] =
  '<svg viewBox="64 64 896 896" focusable="false" class="" data-icon="unordered-list" width="1em" height="1em" fill="currentColor" aria-hidden="true"><path d="M912 192H328c-4.4 0-8 3.6-8 8v56c0 4.4 3.6 8 8 8h584c4.4 0 8-3.6 8-8v-56c0-4.4-3.6-8-8-8zm0 284H328c-4.4 0-8 3.6-8 8v56c0 4.4 3.6 8 8 8h584c4.4 0 8-3.6 8-8v-56c0-4.4-3.6-8-8-8zm0 284H328c-4.4 0-8 3.6-8 8v56c0 4.4 3.6 8 8 8h584c4.4 0 8-3.6 8-8v-56c0-4.4-3.6-8-8-8zM104 228a56 56 0 10112 0 56 56 0 10-112 0zm0 284a56 56 0 10112 0 56 56 0 10-112 0zm0 284a56 56 0 10112 0 56 56 0 10-112 0z"></path></svg>';
icons["blockquote"] =
  '<svg viewBox="64 64 896 896" focusable="false" class="" data-icon="double-right" width="1em" height="1em" fill="currentColor" aria-hidden="true"><path d="M533.2 492.3L277.9 166.1c-3-3.9-7.7-6.1-12.6-6.1H188c-6.7 0-10.4 7.7-6.3 12.9L447.1 512 181.7 851.1A7.98 7.98 0 00188 864h77.3c4.9 0 9.6-2.3 12.6-6.1l255.3-326.1c9.1-11.7 9.1-27.9 0-39.5zm304 0L581.9 166.1c-3-3.9-7.7-6.1-12.6-6.1H492c-6.7 0-10.4 7.7-6.3 12.9L751.1 512 485.7 851.1A7.98 7.98 0 00492 864h77.3c4.9 0 9.6-2.3 12.6-6.1l255.3-326.1c9.1-11.7 9.1-27.9 0-39.5z"></path></svg>';

interface RichTextEditorProps {
  value: string;

  /**
   * The second argument will only be provided if focus is false.
   *
   * Editing will be disabled if this is undefined.
   */
  onFocusChange?: (focus: boolean, value?: string) => void;

  placeholder: string;
}

export const RichTextEditor: React.FC<RichTextEditorProps> = ({
  value,
  onFocusChange,
  placeholder,
}) => {
  const [state, setState] = useState({
    liveValue: value,
    focus: false,
  });

  useEffect(() => {
    setState((v) => ({ ...v, liveValue: value }));
  }, [value]);

  const componentId = useComponentId();

  const classes = ["rich-text-editor"];

  if (!onFocusChange) {
    classes.push("rich-text-editor--disabled");
  } else {
    classes.push("rich-text-editor--enabled");

    if (state.focus) {
      classes.push("rich-text-editor--focus");
    }
  }

  return (
    <div id={componentId} className={classes.join(" ")}>
      <ReactQuill
        value={state.liveValue}
        bounds={componentId ? "#" + componentId : undefined}
        theme="snow"
        readOnly={!onFocusChange}
        placeholder={placeholder}
        modules={{
          toolbar: [["bold", "italic", "link", { list: "bullet" }]], // TODO: add "blockquote"?
        }}
        formats={["bold", "italic", "link", "list"]} // TODO: add "blockquote"?
        onChange={(value) => setState((v) => ({ ...v, liveValue: value }))}
        onFocus={() => {
          setState((v) => ({ ...v, focus: true }));

          if (onFocusChange) {
            onFocusChange(true);
          }
        }}
        onBlur={() => {
          setState((v) => ({ ...v, focus: false }));

          if (onFocusChange) {
            onFocusChange(false, state.liveValue);
          }
        }}
      />
    </div>
  );
};
