import React from "react";
import { Card } from "antd";

import { ITrigger } from "../hooks/useTrigger";
import { IOccupation, IExistingOccupation } from "../interfaces/IOccupation";
import { revert } from "../interfaces/IRevertible";
import { CommittablesDrawer } from "./CommittablesDrawer";
import { Description } from "./Description";
import { OccupationDescription } from "./OccupationDescription";

interface OccupationsDrawerProps {
  selected: IOccupation;

  visible: boolean;

  edit?: {
    onChange: (changed: IOccupation) => void;

    onCommit: () => void;
  };

  select?: {
    selection: IExistingOccupation[];

    onSelect?: (selected: IExistingOccupation) => void;
  };

  onEditToggle?: () => void;

  onClose: () => void;

  resetTrigger: ITrigger;
}

export const OccupationsDrawer: React.FC<OccupationsDrawerProps> = (props) => {
  const {
    selected,
    visible,
    edit,
    select,
    onEditToggle,
    onClose,
    resetTrigger,
  } = props;

  return (
    <CommittablesDrawer<IOccupation, IExistingOccupation>
      visible={visible}
      title="Beruf"
      committable={{
        selected,
        select: select && {
          selection: select.selection,
          toLabel: (option) => option.label,
          onSelect: select.onSelect,
        },
        edit: edit && {
          onCommit: edit.onCommit,
          onRevert: () => edit.onChange(revert(selected)),
        },
        onEditToggle,
      }}
      onClose={onClose}
      resetTrigger={resetTrigger}
    >
      {selected && (
        <Description>
          <Card bordered={false}>
            <OccupationDescription
              subject={selected}
              onChange={edit && edit.onChange}
            />
          </Card>
        </Description>
      )}
    </CommittablesDrawer>
  );
};
