import React from "react";

// A placeholder "transition" to use until there was time to polish animations.

interface AppearTransitionProps {
  type?: "squish" | "fade";

  show: boolean;

  delayed?: boolean;

  keep?: boolean; // keep last visible children around until fully closed
}

export const AppearTransition: React.FC<AppearTransitionProps> = ({
  show = false,
  children,
}) => <>{show && children}</>;
