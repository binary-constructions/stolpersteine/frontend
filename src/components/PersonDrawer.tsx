import React, { useState, useContext } from "react";
import { PlusOutlined } from "@ant-design/icons";

import { ITrigger } from "../hooks/useTrigger";
import { IPerson, changedPerson } from "../interfaces/IPerson";
import { editable } from "../interfaces/IEditable";
import { revert } from "../interfaces/IRevertible";
import { isNode } from "../interfaces/INode";
import { isContent } from "../interfaces/IContent";
import { UserContext } from "../contexts/UserContext";

import { CommittablesDrawer } from "./CommittablesDrawer";
import { DrawerSize } from "./Drawer";
import { RichTextEditor } from "./RichTextEditor";
import { ContentGroup } from "./ContentGroup";
import { Description } from "./Description";
import { PersonGeneralDescription } from "./PersonGeneralDescription";
import { PersecutionDescription } from "./PersecutionDescription";
import { RelationshipsList } from "./RelationshipsList";
import { ImagesList } from "./ImagesList";

export enum PersonDrawerContentGroup {
  GENERAL = "general",
  PERSECUTION = "persecution",
  BIOGRAPHY = "biography",
  RELATIONSHIPS = "relationships",
  IMAGES = "images",
  NOTES = "notes",
}

interface PersonDrawerProps {
  selected: IPerson;

  visible: boolean;

  activeContentGroup: PersonDrawerContentGroup;

  onActivateContentGroup: (group: PersonDrawerContentGroup) => void;

  edit?: {
    // if defined the drawer will be in edit mode
    onChange: (changed: IPerson) => void;

    onCommit: () => void;
  };

  onEditToggle?: () => void;

  onImagesChanged?: (imageIds: string[]) => void;

  onClose: () => void;

  resetTrigger: ITrigger;
}

export const PersonDrawer: React.FC<PersonDrawerProps> = ({
  selected,
  visible,
  activeContentGroup,
  onActivateContentGroup,
  edit,
  onEditToggle,
  onImagesChanged,
  onClose,
  resetTrigger,
}) => {
  const user = useContext(UserContext);

  const { biographyHTML, editingNotesHTML } = editable(selected);

  // If true the following means that an edit might be incoming that
  // has not yet been propagated by on to .onChange().
  const [inputFocused, setInputFocused] = useState(false);

  const [addRelationship, setAddRelationship] = useState(false);

  const [subDrawerOpen, setSubDrawerOpen] = useState(false);

  const loading = false; /* FIXME */

  const [createImage, setCreateImage] = useState(false);

  const disabled = subDrawerOpen || loading;

  return (
    <CommittablesDrawer
      visible={visible}
      title="Person"
      size={DrawerSize.Medium}
      busy={loading}
      awaitingEdit={inputFocused}
      disabled={disabled}
      committable={{
        selected,
        edit: edit && {
          onCommit: edit.onCommit,
          onRevert: () => edit.onChange(revert(selected)),
        },
        onEditToggle:
          !loading &&
          !disabled &&
          (activeContentGroup === PersonDrawerContentGroup.GENERAL ||
            activeContentGroup === PersonDrawerContentGroup.PERSECUTION ||
            activeContentGroup === PersonDrawerContentGroup.BIOGRAPHY ||
            activeContentGroup === PersonDrawerContentGroup.NOTES)
            ? onEditToggle
            : undefined,
      }}
      onClose={onClose}
      resetTrigger={resetTrigger}
    >
      <ContentGroup
        key={PersonDrawerContentGroup.GENERAL}
        header="Allgemein"
        expanded={activeContentGroup === PersonDrawerContentGroup.GENERAL}
        onExpandedChange={() =>
          onActivateContentGroup(PersonDrawerContentGroup.GENERAL)
        }
      >
        <Description horizontal={true}>
          <PersonGeneralDescription
            person={selected}
            onChange={edit ? edit.onChange : undefined}
          />
        </Description>
      </ContentGroup>
      <ContentGroup
        key={PersonDrawerContentGroup.PERSECUTION}
        header="Verfolgung"
        expanded={activeContentGroup === PersonDrawerContentGroup.PERSECUTION}
        onExpandedChange={() =>
          onActivateContentGroup(PersonDrawerContentGroup.PERSECUTION)
        }
      >
        <Description horizontal={true}>
          <PersecutionDescription
            person={selected}
            onChange={edit ? edit.onChange : undefined}
            onLoadingStateChange={(_) =>
              console.debug("TODO: handle PersecutionPretext load status")
            }
            gender={selected.gender}
          />
        </Description>
      </ContentGroup>
      <ContentGroup
        key={PersonDrawerContentGroup.BIOGRAPHY}
        header="Biographie"
        expanded={activeContentGroup === PersonDrawerContentGroup.BIOGRAPHY}
        onExpandedChange={() =>
          onActivateContentGroup(PersonDrawerContentGroup.BIOGRAPHY)
        }
      >
        <RichTextEditor
          value={biographyHTML}
          onFocusChange={
            edit
              ? (focus, value) => {
                  if (focus) {
                    setInputFocused(true);
                  } else if (value !== undefined) {
                    setInputFocused(false);

                    edit.onChange(
                      changedPerson(selected, { biographyHTML: value })
                    );
                  }
                }
              : undefined
          }
          placeholder="Noch kein Biographie-Text."
        />
      </ContentGroup>
      <ContentGroup
        key={PersonDrawerContentGroup.NOTES}
        header="Bearbeitungsnotizen"
        expanded={activeContentGroup === PersonDrawerContentGroup.NOTES}
        onExpandedChange={() =>
          onActivateContentGroup(PersonDrawerContentGroup.NOTES)
        }
      >
        <RichTextEditor
          value={editingNotesHTML}
          onFocusChange={
            edit
              ? (focus, value) => {
                  if (focus) {
                    setInputFocused(true);
                  } else if (value !== undefined) {
                    setInputFocused(false);

                    edit.onChange(
                      changedPerson(selected, { editingNotesHTML: value })
                    );
                  }
                }
              : undefined
          }
          placeholder="Keine Bearbeitungsnotiz."
        />
      </ContentGroup>
      <ContentGroup
        key={PersonDrawerContentGroup.RELATIONSHIPS}
        header="Beziehungen"
        visible={isContent(selected)}
        expanded={activeContentGroup === PersonDrawerContentGroup.RELATIONSHIPS}
        disabled={!!edit}
        actions={
          user &&
          !edit &&
          activeContentGroup === PersonDrawerContentGroup.RELATIONSHIPS
            ? [
                {
                  icon: <PlusOutlined />,
                  label: "Beziehung hinzufügen",
                  callback: () => setAddRelationship(true),
                },
              ]
            : undefined
        }
        onExpandedChange={() =>
          onActivateContentGroup(PersonDrawerContentGroup.RELATIONSHIPS)
        }
      >
        {isContent(selected) && (
          <RelationshipsList
            person={selected}
            create={
              addRelationship
                ? { onDone: () => setAddRelationship(false) }
                : undefined
            }
            emptyListText="Es sind noch keine Beziehungen zu dieser Person gespeichert."
          />
        )}
      </ContentGroup>
      <ContentGroup
        key={PersonDrawerContentGroup.IMAGES}
        header="Bilder"
        visible={isNode(selected)}
        expanded={activeContentGroup === PersonDrawerContentGroup.IMAGES}
        disabled={!!edit}
        actions={
          user &&
          !edit &&
          activeContentGroup === PersonDrawerContentGroup.IMAGES &&
          onImagesChanged
            ? [
                {
                  icon: <PlusOutlined />,
                  label: "Bild hinzufügen",
                  callback: () => setCreateImage(true),
                },
              ]
            : undefined
        }
        onExpandedChange={() =>
          onActivateContentGroup(PersonDrawerContentGroup.IMAGES)
        }
      >
        {isContent(selected) ? (
          <ImagesList
            imageIds={selected.imageIds}
            create={createImage}
            onAbortCreate={() => setCreateImage(false)}
            onChange={
              !onImagesChanged
                ? undefined
                : (imageIds) => {
                    setCreateImage(false);

                    onImagesChanged(imageIds);
                  }
            }
            emptyListText="Es sind noch keine Bilder zu dieser Person gespeichert."
            columns={2}
          />
        ) : undefined}
      </ContentGroup>
    </CommittablesDrawer>
  );
};
