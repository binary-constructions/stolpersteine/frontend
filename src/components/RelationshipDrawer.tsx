import React, { useState } from "react";
import { Alert, Button, Card, Form } from "antd";

import {
  IRelationshipType,
  IExistingRelationshipType,
  newRelationshipType,
} from "../interfaces/IRelationshipType";
import { IExistingPerson } from "../interfaces/IPerson";
import { isNode } from "../interfaces/INode";
import { hasCommitError } from "../interfaces/ICommittable";
import { displayName } from "../helpers/displayName";
import { usePersons } from "../hooks/usePersons";
import { useRelationshipTypes } from "../hooks/useRelationshipTypes";
import { useTrigger, ITrigger } from "../hooks/useTrigger";
import { nodeStoreGetAvailable } from "../helpers/nodeStoreGetAvailable";
import { nodeStoreFetch } from "../helpers/nodeStoreFetch";

import { Drawer, DrawerBailStyle } from "./Drawer";
import { Incoming } from "./Incoming";
import { RelationshipTypesDrawer } from "./RelationshipTypesDrawer";
import { NodeSelector } from "./NodeSelector";
import { RelationshipTypeSelector } from "./RelationshipTypeSelector";

interface RelationshipDrawerProps {
  filterPersonId: string;

  selectedPerson?: IExistingPerson;

  selectedType?: IExistingRelationshipType;

  inverted?: boolean;

  visible: boolean;

  onChange: (
    person: IExistingPerson | undefined,
    type: IExistingRelationshipType | undefined,
    inverted: boolean
  ) => void;

  onCommit?: () => void;

  onRevert?: () => void;

  onClose: () => void;

  isCommitting?: boolean;

  commitError?: boolean;

  isNew?: boolean;

  resetTrigger: ITrigger;
}

export const RelationshipDrawer: React.FC<RelationshipDrawerProps> = (
  props
) => {
  const {
    filterPersonId,
    selectedPerson,
    selectedType,
    inverted = false,
    visible,
    onChange,
    onCommit,
    onRevert,
    onClose,
    isCommitting = false,
    commitError = false,
    isNew = false,
    resetTrigger,
  } = props;

  const persons = usePersons(); // FIXME: should use incremental loading!

  const availablePersons = nodeStoreGetAvailable(persons);

  const relationshipTypes = useRelationshipTypes();

  const availableRelationshipTypes = nodeStoreGetAvailable(relationshipTypes);

  /* relationship type drawer */

  const [relationshipTypeDrawer, setRelationshipTypeDrawer] = useState<
    | undefined
    | {
        relationshipType: IRelationshipType;

        open: boolean;

        edit?: boolean;
      }
  >();

  const relationshipTypeDrawerResetTrigger = useTrigger();

  relationshipTypeDrawerResetTrigger.listen(() =>
    setRelationshipTypeDrawer(undefined)
  );

  return (
    <Drawer
      title="Beziehung"
      visible={visible}
      modal={true}
      bailStyles={{
        one: DrawerBailStyle.Back,
        other: DrawerBailStyle.Revert,
      }}
      bailStyleIsOther={!!onRevert}
      onBail={!!onRevert ? onRevert : onClose}
      errorMessage={
        commitError
          ? "Anlegen der Beziehung fehlgeschlagen! Besteht eine Internet-Verbindung?"
          : undefined
      }
      actionAreaContent={
        <Button
          type="primary"
          style={{ width: "100%" }}
          disabled={!onCommit}
          loading={isCommitting}
          onClick={!isCommitting && onCommit ? onCommit : undefined}
        >
          {isNew ? "Anlegen" : "Speichern"}
        </Button>
      }
      resetTrigger={resetTrigger}
    >
      <Card bordered={false}>
        <Form
          labelAlign="left"
          labelCol={{ xs: { span: 24 } }}
          wrapperCol={{ xs: { span: 24 } }}
          style={{ textAlign: "left" }}
        >
          <Form.Item
            label="Person"
            required={true}
            validateStatus={!selectedPerson ? "error" : undefined}
          >
            <NodeSelector
              selected={selectedPerson}
              available={availablePersons.filter(
                (x) => x.id !== filterPersonId
              )}
              toLabel={(person) => displayName(person.names) || "kein Name :("}
              onChange={(selected) =>
                onChange(selected, selectedType, inverted)
              }
            />
          </Form.Item>
          <Form.Item
            label="Ist bzw. war"
            required={true}
            validateStatus={!selectedType ? "error" : undefined}
          >
            <RelationshipTypeSelector
              gender={selectedPerson ? selectedPerson.gender : undefined}
              selected={selectedType}
              inverted={inverted}
              available={availableRelationshipTypes}
              onChange={(selectedType, inverted) =>
                onChange(selectedPerson, selectedType, !!inverted)
              }
              onView={
                selectedType
                  ? () =>
                      setRelationshipTypeDrawer({
                        relationshipType: selectedType,
                        open: true,
                        edit: false,
                      })
                  : undefined
              }
              onCreate={() =>
                setRelationshipTypeDrawer({
                  relationshipType: newRelationshipType(),
                  open: true,
                })
              }
            />
          </Form.Item>
        </Form>
      </Card>
      {relationshipTypeDrawer && (
        <RelationshipTypesDrawer
          selected={relationshipTypeDrawer.relationshipType}
          visible={relationshipTypeDrawer.open}
          edit={
            !isNode(relationshipTypeDrawer.relationshipType) ||
            relationshipTypeDrawer.edit
              ? {
                  onChange: (changed) =>
                    setRelationshipTypeDrawer({
                      ...relationshipTypeDrawer,
                      relationshipType: changed,
                    }),
                  onCommit: () => {
                    if (!isNode(relationshipTypeDrawer.relationshipType)) {
                      setRelationshipTypeDrawer({
                        ...relationshipTypeDrawer,
                        relationshipType: relationshipTypes.create(
                          relationshipTypeDrawer.relationshipType,
                          (created) => {
                            setRelationshipTypeDrawer({
                              ...relationshipTypeDrawer,
                              relationshipType: created,
                              open: hasCommitError(created),
                            });

                            if (!hasCommitError(created)) {
                              /* assert() */ if (!isNode(created))
                                throw new Error(
                                  "Object should have an id now."
                                );

                              relationshipTypes.refresh();
                            }
                          }
                        ),
                      });
                    } else {
                      setRelationshipTypeDrawer({
                        ...relationshipTypeDrawer,
                        relationshipType: relationshipTypes.update(
                          relationshipTypeDrawer.relationshipType,
                          (updated) => {
                            setRelationshipTypeDrawer({
                              ...relationshipTypeDrawer,
                              relationshipType: updated,
                              open: hasCommitError(updated),
                              edit: hasCommitError(updated),
                            });

                            if (!hasCommitError(updated)) {
                              relationshipTypes.refresh();
                            }
                          }
                        ),
                      });
                    }
                  },
                }
              : undefined
          }
          onEditToggle={
            relationshipTypeDrawer.edit !== undefined
              ? () =>
                  setRelationshipTypeDrawer({
                    ...relationshipTypeDrawer,
                    edit: !relationshipTypeDrawer.edit,
                  })
              : undefined
          }
          onClose={() =>
            setRelationshipTypeDrawer({
              ...relationshipTypeDrawer,
              open: false,
            })
          }
          resetTrigger={relationshipTypeDrawerResetTrigger}
        />
      )}
    </Drawer>
  );
};
