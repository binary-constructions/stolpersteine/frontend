import React from "react";
import { Modal, Image } from "antd";

interface ImprintModalProps {
  visible: boolean;

  onClose: () => void;
}

export const ImprintModal: React.FC<ImprintModalProps> = ({
  visible,
  onClose,
}) => (
  <Modal
    title="Impressum"
    visible={visible}
    onCancel={onClose}
    width={620}
    footer={null}
  >
    <p>
      <strong>Verantwortlich für diese Website:</strong>
    </p>

    <p style={{ paddingLeft: "1em" }}>
      Aktionsbündnis gegen Gewalt, Rechtsextremismus und Fremdenfeindlichkeit
      <br />
      Frauke Büttner
      <br />
      Mittelstr. 38/39
      <br />
      14467 Potsdam
    </p>

    <p>
      <strong>Kontakt:</strong>
    </p>

    <p style={{ paddingLeft: "1em" }}>
      E-Mail: kontakt@aktionsbuendnis-brandenburg.de
      <br />
      Telefon: 0331-50582427
      <br />
      Fax: 0331-50582429
    </p>

    <div
      style={{
        width: "50%",
        textAlign: "center",
        marginTop: 48,
        marginBottom: 24,
        float: "left",
      }}
    >
      <Image width="50%" src="./mwfk-brandenburg-logo.png" />
    </div>

    <p style={{ width: "50%", float: "right", marginTop: 48 }}>
      Gefördert mit Mitteln des Ministeriums für Wissenschaft, Forschung und
      Kultur des Landes Brandenburg.
    </p>

    <div style={{ clear: "both" }} />
  </Modal>
);
