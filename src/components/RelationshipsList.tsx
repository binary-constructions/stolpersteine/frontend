import React, { useState, useEffect, useContext } from "react";
import { Alert, Spin, List, message, Modal } from "antd";
import { EditOutlined, DeleteOutlined } from "@ant-design/icons";

import { useRelationshipsOf } from "../hooks/useRelationshipsOf";
import { useStateMonitor } from "../hooks/useStateMonitor";
import { useTrigger } from "../hooks/useTrigger";
import { displayName } from "../helpers/displayName";
import { displayGendered } from "../helpers/displayGendered";
import { IExistingRelationshipType } from "../interfaces/IRelationshipType";
import { IExistingPerson } from "../interfaces/IPerson";
import { UserContext } from "../contexts/UserContext";

import { Incoming } from "./Incoming";
import { RelationshipDrawer } from "./RelationshipDrawer";
import { Nothing } from "./Nothing";

const DEFAULT_EMPTY_LIST_TEXT = "Keine passenden Beziehungen gefunden.";

interface RelationshipsListProps {
  person: IExistingPerson;

  create?: {
    // if defined a drawer will open for creation of a new relationship
    onDone: () => void; // either committed or aborted
  };

  onDrawerStateChange?: (open: boolean) => void;

  emptyListText?: string;
}

export const RelationshipsList: React.FC<RelationshipsListProps> = ({
  person,
  create,
  onDrawerStateChange,
  emptyListText = DEFAULT_EMPTY_LIST_TEXT,
}) => {
  const user = useContext(UserContext);

  const relationshipsOf = useRelationshipsOf(person.id);

  /* relationship drawer */

  const [relationshipDrawer, setRelationshipDrawer] = useState<
    | undefined
    | {
        origin: {
          other?: IExistingPerson;

          type?: IExistingRelationshipType;

          inverted: boolean;
        };

        other?: IExistingPerson;

        type?: IExistingRelationshipType;

        inverted: boolean;

        isNew?: boolean;

        isCommitting?: boolean;

        error?: Error;

        open: boolean;
      }
  >();

  useEffect(() => {
    if (create && (!relationshipDrawer || !relationshipDrawer.open)) {
      setRelationshipDrawer({
        origin: { inverted: false },
        isNew: true,
        inverted: false,
        open: true,
      });
    } else if (
      !create &&
      relationshipDrawer &&
      relationshipDrawer.isNew &&
      relationshipDrawer.open
    ) {
      setRelationshipDrawer({ ...relationshipDrawer, open: false });
    }
  }, [create, relationshipDrawer]);

  useStateMonitor(
    relationshipDrawer && relationshipDrawer.open,
    onDrawerStateChange
  );

  let relationshipItems: undefined | Error | React.ReactNode[];

  if (
    !relationshipsOf.relationships ||
    relationshipsOf.relationships instanceof Error
  ) {
    relationshipItems = relationshipsOf.relationships;
  } else {
    relationshipItems = [
      ...relationshipsOf.relationships.asSubject.map((v, i) => (
        <List.Item
          key={"subject-" + i}
          actions={
            !user || !v || v instanceof Error
              ? undefined
              : [
                  <EditOutlined
                    onClick={() =>
                      setRelationshipDrawer({
                        origin: {
                          other: v.object,
                          type: v.type,
                          inverted: false,
                        },
                        other: v.object,
                        type: v.type,
                        inverted: false,
                        open: true,
                      })
                    }
                  />,
                  <DeleteOutlined
                    onClick={() =>
                      Modal.confirm({
                        title: "Die Beziehung wirklich löschen?",
                        okText: "Löschen",
                        okType: "danger",
                        cancelText: "Abbrechen",
                        onOk: () =>
                          relationshipsOf.unset(person, v.object, (error) => {
                            if (error) {
                              message.error(error.message);
                            } else {
                              message.success("Die Beziehung wurde entfernt.");
                            }
                          }),
                      })
                    }
                  />,
                ]
          }
        >
          {!v ? (
            <Spin />
          ) : v instanceof Error ? (
            <Alert type="error" message={v.message} />
          ) : (
            <List.Item.Meta
              title={displayName(v.object.names)}
              description={displayGendered(
                v.object.gender,
                v.type.objectLabel,
                v.type.objectLabel_f,
                v.type.objectLabel_m
              )}
            />
          )}
        </List.Item>
      )),
      ...relationshipsOf.relationships.asObject.map((v, i) => (
        <List.Item
          key={"object-" + i}
          actions={
            !user || !v || v instanceof Error
              ? undefined
              : [
                  <EditOutlined
                    onClick={() =>
                      setRelationshipDrawer({
                        origin: {
                          other: v.subject,
                          type: v.type,
                          inverted: true,
                        },
                        other: v.subject,
                        type: v.type,
                        inverted: true,
                        open: true,
                      })
                    }
                  />,
                  <DeleteOutlined
                    onClick={() =>
                      Modal.confirm({
                        title: "Die Beziehung wirklich löschen?",
                        okText: "Löschen",
                        okType: "danger",
                        cancelText: "Abbrechen",
                        onOk: () =>
                          relationshipsOf.unset(person, v.subject, (error) => {
                            if (error) {
                              message.error(error.message);
                            } else {
                              message.success("Die Beziehung wurde entfernt.");
                            }
                          }),
                      })
                    }
                  />,
                ]
          }
        >
          {!v ? (
            <Spin />
          ) : v instanceof Error ? (
            <Alert type="error" message={v.message} />
          ) : (
            <List.Item.Meta
              title={displayName(v.subject.names)}
              description={displayGendered(
                v.subject.gender,
                v.type.subjectLabel,
                v.type.subjectLabel_f,
                v.type.subjectLabel_m
              )}
            />
          )}
        </List.Item>
      )),
    ];
  }

  const relationshipDrawerResetTrigger = useTrigger();

  relationshipDrawerResetTrigger.listen(() => setRelationshipDrawer(undefined));

  return (
    <Incoming loading={!relationshipItems}>
      {!relationshipItems ? undefined : relationshipItems instanceof Error ? (
        <Alert type="error" message={relationshipItems.message} />
      ) : relationshipItems.length ? (
        <List>{relationshipItems}</List>
      ) : (
        <Nothing text={emptyListText} />
      )}
      {relationshipDrawer && (
        <RelationshipDrawer
          filterPersonId={person.id}
          selectedPerson={relationshipDrawer.other}
          selectedType={relationshipDrawer.type}
          inverted={relationshipDrawer.inverted}
          visible={!!create || relationshipDrawer.open}
          onChange={(person, type, inverted) =>
            setRelationshipDrawer({
              ...relationshipDrawer,
              other: person,
              type,
              inverted,
            })
          }
          onCommit={
            relationshipDrawer.other &&
            relationshipDrawer.type &&
            (relationshipDrawer.other !== relationshipDrawer.origin.other ||
              relationshipDrawer.type !== relationshipDrawer.origin.type ||
              relationshipDrawer.inverted !==
                relationshipDrawer.origin.inverted)
              ? () => {
                  /* assert() */ if (!relationshipDrawer.other)
                    throw new Error(
                      "The relationship object has gone missing :("
                    );

                  /* assert() */ if (!relationshipDrawer.type)
                    throw new Error(
                      "The relationship type has gone missing :("
                    );

                  setRelationshipDrawer({
                    ...relationshipDrawer,
                    isCommitting: true,
                  });

                  const subjectPerson = !relationshipDrawer.inverted
                    ? person
                    : relationshipDrawer.other;

                  const objectPerson = !relationshipDrawer.inverted
                    ? relationshipDrawer.other
                    : person;

                  relationshipsOf.set(
                    subjectPerson,
                    objectPerson,
                    relationshipDrawer.type,
                    (error) => {
                      if (error) {
                        setRelationshipDrawer({
                          ...relationshipDrawer,
                          isCommitting: false,
                          error,
                        });
                      } else {
                        setRelationshipDrawer({
                          ...relationshipDrawer,
                          isCommitting: false,
                          open: false,
                        });

                        if (create) {
                          create.onDone();
                        }
                      }
                    }
                  );
                }
              : undefined
          }
          onRevert={
            relationshipDrawer.other !== relationshipDrawer.origin.other ||
            relationshipDrawer.type !== relationshipDrawer.origin.type ||
            relationshipDrawer.inverted !== relationshipDrawer.origin.inverted
              ? () =>
                  setRelationshipDrawer({
                    ...relationshipDrawer,
                    other: relationshipDrawer.origin.other,
                    type: relationshipDrawer.origin.type,
                    inverted: relationshipDrawer.origin.inverted,
                  })
              : undefined
          }
          onClose={() => {
            setRelationshipDrawer({ ...relationshipDrawer, open: false });

            if (create) {
              create.onDone();
            }
          }}
          isCommitting={!!relationshipDrawer.isCommitting}
          commitError={!!relationshipDrawer.error}
          isNew={!!relationshipDrawer.isNew}
          resetTrigger={relationshipDrawerResetTrigger}
        />
      )}
    </Incoming>
  );
};
