import React, { useState } from "react";
import { DatePicker, Input, Select } from "antd";
import moment from "moment";

interface DateSelectorProps {
  value: string;

  onChange?: (value: string) => void; // the selector will be disabled if undefined
}

const dateByMode = (
  date: moment.Moment | null | undefined,
  mode: "date" | "month" | "year"
): string =>
  !date || !date.isValid()
    ? ""
    : mode === "date"
    ? date.format("YYYY-MM-DD")
    : mode === "month"
    ? date.format("YYYY-MM")
    : date.format("YYYY");

export const DateSelector: React.FC<DateSelectorProps> = (props) => {
  moment.locale("de");

  const { value, onChange } = props;

  const [selectorOpen, setSelectorOpen] = useState(false);

  const [lastSelectedMode, setLastSelectedMode] = useState<
    "date" | "month" | "year"
  >("date");

  const mode =
    value.length === 4
      ? "year"
      : value.length === 7
      ? "month"
      : value.length === 10
      ? "date"
      : lastSelectedMode;

  const date = value
    ? moment(value, ["YYYY", "YYYY-MM", "YYYY-MM-DD"])
    : undefined;

  return (
    <Input.Group compact style={{ display: "flex" }}>
      <Select
        value={mode}
        style={{ minWidth: "10ex" }}
        onChange={
          onChange
            ? (value: "date" | "month" | "year") => {
                setLastSelectedMode(value);

                onChange(dateByMode(date, value));

                setSelectorOpen(true);
              }
            : undefined
        }
      >
        <Select.Option value="date">Tag</Select.Option>
        <Select.Option value="month">Monat</Select.Option>
        <Select.Option value="year">Jahr</Select.Option>
      </Select>
      <DatePicker
        value={date}
        open={selectorOpen}
        mode={mode}
        disabled={!onChange}
        placeholder="Datum"
        onChange={
          onChange
            ? (selected) => onChange(dateByMode(selected, mode))
            : undefined
        }
        onOpenChange={
          onChange
            ? (status) => {
                if (status) {
                  setSelectorOpen(true);
                } else {
                  setSelectorOpen(false);
                }
              }
            : undefined
        }
        onPanelChange={
          onChange
            ? (value) => {
                setSelectorOpen(false);

                onChange(dateByMode(value, mode));
              }
            : undefined
        }
        style={{ textAlign: "left", flexGrow: 1 }}
        format={
          mode === "year"
            ? "YYYY"
            : mode === "month"
            ? "MMM YYYY"
            : "D. MMM YYYY"
        }
      />
    </Input.Group>
  );
};
