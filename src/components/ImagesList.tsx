import React, { useState, useEffect } from "react";
import { Spin, Modal } from "antd";

import { useImages } from "../hooks/useImages";
import { useTrigger } from "../hooks/useTrigger";
import { IImage, newImage } from "../interfaces/IImage";
import { isNode } from "../interfaces/INode";
import { hasCommitError } from "../interfaces/ICommittable";
import { nodeStoreFetch } from "../helpers/nodeStoreFetch";

import { Image } from "./Image";
import { ImagesDrawer } from "./ImagesDrawer";
import { Nothing } from "./Nothing";

import "./ImagesList.css";

const DEFAULT_EMPTY_LIST_TEXT = "Die Bilder-Liste ist leer.";

interface ImagesListProps {
  imageIds: string[];

  create: boolean;

  onAbortCreate: () => void;

  onChange?: (imageIds: string[]) => void;

  emptyListText?: string;

  columns?: 1 | 2;
}

export const ImagesList: React.FC<ImagesListProps> = ({
  imageIds,
  create,
  onAbortCreate,
  onChange,
  emptyListText = DEFAULT_EMPTY_LIST_TEXT,
  columns = 1,
}) => {
  const images = useImages(imageIds);

  const fetchedImages = nodeStoreFetch(images, imageIds);

  const [imageDrawer, setImageDrawer] = useState<
    | undefined
    | {
        image: IImage;

        open: boolean;

        edit?: boolean; // undefined will show the drawer without an edit toggle
      }
  >();

  const imageDrawerResetTrigger = useTrigger();

  imageDrawerResetTrigger.listen(() => setImageDrawer(undefined));

  useEffect(() => {
    if (create && (!imageDrawer || !imageDrawer.open)) {
      setImageDrawer({
        image: newImage(),
        open: true,
      });
    } else if (
      !create &&
      imageDrawer &&
      !isNode(imageDrawer.image) &&
      imageDrawer.open
    ) {
      setImageDrawer({ ...imageDrawer, open: false });
    }
  }, [create, imageDrawer]);

  return (
    <>
      {!fetchedImages ? (
        <Spin />
      ) : !imageIds.length ? (
        <Nothing text={emptyListText} />
      ) : (
        <div className="ImagesList">
          {imageIds
            .map((v) => images.nodesById[v])
            .map((v, i) =>
              !v ? null : (
                <div
                  key={i}
                  style={
                    columns === 2
                      ? {
                          width: "50%",
                          padding: i % 2 ? "0 0 24px 12px" : "0 12px 24px 0",
                          float: "left",
                        }
                      : undefined
                  }
                >
                  <Image
                    url={v && (v instanceof Error ? v : v.path)}
                    onOpen={
                      v && !(v instanceof Error)
                        ? () =>
                            setImageDrawer({
                              image: v,
                              open: true,
                              edit: false,
                            })
                        : undefined
                    }
                    onRemove={
                      v && !(v instanceof Error) && onChange
                        ? () =>
                            Modal.confirm({
                              title: "Das Bild wirklich entfernen?",
                              okText: "Entfernen",
                              okType: "danger",
                              cancelText: "Abbrechen",
                              onOk: () =>
                                onChange(imageIds.filter((w) => w !== v.id)),
                            })
                        : undefined
                    }
                  />
                </div>
              )
            )}
        </div>
      )}
      {imageDrawer && (
        <ImagesDrawer
          selected={imageDrawer.image}
          visible={imageDrawer.open}
          edit={
            !isNode(imageDrawer.image) || imageDrawer.edit
              ? {
                  onChange: (changed) =>
                    setImageDrawer({ ...imageDrawer, image: changed }),
                  onCommit: () => {
                    if (!isNode(imageDrawer.image)) {
                      setImageDrawer({
                        ...imageDrawer,
                        image: images.create(imageDrawer.image, (created) => {
                          setImageDrawer({
                            image: created,
                            open: hasCommitError(created),
                          });

                          if (!hasCommitError(created)) {
                            if (!isNode(created))
                              throw new Error("Object should have an id now."); // assert()

                            if (onChange) {
                              onChange([...imageIds, created.id]);
                            }
                          }
                        }),
                      });
                    } else {
                      setImageDrawer({
                        ...imageDrawer,
                        image: images.update(imageDrawer.image, (updated) => {
                          setImageDrawer({
                            image: updated,
                            open: hasCommitError(updated),
                            edit: hasCommitError(updated),
                          });
                        }),
                      });
                    }
                  },
                }
              : undefined
          }
          onEditToggle={
            imageDrawer.edit !== undefined
              ? () =>
                  setImageDrawer({ ...imageDrawer, edit: !imageDrawer.edit })
              : undefined
          }
          onClose={
            create
              ? onAbortCreate
              : () => setImageDrawer({ ...imageDrawer, open: false })
          }
          resetTrigger={imageDrawerResetTrigger}
        />
      )}
    </>
  );
};
