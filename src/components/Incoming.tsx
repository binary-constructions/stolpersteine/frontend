import React from "react";
import { Card, Spin } from "antd";

import { ReplaceTransition } from "./ReplaceTransition";

interface IncomingProps {
  loading: boolean;

  card?: boolean; // whether to display the loading animation inside a (borderless) card or not (default: false)
}

export const Incoming: React.FC<IncomingProps> = (props) => (
  <ReplaceTransition
    one={
      (props.card === undefined ? false : props.card) ? (
        <Card bordered={false}>
          <Spin />
        </Card>
      ) : (
        <Spin />
      )
    }
    other={props.children}
    showOther={!props.loading}
  />
);
