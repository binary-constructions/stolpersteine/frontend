import React from "react";
import { Input, Select, Tag } from "antd";

import { usePlaces } from "../hooks/usePlaces";
import { selectNodeFromAvailable } from "../helpers/selectNodeFromAvailable";
import { IPlace, changedPlace } from "../interfaces/IPlace";
import { editable } from "../interfaces/IEditable";
import { getError, getWarning } from "../interfaces/IValidated";
import { nodeStoreGetAvailable } from "../helpers/nodeStoreGetAvailable";

import { DescriptionItem } from "./DescriptionItem";
import { PlaceSelector } from "./PlaceSelector";
import { StringValueListInput } from "./StringValueListInput";

interface PlaceDescriptionProps<T extends IPlace> {
  place: T;

  onChange?: (place: T) => void; // if undefined the place will not be editable
}

export const PlaceDescription = <T extends IPlace>(
  props: PlaceDescriptionProps<T>
) => {
  const { place, onChange } = props;

  const { label, parentId, alternativeNames } = editable(place);

  const places = usePlaces();

  const availablePlaces = nodeStoreGetAvailable(places);

  const parent =
    parentId === null
      ? null
      : selectNodeFromAvailable(parentId, availablePlaces);

  return (
    <>
      <DescriptionItem
        label="Name"
        content={{
          nonEditable: label.trim() || undefined,
          editable: (
            <Input
              value={label}
              placeholder="Bezeichnung"
              onChange={
                onChange
                  ? ({ target: { value } }) =>
                      onChange(changedPlace(place, { label: value }))
                  : undefined
              }
            />
          ),
          edit: !!onChange,
          error: getError(place, "label"),
          warning: getWarning(place, "label"),
          required: true,
        }}
      />
      <DescriptionItem
        label="Typ"
        content={{
          nonEditable: undefined,
          editable: (
            <Select defaultValue="" disabled>
              <Select.Option value="">TODO</Select.Option>
            </Select>
          ),
          edit: !!onChange,
          error: undefined,
          warning: undefined,
        }}
      />
      <DescriptionItem
        label="Unterort von"
        content={
          parent === undefined || parent instanceof Error
            ? parent
            : !availablePlaces || availablePlaces instanceof Error
            ? availablePlaces
            : {
                nonEditable: parent ? parent.label : undefined,
                editable: (
                  <PlaceSelector
                    id={parent && parent.id}
                    disableId={place.id}
                    available={availablePlaces}
                    select={
                      onChange
                        ? {
                            onSelect: (id) =>
                              onChange(changedPlace(place, { parentId: id })),
                          }
                        : undefined
                    }
                  />
                ),
                edit: !!onChange,
                error: getError(place, "parentId"),
                warning: getWarning(place, "parentId"),
              }
        }
      />
      <DescriptionItem
        label="Alternative Namen"
        content={{
          nonEditable: alternativeNames.length
            ? alternativeNames.map((name) => <Tag>{name}</Tag>)
            : undefined,
          editable: (
            <StringValueListInput
              values={alternativeNames}
              onChange={
                onChange
                  ? (values) =>
                      onChange(
                        changedPlace(place, { alternativeNames: values })
                      )
                  : undefined
              }
            />
          ),
          edit: !!onChange,
          error: getError(place, "alternativeNames"),
          warning: getError(place, "alternativeNames"),
        }}
      />
    </>
  );
};
