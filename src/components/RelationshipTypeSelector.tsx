import React from "react";
import { Alert, Button, Divider, Select } from "antd";
import { PlusOutlined, ArrowRightOutlined } from "@ant-design/icons";

import { Gender } from "../backendApi";
import { IExistingRelationshipType } from "../interfaces/IRelationshipType";
import { displayGendered } from "../helpers/displayGendered";

interface RelationshipTypeSelectorProps {
  gender?: Gender;

  selected?: IExistingRelationshipType | null;

  inverted?: boolean;

  available: IExistingRelationshipType[];

  onChange?: (selected?: IExistingRelationshipType, inverted?: boolean) => void; // if undefined the selector will be disabled

  onView?: () => void;

  onCreate?: () => void;

  placeholder?: string;
}

export const RelationshipTypeSelector: React.FC<RelationshipTypeSelectorProps> = (
  props
) => {
  const {
    gender = Gender.Unspecified,
    selected = null,
    inverted = false,
    available,
    onChange,
    onView,
    onCreate,
    placeholder,
  } = props;

  const toLabels = (relType: IExistingRelationshipType): [string, string] => [
    displayGendered(
      gender,
      relType.objectLabel,
      relType.objectLabel_f,
      relType.objectLabel_m
    ),
    displayGendered(
      gender,
      relType.subjectLabel,
      relType.subjectLabel_f,
      relType.subjectLabel_m
    ),
  ];

  return available.length ? (
    <div style={{ width: "100%", display: "flex" }}>
      <Select
        showSearch
        style={{ flexGrow: 1 }}
        placeholder={placeholder}
        notFoundContent="Keine Treffer"
        dropdownRender={(menu) => (
          <div>
            {menu}
            {onCreate && (
              <>
                <Divider style={{ margin: 0 }} />
                <div
                  style={{ display: "flex", flexWrap: "nowrap", padding: 8 }}
                >
                  <Button
                    style={{ width: "100%" }}
                    type="dashed"
                    onClick={onCreate}
                    disabled={!onChange}
                  >
                    <PlusOutlined />
                  </Button>
                </div>
              </>
            )}
          </div>
        )}
        value={selected ? (inverted ? "-" : "+") + selected.id : undefined}
        filterOption={(value, option) =>
          option?.props?.label
            ?.toString()
            .toLowerCase()
            .includes(value.toLowerCase()) || false
        }
        onSelect={
          !onChange
            ? undefined
            : (value: string) =>
                value
                  ? onChange(
                      available.find((node) => node.id === value.substring(1)),
                      value.startsWith("-")
                    )
                  : onChange(undefined, undefined)
        }
        disabled={!onChange}
      >
        <Select.Option key="" value="" label="">
          &nbsp;
        </Select.Option>
        {available
          .map((relType) => ({ labels: toLabels(relType), type: relType }))
          .sort((a, b) => a.labels[0].localeCompare(b.labels[0]))
          .map((info, index) => [
            <Select.Option
              key={"+" + info.type.id + "-" + index}
              value={"+" + info.type.id}
              label={info.labels[0]}
            >
              {info.labels[0]}
            </Select.Option>,
            info.labels[0] !== info.labels[1] ? (
              <Select.Option
                key={"-" + info.type.id + "-" + index}
                value={"-" + info.type.id}
                label={info.labels[1]}
              >
                {info.labels[1]}
              </Select.Option>
            ) : undefined,
          ])
          .flat()
          .filter((x) => !!x)}
      </Select>
      {onView && (
        <Button
          style={{ marginLeft: 8 }}
          onClick={onView}
          disabled={!onChange || !selected}
        >
          <ArrowRightOutlined />
        </Button>
      )}
    </div>
  ) : (
    <>
      <Alert
        type="warning"
        message="Es scheint hier aktuell noch keine Auswahl zu existieren."
      />
      {onCreate && (
        <Button
          block
          type="dashed"
          style={{ width: "100%" }}
          onClick={onCreate}
          disabled={!onChange}
        >
          <PlusOutlined />
        </Button>
      )}
    </>
  );
};
