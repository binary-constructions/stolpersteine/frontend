import React, { useState } from "react";

import { Input } from "antd";

import { useDisabled } from "../hooks/useDisabled";
import { usePlaces } from "../hooks/usePlaces";
import { useStateMonitor } from "../hooks/useStateMonitor";
import { useTrigger } from "../hooks/useTrigger";
import { ISpot, changedSpot } from "../interfaces/ISpot";
import { IPlace, newPlace } from "../interfaces/IPlace";
import { editable } from "../interfaces/IEditable";
import { getError, getWarning } from "../interfaces/IValidated";
import { isNode } from "../interfaces/INode";
import { hasCommitError } from "../interfaces/ICommittable";
import { displayStreet } from "../helpers/displayStreet";
import { nodeStoreGetAvailable } from "../helpers/nodeStoreGetAvailable";

import { DescriptionItem } from "./DescriptionItem";
import { PlacesDrawer } from "./PlacesDrawer";
import { StreetInput } from "./StreetInput";
import { NodeSelector } from "./NodeSelector";

interface AddressDescriptionProps<T extends ISpot> {
  spot: T;

  onChange?: (spot: T) => void; // if defined the address will be displayed in edit mode

  // TODOs: delayed/async search, fuzzy matching, sort/select by proximity, place search (by postalCode),
  //        partial selection (e.g. only street name), ...
  //streetCompletions?: StreetInputCompletions;

  //cornerCompletions?: StreetInputCompletions;

  onLoadingStateChange?: (loading: boolean | Error) => void;

  onDrawerStateChange?: (open: boolean) => void;
}

export const AddressDescription = <T extends ISpot>(
  props: AddressDescriptionProps<T>
) => {
  const {
    spot,
    onChange,
    //streetCompletions,
    //cornerCompletions,
    onLoadingStateChange,
    onDrawerStateChange,
  } = props;

  const { street, streetDetail, postalCode, placeId, note } = editable(spot);

  const disabled = useDisabled();

  const places = usePlaces();

  const place = placeId === null ? null : places.nodesById[placeId];

  const availablePlaces = nodeStoreGetAvailable(places);

  /* place drawer */

  const [placeDrawer, setPlaceDrawer] = useState<
    undefined | { place: IPlace; open: boolean; edit?: boolean }
  >();

  const placeDrawerResetTrigger = useTrigger();

  placeDrawerResetTrigger.listen(() => setPlaceDrawer(undefined));

  /* loading state */

  useStateMonitor(
    !place ? true : place instanceof Error ? place : false,
    onLoadingStateChange
  );

  /* drawer state */

  useStateMonitor(
    placeDrawer !== undefined && placeDrawer.open,
    onDrawerStateChange
  );

  return (
    <>
      <DescriptionItem
        label="Straße"
        content={{
          nonEditable: displayStreet(
            street,
            streetDetail.value,
            streetDetail.__typename
          ),
          editable: <StreetInput spot={spot} onChange={onChange} />,
          edit: !!onChange,
          error: getError(spot, "street") || getError(spot, "streetDetail"),
          warning:
            getWarning(spot, "street") || getWarning(spot, "streetDetail"),
        }}
      />
      <DescriptionItem
        label="Postleitzahl"
        content={{
          nonEditable: postalCode.trim() || undefined,
          editable: (
            <Input
              placeholder="PLZ"
              value={postalCode}
              onChange={
                onChange
                  ? ({ target: { value } }) =>
                      onChange(changedSpot(spot, { postalCode: value }))
                  : undefined
              }
              disabled={disabled}
            />
          ),
          edit: !!onChange,
          error: getError(spot, "postalCode"),
          warning: getWarning(spot, "postalCode"),
        }}
      />
      <DescriptionItem
        label="Ort"
        content={
          place === undefined || place instanceof Error
            ? place
            : !availablePlaces || availablePlaces instanceof Error
            ? availablePlaces
            : {
                nonEditable: place ? place.label : undefined,
                editable: (
                  <NodeSelector
                    selected={place}
                    available={availablePlaces}
                    toLabel={(place) => place.label}
                    onChange={
                      !disabled && onChange
                        ? (selected) =>
                            onChange(
                              changedSpot(spot, {
                                placeId: selected ? selected.id : null,
                              })
                            )
                        : undefined
                    }
                    onView={() =>
                      place
                        ? setPlaceDrawer({ place, open: true, edit: false })
                        : undefined
                    }
                    onCreate={() =>
                      setPlaceDrawer({ place: newPlace(), open: true })
                    }
                    placeholder="Ort wählen"
                  />
                ),
                edit: !!onChange,
                error: getError(spot, "placeId"),
                warning: getWarning(spot, "placeId"),
              }
        }
      />
      <DescriptionItem
        label="Hinweis"
        content={{
          nonEditable: note.trim() || undefined,
          editable: (
            <Input.TextArea
              style={{ marginTop: "4px" }}
              value={note}
              placeholder="Falls sinnvoll ein Satz (inklusive Satzzeichen!) mit Informationen zum besseren Auffinden des Verlegeorts."
              autoSize={{ minRows: 3, maxRows: 6 }}
              onChange={
                onChange
                  ? ({ target: { value } }) =>
                      onChange(changedSpot(spot, { note: value }))
                  : undefined
              }
              disabled={disabled}
            />
          ),
          edit: !!onChange,
          error: getError(spot, "note"),
          warning: getWarning(spot, "note"),
        }}
      />
      {placeDrawer && (
        <PlacesDrawer
          selected={placeDrawer.place}
          visible={placeDrawer.open}
          edit={
            onChange && (!isNode(placeDrawer.place) || placeDrawer.edit)
              ? {
                  onChange: (changed) =>
                    setPlaceDrawer({ ...placeDrawer, place: changed }),
                  onCommit: () => {
                    if (!isNode(placeDrawer.place)) {
                      setPlaceDrawer({
                        ...placeDrawer,
                        place: places.create(placeDrawer.place, (created) => {
                          setPlaceDrawer({
                            place: created,
                            open: hasCommitError(created),
                          });

                          if (!hasCommitError(created)) {
                            /* assert() */ if (!isNode(created))
                              throw new Error("Object should have an id now.");

                            onChange(
                              changedSpot(spot, { placeId: created.id })
                            );
                          }
                        }),
                      });
                    } else {
                      setPlaceDrawer({
                        ...placeDrawer,
                        place: places.update(placeDrawer.place, (updated) => {
                          setPlaceDrawer({
                            place: updated,
                            open: hasCommitError(updated),
                            edit: hasCommitError(updated),
                          });
                        }),
                      });
                    }
                  },
                }
              : undefined
          }
          onEditToggle={
            placeDrawer.edit !== undefined
              ? () =>
                  setPlaceDrawer({ ...placeDrawer, edit: !placeDrawer.edit })
              : undefined
          }
          onClose={() => setPlaceDrawer({ ...placeDrawer, open: false })}
          resetTrigger={placeDrawerResetTrigger}
        />
      )}
    </>
  );
};
