import React from "react";
import { Typography } from "antd";

interface NothingProps {
  text: string;
  smallText?: boolean;
}

export const Nothing: React.FC<NothingProps> = ({ text, smallText }) => (
  <Typography.Text
    type="secondary"
    style={{
      fontSize: smallText ? "small" : undefined,
    }}
  >
    {text}
  </Typography.Text>
);
