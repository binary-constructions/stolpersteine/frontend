import React from "react";
import { Form } from "antd";

interface DescriptionProps {
  horizontal?: boolean;

  align?: "right" | "left";
}

export const Description: React.FC<DescriptionProps> = ({
  horizontal = false,
  align = "left",
  children,
}) => {
  return horizontal ? (
    <Form
      labelAlign="left"
      layout="horizontal"
      labelCol={{ span: 8 }}
      wrapperCol={{ span: 16 }}
    >
      {children}
    </Form>
  ) : (
    <Form labelAlign={align} layout="vertical" style={{ textAlign: align }}>
      {children}
    </Form>
  );
};
