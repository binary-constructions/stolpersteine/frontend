import React from "react";
import { Input } from "antd";
import { ManOutlined, WomanOutlined } from "@ant-design/icons";

import { IOccupation, changedOccupation } from "../interfaces/IOccupation";
import { editable } from "../interfaces/IEditable";
import { getError, getWarning } from "../interfaces/IValidated";
import { DescriptionItem } from "./DescriptionItem";

interface OccupationDescriptionProps<T extends IOccupation> {
  subject: T;

  onChange?: (changed: T) => void; // if undefined the subject will not be editable
}

export const OccupationDescription = <T extends IOccupation>(
  props: OccupationDescriptionProps<T>
) => {
  const { subject, onChange } = props;

  const { label, label_f, label_m } = editable(subject);

  return (
    <>
      <DescriptionItem
        label="Bezeichnung (generisch)"
        content={{
          nonEditable: label.trim() || undefined,
          editable: (
            <Input
              value={label}
              onChange={
                onChange
                  ? ({ target: { value } }) =>
                      onChange(changedOccupation(subject, { label: value }))
                  : undefined
              }
            />
          ),
          edit: !!onChange,
          error: getError(subject, "label"),
          warning: getWarning(subject, "label"),
          required: true,
        }}
      />
      <DescriptionItem
        label="Bezeichnung (weiblich)"
        content={{
          nonEditable: label_f.trim() ? (
            <div>
              <WomanOutlined /> {label_f}
            </div>
          ) : undefined,
          editable: (
            <Input
              prefix={<WomanOutlined />}
              value={label_f}
              onChange={
                onChange
                  ? ({ target: { value } }) =>
                      onChange(changedOccupation(subject, { label_f: value }))
                  : undefined
              }
            />
          ),
          edit: !!onChange,
          error: getError(subject, "label_f"),
          warning: getWarning(subject, "label_f"),
        }}
      />
      <DescriptionItem
        label="Bezeichnung (männlich)"
        content={{
          nonEditable: label_m.trim() ? (
            <div>
              <ManOutlined /> {label_m}
            </div>
          ) : undefined,
          editable: (
            <Input
              prefix={<ManOutlined />}
              value={label_m}
              onChange={
                onChange
                  ? ({ target: { value } }) =>
                      onChange(changedOccupation(subject, { label_m: value }))
                  : undefined
              }
            />
          ),
          edit: !!onChange,
          error: getError(subject, "label_m"),
          warning: getWarning(subject, "label_m"),
        }}
      />
    </>
  );
};
