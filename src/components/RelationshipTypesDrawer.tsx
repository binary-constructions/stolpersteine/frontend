import React from "react";

import { Card } from "antd";
import { ITrigger } from "../hooks/useTrigger";
import {
  IRelationshipType,
  IExistingRelationshipType,
} from "../interfaces/IRelationshipType";
import { revert } from "../interfaces/IRevertible";

import { CommittablesDrawer } from "./CommittablesDrawer";
import { Description } from "./Description";
import { RelationshipTypeDescription } from "./RelationshipTypeDescription";

interface RelationshipTypesDrawerProps {
  selected: IRelationshipType;

  visible: boolean;

  edit?: {
    onChange: (changed: IRelationshipType) => void;

    onCommit: () => void;
  };

  select?: {
    selection: IExistingRelationshipType[];

    onSelect?: (selected: IExistingRelationshipType) => void;
  };

  onEditToggle?: () => void;

  onClose: () => void;

  resetTrigger: ITrigger;
}

export const RelationshipTypesDrawer: React.FC<RelationshipTypesDrawerProps> = (
  props
) => {
  const {
    selected,
    visible,
    edit,
    select,
    onEditToggle,
    onClose,
    resetTrigger,
  } = props;

  return (
    <CommittablesDrawer<IRelationshipType, IExistingRelationshipType>
      visible={visible}
      title="Beziehungsart"
      committable={{
        selected,
        select: select && {
          selection: select.selection,
          toLabel: (option) => option.label,
          onSelect: select.onSelect,
        },
        edit: edit && {
          onCommit: edit.onCommit,
          onRevert: () => edit.onChange(revert(selected)),
        },
        onEditToggle,
      }}
      onClose={onClose}
      resetTrigger={resetTrigger}
    >
      {selected && (
        <Description>
          <Card bordered={false}>
            <RelationshipTypeDescription
              selected={selected}
              onChange={edit && edit.onChange}
            />
          </Card>
        </Description>
      )}
    </CommittablesDrawer>
  );
};
