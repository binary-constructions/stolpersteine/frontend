import React from "react";

import { ITrigger } from "../hooks/useTrigger";

import { ErrorDisplay } from "./ErrorDisplay";

const hitBottom = () => {
  throw new Error("SOMETHING WENT HORRIBLY WRONG!!!");
};

const HitBottom = () => <>{hitBottom()}</>;

interface BottomProps {
  hit?: boolean; // for testing

  resetLabel?: string;

  resetTrigger: ITrigger;
}

interface BottomState {
  error: any;
}

export class Bottom extends React.Component<BottomProps, BottomState> {
  constructor(props: BottomProps) {
    super(props);

    this.state = {
      error: null,
    };
  }

  static getDerivedStateFromError(error: any) {
    return { error };
  }

  render() {
    this.props.resetTrigger.listen(() =>
      this.setState({
        error: null,
      })
    );

    if (this.state.error) {
      return (
        <ErrorDisplay
          errors={[this.state.error.message]}
          reset={{
            trigger: this.props.resetTrigger,
            label: this.props.resetLabel,
          }}
        />
      );
    } else if (this.props.hit) {
      return <HitBottom />;
    } else {
      return this.props.children;
    }
  }
}
