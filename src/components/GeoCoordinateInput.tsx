import React, { useRef } from "react";
import { Input, Select } from "antd";

import { useDisabled } from "../hooks/useDisabled";

const parseFull = (
  literal: string
): {
  sign?: string;
  integer?: string;
  separator?: string;
  fraction?: string;
} => {
  const match = literal.match(/^(-|\+)?0*(\d+)?((\.|,)(\d+)?)?/);

  if (match !== null) {
    const [, sign, integer, separator, , fraction] = match;

    return { sign, integer, separator, fraction };
  } else {
    return {
      sign: undefined,
      integer: undefined,
      separator: undefined,
      fraction: undefined,
    };
  }
};

const parseFraction = (literal: string): string | undefined => {
  const match = literal.match(/^\d+/);

  if (match !== null) {
    const [fraction] = match;

    return fraction;
  } else {
    return undefined;
  }
};

interface GeoCoordinateInputProps {
  value: string;

  onChange?: (value: string) => void;
}

export const GeoCoordinateInput: React.FC<GeoCoordinateInputProps> = (
  props
) => {
  const { value, onChange } = props;

  const disabled = useDisabled();

  const parsed = parseFull(value);

  const sign = parsed.sign || "+";

  const integer = parsed.integer ? parsed.integer.substring(0, 3) : "0";

  const fraction = parsed.fraction
    ? parsed.fraction.substring(0, 6)
    : undefined;

  const fraction_input = useRef<Input>(null);

  const handleSignChange = (selected: string) => {
    /* assert() */ if (!onChange)
      throw new Error("Input should be disabled...");

    onChange(
      fraction ? selected + integer + "." + fraction : selected + integer
    );
  };

  const handleIntegerChange = (input: string) => {
    /* assert() */ if (!onChange)
      throw new Error("Input should be disabled...");

    const parsed = parseFull(input);

    if (!parsed.integer || parsed.integer.length <= 3) {
      const new_value =
        parsed.separator && parsed.fraction
          ? (parsed.sign || sign) +
            (parsed.integer || "0") +
            "." +
            parsed.fraction.substring(0, 6)
          : fraction
          ? (parsed.sign || sign) + (parsed.integer || "0") + "." + fraction
          : (parsed.sign || sign) + (parsed.integer || "0");

      if (fraction_input.current && parsed.separator) {
        fraction_input.current.focus();
      }

      onChange(new_value);
    }
  };

  const handleFractionChange = (input: string) => {
    /* assert() */ if (!onChange)
      throw new Error("Input should be disabled...");

    const parsed = parseFraction(input);

    if (!parsed || parsed.length <= 6) {
      onChange(parsed ? sign + integer + "." + parsed : sign + integer);
    }
  };

  return (
    <Input.Group
      className="geo-coordinate-input"
      compact
      style={{ display: "flex", flexDirection: "row" }}
    >
      <Select
        value={sign}
        style={{ width: "50px" }}
        onChange={onChange && handleSignChange}
        disabled={disabled}
      >
        <Select.Option value="+">+</Select.Option>
        <Select.Option value="-">-</Select.Option>
      </Select>
      <Input
        disabled={disabled}
        value={integer === "0" ? "" : integer}
        style={{
          textAlign: "center",
          paddingLeft: "3px",
          paddingRight: 0,
          minWidth: "unset",
          width: "52px",
          flexGrow: 1,
        }}
        placeholder="0"
        onChange={
          onChange
            ? ({ target: { value } }) => handleIntegerChange(value)
            : undefined
        }
      />
      <Input
        style={{
          minWidth: "unset",
          width: "7px",
          borderLeft: 0,
          paddingLeft: 0,
          paddingRight: 0,
          textAlign: "center",
          pointerEvents: "none",
          backgroundColor: !disabled ? "#fff" : undefined,
        }}
        placeholder="."
        disabled
      />
      <Input
        disabled={disabled}
        ref={fraction_input}
        value={fraction}
        style={{
          textAlign: "center",
          paddingLeft: 0,
          paddingRight: "3px",
          borderLeft: 0,
          minWidth: "unset",
          width: "82px",
          flexGrow: 1,
        }}
        placeholder="0"
        onChange={
          onChange
            ? ({ target: { value } }) => handleFractionChange(value)
            : undefined
        }
      />
    </Input.Group>
  );
};
