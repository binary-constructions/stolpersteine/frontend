import React from "react";
import { Alert, Checkbox, List } from "antd";

import { Gender } from "../backendApi";
import {
  IExistingPersecutionPretext,
  IPersecutionPretext,
} from "../interfaces/IPersecutionPretext";
import { IPerson, changedPerson } from "../interfaces/IPerson";
import { editable } from "../interfaces/IEditable";
import { getError, getWarning } from "../interfaces/IValidated";
import { usePersecutionPretexts } from "../hooks/usePersecutionPretexts";
import { useStateMonitor } from "../hooks/useStateMonitor";
import { useDisabled } from "../hooks/useDisabled";
import { nodeStoreGetAvailable } from "../helpers/nodeStoreGetAvailable";
import { nodeStoreFetch } from "../helpers/nodeStoreFetch";

import { DescriptionItem } from "./DescriptionItem";

interface PersecutionDescriptionProps<T extends IPerson> {
  person: T;

  onChange?: (person: T) => void;

  onLoadingStateChange?: (loading: boolean | Error) => void;

  //onDrawerStateChange?: (open: boolean) => void;

  gender?: Gender; // changes how labels are displayed
}

const getLabelByGender = (
  pretext: IPersecutionPretext,
  gender?: Gender
): string =>
  gender === Gender.Female
    ? pretext.label_f || pretext.label
    : gender === Gender.Male
    ? pretext.label_m || pretext.label
    : pretext.label;

export const PersecutionDescription = <T extends IPerson>(
  props: PersecutionDescriptionProps<T>
) => {
  const {
    person,
    onChange,
    onLoadingStateChange,
    gender = Gender.Unspecified,
  } = props;

  const { persecutionPretextIds } = editable(person);

  const disabled = useDisabled();

  const persecutionPretexts = usePersecutionPretexts();

  const availablePersecutionPretexts = nodeStoreGetAvailable(
    persecutionPretexts
  );

  const selectedPersecutionPretexts = nodeStoreFetch(
    persecutionPretexts,
    persecutionPretextIds
  );

  /* loading state */

  useStateMonitor(
    availablePersecutionPretexts === undefined
      ? true
      : availablePersecutionPretexts instanceof Error
      ? availablePersecutionPretexts
      : false,
    onLoadingStateChange
  );

  const selected: IExistingPersecutionPretext[] = [];

  if (
    selectedPersecutionPretexts &&
    !(selectedPersecutionPretexts instanceof Error)
  ) {
    for (const pretextId of persecutionPretextIds) {
      const current = selectedPersecutionPretexts.find(
        (pretext) => pretext.id === pretextId
      );

      if (current === undefined) {
        throw new Error(
          "Mindestens einer der angegebenen Verfolgungsvorwände wurde nicht gefunden."
        );
      } else {
        selected.push(current);
      }
    }
  }

  return (
    <DescriptionItem
      label="Verfolgt als"
      content={{
        nonEditable: selected.length ? (
          <ul style={{ paddingLeft: 24 }}>
            {selected.map((pretext, index) => (
              <li key={index}>{getLabelByGender(pretext, gender)}</li>
            ))}
          </ul>
        ) : undefined,
        editable: !availablePersecutionPretexts.length ? (
          <Alert
            type="warning"
            message="Es wurden noch keine Verfolgungsvorwände eingestellt."
          /> // FIXME: allow adding them
        ) : (
          <List size="small" split={false}>
            {availablePersecutionPretexts.map((pretext, index) => (
              <List.Item key={index}>
                <Checkbox
                  checked={persecutionPretextIds.includes(pretext.id)}
                  disabled={disabled}
                  onChange={
                    onChange
                      ? () => {
                          const index = persecutionPretextIds.indexOf(
                            pretext.id
                          );

                          onChange(
                            changedPerson(person, {
                              persecutionPretextIds:
                                index === -1
                                  ? [...persecutionPretextIds, pretext.id]
                                  : [
                                      ...persecutionPretextIds.slice(0, index),
                                      ...persecutionPretextIds.slice(index + 1),
                                    ],
                            })
                          );
                        }
                      : undefined
                  }
                >
                  {getLabelByGender(pretext, gender)}
                </Checkbox>
              </List.Item>
            ))}
          </List>
        ),
        edit: !!onChange,
        error: getError(person, "persecutionPretextIds"),
        warning: getWarning(person, "persecutionPretextIds"),
      }}
    />
  );
};
