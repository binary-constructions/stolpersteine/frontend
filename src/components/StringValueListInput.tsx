import React from "react";
import { Button, Input } from "antd";
import { MinusOutlined, PlusOutlined } from "@ant-design/icons";

interface StringValueListInputProps {
  values: string[];

  onChange?: (values: string[]) => void; // if undefined the input will be disabled
}

export const StringValueListInput: React.FC<StringValueListInputProps> = (
  props
) => {
  const { values, onChange } = props;

  return (
    <>
      {values.map((value, index) => (
        <Input
          key={index}
          value={value}
          onChange={
            onChange
              ? ({ target: { value: newValue } }) =>
                  onChange([
                    ...values.slice(0, index),
                    newValue,
                    ...values.slice(index + 1),
                  ])
              : undefined
          }
          addonAfter={
            onChange ? (
              <MinusOutlined
                onClick={() =>
                  onChange([
                    ...values.slice(0, index),
                    ...values.slice(index + 1),
                  ])
                }
              />
            ) : undefined
          }
          disabled={!onChange}
        />
      ))}
      <Button
        type="dashed"
        style={{ width: "100%" }}
        onClick={onChange ? () => onChange([...values, ""]) : undefined}
        disabled={!onChange}
      >
        <PlusOutlined />
      </Button>
    </>
  );
};
