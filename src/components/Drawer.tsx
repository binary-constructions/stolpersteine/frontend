import React, { useState, useEffect } from "react";
import { Alert, Drawer as AntDrawer, PageHeader, Switch } from "antd";
import {
  EyeOutlined,
  EditOutlined,
  LoadingOutlined,
  CloseOutlined,
  ArrowLeftOutlined,
  UndoOutlined,
} from "@ant-design/icons";

import { ITrigger } from "../hooks/useTrigger";

import { DisabledContextProvider } from "./DisabledContextProvider";
import { AppearTransition } from "./AppearTransition";
import { ReplaceTransition } from "./ReplaceTransition";
import { Bottom } from "./Bottom";

import "./Drawer.css";

export enum DrawerBailStyle {
  Close = "close",
  Back = "arrow-left",
  Revert = "undo",
}

export enum DrawerSize {
  Small = 256,
  Medium = 512,
  Big = 768,
}

interface DrawerProps {
  title: string;

  //subTitle?: string;

  visible?: boolean;

  modal?: boolean;

  busy?: boolean;

  awaitingEdit?: boolean;

  size?: DrawerSize;

  bailStyles?: {
    one: DrawerBailStyle;

    other: DrawerBailStyle;
  };

  bailStyleIsOther?: boolean;

  onBail?: () => void;

  edit?: {
    // an edit toggle will be displayed if this is defined
    active: boolean;

    onToggle?: () => void; // if undefined the toggle will be visible but disabled
  };

  errorMessage?: string;

  warningMessage?: string;

  actionAreaContent?: JSX.Element;

  closeActionArea?: boolean; // FIXME: currently disabled

  //fixedBottomContent?: ReactNode;

  disabled?: boolean; // sets the DisabledContext

  resetTrigger: ITrigger;
}

export const Drawer: React.FC<DrawerProps> = (props) => {
  const {
    title,
    //subTitle,
    visible = false,
    modal = false,
    busy = false,
    awaitingEdit = false,
    size = DrawerSize.Small,
    bailStyles = {
      one: DrawerBailStyle.Close,

      other: DrawerBailStyle.Revert,
    },
    bailStyleIsOther = false,
    onBail,
    edit,
    errorMessage,
    warningMessage,
    actionAreaContent,
    closeActionArea = false,
    //fixedBottomContent,
    disabled = false,
    resetTrigger,
    children,
  } = props;

  const [lastErrorMessage, setLastErrorMessage] = useState(errorMessage || "");

  useEffect(() => {
    if (errorMessage) {
      setLastErrorMessage(errorMessage);
    }
  }, [errorMessage]);

  const [lastWarningMessage, setLastWarningMessage] = useState(
    warningMessage || ""
  );

  useEffect(() => {
    if (warningMessage) {
      setLastWarningMessage(warningMessage);
    }
  }, [warningMessage]);

  const classes: string[] = ["app__drawer"];

  if (disabled) {
    classes.push("app__drawer--disabled");
  }

  return (
    <AntDrawer
      className={classes.join(" ")}
      visible={visible}
      mask={modal}
      width={size}
      closable={false}
      placement="right"
      bodyStyle={{
        padding: 0,
        height: "100%",
        minHeight: 0,
        display: "flex",
        flexDirection: "column",
      }}
    >
      <Bottom resetTrigger={resetTrigger}>
        <div
          style={
            {
              borderBottom: "1px solid #e9e9e9",
            } /* TODO: border only when scrolled? */
          }
        >
          <DisabledContextProvider disable={disabled}>
            <PageHeader
              ghost={true}
              title={title}
              onBack={!awaitingEdit && !busy ? onBail : () => undefined}
              backIcon={
                busy ? (
                  <LoadingOutlined />
                ) : !onBail ? undefined : (
                  <div
                    style={{
                      position: "relative",
                      width: "16px",
                      height: "16px",
                      overflow: "hidden",
                      opacity: awaitingEdit ? 0.2 : undefined,
                      cursor: awaitingEdit ? "default" : undefined,
                    }}
                  >
                    <ReplaceTransition
                      one={
                        bailStyles.one === DrawerBailStyle.Back ? (
                          <ArrowLeftOutlined />
                        ) : bailStyles.one === DrawerBailStyle.Close ? (
                          <CloseOutlined />
                        ) : (
                          <UndoOutlined />
                        )
                      }
                      other={
                        bailStyles.other === DrawerBailStyle.Back ? (
                          <ArrowLeftOutlined />
                        ) : bailStyles.other === DrawerBailStyle.Close ? (
                          <CloseOutlined />
                        ) : (
                          <UndoOutlined />
                        )
                      }
                      showOther={bailStyleIsOther}
                    />
                  </div>
                )
              }
              extra={
                <AppearTransition show={!!edit}>
                  <Switch
                    checked={!!edit && edit.active}
                    onChange={edit && edit.onToggle}
                    disabled={
                      awaitingEdit || disabled || !edit || !edit.onToggle
                    }
                    style={{ marginTop: "6px" }}
                    checkedChildren={<EditOutlined />}
                    unCheckedChildren={<EyeOutlined />}
                  />
                </AppearTransition>
              }
            />
            <AppearTransition show={!!errorMessage}>
              <Alert
                type="error"
                message={errorMessage || lastErrorMessage}
                style={{
                  margin: 0,
                  borderRadius: 0,
                  borderLeft: "none",
                  borderRight: "none",
                  paddingLeft: "24px",
                  paddingRight: "24px",
                }}
              />
            </AppearTransition>
            <AppearTransition show={!!warningMessage}>
              <Alert
                type="warning"
                message={warningMessage || lastWarningMessage}
                style={{
                  margin: 0,
                  borderRadius: 0,
                  borderLeft: "none",
                  borderRight: "none",
                  paddingLeft: "24px",
                  paddingRight: "24px",
                }}
              />
            </AppearTransition>
          </DisabledContextProvider>
        </div>
        <div
          style={{
            minHeight: 0,
            display: "flex",
            flexDirection: "column",
            flexGrow: 1,
            overflowY: "auto",
            overflowX: "hidden",
          }}
        >
          <DisabledContextProvider disable={disabled}>
            {children}
          </DisabledContextProvider>
        </div>
        <DisabledContextProvider disable={disabled}>
          {/* fixedBottomContent || null */}
          <AppearTransition show={!!actionAreaContent} delayed>
            <div
              style={{
                borderTop:
                  "1px solid #e9e9e9" /* TODO: border only when scrolled? */,
                padding: "12px 24px",
                background: "#fff",
                textAlign: "right",
                whiteSpace: "nowrap",
              }}
            >
              {actionAreaContent}
            </div>
          </AppearTransition>
        </DisabledContextProvider>
      </Bottom>
    </AntDrawer>
  );
};
