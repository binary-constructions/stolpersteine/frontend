import React, { useState } from "react";
import { Input, Select, Tag } from "antd";

import { Gender, Fate } from "../backendApi";
import { IPerson, changedPerson } from "../interfaces/IPerson";
import { editable } from "../interfaces/IEditable";
import { hasCommitError } from "../interfaces/ICommittable";
import { getError, getWarning } from "../interfaces/IValidated";
import {
  IOccupation,
  IExistingOccupation,
  newOccupation,
} from "../interfaces/IOccupation";
import { IPlace, newPlace } from "../interfaces/IPlace";
import { isNode } from "../interfaces/INode";
import { useDisabled } from "../hooks/useDisabled";
import { useStateMonitor } from "../hooks/useStateMonitor";
import { useTrigger } from "../hooks/useTrigger";
import { useOccupations } from "../hooks/useOccupations";
import { usePlaces } from "../hooks/usePlaces";
import { displayGender } from "../helpers/displayGender";
import { displayFate } from "../helpers/displayFate";
import { displayGendered } from "../helpers/displayGendered";
import { displayName } from "../helpers/displayName";
import { selectNodeFromAvailable } from "../helpers/selectNodeFromAvailable";
import { displayDate } from "../helpers/displayDate";
import { nodeStoreFetch } from "../helpers/nodeStoreFetch";
import { nodeStoreGetAvailable } from "../helpers/nodeStoreGetAvailable";

import { OccupationsDrawer } from "./OccupationsDrawer";
import { PlacesDrawer } from "./PlacesDrawer";
import { DescriptionItem } from "./DescriptionItem";
import { NamesInput } from "./NamesInput";
import { NodesSelector } from "./NodesSelector";
import { NodeSelector } from "./NodeSelector";
import { DateSelector } from "./DateSelector";

interface PersonGeneralDescriptionProps<T extends IPerson> {
  person: T;

  onChange?: (person: T) => void; // if undefined the person will not be editable

  onLoadingStateChange?: (loading: boolean | Error) => void;

  onDrawerStateChange?: (open: boolean) => void;
}

export const PersonGeneralDescription = <T extends IPerson>(
  props: PersonGeneralDescriptionProps<T>
) => {
  const { person, onChange, onLoadingStateChange, onDrawerStateChange } = props;

  const {
    title,
    names,
    gender,
    fate,
    escapedToPlaceId,
    occupationIds,
    bornDate,
    bornPlaceId,
    diedDate,
    diedPlaceId,
  } = editable(person);

  const disabled = useDisabled();

  const occupations = useOccupations();

  const availableOccupations = nodeStoreGetAvailable(occupations);

  const personOccupations = nodeStoreFetch(occupations, occupationIds);

  const places = usePlaces();

  const availablePlaces = nodeStoreGetAvailable(places);

  const bornPlace =
    bornPlaceId === null
      ? null
      : selectNodeFromAvailable(bornPlaceId, availablePlaces);

  const diedPlace =
    diedPlaceId === null
      ? null
      : selectNodeFromAvailable(diedPlaceId, availablePlaces);

  const escapedToPlace =
    escapedToPlaceId === null
      ? null
      : selectNodeFromAvailable(escapedToPlaceId, availablePlaces);

  /* occupations drawer */

  const [occupationsDrawer, setOccupationsDrawer] = useState<
    | undefined
    | {
        selected: IOccupation;

        selection?: IExistingOccupation[];

        open: boolean;

        edit?: boolean;
      }
  >();

  const occupationsDrawerResetTrigger = useTrigger();

  occupationsDrawerResetTrigger.listen(() => setOccupationsDrawer(undefined));

  /* place drawer */

  const [placeDrawer, setPlaceDrawer] = useState<
    | undefined
    | {
        selected: IPlace;

        target: "born" | "died" | "escapedTo";

        open: boolean;

        edit?: boolean;
      }
  >();

  const placeDrawerResetTrigger = useTrigger();

  placeDrawerResetTrigger.listen(() => setPlaceDrawer(undefined));

  /* loading state */

  const [occupationsLoadingState, setOccupationsLoadingState] = useState<
    undefined | boolean | Error
  >(false);

  const [placesLoadingState, setPlacesLoadingState] = useState<
    undefined | boolean | Error
  >();

  useStateMonitor(
    occupationsLoadingState || placesLoadingState || false,
    onLoadingStateChange
  );

  /* drawer state */

  useStateMonitor(
    occupationsDrawer !== undefined && occupationsDrawer.open,
    onDrawerStateChange
  );

  return (
    <>
      <DescriptionItem
        label="Titel"
        content={{
          nonEditable: title.trim() || undefined,
          editable: (
            <Input
              value={title}
              onChange={
                onChange
                  ? ({ target: { value } }) =>
                      onChange(changedPerson(person, { title: value }))
                  : undefined
              }
              disabled={disabled}
            />
          ),
          edit: !!onChange,
          error: getError(person, "title"),
          warning: getWarning(person, "title"),
        }}
      />
      <DescriptionItem
        label="Name"
        content={{
          nonEditable: displayName(names),
          editable: (
            <NamesInput
              names={names}
              onChange={
                onChange
                  ? (values) =>
                      onChange(changedPerson(person, { names: values }))
                  : undefined
              }
            />
          ),
          edit: !!onChange,
          error: getError(person, "names"),
          warning: getWarning(person, "names"),
          required: true,
        }}
      />
      <DescriptionItem
        label="Geschlecht"
        content={{
          nonEditable: displayGender(gender),
          editable: (
            <Select
              value={gender}
              onChange={
                onChange
                  ? (value: Gender) =>
                      onChange(changedPerson(person, { gender: value }))
                  : undefined
              }
              disabled={disabled}
            >
              <Select.Option value={Gender.Unspecified}>
                unbekannt / keine Angabe
              </Select.Option>
              <Select.Option value={Gender.Female}>
                {displayGender(Gender.Female)}
              </Select.Option>
              <Select.Option value={Gender.Male}>
                {displayGender(Gender.Male)}
              </Select.Option>
              <Select.Option value={Gender.NonBinary}>
                {displayGender(Gender.NonBinary)}
              </Select.Option>
            </Select>
          ),
          edit: !!onChange,
          error: getError(person, "gender"),
          warning: getWarning(person, "gender"),
        }}
      />
      <DescriptionItem
        label="Beruf"
        content={
          personOccupations === undefined || personOccupations instanceof Error
            ? personOccupations
            : !availableOccupations || availableOccupations instanceof Error
            ? availableOccupations
            : {
                nonEditable: personOccupations.length
                  ? personOccupations.map((occupation, index) => (
                      <Tag key={index}>
                        {displayGendered(
                          person.gender,
                          occupation.label,
                          occupation.label_f,
                          occupation.label_m
                        )}
                      </Tag>
                    ))
                  : undefined,
                editable: (
                  <NodesSelector
                    selected={personOccupations}
                    available={availableOccupations}
                    toLabel={(occupation) =>
                      displayGendered(
                        person.gender,
                        occupation.label,
                        occupation.label_f,
                        occupation.label_m
                      )
                    }
                    onChange={
                      onChange
                        ? (selected) =>
                            onChange(
                              changedPerson(person, {
                                occupationIds: selected.map(
                                  (occupation) => occupation.id
                                ),
                              })
                            )
                        : undefined
                    }
                    onCreate={() =>
                      setOccupationsDrawer({
                        selected: newOccupation(),
                        open: true,
                      })
                    }
                    onView={() =>
                      setOccupationsDrawer({
                        selected: personOccupations[0],
                        selection: personOccupations,
                        open: true,
                        edit: false,
                      })
                    }
                  />
                ),
                edit: !!onChange,
                error: getError(person, "occupationIds"),
                warning: getWarning(person, "occupationIds"),
              }
        }
      />
      <DescriptionItem
        label="Geburtsdatum"
        content={{
          nonEditable: displayDate(bornDate),
          editable: (
            <DateSelector
              value={bornDate}
              onChange={
                onChange
                  ? (bornDate) => onChange(changedPerson(person, { bornDate }))
                  : undefined
              }
            />
          ),
          edit: !!onChange,
          error: getError(person, "bornDate"),
          warning: getWarning(person, "bornDate"),
        }}
      />
      <DescriptionItem
        label="Geburtsort"
        content={
          bornPlace === undefined || bornPlace instanceof Error
            ? bornPlace
            : {
                nonEditable: bornPlace ? bornPlace.label : undefined,
                editable: (
                  <NodeSelector
                    selected={bornPlace}
                    available={availablePlaces}
                    toLabel={(place) => place.label}
                    onChange={
                      !disabled && onChange
                        ? (selected) =>
                            onChange(
                              changedPerson(person, {
                                bornPlaceId: selected ? selected.id : null,
                              })
                            )
                        : undefined
                    }
                    onView={() =>
                      bornPlace
                        ? setPlaceDrawer({
                            selected: bornPlace,
                            target: "born",
                            open: true,
                            edit: false,
                          })
                        : undefined
                    }
                    onCreate={() =>
                      setPlaceDrawer({
                        selected: newPlace(),
                        target: "born",
                        open: true,
                      })
                    }
                    placeholder="Ort wählen"
                  />
                ),
                edit: !!onChange,
                error: getError(person, "bornPlaceId"),
                warning: getWarning(person, "bornPlaceId"),
              }
        }
      />
      <DescriptionItem
        label="Verbleib"
        content={{
          nonEditable: displayFate(fate),
          editable: (
            <Select
              value={fate}
              onChange={
                onChange
                  ? (value: Fate) =>
                      onChange(changedPerson(person, { fate: value }))
                  : undefined
              }
              disabled={disabled}
            >
              <Select.Option value={Fate.Unknown}>
                unbekannt / keine Angabe
              </Select.Option>
              <Select.Option value={Fate.Murdered}>
                {displayFate(Fate.Murdered)}
              </Select.Option>
              <Select.Option value={Fate.Escaped}>
                {displayFate(Fate.Escaped)}
              </Select.Option>
              <Select.Option value={Fate.Survived}>
                {displayFate(Fate.Survived)}
              </Select.Option>
            </Select>
          ),
          edit: !!onChange,
          error: getError(person, "fate"),
          warning: getWarning(person, "fate"),
        }}
      />
      {fate !== Fate.Escaped ? null : (
        <DescriptionItem
          label="Geflohen nach"
          content={
            escapedToPlace === undefined || escapedToPlace instanceof Error
              ? escapedToPlace
              : {
                  nonEditable: escapedToPlace
                    ? escapedToPlace.label
                    : undefined,
                  editable: (
                    <NodeSelector
                      selected={escapedToPlace}
                      available={availablePlaces}
                      toLabel={(place) => place.label}
                      onChange={
                        !disabled && onChange
                          ? (selected) =>
                              onChange(
                                changedPerson(person, {
                                  escapedToPlaceId: selected
                                    ? selected.id
                                    : null,
                                })
                              )
                          : undefined
                      }
                      onView={() =>
                        escapedToPlace
                          ? setPlaceDrawer({
                              selected: escapedToPlace,
                              target: "escapedTo",
                              open: true,
                              edit: false,
                            })
                          : undefined
                      }
                      onCreate={() =>
                        setPlaceDrawer({
                          selected: newPlace(),
                          target: "escapedTo",
                          open: true,
                        })
                      }
                      placeholder="Ort wählen"
                    />
                  ),
                  edit: !!onChange,
                  error: getError(person, "escapedToPlaceId"),
                  warning: getWarning(person, "escapedToPlaceId"),
                }
          }
        />
      )}
      <DescriptionItem
        label={fate === Fate.Murdered ? "Datum der Ermordung" : "Todesdatum"}
        content={{
          nonEditable: displayDate(diedDate),
          editable: (
            <DateSelector
              value={diedDate}
              onChange={
                onChange
                  ? (diedDate) => onChange(changedPerson(person, { diedDate }))
                  : undefined
              }
            />
          ),
          edit: !!onChange,
          error: getError(person, "diedDate"),
          warning: getWarning(person, "diedDate"),
        }}
      />
      <DescriptionItem
        label={fate === Fate.Murdered ? "Ort der Ermordung" : "Todesort"}
        content={
          diedPlace === undefined || diedPlace instanceof Error
            ? diedPlace
            : {
                nonEditable: diedPlace ? diedPlace.label : undefined,
                editable: (
                  <NodeSelector
                    selected={diedPlace}
                    available={availablePlaces}
                    toLabel={(place) => place.label}
                    onChange={
                      !disabled && onChange
                        ? (selected) =>
                            onChange(
                              changedPerson(person, {
                                diedPlaceId: selected ? selected.id : null,
                              })
                            )
                        : undefined
                    }
                    onView={() =>
                      diedPlace
                        ? setPlaceDrawer({
                            selected: diedPlace,
                            target: "died",
                            open: true,
                            edit: false,
                          })
                        : undefined
                    }
                    onCreate={() =>
                      setPlaceDrawer({
                        selected: newPlace(),
                        target: "died",
                        open: true,
                      })
                    }
                    placeholder="Ort wählen"
                  />
                ),
                edit: !!onChange,
                error: getError(person, "diedPlaceId"),
                warning: getWarning(person, "diedPlaceId"),
              }
        }
      />
      {occupationsDrawer && (
        <OccupationsDrawer
          selected={occupationsDrawer.selected}
          visible={occupationsDrawer.open}
          edit={
            onChange &&
            (!isNode(occupationsDrawer.selected) || occupationsDrawer.edit)
              ? {
                  onChange: (changed) =>
                    setOccupationsDrawer({
                      ...occupationsDrawer,
                      selected: changed,
                    }),
                  onCommit: () => {
                    if (!isNode(occupationsDrawer.selected)) {
                      setOccupationsDrawer({
                        ...occupationsDrawer,
                        selected: occupations.create(
                          occupationsDrawer.selected,
                          (created) => {
                            setOccupationsDrawer({
                              ...occupationsDrawer,
                              selected: created,
                              open: hasCommitError(created),
                            });

                            if (!hasCommitError(created)) {
                              /* assert() */ if (!isNode(created))
                                throw new Error(
                                  "Object should have an id now."
                                );

                              onChange(
                                changedPerson(person, {
                                  occupationIds: [
                                    ...editable(person).occupationIds,
                                    created.id,
                                  ],
                                })
                              );
                            }
                          }
                        ),
                      });
                    } else {
                      setOccupationsDrawer({
                        ...occupationsDrawer,
                        selected: occupations.update(
                          occupationsDrawer.selected,
                          (updated) => {
                            setOccupationsDrawer({
                              ...occupationsDrawer,
                              selected: updated,
                              open:
                                hasCommitError(updated) ||
                                !!occupationsDrawer.selection,
                              edit: hasCommitError(updated),
                            });
                          }
                        ),
                      });
                    }
                  },
                }
              : undefined
          }
          select={
            occupationsDrawer.selection && {
              selection: occupationsDrawer.selection,
              onSelect: (selected) => {
                setOccupationsDrawer({ ...occupationsDrawer, selected });
              },
            }
          }
          onEditToggle={
            occupationsDrawer.edit !== undefined
              ? () =>
                  setOccupationsDrawer({
                    ...occupationsDrawer,
                    edit: !occupationsDrawer.edit,
                  })
              : undefined
          }
          onClose={() => {
            setOccupationsDrawer({ ...occupationsDrawer, open: false });
          }}
          resetTrigger={occupationsDrawerResetTrigger}
        />
      )}
      {placeDrawer && (
        <PlacesDrawer
          selected={placeDrawer.selected}
          visible={placeDrawer.open}
          edit={
            onChange && (!isNode(placeDrawer.selected) || placeDrawer.edit)
              ? {
                  onChange: (changed) =>
                    setPlaceDrawer({ ...placeDrawer, selected: changed }),
                  onCommit: () => {
                    if (!isNode(placeDrawer.selected)) {
                      setPlaceDrawer({
                        ...placeDrawer,
                        selected: places.create(
                          placeDrawer.selected,
                          (created) => {
                            setPlaceDrawer({
                              ...placeDrawer,
                              selected: created,
                              open: hasCommitError(created),
                            });

                            if (!hasCommitError(created)) {
                              /* assert() */ if (!isNode(created))
                                throw new Error(
                                  "Object should have an id now."
                                );

                              onChange(
                                changedPerson(
                                  person,
                                  placeDrawer.target === "born"
                                    ? { bornPlaceId: created.id }
                                    : placeDrawer.target === "escapedTo"
                                    ? { escapedToPlaceId: created.id }
                                    : { diedPlaceId: created.id }
                                )
                              );
                            }
                          }
                        ),
                      });
                    } else {
                      setPlaceDrawer({
                        ...placeDrawer,
                        selected: places.update(
                          placeDrawer.selected,
                          (updated) => {
                            setPlaceDrawer({
                              ...placeDrawer,
                              selected: updated,
                              open: hasCommitError(updated),
                              edit: hasCommitError(updated),
                            });
                          }
                        ),
                      });
                    }
                  },
                }
              : undefined
          }
          onEditToggle={
            placeDrawer.edit !== undefined
              ? () =>
                  setPlaceDrawer({ ...placeDrawer, edit: !placeDrawer.edit })
              : undefined
          }
          onClose={() => setPlaceDrawer({ ...placeDrawer, open: false })}
          resetTrigger={placeDrawerResetTrigger}
        />
      )}
    </>
  );
};
