import React, { useState, useEffect, useContext } from "react";
import { Button, Alert, Spin, Card } from "antd";
import { PlusOutlined } from "@ant-design/icons";

import { ITrigger } from "../hooks/useTrigger";
import { IGeoPoint } from "../interfaces/IGeoPoint";
import { ISpot, changedSpot } from "../interfaces/ISpot";
import { editable } from "../interfaces/IEditable";
import { revert } from "../interfaces/IRevertible";
import { isNode } from "../interfaces/INode";
import { isContent } from "../interfaces/IContent";
import { UserContext } from "../contexts/UserContext";

import { CommittablesDrawer } from "./CommittablesDrawer";
import { Description } from "./Description";
import { ContentGroup } from "./ContentGroup";
import { GeoPointDescription } from "./GeoPointDescription";
import { AddressDescription } from "./AddressDescription";
import { StolpersteineList } from "./StolpersteineList";
import { ImagesList } from "./ImagesList";

import "./PoiDrawer.css";

type Poi = ISpot | IGeoPoint;

export enum PoiDrawerContentGroup {
  COORDINATES = "coordinates",
  ADDRESS = "address",
  STOLPERSTEINE = "stolpersteine",
  IMAGES = "images",
}

interface PoiDrawerProps {
  selected: Poi | Error | undefined;

  //fade?: "in" | "out"; // to use before/after exchanging a visible POI with a different one (FIXME: ?)

  visible: boolean;

  activeContentGroup: PoiDrawerContentGroup;

  onActivateContentGroup: (group: PoiDrawerContentGroup) => void;

  edit?: {
    onChange: (changed: Poi) => void;

    onCommit: () => void; // if `selected` is of type IGeoPoint this will be called to have a new ISpot created from it
  };

  onEditToggle?: () => void;

  onStolpersteineChanged?: (ids: string[]) => void;

  onImagesChanged?: (imageIds: string[]) => void;

  onDeepDelete?: () => void;

  onClose: () => void;

  resetTrigger: ITrigger;
}

export const PoiDrawer: React.FC<PoiDrawerProps> = ({
  selected,
  //fade,
  visible,
  activeContentGroup,
  onActivateContentGroup,
  edit,
  onEditToggle,
  onStolpersteineChanged,
  onImagesChanged,
  onDeepDelete,
  onClose,
  resetTrigger,
}) => {
  const user = useContext(UserContext);

  const [subDrawerOpen, setSubDrawerOpen] = useState(false);

  const [addressLoading, setAddressLoading] = useState<boolean | Error>(false);

  useEffect(() => {
    setAddressLoading(false);
  }, [selected]);

  const [createStolperstein, setCreateStolperstein] = useState(false);

  const [createImage, setCreateImage] = useState(false);

  const loading = !selected || addressLoading === true;

  //const disabled = subDrawerOpen || loading;
  const disabled = !selected || loading; // FIXME: the above does not properly reset on sub-drawer close currently

  return (
    <CommittablesDrawer
      visible={visible}
      title="Markierung"
      primary={true}
      modal={false}
      busy={loading}
      disabled={disabled}
      committable={
        !selected ||
        selected instanceof Error ||
        selected.__typename === "GeoPoint"
          ? undefined
          : {
              selected,
              edit: edit && {
                onCommit: edit.onCommit,
                onRevert: () => edit.onChange(revert(selected)),
              },
              onEditToggle:
                !loading &&
                !disabled &&
                (activeContentGroup === PoiDrawerContentGroup.COORDINATES ||
                  activeContentGroup === PoiDrawerContentGroup.ADDRESS)
                  ? onEditToggle
                  : undefined,
              delete: onDeepDelete && {
                areYouSureText:
                  "Diesen Ort und alle dazugehörigen Stolpersteine und Personen wirklich löschen?",
                callback: onDeepDelete,
              },
            }
      }
      onClose={onClose}
      resetTrigger={resetTrigger}
    >
      {!selected ? (
        <Card bordered={false}>
          <Spin />
        </Card>
      ) : selected instanceof Error ? (
        <Alert type="error" message={selected.message} />
      ) : (
        <div>
          <ContentGroup
            key={PoiDrawerContentGroup.COORDINATES}
            header="Koordinaten"
            visible={true}
            expanded={activeContentGroup === PoiDrawerContentGroup.COORDINATES}
            focused={selected.__typename === "GeoPoint"}
            onExpandedChange={() =>
              onActivateContentGroup(PoiDrawerContentGroup.COORDINATES)
            }
          >
            <>
              <Description>
                <GeoPointDescription
                  point={
                    selected.__typename === "GeoPoint"
                      ? selected
                      : editable(selected).point
                  }
                  onChange={
                    edit &&
                    ((point) =>
                      edit.onChange(
                        selected.__typename === "GeoPoint"
                          ? point
                          : changedSpot(selected, { point })
                      ))
                  }
                />
              </Description>
              {user && selected.__typename === "GeoPoint" && edit && (
                <Button
                  type="dashed"
                  icon={<PlusOutlined />}
                  block
                  onClick={() => edit.onCommit()}
                  style={{ width: "100%", marginTop: 48 }}
                >
                  Markierung anlegen...
                </Button>
              )}
            </>
          </ContentGroup>
          <ContentGroup
            key={PoiDrawerContentGroup.ADDRESS}
            header="Adresse"
            visible={selected.__typename === "Spot"}
            expanded={activeContentGroup === PoiDrawerContentGroup.ADDRESS}
            onExpandedChange={() =>
              onActivateContentGroup(PoiDrawerContentGroup.ADDRESS)
            }
          >
            {selected.__typename === "Spot" && (
              <Description>
                <AddressDescription
                  spot={selected}
                  onChange={edit ? (spot) => edit.onChange(spot) : undefined}
                  onLoadingStateChange={(status) => setAddressLoading(status)}
                  onDrawerStateChange={setSubDrawerOpen}
                />
              </Description>
            )}
          </ContentGroup>
          <ContentGroup
            key={PoiDrawerContentGroup.STOLPERSTEINE}
            header="Stolpersteine"
            visible={selected.__typename === "Spot" && isNode(selected)}
            expanded={
              activeContentGroup === PoiDrawerContentGroup.STOLPERSTEINE
            }
            disabled={!!edit}
            actions={
              user &&
              !edit &&
              activeContentGroup === PoiDrawerContentGroup.STOLPERSTEINE &&
              onStolpersteineChanged
                ? [
                    {
                      icon: <PlusOutlined />,
                      label: "Stolperstein hinzufügen",
                      callback: () => setCreateStolperstein(true),
                    },
                  ]
                : undefined
            }
            onExpandedChange={() =>
              onActivateContentGroup(PoiDrawerContentGroup.STOLPERSTEINE)
            }
          >
            {selected.__typename === "Spot" && isContent(selected) ? (
              <StolpersteineList
                spotId={selected.id}
                create={createStolperstein}
                onCreateDone={() => setCreateStolperstein(false)}
                emptyListText="Es sind noch keine Stolpersteine zu diesem Ort gespeichert."
              />
            ) : undefined}
          </ContentGroup>
          <ContentGroup
            key={PoiDrawerContentGroup.IMAGES}
            header="Bilder"
            visible={selected.__typename === "Spot" && isNode(selected)}
            expanded={activeContentGroup === PoiDrawerContentGroup.IMAGES}
            disabled={!!edit}
            actions={
              user &&
              !edit &&
              activeContentGroup === PoiDrawerContentGroup.IMAGES &&
              onImagesChanged
                ? [
                    {
                      icon: <PlusOutlined />,
                      label: "Bild hinzufügen",
                      callback: () => setCreateImage(true),
                    },
                  ]
                : undefined
            }
            onExpandedChange={() =>
              onActivateContentGroup(PoiDrawerContentGroup.IMAGES)
            }
          >
            {selected.__typename === "Spot" && isContent(selected) ? (
              <ImagesList
                imageIds={selected.imageIds}
                create={createImage}
                onAbortCreate={() => setCreateImage(false)}
                onChange={
                  !onImagesChanged
                    ? undefined
                    : (imageIds) => {
                        setCreateImage(false);

                        onImagesChanged(imageIds);
                      }
                }
                emptyListText="Es sind noch keine Bilder zu diesem Ort gespeichert."
              />
            ) : undefined}
          </ContentGroup>
        </div>
      )}
    </CommittablesDrawer>
  );
};
