import React from "react";
import { Alert, Button, Divider, Select } from "antd";
import { PlusOutlined, ArrowRightOutlined } from "@ant-design/icons";

import { INode } from "../interfaces/INode";

interface NodesSelectorProps<T extends INode> {
  selected: T[];

  available: T[];

  toLabel: (node: T) => string;

  onChange?: (selected: T[]) => void; // if undefined the selector will be disabled

  onView?: () => void;

  onCreate?: () => void;

  placeholder?: string;
}

export const NodesSelector = <T extends INode>(
  props: NodesSelectorProps<T>
) => {
  const {
    selected,
    available,
    toLabel,
    onChange,
    onView,
    onCreate,
    placeholder,
  } = props;

  const toggle = onChange
    ? (id: string): void => {
        const index = selected.findIndex((node) => node.id === id);

        if (index === -1) {
          const toggled = available.find((node) => node.id === id);

          /* assert() */ if (!toggled)
            throw new Error("The selected node just dissappeared :(");

          onChange([...selected, toggled]);
        } else {
          onChange([...selected.slice(0, index), ...selected.slice(index + 1)]);
        }
      }
    : undefined;

  return available.length ? (
    <div style={{ width: "100%", display: "flex" }}>
      <Select
        style={{ flexGrow: 1 }}
        mode="multiple"
        placeholder={placeholder}
        notFoundContent="Keine Treffer"
        dropdownRender={(menu) => (
          <div>
            {menu}
            {onCreate && (
              <>
                <Divider style={{ margin: 0 }} />
                <div
                  style={{ display: "flex", flexWrap: "nowrap", padding: 8 }}
                >
                  <Button
                    style={{ width: "100%" }}
                    type="dashed"
                    onClick={onCreate}
                    disabled={!onChange}
                  >
                    <PlusOutlined />
                  </Button>
                </div>
              </>
            )}
          </div>
        )}
        value={selected.map((node) => node.id)}
        filterOption={(value, option) =>
          option?.props?.label
            ?.toString()
            .toLowerCase()
            .includes(value.toLowerCase()) || false
        }
        onSelect={toggle}
        onDeselect={toggle}
        disabled={!onChange}
      >
        {available
          .slice()
          .sort((a, b) => toLabel(a).localeCompare(toLabel(b)))
          .map((node, index) => (
            <Select.Option
              key={node.id + "-" + index}
              value={node.id}
              label={toLabel(node)}
            >
              {toLabel(node)}
            </Select.Option>
          ))}
      </Select>
      {onView && (
        <Button
          style={{ marginLeft: 8 }}
          onClick={onView}
          disabled={!onChange || !selected.length}
        >
          <ArrowRightOutlined />
        </Button>
      )}
    </div>
  ) : (
    <>
      <Alert
        type="warning"
        message="Es scheint hier aktuell noch keine Auswahl zu existieren."
      />
      {onCreate && (
        <Button
          block
          type="dashed"
          style={{ width: "100%" }}
          onClick={onCreate}
          disabled={!onChange}
        >
          <PlusOutlined />
        </Button>
      )}
    </>
  );
};
