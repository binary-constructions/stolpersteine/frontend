import React, { useMemo, useState } from "react";
import { Card, List, Typography, Pagination, Select, Space } from "antd";
import {
  UserOutlined,
  CameraOutlined,
  PictureOutlined,
  BorderOutlined,
} from "@ant-design/icons";
import classnames from "classnames";

import { Config } from "../globals/Config";
import { IAnnotatedSpotSummary } from "../interfaces/IAnnotatedSpotSummary";
import { useSpotList } from "../hooks/useSpotList";
import { Sorts } from "../globals/Sorts";

import { ExportToCsvButton } from "./ExportToCsvButton";
import { Nothing } from "./Nothing";

import "./SpotList.css";

interface SpotListProps {
  spots: undefined | IAnnotatedSpotSummary[];

  sort: keyof typeof Sorts;

  reverseSort?: undefined | boolean;

  onSelectSpot?: undefined | ((spot: IAnnotatedSpotSummary) => void);

  selectedSpotId?: undefined | string;
}

export const SpotList: React.FC<SpotListProps> = ({
  spots,
  sort,
  reverseSort = false,
  onSelectSpot,
  selectedSpotId,
}) => {
  const [spotCount, stolpersteinCount] = useMemo(
    () => [
      spots?.length || 0,
      spots?.reduce((a, c) => a + c.stolpersteinSummaries.length, 0) || 0,
    ],
    [spots]
  );

  const spotList = useSpotList(spots, sort, reverseSort);

  const [page, setPage] = useState({
    current: 1,
    size: 10,
  });

  return (
    <div
      className={classnames({
        SpotList: true,
        SpotList___selectable: !!onSelectSpot,
      })}
      style={{ display: "flex", flexDirection: "column", height: "100%" }}
    >
      <Card
        size="small"
        style={{
          height: 100,
          flex: "1 1 auto",
          overflowY: "auto",
          overflowX: "auto",
        }}
        bodyStyle={{
          padding: 0,
        }}
        bordered={false}
      >
        {!spotList ? (
          <List loading={true} />
        ) : !spotList.length ? (
          <Card size="small" bordered={false}>
            <Nothing text="Die Auswahl ist leer." />
          </Card>
        ) : (
          <>
            <List
              dataSource={
                (page.current - 1) * page.size >= spotCount
                  ? spotList
                  : spotList.slice(
                      (page.current - 1) * page.size,
                      page.current * page.size
                    )
              }
              itemLayout="vertical"
              split={false}
              renderItem={(v, i) => (
                <List.Item
                  className={
                    selectedSpotId && selectedSpotId === v.id
                      ? "SpotList_listItem SpotList_listItem___selected"
                      : "SpotList_listItem"
                  }
                  onClick={() => onSelectSpot && onSelectSpot(v)}
                >
                  <List.Item.Meta
                    title={
                      <>
                        {v._streetLabel || <i>ohne Straßenangabe</i>}
                        {v._placeLabel && (
                          <Typography.Text type="secondary">
                            {" "}
                            ({v._placeLabel})
                          </Typography.Text>
                        )}
                        {v.imageCount ? (
                          <>
                            {" "}
                            <PictureOutlined /> {v.imageCount}
                          </>
                        ) : null}
                      </>
                    }
                  />
                  {v.stolpersteinSummaries.length ? (
                    v.stolpersteinSummaries.map((w, j) => (
                      <div key={j}>
                        {w.personSummaries.length ? (
                          w.personSummaries.map((x, k) => (
                            <div key={k}>
                              <UserOutlined />{" "}
                              {w.hasImage ? <CameraOutlined /> : null}{" "}
                              {x._name?.displayName || <i>-- ohne Namen --</i>}
                              {(x._bornYear || x._diedYear) && (
                                <>
                                  <Typography.Text type="secondary">
                                    {" " +
                                      (x._bornYear ? "*" + x._bornYear : "") +
                                      " " +
                                      (x._diedYear
                                        ? "\u2020" + x._diedYear
                                        : "")}
                                  </Typography.Text>
                                  {x.imageCount ? (
                                    <>
                                      {" "}
                                      <PictureOutlined /> {x.imageCount}
                                    </>
                                  ) : null}
                                </>
                              )}
                            </div>
                          ))
                        ) : (
                          <>
                            <BorderOutlined />{" "}
                            {w.hasImage ? <CameraOutlined /> : null}{" "}
                            <i>-- Stein ohne Person --</i>
                          </>
                        )}
                      </div>
                    ))
                  ) : (
                    <i>-- keine Steine --</i>
                  )}
                </List.Item>
              )}
            />
          </>
        )}
      </Card>
      {spotCount && (
        <Card size="small" bordered={true}>
          <Space direction="vertical" style={{ width: "100%" }}>
            {spotCount <= Config.spotListPageSizes[0] ? null : (
              <div style={{ display: "flex" }}>
                <Pagination
                  simple
                  style={{
                    flex: "1 1",
                    display: "flex",
                    justifyContent: "flex-start",
                  }}
                  current={page.current}
                  pageSize={page.size}
                  responsive
                  showLessItems={true}
                  showSizeChanger={false}
                  total={spotCount}
                  onChange={(v) => setPage((w) => ({ ...w, current: v }))}
                />
                <div>
                  <Select
                    value={page.size}
                    size="small"
                    onSelect={(v) =>
                      setPage((w) => ({
                        current: Math.floor(((w.current - 1) * w.size) / v) + 1,
                        size: v,
                      }))
                    }
                  >
                    {Config.spotListPageSizes
                      .filter((v) => v <= spotCount)
                      .map((v, i) => (
                        <Select.Option key={i} value={v}>
                          {v} / Seite
                        </Select.Option>
                      ))}
                  </Select>
                </div>
              </div>
            )}
            <div
              style={{
                display: "flex",
                flexDirection: "row",
                alignItems: "center",
              }}
            >
              <div style={{ flex: "1 1" }}>
                <b>{stolpersteinCount}</b>{" "}
                {stolpersteinCount === 1 ? "Stolperstein" : "Stolpersteine"} an{" "}
                <b>{spotCount}</b> {spotCount === 1 ? "Ort" : "Orten"}
              </div>
              <ExportToCsvButton spotSummaries={spots} />
            </div>
          </Space>
        </Card>
      )}
    </div>
  );
};
