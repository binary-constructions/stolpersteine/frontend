import React, { useState } from "react";
import { Input, Upload, message } from "antd";
import { UploadOutlined } from "@ant-design/icons";

import { IImage, changedImage } from "../interfaces/IImage";
import { editable } from "../interfaces/IEditable";
import { isNode } from "../interfaces/INode";
import { getError, getWarning } from "../interfaces/IValidated";

import { DescriptionItem } from "./DescriptionItem";
import { Image } from "./Image";

interface ImageDescriptionProps<T extends IImage> {
  subject: T;

  onChange?: (changed: T) => void; // if undefined the subject will not be editable
}

export const ImageDescription = <T extends IImage>(
  props: ImageDescriptionProps<T>
) => {
  const { subject, onChange } = props;

  const {
    dataUrl,
    fileName,
    authorshipRemark,
    caption,
    alternativeText,
  } = editable(subject);

  const [loading, setLoading] = useState(false);

  return (
    <>
      <DescriptionItem
        label="Bild"
        content={{
          nonEditable: subject.path ? <Image url={subject.path} /> : undefined,
          editable: !onChange ? undefined : loading || dataUrl ? (
            <Image
              url={loading ? undefined : dataUrl || "not gonna happen"}
              onRemove={
                !loading
                  ? () =>
                      onChange(
                        changedImage(subject, { dataUrl: null, fileName: null })
                      )
                  : undefined
              }
            />
          ) : (
            <Upload.Dragger
              accept="image/*"
              listType="picture-card"
              beforeUpload={(file) => {
                if (file.type !== "image/jpeg" && file.type !== "image/png") {
                  message.error(
                    "Aktuell sind nur JPG- und PNG-Dateien erlaubt!"
                  );
                } else if (file.size / 1024 / 1024 > 10) {
                  message.error(
                    "Bild-Dateien müssen derzeit kleiner als 10MB sein!"
                  );
                } else {
                  setLoading(true);

                  const reader = new FileReader();

                  reader.addEventListener("loadend", () => {
                    setLoading(false);

                    onChange(
                      changedImage(subject, {
                        dataUrl:
                          reader.result && typeof reader.result === "string"
                            ? reader.result
                            : null,
                        fileName: file.name,
                      })
                    );
                  });

                  reader.readAsDataURL(file);
                }

                return false;
              }}
              multiple={false}
            >
              <p className="ant-upload-drag-icon">
                <UploadOutlined />
              </p>
              <p className="ant-upload-text">
                {!isNode(subject) ? "Bilddatei" : "Neue Bilddatei"}
              </p>
              <p className="ant-upload-hint">
                {!isNode(subject)
                  ? "Zum Auswählen hier klicken oder ein Bild in diesen Bereich ziehen."
                  : "Falls nicht gesetzt wird das derzeitige Bild beibehalten."}
              </p>
            </Upload.Dragger>
          ),
          edit: !!onChange,
          error: getError(subject, "data"),
          warning: getWarning(subject, "data"),
          required: !isNode(subject),
        }}
      />
      <DescriptionItem
        label="Dateiname"
        content={{
          nonEditable: fileName ? fileName.trim() : undefined,
          editable: (
            <Input
              value={fileName || ""}
              onChange={
                onChange
                  ? ({ target: { value } }) =>
                      onChange(changedImage(subject, { fileName: value }))
                  : undefined
              }
            />
          ),
          edit: !!onChange,
          error: getError(subject, "fileName"),
          warning: getWarning(subject, "fileName"),
          required: !isNode(subject),
        }}
      />
      <DescriptionItem
        label="Urheberschaftsvermerk"
        content={{
          nonEditable: authorshipRemark.trim() || undefined,
          editable: (
            <Input
              value={authorshipRemark}
              onChange={
                onChange
                  ? ({ target: { value } }) =>
                      onChange(
                        changedImage(subject, { authorshipRemark: value })
                      )
                  : undefined
              }
            />
          ),
          edit: !!onChange,
          error: getError(subject, "authorshipRemark"),
          warning: getWarning(subject, "authorshipRemark"),
          required: true,
        }}
      />
      <DescriptionItem
        label="Bildunterschrift"
        content={{
          nonEditable: caption.trim() || undefined,
          editable: (
            <Input
              value={caption}
              onChange={
                onChange
                  ? ({ target: { value } }) =>
                      onChange(changedImage(subject, { caption: value }))
                  : undefined
              }
            />
          ),
          edit: !!onChange,
          error: getError(subject, "caption"),
          warning: getWarning(subject, "caption"),
        }}
      />
      <DescriptionItem
        label="Alternativ-Text"
        content={{
          nonEditable: alternativeText.trim() || undefined,
          editable: (
            <Input
              value={alternativeText}
              onChange={
                onChange
                  ? ({ target: { value } }) =>
                      onChange(
                        changedImage(subject, { alternativeText: value })
                      )
                  : undefined
              }
            />
          ),
          edit: !!onChange,
          error: getError(subject, "alternativeText"),
          warning: getWarning(subject, "alternativeText"),
        }}
      />
    </>
  );
};
