import React, { useState, useEffect } from "react";

import { useSummariesQuery } from "../backendApi";
import { useTrigger } from "../hooks/useTrigger";
import { IAnnotatedSpotSummary } from "../interfaces/IAnnotatedSpotSummary";
import { IGeoPoint } from "../interfaces/IGeoPoint";
import {
  ISpot,
  newSpot,
  changedSpot,
  withLocalStolpersteineUpdate,
} from "../interfaces/ISpot";
import { isNode } from "../interfaces/INode";
import { isContent } from "../interfaces/IContent";
import { hasCommitError } from "../interfaces/ICommittable";
import { useSpots } from "../hooks/useSpots";

import { PoiDrawer, PoiDrawerContentGroup } from "./PoiDrawer";

interface PoiDrawerControllerProps {
  selected: undefined | IGeoPoint | IAnnotatedSpotSummary | ISpot;

  onUpdate: (updated: IGeoPoint | ISpot) => void;

  editToggleState: boolean;

  onEditToggleStateChange: (state: boolean) => void;

  onClose: () => void;
}

export const PoiDrawerController: React.FC<PoiDrawerControllerProps> = ({
  selected,
  onUpdate,
  editToggleState,
  onEditToggleStateChange,
  onClose,
}) => {
  const { refetch: refetchSummaries } = useSummariesQuery();

  const spotId =
    selected && "id" in selected && selected.id ? selected.id : undefined;

  const spots = useSpots(spotId ? [spotId] : []);

  const [poi, setPoi] = useState<undefined | Error | IGeoPoint | ISpot>(
    new Error("Es wurd noch kein Ort ausgewählt.")
  );

  useEffect(() => {
    if (selected && selected.__typename === "SpotSummary") {
      const loaded = spots.nodesById[selected.id];

      if (loaded !== poi) {
        if (!loaded || loaded instanceof Error) {
          setPoi(loaded);
        } else {
          onUpdate(loaded);
        }
      }
    } else if (selected !== poi) {
      setPoi(selected);
    }
  }, [selected, spots.nodesById, poi, onUpdate]);

  const [activeContentGroup, setActiveContentGroup] = useState<
    PoiDrawerContentGroup
  >(PoiDrawerContentGroup.COORDINATES);

  const poiDrawerResetTrigger = useTrigger();

  poiDrawerResetTrigger.listen(() => onClose());

  return (
    <PoiDrawer
      selected={poi}
      visible={!!selected}
      activeContentGroup={activeContentGroup}
      onActivateContentGroup={(group) => setActiveContentGroup(group)}
      edit={
        poi &&
        !(poi instanceof Error) &&
        (poi.__typename === "GeoPoint" || !isNode(poi) || editToggleState)
          ? {
              onChange: onUpdate,
              onCommit: ((poiMemo) => () => {
                if (poiMemo.__typename === "GeoPoint") {
                  onUpdate(newSpot(poiMemo));

                  setActiveContentGroup(PoiDrawerContentGroup.ADDRESS);

                  onEditToggleStateChange(true);
                } else {
                  if (!isContent(poiMemo)) {
                    onUpdate(
                      spots.create(poiMemo, (created) => {
                        onUpdate(created);

                        if (!hasCommitError(created)) {
                          onEditToggleStateChange(false);
                        }

                        refetchSummaries();
                      })
                    );
                  } else {
                    onUpdate(
                      spots.update(poiMemo, (updated) => {
                        onUpdate(updated);

                        if (!hasCommitError(updated)) {
                          onEditToggleStateChange(false);
                        }
                      })
                    );
                  }
                }
              })(poi),
            }
          : undefined
      }
      onEditToggle={() => onEditToggleStateChange(!editToggleState)}
      onStolpersteineChanged={
        poi &&
        !(poi instanceof Error) &&
        poi.__typename === "Spot" &&
        isNode(poi)
          ? ((spotMemo) => (ids: string[]) => {
              // FIXME: ?
              onUpdate(withLocalStolpersteineUpdate(spotMemo, ids));

              refetchSummaries();
            })(poi)
          : undefined
      }
      onImagesChanged={
        poi &&
        !(poi instanceof Error) &&
        poi.__typename === "Spot" &&
        isContent(poi)
          ? ((spotMemo) => (imageIds: string[]) =>
              onUpdate(
                spots.update(changedSpot(spotMemo, { imageIds }), onUpdate)
              ))(poi)
          : undefined
      }
      onDeepDelete={
        poi &&
        !(poi instanceof Error) &&
        poi.__typename === "Spot" &&
        isContent(poi)
          ? ((spotMemo) => () =>
              onUpdate(
                spots.deepDelete(spotMemo, (v) => {
                  onUpdate(v);

                  if (!hasCommitError(v)) {
                    onClose();
                  }

                  refetchSummaries();
                })
              ))(poi)
          : undefined
      }
      onClose={onClose}
      resetTrigger={poiDrawerResetTrigger}
    />
  );
};
