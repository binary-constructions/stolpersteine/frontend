import React from "react";
import { Alert, Form } from "antd";

import { AppearTransition } from "./AppearTransition";
import { ReplaceTransition } from "./ReplaceTransition";
import { Nothing } from "./Nothing";
import { Incoming } from "./Incoming";

interface DescriptionItemProps {
  label: string;

  content:
    | undefined
    | Error
    | {
        // undefined means loading
        nonEditable?: React.ReactNode;

        nonEditableEmptyText?: string;

        editable?: React.ReactNode;

        edit?: boolean;

        hideOnEmpty?: boolean;

        required?: boolean;

        error?: string;

        warning?: string;
      };
}

export const DescriptionItem: React.FC<DescriptionItemProps> = ({
  label,
  content,
}) => (
  <AppearTransition
    show={
      !content ||
      content instanceof Error ||
      !content.hideOnEmpty ||
      (!content.edit && !!content.nonEditable) ||
      (!!content.edit && !!content.editable)
    }
  >
    <Form.Item
      label={label}
      required={
        !!content &&
        !(content instanceof Error) &&
        content.edit &&
        content.required
      }
      help={
        !!content && !(content instanceof Error) && content.edit
          ? content.error || content.warning
          : undefined
      }
      validateStatus={
        !!content && !(content instanceof Error) && content.edit
          ? content.error !== undefined
            ? "error"
            : content.warning !== undefined
            ? "warning"
            : undefined
          : undefined
      }
    >
      <Incoming loading={!content}>
        {content &&
          (content instanceof Error ? (
            <Alert type="error" message={content.message} />
          ) : (
            <ReplaceTransition
              one={
                content.nonEditable || (
                  <Nothing
                    text={content.nonEditableEmptyText || "- keine Angaben -"}
                  />
                )
              }
              other={content.editable || "-"}
              showOther={content.edit || false}
            />
          ))}
      </Incoming>
    </Form.Item>
  </AppearTransition>
);
