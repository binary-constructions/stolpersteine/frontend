import React from "react";

import { IGeoPoint, changedGeoPoint } from "../interfaces/IGeoPoint";
import { editable } from "../interfaces/IEditable";
import { getError, getWarning } from "../interfaces/IValidated";

import { DescriptionItem } from "./DescriptionItem";
import { GeoCoordinateInput } from "./GeoCoordinateInput";

interface GeoPointDescriptionProps<T extends IGeoPoint> {
  point: T;

  onChange?: (point: T) => void; // if undefined the point will not be editable
}

export const GeoPointDescription = <T extends IGeoPoint>(
  props: GeoPointDescriptionProps<T>
) => {
  const { point, onChange } = props;

  const { longitude, latitude } = editable(point);

  return (
    <>
      <DescriptionItem
        label="Längengrad"
        content={{
          nonEditable: longitude,
          editable: (
            <GeoCoordinateInput
              value={longitude}
              onChange={
                onChange
                  ? (value) =>
                      onChange(changedGeoPoint(point, { longitude: value }))
                  : undefined
              }
            />
          ),
          edit: !!onChange,
          error: getError(point, "longitude"),
          warning: getWarning(point, "longitude"),
        }}
      />
      <DescriptionItem
        label="Breitengrad"
        content={{
          nonEditable: latitude,
          editable: (
            <GeoCoordinateInput
              value={latitude}
              onChange={
                onChange
                  ? (value) =>
                      onChange(changedGeoPoint(point, { latitude: value }))
                  : undefined
              }
            />
          ),
          edit: !!onChange,
          error: getError(point, "latitude"),
          warning: getWarning(point, "latitude"),
        }}
      />
    </>
  );
};
