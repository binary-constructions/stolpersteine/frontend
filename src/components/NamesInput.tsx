import React from "react";
import { Button, Input, Select } from "antd";
import { MinusOutlined, PlusOutlined } from "@ant-design/icons";

type NameType =
  | "GivenName"
  | "Nickname"
  | "Pseudonym"
  | "FamilyName"
  | "BirthName"
  | "MarriedName";

type Name =
  | {
      __typename: "GivenName";

      value: string;
    }
  | {
      __typename: "Nickname";

      value: string;
    }
  | {
      __typename: "Pseudonym";

      value: string;
    }
  | {
      __typename: "FamilyName";

      value: string;
    }
  | {
      __typename: "BirthName";

      value: string;
    }
  | {
      __typename: "MarriedName";

      value: string;
    };

const nameSelect = (
  selected: NameType,
  onSelect: (selected: NameType) => void,
  disabled = false
) => (
  <Select
    value={selected}
    disabled={disabled}
    onChange={(value: NameType) => onSelect(value)}
    style={{ width: "16ex" }}
  >
    <Select.Option value="GivenName">Vorname</Select.Option>
    <Select.Option value="Nickname">Spitzname</Select.Option>
    <Select.Option value="FamilyName">Nachname</Select.Option>
    <Select.Option value="BirthName">Geburtsname</Select.Option>
    <Select.Option value="MarriedName">Heiratsname</Select.Option>
    <Select.Option value="Pseudonym">Pseudonym</Select.Option>
  </Select>
);

interface NamesInputProps {
  names: Name[];

  onChange?: (values: Name[]) => void;
}

export const NamesInput: React.FC<NamesInputProps> = (props) => {
  const { names, onChange } = props;

  return (
    <div style={{ width: "100%" }}>
      {names.map((name, index) => (
        <Input
          key={index}
          value={name.value}
          onChange={
            onChange
              ? ({ target: { value } }) =>
                  onChange([
                    ...names.slice(0, index),
                    { ...name, value },
                    ...names.slice(index + 1),
                  ])
              : undefined
          }
          addonBefore={
            onChange
              ? nameSelect(
                  name.__typename,
                  (type) =>
                    onChange([
                      ...names.slice(0, index),
                      { ...name, __typename: type },
                      ...names.slice(index + 1),
                    ]),
                  !onChange
                )
              : undefined
          }
          addonAfter={
            onChange ? (
              <MinusOutlined
                onClick={() =>
                  onChange([
                    ...names.slice(0, index),
                    ...names.slice(index + 1),
                  ])
                }
              />
            ) : undefined
          }
          disabled={!onChange}
        />
      ))}
      <Button
        block
        type="dashed"
        style={{ width: "100%" }}
        onClick={
          onChange
            ? () => onChange([...names, { __typename: "GivenName", value: "" }])
            : undefined
        }
        disabled={!onChange}
      >
        <PlusOutlined />
      </Button>
    </div>
  );
};
