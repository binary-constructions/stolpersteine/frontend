import React, { useState, useContext } from "react";
import { Button, Input } from "antd";
import { PlusOutlined } from "@ant-design/icons";

import { useDisabled } from "../hooks/useDisabled";
import { useImages } from "../hooks/useImages";
import { usePlaces } from "../hooks/usePlaces";
import { useStateMonitor } from "../hooks/useStateMonitor";
import { useTrigger } from "../hooks/useTrigger";
import { DescriptionItem } from "./DescriptionItem";
import { SquareCard } from "./SquareCard";
import {
  IStolperstein,
  changedStolperstein,
} from "../interfaces/IStolperstein";
import { IImage, newImage } from "../interfaces/IImage";
import { IPlace, newPlace } from "../interfaces/IPlace";
import { isNode } from "../interfaces/INode";
import { hasCommitError } from "../interfaces/ICommittable";
import { editable } from "../interfaces/IEditable";
import { getError, getWarning } from "../interfaces/IValidated";
import { displayDate } from "../helpers/displayDate";
import { displayStreet } from "../helpers/displayStreet";
import { UserContext } from "../contexts/UserContext";
import { nodeStoreGetAvailable } from "../helpers/nodeStoreGetAvailable";

import { DateSelector } from "./DateSelector";
import { HistoricalStreetInput } from "./HistoricalStreetInput";
import { NodeSelector } from "./NodeSelector";
import { Image } from "./Image";
import { RichTextEditor } from "./RichTextEditor";
import { ImagesDrawer } from "./ImagesDrawer";
import { PlacesDrawer } from "./PlacesDrawer";

interface StolpersteinDescriptionProps<T extends IStolperstein> {
  stolperstein: T;

  onChange?: (stolperstein: T) => void; // if undefined the Stolperstein will not be editable

  focus: boolean;

  onFocusChange: (focus: boolean) => void;

  onLoadingStateChange?: (loading: boolean | Error) => void;

  onDrawerStateChange?: (open: boolean) => void;
}

export const StolpersteinDescription = <T extends IStolperstein>(
  props: StolpersteinDescriptionProps<T>
) => {
  const {
    stolperstein,
    onChange,
    focus,
    onFocusChange,
    onLoadingStateChange,
    onDrawerStateChange,
  } = props;

  const {
    laid,
    inscription,
    imageId,
    historicalStreet,
    historicalStreetDetail,
    historicalPlaceId,
    initiativeHTML,
    patronageHTML,
    dataGatheringHTML,
    fundingHTML,
  } = editable(stolperstein);

  const user = useContext(UserContext);

  const disabled = useDisabled();

  const images = useImages(imageId ? [imageId] : []);

  const image = imageId === null ? null : images.nodesById[imageId];

  const [imageDrawer, setImageDrawer] = useState<
    undefined | { image: IImage; open: boolean; edit?: boolean }
  >();

  const imageDrawerResetTrigger = useTrigger();

  imageDrawerResetTrigger.listen(() => setImageDrawer(undefined));

  const places = usePlaces();

  const historicalPlace =
    historicalPlaceId === null ? null : places.nodesById[historicalPlaceId];

  const availablePlaces = nodeStoreGetAvailable(places);

  /* place drawer */

  const [placeDrawer, setPlaceDrawer] = useState<
    undefined | { place: IPlace; open: boolean; edit?: boolean }
  >();

  const placeDrawerResetTrigger = useTrigger();

  placeDrawerResetTrigger.listen(() => setPlaceDrawer(undefined));

  /* loading state */

  useStateMonitor(
    !image || !historicalPlace
      ? true
      : image instanceof Error
      ? image
      : historicalPlace instanceof Error
      ? historicalPlace
      : false,
    onLoadingStateChange
  );

  /* drawer state */

  useStateMonitor(
    (imageDrawer && imageDrawer.open) || (placeDrawer && placeDrawer.open),
    onDrawerStateChange
  );

  return (
    <>
      <DescriptionItem
        label="Verlegt"
        content={{
          nonEditable: displayDate(laid),
          editable: (
            <DateSelector
              value={laid}
              onChange={
                onChange && !disabled
                  ? (laid) =>
                      onChange(changedStolperstein(stolperstein, { laid }))
                  : undefined
              }
            />
          ),
          edit: !!onChange,
          error: getError(stolperstein, "laid"),
          warning: getWarning(stolperstein, "laid"),
        }}
      />
      <DescriptionItem
        label="Bild"
        content={{
          nonEditable: image ? (
            <Image
              url={image && (image instanceof Error ? image : image.path)}
              onOpen={
                image && !(image instanceof Error)
                  ? () =>
                      setImageDrawer({
                        image: image,
                        open: true,
                        edit: false,
                      })
                  : undefined
              }
              onRemove={
                onChange && image && !(image instanceof Error)
                  ? () =>
                      onChange(
                        changedStolperstein(stolperstein, { imageId: null })
                      )
                  : undefined
              }
            />
          ) : user && onChange ? (
            <Button
              block
              type="dashed"
              icon={<PlusOutlined />}
              onClick={() => setImageDrawer({ image: newImage(), open: true })}
            />
          ) : undefined,
          nonEditableEmptyText: "- kein Bild -",
          error: getError(stolperstein, "imageId"),
          warning: getWarning(stolperstein, "imageId"),
        }}
      />
      <DescriptionItem
        label="Inschrift"
        content={{
          nonEditable: inscription ? (
            <SquareCard>
              <div
                style={{ width: "100%", textAlign: "center", padding: "12px" }}
              >
                {inscription.split("\n").map((line, key) => (
                  <span key={key}>
                    {line}
                    <br />
                  </span>
                ))}
              </div>
            </SquareCard>
          ) : undefined,
          editable: (
            <Input.TextArea
              style={{ textAlign: "center" }}
              rows={8}
              value={inscription}
              disabled={disabled}
              onChange={
                onChange
                  ? ({ target: { value } }) =>
                      onChange(
                        changedStolperstein(stolperstein, {
                          inscription: value,
                        })
                      )
                  : undefined
              }
            />
          ),
          edit: !!onChange,
          error: getError(stolperstein, "inscription"),
          warning: getWarning(stolperstein, "inscription"),
        }}
      />
      <DescriptionItem
        label="Historische Straße"
        content={{
          nonEditable: displayStreet(
            historicalStreet,
            historicalStreetDetail.value,
            historicalStreetDetail.__typename
          ),
          editable: (
            <HistoricalStreetInput
              stolperstein={stolperstein}
              onChange={onChange}
            />
          ),
          edit: !!onChange,
          error:
            getError(stolperstein, "historicalStreet") ||
            getError(stolperstein, "historicalStreetDetail"),
          warning:
            getWarning(stolperstein, "historicalStreet") ||
            getWarning(stolperstein, "historicalStreetDetail"),
        }}
      />
      <DescriptionItem
        label="Historischer Ort"
        content={
          historicalPlace === undefined || historicalPlace instanceof Error
            ? historicalPlace
            : !availablePlaces || availablePlaces instanceof Error
            ? availablePlaces
            : {
                nonEditable: historicalPlace
                  ? historicalPlace.label
                  : undefined,
                editable: (
                  <NodeSelector
                    selected={historicalPlace}
                    available={availablePlaces}
                    toLabel={(historicalPlace) => historicalPlace.label}
                    onChange={
                      !disabled && onChange
                        ? (selected) =>
                            onChange(
                              changedStolperstein(stolperstein, {
                                historicalPlaceId: selected
                                  ? selected.id
                                  : null,
                              })
                            )
                        : undefined
                    }
                    onView={() =>
                      historicalPlace
                        ? setPlaceDrawer({
                            place: historicalPlace,
                            open: true,
                            edit: false,
                          })
                        : undefined
                    }
                    onCreate={() =>
                      setPlaceDrawer({ place: newPlace(), open: true })
                    }
                    placeholder="Ort wählen"
                  />
                ),
                edit: !!onChange,
                error: getError(stolperstein, "historicalPlaceId"),
                warning: getWarning(stolperstein, "historicalPlaceId"),
              }
        }
      />
      <DescriptionItem
        label="Initiative"
        content={{
          nonEditable:
            initiativeHTML || onChange ? (
              <RichTextEditor
                value={initiativeHTML}
                onFocusChange={
                  onChange
                    ? (focus, value) => {
                        if (focus) {
                          onFocusChange(true);
                        } else if (value !== undefined) {
                          onFocusChange(false);

                          onChange(
                            changedStolperstein(stolperstein, {
                              initiativeHTML: value,
                            })
                          );
                        }
                      }
                    : undefined
                }
                placeholder="Personen/Gruppen, welche die Stolperstein-Verlegung initiiert haben."
              />
            ) : undefined,
          error: getError(stolperstein, "initiativeHTML"),
          warning: getWarning(stolperstein, "initiativeHTML"),
        }}
      />
      <DescriptionItem
        label="Patenschaft"
        content={{
          nonEditable:
            patronageHTML || onChange ? (
              <RichTextEditor
                value={patronageHTML}
                onFocusChange={
                  onChange
                    ? (focus, value) => {
                        if (focus) {
                          onFocusChange(true);
                        } else if (value !== undefined) {
                          onFocusChange(false);

                          onChange(
                            changedStolperstein(stolperstein, {
                              patronageHTML: value,
                            })
                          );
                        }
                      }
                    : undefined
                }
                placeholder="Personen/Gruppen, die eine Patenschaft für den Stolperstein übernommen haben."
              />
            ) : undefined,
          error: getError(stolperstein, "patronageHTML"),
          warning: getWarning(stolperstein, "patronageHTML"),
        }}
      />
      <DescriptionItem
        label="Datensammlung"
        content={{
          nonEditable:
            dataGatheringHTML || onChange ? (
              <RichTextEditor
                value={dataGatheringHTML}
                onFocusChange={
                  onChange
                    ? (focus, value) => {
                        if (focus) {
                          onFocusChange(true);
                        } else if (value !== undefined) {
                          onFocusChange(false);

                          onChange(
                            changedStolperstein(stolperstein, {
                              dataGatheringHTML: value,
                            })
                          );
                        }
                      }
                    : undefined
                }
                placeholder="Personen/Gruppen, die Recherchen im Zusammenhang mit der Stolperstein-Verlegung unternommen haben."
              />
            ) : undefined,
          error: getError(stolperstein, "dataGatheringHTML"),
          warning: getWarning(stolperstein, "dataGatheringHTML"),
        }}
      />
      <DescriptionItem
        label="Finanzierung"
        content={{
          nonEditable:
            fundingHTML || onChange ? (
              <RichTextEditor
                value={fundingHTML}
                onFocusChange={
                  onChange
                    ? (focus, value) => {
                        if (focus) {
                          onFocusChange(true);
                        } else if (value !== undefined) {
                          onChange(
                            changedStolperstein(stolperstein, {
                              fundingHTML: value,
                            })
                          );
                        }
                      }
                    : undefined
                }
                placeholder="Personen/Gruppen, welche die Stolperstein-Verlegung finanziert haben."
              />
            ) : undefined,
          error: getError(stolperstein, "fundingHTML"),
          warning: getWarning(stolperstein, "fundingHTML"),
        }}
      />
      {imageDrawer && (
        <ImagesDrawer
          selected={imageDrawer.image}
          visible={imageDrawer.open}
          edit={
            !isNode(imageDrawer.image) || imageDrawer.edit
              ? {
                  onChange: (changed) =>
                    setImageDrawer({ ...imageDrawer, image: changed }),
                  onCommit: () => {
                    if (!isNode(imageDrawer.image)) {
                      /* assert() */ if (!onChange)
                        throw new Error("How did this new image get created?");

                      setImageDrawer({
                        ...imageDrawer,
                        image: images.create(imageDrawer.image, (created) => {
                          setImageDrawer({
                            image: created,
                            open: hasCommitError(created),
                          });

                          if (!hasCommitError(created)) {
                            /* assert() */ if (!isNode(created))
                              throw new Error("Object should have an id now.");

                            onChange(
                              changedStolperstein(stolperstein, {
                                imageId: created.id,
                              })
                            );
                          }
                        }),
                      });
                    } else {
                      setImageDrawer({
                        ...imageDrawer,
                        image: images.update(imageDrawer.image, (updated) => {
                          setImageDrawer({
                            image: updated,
                            open: hasCommitError(updated),
                            edit: hasCommitError(updated),
                          });
                        }),
                      });
                    }
                  },
                }
              : undefined
          }
          onEditToggle={
            imageDrawer.edit !== undefined
              ? () =>
                  setImageDrawer({ ...imageDrawer, edit: !imageDrawer.edit })
              : undefined
          }
          onClose={() => setImageDrawer({ ...imageDrawer, open: false })}
          resetTrigger={imageDrawerResetTrigger}
        />
      )}
      {placeDrawer && (
        <PlacesDrawer
          selected={placeDrawer.place}
          visible={placeDrawer.open}
          edit={
            onChange && (!isNode(placeDrawer.place) || placeDrawer.edit)
              ? {
                  onChange: (changed) =>
                    setPlaceDrawer({ ...placeDrawer, place: changed }),
                  onCommit: () => {
                    if (!isNode(placeDrawer.place)) {
                      setPlaceDrawer({
                        ...placeDrawer,
                        place: places.create(placeDrawer.place, (created) => {
                          setPlaceDrawer({
                            place: created,
                            open: hasCommitError(created),
                          });

                          if (!hasCommitError(created)) {
                            if (!isNode(created))
                              throw new Error("Object should have an id now."); // assert()

                            onChange(
                              changedStolperstein(stolperstein, {
                                historicalPlaceId: created.id,
                              })
                            );
                          }
                        }),
                      });
                    } else {
                      setPlaceDrawer({
                        ...placeDrawer,
                        place: places.update(placeDrawer.place, (updated) => {
                          setPlaceDrawer({
                            place: updated,
                            open: hasCommitError(updated),
                            edit: hasCommitError(updated),
                          });
                        }),
                      });
                    }
                  },
                }
              : undefined
          }
          onEditToggle={
            placeDrawer.edit !== undefined
              ? () =>
                  setPlaceDrawer({ ...placeDrawer, edit: !placeDrawer.edit })
              : undefined
          }
          onClose={() => setPlaceDrawer({ ...placeDrawer, open: false })}
          resetTrigger={placeDrawerResetTrigger}
        />
      )}
    </>
  );
};
