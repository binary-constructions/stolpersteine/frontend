import React from "react";
import { Button } from "antd";
import { DownloadOutlined } from "@ant-design/icons";
import { ExportToCsv } from "export-to-csv";

import { IAnnotatedSpotSummary } from "../interfaces/IAnnotatedSpotSummary";

interface ExportToCsvButtonProps {
  spotSummaries?: undefined | IAnnotatedSpotSummary[];
}

const toCsv = (data: IAnnotatedSpotSummary[]): string =>
  new ExportToCsv({
    fieldSeparator: ",",
    quoteStrings: '"',
    decimalSeparator: ".",
    showLabels: true,
    useTextFile: false,
    useBom: false,
    useKeysAsHeaders: true,
    // headers: ['Column 1', 'Column 2', etc...] <-- Won't work with useKeysAsHeaders present!
  }).generateCsv(
    data.map((v) => ({
      //"Ort/ID": v.id,
      Latitude: v.point.latitude,
      Longitude: v.point.longitude,
      PLZ: v.postalCode || "-",
      Ort: v._placeLabel || "-",
      Strasse: v._streetLabel || "-",
      Verlegedatum: v.stolpersteinSummaries[0]?.laid || "-",
      Foto: v.stolpersteinSummaries[0]?.hasImage ? "ja" : "nein" || "nein",
      Name:
        v.stolpersteinSummaries[0]?.personSummaries[0]?._name?.displayName ||
        "-",
      Geboren: v.stolpersteinSummaries[0]?.personSummaries[0]?.bornDate || "-",
      Gestorben:
        v.stolpersteinSummaries[0]?.personSummaries[0]?.diedDate || "-",
    }))
  );

const toCsvDownload = (csv: string): void => {
  const a = document.createElement("a");

  const file = new Blob([csv], { type: "text/csv" });

  a.href = URL.createObjectURL(file);

  a.download = "Stolpersteine-in-Brandenburg.csv";

  document.body.appendChild(a);
};

export const ExportToCsvButton: React.FC<ExportToCsvButtonProps> = ({
  spotSummaries,
}) => {
  return (
    <Button
      icon={<DownloadOutlined />}
      style={{ marginLeft: 12 }}
      disabled={!spotSummaries || !spotSummaries.length}
      onClick={
        spotSummaries
          ? () =>
              toCsvDownload(
                toCsv(
                  spotSummaries.reduce<IAnnotatedSpotSummary[]>((a, c) => {
                    if (!c.stolpersteinSummaries.length) {
                      a.push(c);
                    } else {
                      c.stolpersteinSummaries.forEach((v) => {
                        if (v.personSummaries.length < 2) {
                          a.push({ ...c, stolpersteinSummaries: [v] });
                        } else {
                          v.personSummaries.forEach((w) => {
                            a.push({
                              ...c,
                              stolpersteinSummaries: [
                                { ...v, personSummaries: [w] },
                              ],
                            });
                          });
                        }
                      });
                    }

                    return a;
                  }, [])
                )
              )
          : undefined
      }
    />
  );
};
