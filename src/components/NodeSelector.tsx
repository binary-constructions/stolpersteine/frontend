import React from "react";
import { Alert, Button, Divider, Select } from "antd";
import { PlusOutlined, ArrowRightOutlined } from "@ant-design/icons";

import { INode } from "../interfaces/INode";

interface NodeSelectorProps<T extends INode> {
  selected?: T | null;

  available: T[];

  toLabel: (node: T) => string;

  onChange?: (selected?: T) => void; // if undefined the selector will be disabled

  onView?: () => void;

  onCreate?: () => void;

  placeholder?: string;
}

export const NodeSelector = <T extends INode>(props: NodeSelectorProps<T>) => {
  const {
    selected = null,
    available,
    toLabel,
    onChange,
    onView,
    onCreate,
    placeholder,
  } = props;

  return available.length ? (
    <div style={{ width: "100%", display: "flex" }}>
      <Select
        showSearch
        style={{ flexGrow: 1 }}
        placeholder={placeholder}
        notFoundContent="Keine Treffer"
        dropdownRender={(menu) => (
          <div>
            {menu}
            {onCreate && (
              <>
                <Divider style={{ margin: 0 }} />
                <div
                  style={{ display: "flex", flexWrap: "nowrap", padding: 8 }}
                >
                  <Button
                    style={{ width: "100%" }}
                    type="dashed"
                    onClick={onCreate}
                    disabled={!onChange}
                  >
                    <PlusOutlined />
                  </Button>
                </div>
              </>
            )}
          </div>
        )}
        value={selected ? selected.id : undefined}
        filterOption={(value, option) =>
          option?.props?.label
            ?.toString()
            .toLowerCase()
            .includes(value.toLowerCase()) || false
        }
        onSelect={
          onChange
            ? (id: string) =>
                onChange(
                  id ? available.find((node) => node.id === id) : undefined
                )
            : undefined
        }
        disabled={!onChange}
      >
        <Select.Option key="" value="" label="">
          &nbsp;
        </Select.Option>
        {available
          .slice()
          .sort((a, b) => toLabel(a).localeCompare(toLabel(b)))
          .map((node, index) => (
            <Select.Option
              key={node.id + "-" + index}
              value={node.id}
              label={toLabel(node)}
            >
              {toLabel(node)}
            </Select.Option>
          ))}
      </Select>
      {onView && (
        <Button
          style={{ marginLeft: 8 }}
          onClick={onView}
          disabled={!onChange || !selected}
        >
          <ArrowRightOutlined />
        </Button>
      )}
    </div>
  ) : (
    <>
      <Alert
        type="warning"
        message="Es scheint hier aktuell noch keine Auswahl zu existieren."
      />
      {onCreate && (
        <Button
          block
          type="dashed"
          style={{ width: "100%" }}
          onClick={onCreate}
          disabled={!onChange}
        >
          <PlusOutlined />
        </Button>
      )}
    </>
  );
};
