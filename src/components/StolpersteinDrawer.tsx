import React, { useState } from "react";
import { Card } from "antd";

import { ITrigger } from "../hooks/useTrigger";
import { IStolperstein } from "../interfaces/IStolperstein";
import { revert } from "../interfaces/IRevertible";

import { CommittablesDrawer } from "./CommittablesDrawer";
import { Description } from "./Description";
import { StolpersteinDescription } from "./StolpersteinDescription";

interface StolpersteinDrawerProps {
  selected: IStolperstein;

  visible: boolean;

  edit?: {
    onChange: (changed: IStolperstein) => void;

    onCommit: () => void;
  };

  onEditToggle?: () => void;

  onClose: () => void;

  resetTrigger: ITrigger;
}

export const StolpersteinDrawer: React.FC<StolpersteinDrawerProps> = (
  props
) => {
  const {
    selected,
    visible,
    edit,
    onEditToggle,
    onClose,
    resetTrigger,
  } = props;

  const [descriptionFocus, setDescriptionFocus] = useState(false);

  return (
    <CommittablesDrawer
      visible={visible}
      title="Stolperstein"
      committable={{
        selected,
        edit: edit && {
          onCommit: edit.onCommit,
          onRevert: () => edit.onChange(revert(selected)),
        },
        onEditToggle,
      }}
      onClose={onClose}
      resetTrigger={resetTrigger}
    >
      <Card bordered={false}>
        <Description>
          <StolpersteinDescription
            stolperstein={selected}
            focus={descriptionFocus}
            onFocusChange={(focus) => setDescriptionFocus(focus)}
            onChange={edit && edit.onChange}
          />
        </Description>
      </Card>
    </CommittablesDrawer>
  );
};
