import React from "react";
import { Alert, Card, Spin } from "antd";

interface SquareCardProps {
  title?: string;

  extra?: React.ReactNode;

  footer?: string;

  backgroundImageUrls?: string[];

  borderRadius?: number;

  loading?: boolean;

  error?: string;
}

/* 
   {loaded.laid && <div style={{ textAlign: "center", background: "rgb(255, 255, 255)", padding: "4px 12px" }}>
   <Typography.Text type="secondary" style={{ fontSize: "small" }}>Verlegt: {displayDate(loaded.laid)}</Typography.Text>
   </div>}
 */

export const SquareCard: React.FC<SquareCardProps> = (props) => {
  const {
    title,
    extra,
    footer,
    backgroundImageUrls = [],
    borderRadius = 8,
    loading = false,
    error,
    children,
  } = props;

  return (
    <div
      className="square-card"
      style={{
        width: "100%",
        minHeight: "192px" /* FIXME: temporary hack!!! */,
        position: "relative",
      }}
    >
      <div style={{ width: "100%", height: 0, paddingBottom: "100%" }} />
      <div
        style={{
          position: "absolute",
          backgroundImage: backgroundImageUrls.length
            ? backgroundImageUrls.map((url) => `url(${url})`).join(", ")
            : undefined,
          backgroundPosition: "center",
          backgroundSize: "cover",
          borderRadius: borderRadius + "px",
          top: 1,
          bottom: 1,
          left: 1,
          right: 1,
          overflow: "hidden",
        }}
      />
      <Card
        bordered={true}
        title={!loading && !error ? title : undefined}
        extra={!loading && !error ? extra : undefined}
        size="small"
        style={{
          position: "absolute",
          top: 0,
          left: 0,
          width: "100%",
          height: "100%",
          background: "transparent",
          borderRadius: borderRadius + "px",
          display: "flex",
          flexDirection: "column",
        }}
        headStyle={{
          background: "rgba(255, 255, 255, 0.75)",
          border: "none",
          margin: 0,
        }}
        bodyStyle={{
          background: "rgba(255, 255, 255, 0.75)",
          flexGrow: 1,
          minHeight: 0,
          overflowY: "auto",
          overflowX: "hidden",
          padding: 0,
          display: "flex",
          flexDirection: "column",
        }}
      >
        {loading ? (
          <div
            style={{
              width: "100%",
              height: "100%",
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            <Spin />
          </div>
        ) : error ? (
          <Alert
            type="error"
            message={error}
            style={{
              width: "100%",
              height: "100%",
              borderRadius: borderRadius + "px",
            }}
          />
        ) : (
          children
        )}
      </Card>
    </div>
  );
};
