import React from "react";
import { Input } from "antd";
import { WomanOutlined, ManOutlined } from "@ant-design/icons";

import {
  IRelationshipType,
  changedRelationshipType,
} from "../interfaces/IRelationshipType";
import { editable } from "../interfaces/IEditable";
import { getError, getWarning } from "../interfaces/IValidated";

import { DescriptionItem } from "./DescriptionItem";

interface RelationshipTypeDescriptionProps<T extends IRelationshipType> {
  selected: T;

  onChange?: (changed: T) => void; // if undefined the relationship type will not be editable
}

export const RelationshipTypeDescription = <T extends IRelationshipType>(
  props: RelationshipTypeDescriptionProps<T>
) => {
  const { selected, onChange } = props;

  const {
    subjectLabel,
    subjectLabel_f,
    subjectLabel_m,
    objectLabel,
    objectLabel_f,
    objectLabel_m,
  } = editable(selected);

  return (
    <>
      <DescriptionItem
        label="Subjekt (generisch)"
        content={{
          nonEditable: subjectLabel.trim() || undefined,
          editable: (
            <Input
              value={subjectLabel}
              onChange={
                onChange
                  ? ({ target: { value } }) =>
                      onChange(
                        changedRelationshipType(selected, {
                          subjectLabel: value,
                        })
                      )
                  : undefined
              }
              placeholder="Bezeichnung"
            />
          ),
          edit: !!onChange,
          error: getError(selected, "subjectLabel"),
          warning: getWarning(selected, "subjectLabel"),
          required: true,
        }}
      />
      <DescriptionItem
        label="Subjekt (weiblich)"
        content={{
          nonEditable: subjectLabel_f.trim() ? (
            <div>
              <WomanOutlined /> {subjectLabel_f}
            </div>
          ) : undefined,
          editable: (
            <Input
              prefix={<WomanOutlined />}
              value={subjectLabel_f}
              onChange={
                onChange
                  ? ({ target: { value } }) =>
                      onChange(
                        changedRelationshipType(selected, {
                          subjectLabel_f: value,
                        })
                      )
                  : undefined
              }
              placeholder="Bezeichnung"
            />
          ),
          edit: !!onChange,
          error: getError(selected, "subjectLabel_f"),
          warning: getWarning(selected, "subjectLabel_f"),
        }}
      />
      <DescriptionItem
        label="Subjekt (männlich)"
        content={{
          nonEditable: subjectLabel_m.trim() ? (
            <div>
              <ManOutlined /> {subjectLabel_m}
            </div>
          ) : undefined,
          editable: (
            <Input
              prefix={<ManOutlined />}
              value={subjectLabel_m}
              onChange={
                onChange
                  ? ({ target: { value } }) =>
                      onChange(
                        changedRelationshipType(selected, {
                          subjectLabel_m: value,
                        })
                      )
                  : undefined
              }
              placeholder="Bezeichnung"
            />
          ),
          edit: !!onChange,
          error: getError(selected, "subjectLabel_m"),
          warning: getWarning(selected, "subjectLabel_m"),
        }}
      />
      <DescriptionItem
        label="Objekt (generisch)"
        content={{
          nonEditable: objectLabel.trim() || undefined,
          editable: (
            <Input
              value={objectLabel}
              onChange={
                onChange
                  ? ({ target: { value } }) =>
                      onChange(
                        changedRelationshipType(selected, {
                          objectLabel: value,
                        })
                      )
                  : undefined
              }
              placeholder="Bezeichnung"
            />
          ),
          edit: !!onChange,
          error: getError(selected, "objectLabel"),
          warning: getWarning(selected, "objectLabel"),
          required: true,
        }}
      />
      <DescriptionItem
        label="Objekt (weiblich)"
        content={{
          nonEditable: objectLabel_f.trim() ? (
            <div>
              <WomanOutlined /> {objectLabel_f}
            </div>
          ) : undefined,
          editable: (
            <Input
              prefix={<WomanOutlined />}
              value={objectLabel_f}
              onChange={
                onChange
                  ? ({ target: { value } }) =>
                      onChange(
                        changedRelationshipType(selected, {
                          objectLabel_f: value,
                        })
                      )
                  : undefined
              }
              placeholder="Bezeichnung"
            />
          ),
          edit: !!onChange,
          error: getError(selected, "objectLabel_f"),
          warning: getWarning(selected, "objectLabel_f"),
        }}
      />
      <DescriptionItem
        label="Objekt (männlich)"
        content={{
          nonEditable: objectLabel_m.trim() ? (
            <div>
              <ManOutlined /> {objectLabel_m}
            </div>
          ) : undefined,
          editable: (
            <Input
              prefix={<ManOutlined />}
              value={objectLabel_m}
              onChange={
                onChange
                  ? ({ target: { value } }) =>
                      onChange(
                        changedRelationshipType(selected, {
                          objectLabel_m: value,
                        })
                      )
                  : undefined
              }
              placeholder="Bezeichnung"
            />
          ),
          edit: !!onChange,
          error: getError(selected, "objectLabel_m"),
          warning: getWarning(selected, "objectLabel_m"),
        }}
      />
    </>
  );
};
