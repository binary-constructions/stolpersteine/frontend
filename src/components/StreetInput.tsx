import React, { useMemo } from "react";
import { AutoComplete, Select, Input, Typography } from "antd";

import { ISpot, changedSpot } from "../interfaces/ISpot";
import { editable } from "../interfaces/IEditable";
import { useDisabled } from "../hooks/useDisabled";

import "./StreetInput.css";

export interface StreetInputCompletions {
  [id: string]: {
    street: string;

    postalCode?: string;

    place?: {
      id: string;

      label: string;
    };
  };
}

interface StreetInputProps<T extends ISpot> {
  spot: T;

  onChange?: (spot: T) => void;

  streetCompletions?: StreetInputCompletions;

  cornerCompletions?: StreetInputCompletions;
}

export const StreetInput = <T extends ISpot>(props: StreetInputProps<T>) => {
  const { spot, onChange, streetCompletions, cornerCompletions } = props;

  const disabled = useDisabled();

  const { street, streetDetail } = editable(spot);

  const streetOptions = useMemo(
    () =>
      streetCompletions
        ? Object.entries(streetCompletions).map(([id, match]) => (
            <AutoComplete.Option key={id} value={id}>
              <Typography.Text>{match.street}</Typography.Text> <br />
              {match.postalCode || match.place ? (
                <Typography.Text type="secondary" style={{ fontSize: "small" }}>
                  {match.postalCode && match.place
                    ? match.postalCode + " " + match.place.label
                    : match.postalCode || match.place?.label}
                </Typography.Text>
              ) : null}
            </AutoComplete.Option>
          ))
        : [],
    [streetCompletions]
  );

  return (
    <div className="street-input">
      <AutoComplete
        className="street-input__street"
        disabled={disabled}
        placeholder="Straßename"
        filterOption={false}
        defaultActiveFirstOption={false}
        value={street}
        onChange={
          onChange
            ? (value) =>
                onChange(changedSpot(spot, { street: value.toString() }))
            : undefined
        }
        onSelect={
          onChange
            ? (value) => {
                /* assert() */ if (!streetCompletions)
                  throw new Error("Selected a match from nothing...");

                const selected = streetCompletions[value.toString()];

                onChange(
                  changedSpot(spot, {
                    street: selected.street,
                    postalCode:
                      selected.postalCode || editable(spot).postalCode,
                    placeId: selected.place
                      ? selected.place.id
                      : editable(spot).placeId,
                  })
                );
              }
            : undefined
        }
      >
        {streetOptions}
      </AutoComplete>
      <Input.Group compact className="street-input__detail">
        <Select
          disabled={disabled}
          style={{ width: "25%" }}
          defaultValue={
            streetDetail.__typename === "StreetDetailNumber" ? "#" : "/"
          }
          onChange={
            onChange
              ? (value: any) => {
                  /* assert() */ if (value !== "/" && value !== "#")
                    throw new Error(
                      "Unrecognized selection for street detail type :("
                    );

                  if (value === "/") {
                    onChange(
                      changedSpot(spot, {
                        streetDetail: {
                          __typename: "StreetDetailCorner",
                          value: "",
                        },
                      })
                    );
                  } else {
                    onChange(
                      changedSpot(spot, {
                        streetDetail: {
                          __typename: "StreetDetailNumber",
                          value: "",
                        },
                      })
                    );
                  }
                }
              : undefined
          }
        >
          <Select.Option value="#">#</Select.Option>
          <Select.Option value="/">/</Select.Option>
        </Select>
        {streetDetail.__typename === "StreetDetailNumber" ? (
          <Input
            disabled={disabled}
            style={{ width: "75%" }}
            value={streetDetail.value}
            placeholder="Hausnummer"
            onChange={
              onChange
                ? ({ target: { value } }) =>
                    onChange(
                      changedSpot(spot, {
                        streetDetail: {
                          __typename: "StreetDetailNumber",
                          value,
                        },
                      })
                    )
                : undefined
            }
          />
        ) : (
          <AutoComplete
            disabled={disabled}
            style={{ width: "75%" }}
            value={streetDetail.value}
            placeholder="Straßename (Ecke)"
            onChange={
              onChange
                ? (value) =>
                    onChange(
                      changedSpot(spot, {
                        streetDetail: {
                          __typename: "StreetDetailCorner",
                          value: value.toString(),
                        },
                      })
                    )
                : undefined
            }
          >
            {
              [
                /* TODO: completions */
              ]
            }
          </AutoComplete>
        )}
      </Input.Group>
    </div>
  );
};
