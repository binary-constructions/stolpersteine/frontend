import React, { useState, useEffect } from "react";
import { Spin, Modal, message, Alert } from "antd";

import { useStolpersteine } from "../hooks/useStolpersteine";
import { usePersons } from "../hooks/usePersons";
import { useImages } from "../hooks/useImages";
import { useTrigger } from "../hooks/useTrigger";
import {
  IStolperstein,
  IExistingStolperstein,
  changedStolperstein,
  newStolperstein,
} from "../interfaces/IStolperstein";
import { IPerson, newPerson, changedPerson } from "../interfaces/IPerson";
import { editable } from "../interfaces/IEditable";
import { isContent } from "../interfaces/IContent";
import { hasCommitError } from "../interfaces/ICommittable";
import { nodeStoreFetch } from "../helpers/nodeStoreFetch";
import { useSpots } from "../hooks/useSpots";

import { Stolperstein } from "./Stolperstein";
import { StolpersteinDrawer } from "./StolpersteinDrawer";
import { PersonDrawer, PersonDrawerContentGroup } from "./PersonDrawer";
import { Nothing } from "./Nothing";

import "./StolpersteineList.css";

const DEFAULT_EMPTY_LIST_TEXT = "Die Stolperstein-Liste ist leer.";

interface StolpersteineListProps {
  spotId: string;

  create: boolean;

  onCreateDone: () => void; // either aborted or created

  emptyListText?: string;
}

export const StolpersteineList: React.FC<StolpersteineListProps> = ({
  spotId,
  create,
  onCreateDone,
  emptyListText = DEFAULT_EMPTY_LIST_TEXT,
}) => {
  const spots = useSpots([spotId]);

  const spot = spots.nodesById[spotId];

  const stolpersteinIds =
    spot && !(spot instanceof Error) ? spot.stolpersteinIds : [];

  const stolpersteine = useStolpersteine(stolpersteinIds);

  const fetchedStolpersteine = nodeStoreFetch(stolpersteine, stolpersteinIds);

  const personIds =
    fetchedStolpersteine && !(fetchedStolpersteine instanceof Error)
      ? fetchedStolpersteine.reduce<string[]>((a, c) => {
          c.subjects.forEach((v) => a.push(v.id));

          return a;
        }, [])
      : [];

  const persons = usePersons(personIds);

  const imageIds =
    fetchedStolpersteine && !(fetchedStolpersteine instanceof Error)
      ? fetchedStolpersteine.reduce<string[]>((a, c) => {
          if (c.image) {
            a.push(c.image.id);
          }

          return a;
        }, [])
      : [];

  const images = useImages(imageIds);

  const [stolpersteinDrawer, setStolpersteinDrawer] = useState<
    | undefined
    | {
        stolperstein: IStolperstein;

        open: boolean;

        edit?: boolean; // undefined will show the drawer without an edit toggle
      }
  >();

  const stolpersteinDrawerResetTrigger = useTrigger();

  stolpersteinDrawerResetTrigger.listen(() => setStolpersteinDrawer(undefined));

  useEffect(() => {
    if (spot && !(spot instanceof Error)) {
      if (create && (!stolpersteinDrawer || !stolpersteinDrawer.open)) {
        setStolpersteinDrawer({
          stolperstein: newStolperstein(spot),
          open: true,
        });
      } else if (
        !create &&
        stolpersteinDrawer &&
        !isContent(stolpersteinDrawer.stolperstein) &&
        stolpersteinDrawer.open
      ) {
        setStolpersteinDrawer({ ...stolpersteinDrawer, open: false });
      }
    }
  }, [spot, create, stolpersteinDrawer]);

  /* person drawer */

  const [personDrawer, setPersonDrawer] = useState<
    | undefined
    | {
        person: IPerson;

        stolperstein: IExistingStolperstein;

        activeGroup: PersonDrawerContentGroup;

        open: boolean;

        edit?: boolean;
      }
  >();

  const personDrawerResetTrigger = useTrigger();

  personDrawerResetTrigger.listen(() => setPersonDrawer(undefined));

  return (
    <>
      {spot instanceof Error ? (
        <Alert type="error" message="Laden der Steine fehlgeschlagen." />
      ) : !spot || !fetchedStolpersteine ? (
        <Spin />
      ) : !(fetchedStolpersteine instanceof Error) &&
        !fetchedStolpersteine.some((v) => !v.deleted) ? (
        <Nothing text={emptyListText} />
      ) : (
        <div className="stolpersteine-list">
          {stolpersteinIds
            .map((v) => stolpersteine.nodesById[v])
            .map((v, i) =>
              !v || (!(v instanceof Error) && v.deleted) ? null : (
                <Stolperstein
                  key={i}
                  header={"Stolperstein #" + (i + 1)}
                  stolperstein={v}
                  image={
                    v instanceof Error || !v.image
                      ? undefined
                      : images.nodesById[v.image.id]
                  }
                  subjects={
                    v instanceof Error
                      ? undefined
                      : v.subjects
                          .map((w) => persons.nodesById[w.id])
                          .filter((w) => !w || w instanceof Error || !w.deleted)
                  }
                  onViewStolperstein={(w) =>
                    setStolpersteinDrawer({
                      stolperstein: w,
                      open: true,
                      edit: false,
                    })
                  }
                  onViewSubject={
                    v instanceof Error
                      ? undefined
                      : (w) =>
                          setPersonDrawer({
                            person: w,
                            stolperstein: v,
                            activeGroup: PersonDrawerContentGroup.GENERAL,
                            open: true,
                            edit: false,
                          })
                  }
                  onAddSubject={
                    v instanceof Error ||
                    v.subjects
                      .map((w) => persons.nodesById[w.id])
                      .filter((w) => !!w && !(w instanceof Error) && !w.deleted)
                      .length
                      ? undefined
                      : () =>
                          setPersonDrawer({
                            person: newPerson(),
                            stolperstein: v,
                            activeGroup: PersonDrawerContentGroup.GENERAL,
                            open: true,
                          })
                  }
                  onDelete={(w) => {
                    Modal.confirm({
                      title:
                        "Den Stolperstein (inklusive ggf. dazugehöriger Person) wirklich löschen?",
                      okText: "Löschen",
                      okType: "danger",
                      cancelText: "Abbrechen",
                      onOk: () => {
                        stolpersteine.deepDelete(w, (x) => {
                          if (hasCommitError(x)) {
                            message.error(
                              "Löschen des Stolpersteins fehlgeschlagen :("
                            );
                          }
                        });
                      },
                    });
                  }}
                  // onReviewStatusChange={(stein, status) => {
                  //   stolpersteine.setReviewStatus(stein, status, (stein) => {
                  //     // TODO: b0rken... but is meant to be moved anyway
                  //     if (hasCommitError(stein)) {
                  //       console.debug(stein);
                  //     } else {
                  //       stolpersteine.refresh();
                  //     }
                  //   });
                  // }}
                />
              )
            )}
        </div>
      )}
      {stolpersteinDrawer ? (
        <StolpersteinDrawer
          selected={stolpersteinDrawer.stolperstein}
          visible={stolpersteinDrawer.open}
          edit={
            !isContent(stolpersteinDrawer.stolperstein) ||
            stolpersteinDrawer.edit
              ? {
                  onChange: (changed) =>
                    setStolpersteinDrawer({
                      ...stolpersteinDrawer,
                      stolperstein: changed,
                    }),
                  onCommit: () => {
                    if (!isContent(stolpersteinDrawer.stolperstein)) {
                      if (!create)
                        throw new Error(
                          "Where does this new Stolperstein come from?"
                        ); // assert()

                      setStolpersteinDrawer({
                        ...stolpersteinDrawer,
                        stolperstein: stolpersteine.create(
                          stolpersteinDrawer.stolperstein,
                          (created) => {
                            setStolpersteinDrawer({
                              stolperstein: created,
                              open: hasCommitError(created),
                            });

                            if (!hasCommitError(created)) {
                              if (!isContent(created))
                                throw new Error(
                                  "Object should have an id now."
                                ); // assert()

                              spots.refresh([spotId]);

                              onCreateDone();
                            }
                          }
                        ),
                      });
                    } else {
                      setStolpersteinDrawer({
                        ...stolpersteinDrawer,
                        stolperstein: stolpersteine.update(
                          stolpersteinDrawer.stolperstein,
                          (updated) => {
                            setStolpersteinDrawer({
                              stolperstein: updated,
                              open: hasCommitError(updated),
                              edit: hasCommitError(updated),
                            });
                          }
                        ),
                      });
                    }
                  },
                }
              : undefined
          }
          onEditToggle={
            stolpersteinDrawer.edit !== undefined
              ? () =>
                  setStolpersteinDrawer({
                    ...stolpersteinDrawer,
                    edit: !stolpersteinDrawer.edit,
                  })
              : undefined
          }
          onClose={
            create
              ? onCreateDone
              : () =>
                  setStolpersteinDrawer({ ...stolpersteinDrawer, open: false })
          }
          resetTrigger={stolpersteinDrawerResetTrigger}
        />
      ) : undefined}
      {personDrawer && (
        <PersonDrawer
          selected={personDrawer.person}
          visible={personDrawer.open}
          activeContentGroup={personDrawer.activeGroup}
          onActivateContentGroup={(activeGroup) =>
            setPersonDrawer({ ...personDrawer, activeGroup })
          }
          edit={
            !isContent(personDrawer.person) || personDrawer.edit
              ? {
                  onChange: (changed) =>
                    setPersonDrawer({ ...personDrawer, person: changed }),
                  onCommit: () => {
                    if (!isContent(personDrawer.person)) {
                      setPersonDrawer({
                        ...personDrawer,
                        person: persons.create(
                          personDrawer.person,
                          (created) => {
                            setPersonDrawer({
                              ...personDrawer,
                              person: created,
                              open: hasCommitError(created),
                            });

                            if (!hasCommitError(created)) {
                              if (!isContent(created))
                                throw new Error(
                                  "Object should have an id now."
                                ); // assert()

                              stolpersteine.update(
                                changedStolperstein(personDrawer.stolperstein, {
                                  subjectIds: [
                                    ...editable(personDrawer.stolperstein)
                                      .subjectIds,
                                    created.id,
                                  ],
                                }),
                                (updated) => {
                                  if (hasCommitError(updated)) {
                                    message.error(
                                      "Fehler beim Hinzufügen der Person zum Stolperstein :("
                                    );
                                  }
                                }
                              );
                            }
                          }
                        ),
                      });
                    } else {
                      setPersonDrawer({
                        ...personDrawer,
                        person: persons.update(
                          personDrawer.person,
                          (updated) => {
                            setPersonDrawer({
                              ...personDrawer,
                              person: updated,
                              open: hasCommitError(updated),
                              edit: hasCommitError(updated),
                            });
                          }
                        ),
                      });
                    }
                  },
                }
              : undefined
          }
          onEditToggle={
            personDrawer.edit !== undefined
              ? () =>
                  setPersonDrawer({ ...personDrawer, edit: !personDrawer.edit })
              : undefined
          }
          onImagesChanged={
            isContent(personDrawer.person)
              ? ((personMemo) => (imageIds: string[]) =>
                  setPersonDrawer({
                    ...personDrawer,
                    person: persons.update(
                      changedPerson(personMemo, { imageIds }),
                      (changed) =>
                        setPersonDrawer({ ...personDrawer, person: changed })
                    ),
                  }))(personDrawer.person)
              : undefined
          }
          onClose={() => setPersonDrawer({ ...personDrawer, open: false })}
          resetTrigger={personDrawerResetTrigger}
        />
      )}
    </>
  );
};
