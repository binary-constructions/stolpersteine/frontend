import React from "react";
import { Button } from "antd";
import { ReloadOutlined, ExclamationOutlined } from "@ant-design/icons";

interface NetworkStatusProps {
  busy: boolean;

  hasErrors: boolean;

  onReload: () => void;

  onShowErrors: () => void;
}

export const NetworkStatus: React.FC<NetworkStatusProps> = ({
  busy,
  hasErrors,
  onReload,
  onShowErrors,
}) => {
  return (
    <div
      style={{
        position: "absolute",
        top: "12px",
        left: "12px",
        zIndex: 1000,
        textAlign: "left",
      }}
    >
      <Button
        size="large"
        shape="circle"
        icon={<ReloadOutlined />}
        loading={busy}
        onClick={onReload}
      />
      {hasErrors && (
        <Button
          size="large"
          shape="circle"
          danger
          icon={<ExclamationOutlined />}
          style={{ marginLeft: "12px" }}
          onClick={onShowErrors}
        />
      )}
    </div>
  );
};
