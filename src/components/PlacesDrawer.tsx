import React from "react";
import { Card } from "antd";

import { ITrigger } from "../hooks/useTrigger";
import { IPlace, IExistingPlace } from "../interfaces/IPlace";
import { revert } from "../interfaces/IRevertible";

import { CommittablesDrawer } from "./CommittablesDrawer";
import { Description } from "./Description";
import { PlaceDescription } from "./PlaceDescription";

interface PlacesDrawerProps {
  selected: IPlace;

  visible: boolean;

  edit?: {
    onChange: (changed: IPlace) => void;

    onCommit: () => void;
  };

  select?: {
    selection: IExistingPlace[];

    onSelect?: (selected: IExistingPlace) => void;
  };

  onEditToggle?: () => void;

  onClose: () => void;

  resetTrigger: ITrigger;
}

export const PlacesDrawer: React.FC<PlacesDrawerProps> = (props) => {
  const {
    selected,
    visible,
    edit,
    select,
    onEditToggle,
    onClose,
    resetTrigger,
  } = props;

  return (
    <CommittablesDrawer
      visible={visible}
      title="Ort"
      committable={{
        selected,
        edit: edit && {
          onCommit: edit.onCommit,
          onRevert: () => edit.onChange(revert(selected)),
        },
        onEditToggle,
      }}
      onClose={onClose}
      resetTrigger={resetTrigger}
    >
      <Card bordered={false}>
        <Description>
          <PlaceDescription place={selected} onChange={edit && edit.onChange} />
        </Description>
      </Card>
    </CommittablesDrawer>
  );
};
