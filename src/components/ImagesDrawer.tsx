import React from "react";
import { Card } from "antd";

import { ITrigger } from "../hooks/useTrigger";
import { IImage, IExistingImage } from "../interfaces/IImage";
import { revert } from "../interfaces/IRevertible";
import { CommittablesDrawer } from "./CommittablesDrawer";
import { Description } from "./Description";
import { ImageDescription } from "./ImageDescription";

interface ImagesDrawerProps {
  selected: IImage;

  visible: boolean;

  edit?: {
    onChange: (changed: IImage) => void;

    onCommit: () => void;
  };

  select?: {
    selection: IExistingImage[];

    onSelect?: (selected: IExistingImage) => void;
  };

  onEditToggle?: () => void;

  onClose: () => void;

  resetTrigger: ITrigger;
}

export const ImagesDrawer: React.FC<ImagesDrawerProps> = ({
  selected,
  visible,
  edit,
  select,
  onEditToggle,
  onClose,
  resetTrigger,
}) => {
  return (
    <CommittablesDrawer<IImage, IExistingImage>
      visible={visible}
      title="Bild"
      committable={{
        selected,
        select: select && {
          selection: select.selection,
          toLabel: (option) => option.label,
          onSelect: select.onSelect,
        },
        edit: edit && {
          onCommit: edit.onCommit,
          onRevert: () => edit.onChange(revert(selected)),
        },
        onEditToggle,
      }}
      onClose={onClose}
      resetTrigger={resetTrigger}
    >
      {selected && (
        <Description>
          <Card bordered={false}>
            <ImageDescription
              subject={selected}
              onChange={edit && edit.onChange}
            />
          </Card>
        </Description>
      )}
    </CommittablesDrawer>
  );
};
