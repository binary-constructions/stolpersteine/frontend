import React from "react";

import { DisabledContext } from "../contexts/DisabledContext";

import "./DisabledContextProvider.css";

interface DisabledContextProviderProps {
  disable: boolean;
}

export const DisabledContextProvider: React.FC<DisabledContextProviderProps> = (
  props
) => {
  const { disable, children } = props;

  const classes: string[] = ["disabled-context"];

  if (disable) {
    classes.push("disabled-context--active");
  }

  return (
    <div className={classes.join(" ")}>
      <DisabledContext.Provider value={disable}>
        {children}
      </DisabledContext.Provider>
    </div>
  );
};
