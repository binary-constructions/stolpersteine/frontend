import React from "react";
import { Button, Result, Typography } from "antd";
import { FrownTwoTone, CloseCircleOutlined } from "@ant-design/icons";

import { ITrigger } from "../hooks/useTrigger";

interface ErrorDisplayProps {
  title?: {
    main: string;

    sub?: string;
  };

  errorsHeader?: string | null;

  errors: string[];

  reset?: {
    trigger: ITrigger;

    label?: string;
  };
}

export const ErrorDisplay: React.FC<ErrorDisplayProps> = (props) => {
  const { title, errorsHeader, errors, reset } = props;

  return (
    <Result
      style={{ paddingLeft: 0, paddingRight: 0 }}
      status="error"
      icon={<FrownTwoTone color="#f5222d" />}
      title={title?.main || "Probelm!"}
      subTitle={title ? title.sub : "Irgendwas lief schief..."}
      extra={
        reset && (
          <Button type="primary" onClick={(event) => reset.trigger.activate()}>
            {reset.label || "m'kay"}
          </Button>
        )
      }
    >
      <div className="desc" style={{ textAlign: "left" }}>
        <div style={{ maxWidth: "50ex", margin: "0 auto" }}>
          {errorsHeader !== null && (
            <Typography.Paragraph>
              <Typography.Text strong style={{ fontSize: 16 }}>
                {errorsHeader ||
                  (errors.length > 1 ? "Meldungen:" : "Meldung:")}
              </Typography.Text>
            </Typography.Paragraph>
          )}
          <div style={{ maxHeight: "20ex", overflow: "auto" }}>
            {errors.map((error, index) => (
              <Typography.Paragraph key={index} style={{ marginTop: "14px" }}>
                <CloseCircleOutlined style={{ color: "red" }} /> {error}
              </Typography.Paragraph>
            ))}
          </div>
        </div>
      </div>
    </Result>
  );
};
