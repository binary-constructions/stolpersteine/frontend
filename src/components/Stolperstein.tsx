import React, { useContext } from "react";
import { Alert, Card, List, Select, Spin } from "antd";
import {
  DeleteOutlined,
  UserAddOutlined,
  ArrowRightOutlined,
} from "@ant-design/icons";

import { ReviewStatus } from "../backendApi";
import { displayDate } from "../helpers/displayDate";
import { displayYear } from "../helpers/displayYear";
import { displayName } from "../helpers/displayName";
import { IExistingStolperstein } from "../interfaces/IStolperstein";
import { IExistingPerson } from "../interfaces/IPerson";
import { IExistingImage } from "../interfaces/IImage";
import { UserContext } from "../contexts/UserContext";

import { SquareCard } from "./SquareCard";
import { Nothing } from "./Nothing";

import "./Stolperstein.css";

const FALLBACK_STOLPERSTEIN_IMAGE_URL =
  "/images/FALLBACK_STOLPERSTEIN_BACKGROUND.jpg";

interface StolpersteinProps {
  header: string;

  stolperstein: undefined | Error | IExistingStolperstein;

  image?: Error | IExistingImage;

  subjects?: (undefined | Error | IExistingPerson)[]; // the property itself may be undefined if the stone is still loading

  onViewStolperstein?: (stolperstein: IExistingStolperstein) => void;

  onViewSubject?: (subject: IExistingPerson) => void;

  onAddSubject?: (stolperstein: IExistingStolperstein) => void;

  onReviewStatusChange?: (
    stolperstein: IExistingStolperstein,
    status: ReviewStatus
  ) => void;

  onDelete?: (stolperstein: IExistingStolperstein) => void;
}

export const Stolperstein: React.FC<StolpersteinProps> = ({
  header,
  stolperstein,
  image,
  subjects,
  onViewStolperstein,
  onViewSubject,
  onAddSubject,
  onReviewStatusChange,
  onDelete,
}) => {
  const user = useContext(UserContext);

  const loaded: undefined | IExistingStolperstein =
    stolperstein && !(stolperstein instanceof Error) ? stolperstein : undefined;

  return (
    <div className="stolperstein">
      <SquareCard
        title={loaded && header}
        extra={
          loaded &&
          (onDelete || onViewStolperstein || onAddSubject) && (
            <>
              {user && onDelete && (
                <DeleteOutlined
                  onClick={() => onDelete(loaded)}
                  style={{ marginLeft: "8px" }}
                />
              )}
              {user && onAddSubject && subjects && !subjects.length && (
                <UserAddOutlined
                  onClick={() => onAddSubject(loaded)}
                  style={{ marginLeft: "8px" }}
                />
              )}
              {onViewStolperstein && (
                <ArrowRightOutlined
                  onClick={() => onViewStolperstein(loaded)}
                  style={{ marginLeft: "8px" }}
                />
              )}
            </>
          )
        }
        footer={loaded && loaded.laid ? displayDate(loaded.laid) : undefined}
        backgroundImageUrls={
          image && !(image instanceof Error)
            ? [image.path, FALLBACK_STOLPERSTEIN_IMAGE_URL]
            : [FALLBACK_STOLPERSTEIN_IMAGE_URL]
        }
        loading={stolperstein === undefined}
        error={
          stolperstein instanceof Error
            ? "Fehler beim Laden des Stolpersteins :("
            : undefined
        }
      >
        {loaded && subjects && (
          <>
            <div
              style={{
                flexGrow: 1,
                width: "100%",
                verticalAlign: "middle",
                textAlign: "center",
              }}
            >
              {!subjects.length ? (
                <Nothing text="Es ist noch keine Person mit diesem Stein verknüpft." />
              ) : (
                <Card
                  bordered={false}
                  size="small"
                  style={{ background: "none" }}
                >
                  <List size="small">
                    {subjects.map((subject, index) => (
                      <List.Item
                        key={index}
                        actions={
                          onViewSubject &&
                          subject &&
                          !(subject instanceof Error)
                            ? [
                                <ArrowRightOutlined
                                  style={{ paddingLeft: 8 }}
                                  onClick={() => onViewSubject(subject)}
                                />,
                              ]
                            : undefined
                        }
                      >
                        {!subject ? (
                          <Spin />
                        ) : subject instanceof Error ? (
                          <Alert
                            type="error"
                            message="Fehler beim Laden der Person :("
                          />
                        ) : (
                          <List.Item.Meta
                            title={displayName(subject.names)}
                            description={
                              subject.bornDate
                                ? "geb. " + displayYear(subject.bornDate)
                                : undefined
                            }
                          />
                        )}
                      </List.Item>
                    ))}
                  </List>
                </Card>
              )}
              {user &&
                stolperstein &&
                !(stolperstein instanceof Error) &&
                onReviewStatusChange && (
                  <div>
                    <Select
                      onChange={(x: ReviewStatus) =>
                        onReviewStatusChange(stolperstein, x)
                      }
                      value={loaded.reviewStatus}
                      style={{ width: "80%", paddingTop: 12 }}
                    >
                      <Select.Option value={ReviewStatus.Undefined}>
                        --
                      </Select.Option>
                      <Select.Option value={ReviewStatus.Stub}>
                        Platzhalter
                      </Select.Option>
                      <Select.Option value={ReviewStatus.WorkInProgress}>
                        in Bearbeitung
                      </Select.Option>
                      <Select.Option value={ReviewStatus.NeedsReview}>
                        benötigt Review
                      </Select.Option>
                      <Select.Option value={ReviewStatus.Released}>
                        zur Freigabe
                      </Select.Option>
                    </Select>
                  </div>
                )}
            </div>
          </>
        )}
      </SquareCard>
    </div>
  );
};
