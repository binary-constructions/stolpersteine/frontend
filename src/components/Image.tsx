import React, { useState, useContext } from "react";
import { Modal } from "antd";
import {
  EyeOutlined,
  DeleteOutlined,
  ArrowRightOutlined,
} from "@ant-design/icons";

import { UserContext } from "../contexts/UserContext";

import { SquareCard } from "./SquareCard";

interface ImageProps {
  url: undefined | Error | string; // undefined means loading

  onOpen?: () => void;

  onRemove?: () => void;
}

export const Image: React.FC<ImageProps> = ({ url, onOpen, onRemove }) => {
  const user = useContext(UserContext);

  const [preview, setPreview] = useState(false);

  return (
    <div className="Image">
      <SquareCard
        loading={!url}
        error={url instanceof Error ? url.message : undefined}
        extra={
          url && !(url instanceof Error) ? (
            <>
              {user && onRemove && (
                <DeleteOutlined onClick={onRemove} style={{ marginLeft: 8 }} />
              )}
              <EyeOutlined
                onClick={() => setPreview(true)}
                style={{ marginLeft: 8 }}
              />
              {onOpen && (
                <ArrowRightOutlined
                  onClick={onOpen}
                  style={{ marginLeft: 8 }}
                />
              )}
            </>
          ) : undefined
        }
        backgroundImageUrls={url && !(url instanceof Error) ? [url] : undefined}
      />
      {url && !(url instanceof Error) ? (
        <Modal
          visible={preview}
          footer={null}
          onCancel={() => setPreview(false)}
        >
          <img alt="Bild-Vorschau" style={{ width: "100%" }} src={url} />
        </Modal>
      ) : undefined}
    </div>
  );
};
