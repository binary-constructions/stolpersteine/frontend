import React, { useState } from "react";
import { Button } from "antd";
import { PlusOutlined, MinusOutlined } from "@ant-design/icons";

import { Config } from "../globals/Config";
import { Attribution } from "../semanticMap/Attribution";
import { Icon } from "../semanticMap/Icon";
import { SemanticMap } from "../semanticMap/SemanticMap";
import { Point } from "../semanticMap/Point";
import { ReactComponent as StolpersteinMarker } from "../mapMarkers/Stolperstein.svg";
import { ReactComponent as StolpersteinFadeMarker } from "../mapMarkers/StolpersteinFade.svg";
import { ReactComponent as StolpersteinFocusMarker } from "../mapMarkers/StolpersteinFocus.svg";
import { ReactComponent as BlankMarker } from "../mapMarkers/Blank.svg";
import { ReactComponent as BlankFadeMarker } from "../mapMarkers/BlankFade.svg";
import { ReactComponent as BlankFocusMarker } from "../mapMarkers/BlankFocus.svg";

import { IAnnotatedSpotSummary } from "../interfaces/IAnnotatedSpotSummary";
import { IGeoPoint, newGeoPoint } from "../interfaces/IGeoPoint";
import { ISpot } from "../interfaces/ISpot";
import { isNode } from "../interfaces/INode";

interface MapProps {
  spots: IAnnotatedSpotSummary[];

  selected: undefined | IGeoPoint | ISpot | IAnnotatedSpotSummary;

  onSelect?: (spot: IGeoPoint | IAnnotatedSpotSummary) => void;

  onMove?: (newLocation: {
    longitude: number;

    latitude: number;
  }) => void;
}

// TODO: pass features in as JSON for speed reasons

export const Map: React.FC<MapProps> = ({
  spots,
  selected,
  onSelect,
  onMove,
}) => {
  const [view, setView] = useState(Config.initialMapView);

  const markerFor = (spot: IAnnotatedSpotSummary) => {
    let circle:
      | {
          color: string;
          weight?: number;
          opacity?: number;
          fillOpacity?: number;
        }
      | undefined;

    let clickable: boolean = false;

    const stolpersteinCount = spot.stolpersteinSummaries.filter(
      (v) => !v.deleted
    ).length;

    if (onSelect) {
      circle = stolpersteinCount ? { color: "#DAA520" } : { color: "#828282" };

      clickable = true;
    } else {
      circle = stolpersteinCount
        ? { color: "#DAA520", weight: 0 }
        : { color: "#828282", weight: 0 };
    }

    return (
      <Point
        key={spot.id}
        id={spot.id}
        longitude={spot.point.longitude}
        latitude={spot.point.latitude}
        circle={circle}
        clickable={clickable}
      />
    );
  };

  const findSelected = (id: string): IAnnotatedSpotSummary => {
    const spot = spots.find((spot) => spot.id === id);

    /* assert() */ if (!spot)
      throw new Error("Unable to find the selected spot :(");

    return spot;
  };

  return (
    <div
      className="Map"
      style={{ width: "100%", height: "100%", position: "relative" }}
    >
      <div
        style={{
          position: "absolute",
          bottom: "12px",
          left: "12px",
          zIndex: 1000,
          textAlign: "left",
        }}
      >
        <Button
          size="large"
          shape="circle"
          icon={<PlusOutlined />}
          style={{ marginBottom: "12px" }}
          onClick={() => setView({ ...view, zoom: view.zoom + 1 })}
        />
        <br />
        <Button
          size="large"
          shape="circle"
          icon={<MinusOutlined />}
          onClick={() => setView({ ...view, zoom: view.zoom - 1 })}
        />
      </div>
      <SemanticMap
        tileUrlTemplate="https://api.maptiler.com/maps/basic/256/{z}/{x}/{y}.png?key=7HLnYROUe0lC4QRXGkFL"
        maxZoom={23}
        maxNativeZoom={20}
        view={view}
        onViewChange={(v) => setView(v)}
        onMapClick={(point) =>
          onSelect
            ? onSelect(
                newGeoPoint({
                  longitude: point.longitude,
                  latitude: point.latitude,
                })
              )
            : undefined
        }
        onFeatureClick={(id) =>
          id && onSelect ? onSelect(findSelected(id)) : undefined
        }
        onFeatureDrag={(_, point) =>
          onMove &&
          onMove({
            longitude: point.longitude,
            latitude: point.latitude,
          })
        }
      >
        <Icon size={20}>
          <BlankMarker />
        </Icon>
        <Icon name="Fade" size={20}>
          <BlankFadeMarker />
        </Icon>
        <Icon name="Focus" size={29}>
          <BlankFocusMarker />
        </Icon>
        <Icon name="Stolperstein" size={20}>
          <StolpersteinMarker />
        </Icon>
        <Icon name="StolpersteinFade" size={20}>
          <StolpersteinFadeMarker />
        </Icon>
        <Icon name="StolpersteinFocus" size={29}>
          <StolpersteinFocusMarker />
        </Icon>
        {spots.map((v) =>
          selected && "id" in selected && selected.id === v.id ? (
            <Point
              id={""}
              key={""}
              longitude={selected.point.longitude}
              latitude={selected.point.latitude}
              icon={
                selected.__typename === "Spot"
                  ? selected.stolpersteinIds.length
                    ? "StolpersteinFocus"
                    : "Focus"
                  : selected.stolpersteinSummaries.length
                  ? "StolpersteinFocus"
                  : "Focus"
              }
              clickable={false}
              draggable={onMove !== undefined}
            />
          ) : (
            markerFor(v)
          )
        )}
        {!selected ? undefined : selected.__typename === "GeoPoint" ? (
          <Point
            id={""}
            key={""}
            longitude={selected.longitude}
            latitude={selected.latitude}
            icon={"Focus"}
            clickable={false}
            draggable={onMove !== undefined}
          />
        ) : !isNode(selected) ? (
          <Point
            id={""}
            key={""}
            longitude={selected.point.longitude}
            latitude={selected.point.latitude}
            icon={"Focus"}
            clickable={false}
            draggable={onMove !== undefined}
          />
        ) : undefined}
        <Attribution>
          Map data &copy;{" "}
          <a href="https://www.openstreetmap.org/">OpenStreetMap</a>{" "}
          contributors,{" "}
          <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>
          , Imagery &copy; <a href="https://www.mapbox.com/">Mapbox</a>
        </Attribution>
      </SemanticMap>
    </div>
  );

  // TODO: condensed attribution display with details on click for mobile/small screens
};
