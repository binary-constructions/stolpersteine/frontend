import React from "react";

// A placeholder "transition" to use until there was time to polish animations.

interface ReplaceTransitionProps {
  one: React.ReactNode;

  other: React.ReactNode;

  showOther: boolean;

  keep?: boolean; // keep showing last visible children while closing (even if they are not around anymore)
}

export const ReplaceTransition: React.FC<ReplaceTransitionProps> = ({
  one,
  other,
  showOther,
}) => <>{showOther ? other : one}</>;
