import { useContext } from "react";

import { DisabledContext } from "../contexts/DisabledContext";

export const useDisabled = (inputDisabled?: boolean): boolean => {
  const contextDisabled = useContext(DisabledContext);

  return inputDisabled || contextDisabled;
};
