import { useState, useEffect } from "react";

import { GlobalState } from "../types/GlobalState";
import {
  useOccupationsQuery,
  useCreateOccupationMutation,
  useUpdateOccupationMutation,
} from "../backendApi";
import {
  IOccupation,
  IExistingOccupation,
  fromOccupationFragment,
  toOccupationInput,
} from "../interfaces/IOccupation";
import { withCommitStatus } from "../interfaces/ICommittable";

import { useGlobalState } from "./useGlobalState";

// TODO: this is mainly boilerplate and should probably be abstracted
// over.  Until then the useSpots version is meant to be the canonical
// implementation to copy.

type ExistingOccupationsById = {
  [id: string]: undefined | Error | IExistingOccupation;
};

export interface Occupations {
  /**
   * The already loaded nodes.  Specifically requested nodes will
   * eventually show up as either successfully loaded or an Error.
   */
  nodesById: ExistingOccupationsById;

  /**
   * Attempt to save the specified node as a new entity.
   *
   * The onDone callback will be called with either the created INode
   * or the original object with an Error as commit status.
   *
   * @returns The original object with a commit status of true.
   */
  create: (
    occupation: IOccupation,
    onDone: (occupation: IOccupation) => void
  ) => IOccupation;

  /**
   * Attempt to save an update to the specified occupation.
   *
   * The onDone callback will be called with either the updated object
   * or the original object with an Error as commit status.
   *
   * @returns The original object with a commit status of true.
   */
  update: (
    occupation: IExistingOccupation,
    onDone: (occupation: IExistingOccupation) => void
  ) => IExistingOccupation;

  /**
   * Dump all cached data and refetch active queries.
   */
  refresh: () => void;
}

const cache = new GlobalState<ExistingOccupationsById>({});

const refreshTriggerCount = new GlobalState(0);

/**
 * A query wrapper that caches already loaded entities and also
 * provides a few convenience methods for the manipulation of such
 * entities (with those methods updating the cached entities so that
 * they continue to represent the server side state).
 *
 * WARNING: when checking the returned object for updates (e.g. when
 * using it as a dependency for useEffect or similar) the .loaded
 * property is the one to watch because all other properties currently
 * change with every call! (FIXME?)
 *
 * @param ids Either a list of specific ids to fetch or undefined to
 * fetch all.  If the array is empty no new nodes will be loaded and
 * only the already fetched ones will be available.
 */
export const useOccupations = (
  /**
   */
  ids?: string[]
): Occupations => {
  const [cached, setCached] = useGlobalState(cache);

  const [refreshTrigger, setRefreshTrigger] = useGlobalState(
    refreshTriggerCount
  );

  // The requested ids (if any) without those already successfully loaded.
  const requestedButNotLoadedIds = !ids
    ? []
    : ids.filter((v) => !cached[v] || cached[v] instanceof Error);

  // The ids to load as passed to the query.  Saved as state and
  // updated only when new ids actually need fetching to avoid
  // unnecessary queries.
  const [idsToLoad, setIdsToLoad] = useState<undefined | string[]>([]);

  // Update idsToLoad if necessary.
  if (ids) {
    if (
      !requestedButNotLoadedIds.every(
        (v) => !idsToLoad || idsToLoad.includes(v)
      )
    ) {
      setIdsToLoad(requestedButNotLoadedIds);
    }
  } else if (idsToLoad) {
    setIdsToLoad(undefined);
  }

  const { data, loading, error, networkStatus, refetch } = useOccupationsQuery({
    variables: { ids: idsToLoad },
    notifyOnNetworkStatusChange: true,
    // Skip the query if no new ids need to be fetched.
    skip: ids && !requestedButNotLoadedIds.length,
  });

  useEffect(() => {
    if (refreshTrigger) {
      refetch();
    }
  }, [refreshTrigger, refetch]);

  // Update the cache if necessary.
  useEffect(() => {
    if (loading || networkStatus === 4) {
      const oldErrorIds = Object.entries(cached).reduce<string[]>(
        (a, [k, v]) => {
          if (v instanceof Error) {
            a.push(k);
          }

          return a;
        },
        []
      );

      if (oldErrorIds.length) {
        setCached((v) =>
          oldErrorIds.reduce(
            (a, c) => {
              delete a[c];

              return a;
            },
            { ...v }
          )
        );
      }
    } else if (data) {
      const newlyLoaded = data.occupations.filter(
        (v) => !cached[v.id] || cached[v.id] instanceof Error
      );

      if (newlyLoaded.length) {
        setCached((v) =>
          newlyLoaded.reduce(
            (a, c) => {
              a[c.id] = fromOccupationFragment(c);

              return a;
            },
            { ...v }
          )
        );
      }
    } else if (error && requestedButNotLoadedIds.length) {
      const newErrorIds = idsToLoad?.filter((v) => !cached[v]) || [];

      if (newErrorIds.length) {
        setCached((v) =>
          newErrorIds.reduce(
            (a, c) => {
              a[c] = new Error("Laden des Objekts fehlgeschlagen.");

              return a;
            },
            { ...v }
          )
        );
      }
    }
  }, [
    cached,
    setCached,
    loading,
    networkStatus,
    error,
    data,
    requestedButNotLoadedIds.length,
    idsToLoad,
  ]);

  const [createOccupationMutation] = useCreateOccupationMutation();

  const [updateOccupationMutation] = useUpdateOccupationMutation();

  return {
    nodesById: cached,
    create: (occupation, onDone) => {
      if ("id" in occupation)
        throw new Error(
          "Attempt to create a new node from an input that already has an id."
        ); // assert()

      createOccupationMutation({
        variables: { input: toOccupationInput(occupation) },
      })
        .then((res) => {
          if (!res.data) throw new Error("Shouldn't there be data here?"); // assert()

          const created = fromOccupationFragment(res.data.createOccupation);

          setCached((v) => ({ ...v, [created.id]: created }));

          onDone(created);
        })
        .catch((_) => {
          onDone(
            withCommitStatus(
              occupation,
              new Error("Anlegen des Objekts fehlgeschlagen.")
            )
          );
        });

      return withCommitStatus(occupation, true);
    },
    update: (occupation, onDone) => {
      updateOccupationMutation({
        variables: { id: occupation.id, input: toOccupationInput(occupation) },
      })
        .then((res) => {
          if (!res.data) throw new Error("Shouldn't there be data here?"); // assert()

          const updated = fromOccupationFragment(res.data.updateOccupation);

          setCached((v) => ({ ...v, [updated.id]: updated }));

          onDone(updated);
        })
        .catch((_) => {
          onDone(
            withCommitStatus(
              occupation,
              new Error("Speichern der Änderungen fehlgeschlagen.")
            )
          );
        });

      return withCommitStatus(occupation, true);
    },
    refresh: () => {
      setCached({});

      setRefreshTrigger((v) => v + 1);
    },
  };
};
