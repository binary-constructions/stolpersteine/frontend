import { useState, useEffect } from "react";

import { GlobalState } from "../types/GlobalState";

export function useGlobalState<T>(
  globalState: GlobalState<T>
): [T, (value: T | ((previousValue: T) => T)) => void] {
  const [localState, setLocalState] = useState<T>(globalState.getValue());

  useEffect(() => {
    globalState.subscribe(setLocalState);

    return () => {
      globalState.unsubscribe(setLocalState);
    };
  }, [globalState]);

  return [localState, globalState.setValue];
}
