import { useState, useEffect } from "react";

import { Sorts } from "../globals/Sorts";
import { IAnnotatedSpotSummary } from "../interfaces/IAnnotatedSpotSummary";

// TODO: delay updates using setTimeout or something?

export const useSpotList = (
  spots: IAnnotatedSpotSummary[] | undefined,
  sort: keyof typeof Sorts,
  reverseSort: boolean
) => {
  const [individualizedSpots, setIndividualizedSpots] = useState<
    undefined | IAnnotatedSpotSummary[]
  >();

  const [sortedAndRegroupedSpots, setSortedAndRegroupedSpots] = useState<
    undefined | IAnnotatedSpotSummary[]
  >();

  useEffect(() => {
    const update = async () => {
      if (spots) {
        console.debug("individualizing spots...");

        setIndividualizedSpots(
          spots.reduce<IAnnotatedSpotSummary[]>((a, c) => {
            if (c.stolpersteinSummaries.length > 1) {
              c.stolpersteinSummaries.forEach((v) => {
                if (v.personSummaries.length > 1) {
                  v.personSummaries.forEach((w) =>
                    a.push({
                      ...c,
                      stolpersteinSummaries: [{ ...v, personSummaries: [w] }],
                    })
                  );
                } else {
                  a.push({ ...c, stolpersteinSummaries: [v] });
                }
              });
            } else {
              a.push(c);
            }

            return a;
          }, [])
        );
      }
    };

    setIndividualizedSpots(undefined);

    update();
  }, [spots]);

  useEffect(() => {
    const update = async () => {
      if (individualizedSpots) {
        console.debug("sorting and regrouping spots...");

        setSortedAndRegroupedSpots(
          [...individualizedSpots]
            .sort((a, b) => {
              let result = 0;

              for (const compare of Sorts[sort].compares) {
                result = reverseSort ? compare(b, a) : compare(a, b);

                if (result) {
                  break;
                }
              }

              return result;
            })
            .reduceRight<IAnnotatedSpotSummary[]>((a, c) => {
              // -> re-merge spots that are still adjacent to each other
              if (a.length && c.id === a[0].id) {
                console.assert(
                  c.stolpersteinSummaries.length === 1,
                  "The array of stolperstein summaries should have a length of one here!"
                );

                if (
                  c.stolpersteinSummaries[0].id ===
                  a[0].stolpersteinSummaries[0].id
                ) {
                  console.assert(
                    c.stolpersteinSummaries[0].personSummaries.length === 1,
                    "The array of person summaries should have a length of one here!"
                  );

                  a[0] = {
                    ...a[0],
                    stolpersteinSummaries: [
                      {
                        ...a[0].stolpersteinSummaries[0],
                        personSummaries: [
                          c.stolpersteinSummaries[0].personSummaries[0],
                          ...a[0].stolpersteinSummaries[0].personSummaries,
                        ],
                      },
                      ...a[0].stolpersteinSummaries.slice(1),
                    ],
                  };
                } else {
                  a[0] = {
                    ...a[0],
                    stolpersteinSummaries: [
                      c.stolpersteinSummaries[0],
                      ...a[0].stolpersteinSummaries,
                    ],
                  };
                }
              } else {
                a.unshift(c);
              }

              return a;
            }, [])
        );
      }
    };

    setSortedAndRegroupedSpots(undefined);

    update();
  }, [individualizedSpots, sort, reverseSort]);

  return sortedAndRegroupedSpots;
};
