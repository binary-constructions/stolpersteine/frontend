import { useState, useMemo, useEffect, useCallback } from "react";

import { useSummariesQuery, ReviewStatus } from "../backendApi";
import { NaturalSortCollators } from "../globals/NaturalSortCollators";
import { IAnnotatedSpotSummary } from "../interfaces/IAnnotatedSpotSummary";
import { asAnnotatedSpotSummary } from "../helpers/asAnnotatedSpotSummary";

type AnnotatedSpotFilter = (
  spot: IAnnotatedSpotSummary
) => undefined | IAnnotatedSpotSummary;

export interface IPlaceOption {
  value: string;

  label: string;

  children: IPlaceOption[];
}

export interface IFilterConfig {
  includeDeleted: boolean;

  searchTerms: string[];

  reviewStatus: undefined | ReviewStatus;

  placeIds: string[];

  withImages: ("spot" | "stolperstein" | "person")[];

  withImagesInvert: boolean;
}

export const usePreprocessedSummaries = (filterConfig: IFilterConfig) => {
  const [results, setResults] = useState<{
    updating: boolean;

    spots: IAnnotatedSpotSummary[];

    placeOptions: IPlaceOption[];

    error: undefined | Error;
  }>({
    updating: true,
    spots: [],
    placeOptions: [],
    error: undefined,
  });

  const summaries = useSummariesQuery();

  const refetch = summaries.refetch;

  const refetchMemo = useCallback(() => refetch(), [refetch]);

  const placeAnnotationsMap = useMemo<{
    [placeId: string]:
      | undefined
      | { _placeLabel: string; _placeAltLabels: string[] };
  }>(
    () =>
      summaries.data?.placeSummaries.reduce<{
        [placeId: string]:
          | undefined
          | { _placeLabel: string; _placeAltLabels: string[] };
      }>((a, c) => {
        a[c.id] = { _placeLabel: c.label, _placeAltLabels: c.alternativeNames };

        return a;
      }, {}) || {},
    [summaries.data?.placeSummaries]
  );

  const placeOptions = useMemo<IPlaceOption[]>(() => {
    const stripUnreferenced = (options: IPlaceOption[]): IPlaceOption[] =>
      options
        .map((v) => ({
          ...v,
          children: v.children.length ? stripUnreferenced(v.children) : [],
        }))
        .filter(
          (v) =>
            v.children.length ||
            (summaries.data &&
              summaries.data.spotSummaries.findIndex(
                (w) => w.placeId === v.value
              ) !== -1)
        );

    return stripUnreferenced(
      summaries.data?.placeSummaries
        .map((v) => ({
          id: v.id,
          parentId: v.parentId,
          label: v.label,
          children: [] as IPlaceOption[],
        }))
        .sort((a, b) => NaturalSortCollators.de.compare(a.label, b.label))
        .reduce<IPlaceOption[]>((a, c, _, s) => {
          if (!c.parentId) {
            a.push({ value: c.id, label: c.label, children: c.children });
          } else {
            const parent = s.find((v) => v.id === c.parentId);

            if (parent) {
              parent.children.push({
                value: c.id,
                label: c.label,
                children: c.children,
              });
            } else {
              console.warn("Place references a non-existant parent: ", c.id);
            }
          }

          return a;
        }, []) || []
    );
  }, [summaries.data]);

  const annotatedSpots = useMemo<IAnnotatedSpotSummary[]>(
    () =>
      summaries.data?.spotSummaries.map((v) =>
        asAnnotatedSpotSummary(v, placeAnnotationsMap)
      ) || [],
    [summaries.data?.spotSummaries, placeAnnotationsMap]
  );

  useEffect(() => {
    const update = async (): Promise<void> => {
      console.debug("preprocessing spots...");

      const allFilters: AnnotatedSpotFilter[] = [];

      if (!filterConfig.includeDeleted) {
        allFilters.push((v) => {
          return v.deleted
            ? undefined
            : {
                ...v,
                stolpersteinSummaries: v.stolpersteinSummaries
                  .filter((w) => !w.deleted)
                  .map((x) => ({
                    ...x,
                    personSummaries: x.personSummaries.filter(
                      (y) => !y.deleted
                    ),
                  })),
              };
        });
      }

      if (filterConfig.searchTerms.length) {
        allFilters.push((v) => {
          let spot = v;

          for (const term of filterConfig.searchTerms) {
            if (spot._streetLabel?.toLowerCase().includes(term)) {
              continue;
            } else {
              spot = {
                ...spot,
                stolpersteinSummaries: spot.stolpersteinSummaries
                  .map((w) => ({
                    ...w,
                    personSummaries: w.personSummaries.filter((x) =>
                      x._name?.displayName.toLowerCase().includes(term)
                    ),
                  }))
                  .filter((w) => w.personSummaries.length),
              };

              // Return early if we did not find the search term anywhere.
              if (!spot.stolpersteinSummaries.length) {
                return undefined;
              }
            }
          }

          return spot;
        }); // TODO: searchTerms filter
      }

      if (filterConfig.placeIds.length) {
        allFilters.push((v) =>
          v.placeId && filterConfig.placeIds.includes(v.placeId) ? v : undefined
        );
      }

      if (filterConfig.reviewStatus) {
        // FIXME: currently only checks review status of stones
        allFilters.push((v) => {
          const spot = {
            ...v,
            stolpersteinSummaries: v.stolpersteinSummaries.filter(
              (w) => w.reviewStatus === filterConfig.reviewStatus
            ),
          };

          return spot.stolpersteinSummaries.length ? spot : undefined;
        });
      }

      if (filterConfig.withImages.length) {
        const onSpots = filterConfig.withImages.includes("spot");

        const onStolpersteine = filterConfig.withImages.includes(
          "stolperstein"
        );

        const onPersons = filterConfig.withImages.includes("person");

        allFilters.push((v) => {
          const spot = filterConfig.withImagesInvert
            ? onSpots && v.imageCount
              ? undefined
              : {
                  ...v,
                  stolpersteinSummaries:
                    !onStolpersteine && !onPersons
                      ? v.stolpersteinSummaries
                      : v.stolpersteinSummaries
                          .filter((w) => !onStolpersteine || !w.hasImage)
                          .map((w) => ({
                            ...w,
                            personSummaries: !onPersons
                              ? w.personSummaries
                              : w.personSummaries.filter((x) => !x.imageCount),
                          })),
                }
            : onSpots && !v.imageCount
            ? undefined
            : {
                ...v,
                stolpersteinSummaries:
                  !onStolpersteine && !onPersons
                    ? v.stolpersteinSummaries
                    : v.stolpersteinSummaries
                        .filter((w) => !onStolpersteine || w.hasImage)
                        .map((w) => ({
                          ...w,
                          personSummaries: !onPersons
                            ? w.personSummaries
                            : w.personSummaries.filter((x) => x.imageCount),
                        })),
              };

          return spot &&
            (!onStolpersteine || spot.stolpersteinSummaries.length) &&
            (!onPersons ||
              spot.stolpersteinSummaries.every((w) => w.personSummaries.length))
            ? spot
            : undefined;
        });
      }

      setResults({
        updating: false,
        spots: annotatedSpots.reduce<IAnnotatedSpotSummary[]>((a, c) => {
          let spot = c;

          for (const filter of allFilters) {
            const filteredSpot = filter(spot);

            if (filteredSpot === undefined) {
              return a;
            }

            spot = filteredSpot;
          }

          a.push(spot);

          return a;
        }, []),
        placeOptions,
        error: undefined,
      });
    };

    setResults((v) => ({ ...v, updating: true }));

    if (!summaries.loading) {
      if (summaries.error) {
        setResults((v) => ({ ...v, updating: false, error: summaries.error }));
      } else {
        update();
      }
    }
  }, [
    summaries.loading,
    summaries.error,
    placeOptions,
    annotatedSpots,
    filterConfig,
  ]);

  return {
    ...results,
    refetch: refetchMemo,
  };
};
