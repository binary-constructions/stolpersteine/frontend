import { useState, useEffect } from "react";

const nextId: () => number = (() => {
  let next: number = 0;

  return () => next++;
})();

export const useComponentId = (): undefined | string => {
  const [componentId, setComponentId] = useState<undefined | string>();

  useEffect(() => {
    setComponentId("component-with-unique-id-" + nextId());
  }, []);

  return componentId;
};
