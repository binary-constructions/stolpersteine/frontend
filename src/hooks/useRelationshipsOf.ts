import { useMemo } from "react";

import {
  useRelationshipsOfQuery,
  useSetRelationshipMutation,
  useUnsetRelationshipMutation,
} from "../backendApi";
import { IExistingRelationshipType } from "../interfaces/IRelationshipType";
import { IExistingPerson } from "../interfaces/IPerson";

import { useRelationshipTypes } from "./useRelationshipTypes";
import { usePersons } from "./usePersons";

export interface RelationshipsOf {
  relationships:
    | undefined
    | Error
    | {
        asSubject: (
          | undefined
          | Error
          | {
              type: IExistingRelationshipType;

              object: IExistingPerson;
            }
        )[];

        asObject: (
          | undefined
          | Error
          | {
              type: IExistingRelationshipType;

              subject: IExistingPerson;
            }
        )[];
      };

  set: (
    subject: IExistingPerson,
    object: IExistingPerson,
    type: IExistingRelationshipType,
    onDone?: (error?: Error) => void
  ) => void;

  unset: (
    one: IExistingPerson,
    other: IExistingPerson,
    onDone?: (error?: Error) => void
  ) => void;
}

export const useRelationshipsOf = (id: string): RelationshipsOf => {
  const relationshipsOfQuery = useRelationshipsOfQuery({
    notifyOnNetworkStatusChange: true,
    variables: { id },
  });

  const toFetch = useMemo(
    () =>
      relationshipsOfQuery.networkStatus !== 4 &&
      !relationshipsOfQuery.loading &&
      !relationshipsOfQuery.error &&
      relationshipsOfQuery.data
        ? {
            relationshipTypeIds: [
              ...relationshipsOfQuery.data.relationshipsWithObject.map(
                (v) => v.type.id
              ),
              ...relationshipsOfQuery.data.relationshipsWithSubject.map(
                (v) => v.type.id
              ),
            ].reduce((acc: string[], cur) => {
              if (!acc.includes(cur)) {
                acc.push(cur);
              }

              return acc;
            }, []),
            personIds: [
              ...relationshipsOfQuery.data.relationshipsWithObject.map(
                (v) => v.subject.id
              ),
              ...relationshipsOfQuery.data.relationshipsWithSubject.map(
                (v) => v.object.id
              ),
            ].reduce((acc: string[], cur) => {
              if (!acc.includes(cur)) {
                acc.push(cur);
              }

              return acc;
            }, []),
          }
        : { relationshipTypeIds: [], personIds: [] },
    [
      relationshipsOfQuery.networkStatus,
      relationshipsOfQuery.loading,
      relationshipsOfQuery.error,
      relationshipsOfQuery.data,
    ]
  );

  const relationshipTypes = useRelationshipTypes(toFetch.relationshipTypeIds);

  const persons = usePersons(toFetch.personIds);

  const relationships = useMemo(
    () =>
      relationshipsOfQuery.variables!.id !== id ||
      relationshipsOfQuery.networkStatus === 4 ||
      relationshipsOfQuery.loading
        ? undefined
        : relationshipsOfQuery.error
        ? new Error("Fehler beim Laden der Beziehungen.")
        : {
            asSubject: relationshipsOfQuery.data!.relationshipsWithSubject.map(
              (v) => {
                const type = relationshipTypes.nodesById[v.type.id];

                const object = persons.nodesById[v.object.id];

                return !type || !object
                  ? undefined
                  : type instanceof Error
                  ? new Error(
                      "Die gewählte Beziehungsart konnte nicht geladen werden."
                    )
                  : object instanceof Error
                  ? new Error(
                      "Die gewählte Person konnte nicht geladen werden."
                    )
                  : { type, object };
              }
            ),
            asObject: relationshipsOfQuery.data!.relationshipsWithObject.map(
              (v) => {
                const type = relationshipTypes.nodesById[v.type.id];

                const subject = persons.nodesById[v.subject.id];

                return !type || !subject
                  ? undefined
                  : type instanceof Error
                  ? new Error(
                      "Die gewählte Beziehungsart konnte nicht geladen werden."
                    )
                  : subject instanceof Error
                  ? new Error(
                      "Die gewählte Person konnte nicht geladen werden."
                    )
                  : { type, subject };
              }
            ),
          },
    [
      id,
      relationshipsOfQuery.variables,
      relationshipsOfQuery.networkStatus,
      relationshipsOfQuery.loading,
      relationshipsOfQuery.error,
      relationshipsOfQuery.data,
      relationshipTypes.nodesById,
      persons.nodesById,
    ]
  );

  const [setRelationshipMuation] = useSetRelationshipMutation();

  const [unsetRelationshipMuation] = useUnsetRelationshipMutation();

  return {
    relationships,
    set: (subject, object, type, onDone) => {
      setRelationshipMuation({
        variables: {
          subjectId: subject.id,
          objectId: object.id,
          typeId: type.id,
        },
      })
        .then((_) => {
          relationshipsOfQuery.refetch();

          if (onDone) {
            onDone();
          }
        })
        .catch((_) => {
          if (onDone) {
            onDone(new Error("Speichern der Beziehung fehlgeschlagen :("));
          }
        });
    },
    unset: (one, other, onDone) => {
      unsetRelationshipMuation({
        variables: { oneId: one.id, otherId: other.id },
      })
        .then((_) => {
          relationshipsOfQuery.refetch();

          if (onDone) {
            onDone();
          }
        })
        .catch((_) => {
          if (onDone) {
            onDone(new Error("Entfernen der Beziehung fehlgeschlagen :("));
          }
        });
    },
  };
};
