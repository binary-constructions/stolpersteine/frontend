import { useState, useEffect } from "react";

export const useStateMonitor = <T>(
  current: undefined | T,
  onChange?: (changed: T) => void
): void => {
  const [previousState, setPreviousState] = useState<undefined | T>();

  useEffect(() => {
    if (onChange && current !== undefined && current !== previousState) {
      setPreviousState(current);

      onChange(current);
    }
  }, [current, previousState, onChange]);
};
