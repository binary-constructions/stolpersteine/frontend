import { useState } from "react";

export interface ITrigger {
  activation_count: number;

  last_activated: undefined | number;

  listen: (callback: () => void) => void;

  activate: () => void;
}

export const useTrigger = (): ITrigger => {
  const [state, setState] = useState<{
    activation_count: number;

    last_activated: undefined | number;
  }>({
    activation_count: 0,

    last_activated: undefined,
  });

  return (() => {
    const listeners: (() => void)[] = [];

    return {
      activation_count: state.activation_count,

      last_activated: state.last_activated,

      listen: (callback: () => void) => {
        listeners.push(callback);
      },

      activate: () => {
        setState({
          activation_count: state.activation_count++,

          last_activated: Date.now(),
        });

        for (const listener of listeners) {
          listener();
        }
      },
    };
  })();
};
