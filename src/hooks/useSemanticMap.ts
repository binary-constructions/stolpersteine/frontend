import { useState, useEffect } from "react";
import * as semanticMap from "@binary-constructions/semantic-map";

export const useSemanticMap = (() => {
  let semanticMapPromise: Promise<any> | undefined = undefined;

  return (id: string | number) => {
    const [map, setMap] = useState<any>();

    useEffect(() => {
      if (!semanticMapPromise) {
        semanticMapPromise = semanticMap.load().then((v) => {
          v.mount();

          return v;
        });
      } else {
        semanticMapPromise = semanticMapPromise.then((v) => {
          v.refresh();

          return v;
        });
      }

      if (!map) {
        semanticMapPromise.then((v) => {
          if (!(id in v.maps)) {
            console.error("No map with id '" + id + "' found.");
          } else {
            setMap(v.maps[id]);
          }
        });
      }
    });

    return map;
  };
})();
