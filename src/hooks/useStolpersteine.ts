import { useState, useEffect } from "react";

import { GlobalState } from "../types/GlobalState";
import {
  useStolpersteineQuery,
  useCreateStolpersteinMutation,
  useUpdateStolpersteinMutation,
  useDeepDeleteStolpersteinMutation,
  ReviewStatus,
  useSetStolpersteinReviewStatusMutation,
} from "../backendApi";
import {
  IStolperstein,
  IExistingStolperstein,
  fromStolpersteinFragment,
  toStolpersteinInput,
  withLocalReviewStatusChange,
} from "../interfaces/IStolperstein";
import { withCommitStatus } from "../interfaces/ICommittable";

import { useGlobalState } from "./useGlobalState";

// TODO: this is mainly boilerplate and should probably be abstracted
// over.  Until then the useSpots version is meant to be the canonical
// implementation to copy.

type ExistingStolpersteineById = {
  [id: string]: undefined | Error | IExistingStolperstein;
};

export interface Stolpersteine {
  /**
   * The already loaded nodes.  Specifically requested nodes will
   * eventually show up as either successfully loaded or an Error.
   */
  nodesById: ExistingStolpersteineById;

  /**
   * Attempt to save the specified node as a new entity.
   *
   * The onDone callback will be called with either the created INode
   * or the original object with an Error as commit status.
   *
   * @returns The original object with a commit status of true.
   */
  create: (
    stolperstein: IStolperstein,
    onDone: (stolperstein: IStolperstein) => void
  ) => IStolperstein;

  /**
   * Attempt to save an update to the specified stolperstein.
   *
   * The onDone callback will be called with either the updated object
   * or the original object with an Error as commit status.
   *
   * @returns The original object with a commit status of true.
   */
  update: (
    stolperstein: IExistingStolperstein,
    onDone: (stolperstein: IExistingStolperstein) => void
  ) => IExistingStolperstein;

  setReviewStatus: (
    stolperstein: IExistingStolperstein,
    status: ReviewStatus,
    onDone: (stolperstein: IExistingStolperstein) => void
  ) => IExistingStolperstein;

  /**
   * Fixme: hacky way of deleting stuff.  Only applicable to Spots and
   * Stolpersteine.
   */
  deepDelete: (
    stolperstein: IExistingStolperstein,
    onDone: (stolperstein: IExistingStolperstein) => void
  ) => IExistingStolperstein;

  /**
   * Dump all cached data and refetch active queries.
   */
  refresh: () => void;
}

const cache = new GlobalState<ExistingStolpersteineById>({});

const refreshTriggerCount = new GlobalState(0);

/**
 * A query wrapper that caches already loaded entities and also
 * provides a few convenience methods for the manipulation of such
 * entities (with those methods updating the cached entities so that
 * they continue to represent the server side state).
 *
 * WARNING: when checking the returned object for updates (e.g. when
 * using it as a dependency for useEffect or similar) the .loaded
 * property is the one to watch because all other properties currently
 * change with every call! (FIXME?)
 *
 * @param ids Either a list of specific ids to fetch or undefined to
 * fetch all.  If the array is empty no new nodes will be loaded and
 * only the already fetched ones will be available.
 */
export const useStolpersteine = (
  /**
   */
  ids?: string[]
): Stolpersteine => {
  const [cached, setCached] = useGlobalState(cache);

  const [refreshTrigger, setRefreshTrigger] = useGlobalState(
    refreshTriggerCount
  );

  // The requested ids (if any) without those already successfully loaded.
  const requestedButNotLoadedIds = !ids
    ? []
    : ids.filter((v) => !cached[v] || cached[v] instanceof Error);

  // The ids to load as passed to the query.  Saved as state and
  // updated only when new ids actually need fetching to avoid
  // unnecessary queries.
  const [idsToLoad, setIdsToLoad] = useState<undefined | string[]>([]);

  // Update idsToLoad if necessary.
  if (ids) {
    if (
      !requestedButNotLoadedIds.every(
        (v) => !idsToLoad || idsToLoad.includes(v)
      )
    ) {
      setIdsToLoad(requestedButNotLoadedIds);
    }
  } else if (idsToLoad) {
    setIdsToLoad(undefined);
  }

  const {
    data,
    loading,
    error,
    networkStatus,
    refetch,
  } = useStolpersteineQuery({
    variables: { ids: idsToLoad },
    notifyOnNetworkStatusChange: true,
    // Skip the query if no new ids need to be fetched.
    skip: ids && !requestedButNotLoadedIds.length,
  });

  useEffect(() => {
    if (refreshTrigger) {
      refetch();
    }
  }, [refreshTrigger, refetch]);

  // Update the cache if necessary.
  useEffect(() => {
    if (loading || networkStatus === 4) {
      const oldErrorIds = Object.entries(cached).reduce<string[]>(
        (a, [k, v]) => {
          if (v instanceof Error) {
            a.push(k);
          }

          return a;
        },
        []
      );

      if (oldErrorIds.length) {
        setCached((v) =>
          oldErrorIds.reduce(
            (a, c) => {
              delete a[c];

              return a;
            },
            { ...v }
          )
        );
      }
    } else if (data) {
      const newlyLoaded = data.stolpersteine.filter(
        (v) => !cached[v.id] || cached[v.id] instanceof Error
      );

      if (newlyLoaded.length) {
        setCached((v) =>
          newlyLoaded.reduce(
            (a, c) => {
              a[c.id] = fromStolpersteinFragment(c);

              return a;
            },
            { ...v }
          )
        );
      }
    } else if (error && requestedButNotLoadedIds.length) {
      const newErrorIds = idsToLoad?.filter((v) => !cached[v]) || [];

      if (newErrorIds.length) {
        setCached((v) =>
          newErrorIds.reduce(
            (a, c) => {
              a[c] = new Error("Laden des Objekts fehlgeschlagen.");

              return a;
            },
            { ...v }
          )
        );
      }
    }
  }, [
    cached,
    setCached,
    loading,
    networkStatus,
    error,
    data,
    requestedButNotLoadedIds.length,
    idsToLoad,
  ]);

  const [createStolpersteinMutation] = useCreateStolpersteinMutation();

  const [updateStolpersteinMutation] = useUpdateStolpersteinMutation();

  const [deepDeleteStolpersteinMutation] = useDeepDeleteStolpersteinMutation();

  const [
    setStolpersteinReviewStatusMutation,
  ] = useSetStolpersteinReviewStatusMutation();

  return {
    nodesById: cached,
    create: (stolperstein, onDone) => {
      if ("id" in stolperstein)
        throw new Error(
          "Attempt to create a new node from an input that already has an id."
        ); // assert()

      createStolpersteinMutation({
        variables: { input: toStolpersteinInput(stolperstein) },
      })
        .then((res) => {
          if (!res.data) throw new Error("Shouldn't there be data here?"); // assert()

          const created = fromStolpersteinFragment(res.data.createStolperstein);

          setCached((v) => ({ ...v, [created.id]: created }));

          onDone(created);
        })
        .catch((_) => {
          onDone(
            withCommitStatus(
              stolperstein,
              new Error("Anlegen des Objekts fehlgeschlagen.")
            )
          );
        });

      return withCommitStatus(stolperstein, true);
    },
    update: (stolperstein, onDone) => {
      updateStolpersteinMutation({
        variables: {
          id: stolperstein.id,
          input: toStolpersteinInput(stolperstein),
        },
      })
        .then((res) => {
          if (!res.data) throw new Error("Shouldn't there be data here?"); // assert()

          const updated = fromStolpersteinFragment(res.data.updateStolperstein);

          setCached((v) => ({ ...v, [updated.id]: updated }));

          onDone(updated);
        })
        .catch((_) => {
          onDone(
            withCommitStatus(
              stolperstein,
              new Error("Speichern der Änderungen fehlgeschlagen.")
            )
          );
        });

      return withCommitStatus(stolperstein, true);
    },
    setReviewStatus: (stolperstein, status, onDone) => {
      setStolpersteinReviewStatusMutation({
        variables: { id: stolperstein.id, status },
      })
        .then((res) => {
          if (!res.data) throw new Error("Shouldn't there be data here?"); // assert()

          const changed = fromStolpersteinFragment(
            res.data.setStolpersteinReviewStatus
          );

          setCached((v) => ({ ...v, [changed.id]: changed }));

          onDone(
            // FIXME: shouldn't be necessary, should it?
            withLocalReviewStatusChange(
              stolperstein,
              res.data.setStolpersteinReviewStatus.reviewStatus
            )
          );
        })
        .catch((_) => {
          onDone(
            withCommitStatus(
              stolperstein,
              new Error("Änderung des Review Status fehlgeschlagen :(")
            )
          );
        });

      return withCommitStatus(stolperstein, true);
    },
    deepDelete: (stolperstein, onDone) => {
      deepDeleteStolpersteinMutation({ variables: { id: stolperstein.id } })
        .then((res) => {
          if (!res.data) throw new Error("Shouldn't there be data here?"); // assert()

          const deleted = fromStolpersteinFragment(
            res.data.deepDeleteStolperstein
          );

          setCached((v) => ({ ...v, [deleted.id]: deleted }));

          onDone(deleted);
        })
        .catch((_) => {
          onDone(
            withCommitStatus(
              stolperstein,
              new Error("Löschen des Objekts fehlgeschlagen.")
            )
          );
        });

      return withCommitStatus(stolperstein, true);
    },
    refresh: () => {
      setCached({});

      setRefreshTrigger((v) => v + 1);
    },
  };
};
