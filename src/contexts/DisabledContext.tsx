import React from "react";

export const DisabledContext = React.createContext(false);
