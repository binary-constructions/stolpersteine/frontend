import React from "react";

export interface IUser {
  name: string;
}

export const UserContext = React.createContext<undefined | IUser>(undefined);
