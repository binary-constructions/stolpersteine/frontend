import React from "react";

export interface Config {
  animationDuration: number; // ms
}

const defaultConfig = {
  animationDuration: 300,
};

export const ConfigContext = React.createContext<Config>(defaultConfig);
