/**
 * Instances of `Intl.Collator` by language, with each configured for
 * natural sorting.
 *
 * @example
 * ```
 * ["1", "10", "2"].sort(NaturalSortCollators.en.compare)
 * // -> ["1", "2", "10"]
 * ```
 */
export const NaturalSortCollators = {
  de: new Intl.Collator("de", { sensitivity: "base", numeric: true }),
  en: new Intl.Collator("en", { sensitivity: "base", numeric: true }),
};
