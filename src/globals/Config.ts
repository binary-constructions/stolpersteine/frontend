export const Config = {
  spotListWidth: 340,

  spotListPageSizes: [10, 20, 50, 100],

  initialMapView: {
    longitude: 13.059022,
    latitude: 52.402899,
    zoom: 8,
  },
};
