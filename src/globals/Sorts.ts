import { IAnnotatedSpotSummary } from "../interfaces/IAnnotatedSpotSummary";
import { HistoricalDate } from "../types/HistoricalDate";

import { NaturalSortCollators } from "./NaturalSortCollators";

/* Note: sort functions will be passed *individualized+ spots to
 * compare (i.e. spots having at most one stolperstein and person). */
type SpotCompareFunc = (
  a: IAnnotatedSpotSummary,
  b: IAnnotatedSpotSummary
) => number;

const sortByLastName: SpotCompareFunc = (a, b) =>
  NaturalSortCollators.de.compare(
    a.stolpersteinSummaries[0]?.personSummaries[0]?._name?.lastNameSort || "",
    b.stolpersteinSummaries[0]?.personSummaries[0]?._name?.lastNameSort || ""
  );

const sortByFirstName: SpotCompareFunc = (a, b) =>
  NaturalSortCollators.de.compare(
    a.stolpersteinSummaries[0]?.personSummaries[0]?._name?.firstNameSort || "",
    b.stolpersteinSummaries[0]?.personSummaries[0]?._name?.firstNameSort || ""
  );

const sortByStreet: SpotCompareFunc = (a, b) =>
  NaturalSortCollators.de.compare(a._streetLabel || "", b._streetLabel || "");

const sortByPlace: SpotCompareFunc = (a, b) =>
  NaturalSortCollators.de.compare(a._placeLabel || "", b._placeLabel || "");

const sortByLastTouched: SpotCompareFunc = (a, b) => {
  const lastTouchedA = [
    a._lastTouched,
    a.stolpersteinSummaries[0]?._lastTouched || "",
    a.stolpersteinSummaries[0]?.personSummaries[0]?._lastTouched || "",
  ].reduce((a, c) => (c > a ? c : a));

  const lastTouchedB = [
    b._lastTouched,
    b.stolpersteinSummaries[0]?._lastTouched || "",
    b.stolpersteinSummaries[0]?.personSummaries[0]?._lastTouched || "",
  ].reduce((a, c) => (c > a ? c : a));

  if (lastTouchedA > lastTouchedB) {
    return -1;
  } else if (lastTouchedA < lastTouchedB) {
    return 1;
  } else {
    return 0;
  }
};

const sortByDateBorn: SpotCompareFunc = (a, b) => {
  const bornMomentA = a.stolpersteinSummaries[0]?.personSummaries[0]?.bornDate
    ? new HistoricalDate(
        a.stolpersteinSummaries[0]?.personSummaries[0]?.bornDate
      ).asOneMoment()
    : undefined;

  const bornMomentB = b.stolpersteinSummaries[0]?.personSummaries[0]?.bornDate
    ? new HistoricalDate(
        b.stolpersteinSummaries[0]?.personSummaries[0]?.bornDate
      ).asOneMoment()
    : undefined;

  const now = Date.now();

  const bornA: number =
    bornMomentA && !(bornMomentA instanceof Error) ? bornMomentA.unix() : now;

  const bornB: number =
    bornMomentB && !(bornMomentB instanceof Error) ? bornMomentB.unix() : now;

  return bornA - bornB;
};

const sortByDateDied: SpotCompareFunc = (a, b) => {
  const diedMomentA = a.stolpersteinSummaries[0]?.personSummaries[0]?.diedDate
    ? new HistoricalDate(
        a.stolpersteinSummaries[0]?.personSummaries[0]?.diedDate
      ).asOneMoment()
    : undefined;

  const diedMomentB = b.stolpersteinSummaries[0]?.personSummaries[0]?.diedDate
    ? new HistoricalDate(
        b.stolpersteinSummaries[0]?.personSummaries[0]?.diedDate
      ).asOneMoment()
    : undefined;

  const now = Date.now();

  const diedA: number =
    diedMomentA && !(diedMomentA instanceof Error) ? diedMomentA.unix() : now;

  const diedB: number =
    diedMomentB && !(diedMomentB instanceof Error) ? diedMomentB.unix() : now;

  return diedA - diedB;
};

export const Sorts: {
  [name: string]: {
    label: string;

    compares: SpotCompareFunc[];
  };
} = {
  place: {
    label: "Ort",
    compares: [sortByPlace, sortByStreet, sortByLastName, sortByFirstName],
  },
  street: {
    label: "Straße",
    compares: [sortByStreet, sortByLastName, sortByFirstName],
  },
  lastName: {
    label: "Nachname",
    compares: [sortByLastName, sortByFirstName],
  },
  firstName: {
    label: "Vorname",
    compares: [sortByFirstName, sortByLastName],
  },
  bornDate: {
    label: "Geburtsdatum",
    compares: [sortByDateBorn],
  },
  diedDate: {
    label: "Todesdatum",
    compares: [sortByDateDied],
  },
  lastTouched: {
    label: "Letzte Änderung",
    compares: [sortByLastTouched],
  },
};
